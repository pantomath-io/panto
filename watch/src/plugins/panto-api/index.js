// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';

/** @module PantoAPI */

// Init Axios BaseURL
axios.defaults.baseURL = process.env.VUE_APP_PANTO_SERVER_URL;

// Axios resolves the promise for all HTTP status codes. Calling code must
// handle the HTTP status code and throw an error if needed.
axios.defaults.validateStatus = null;

axios.defaults.withCredentials = true;

/**
 * `PantoAPIError` encapsulates an error during a call to the Panto API.
 *
 * @property {object} data Response object from a failed API call. May be `null` or `undefined`.
 * @augments {Error}
 */
class PantoAPIError extends Error {
  /**
   * Creates an instance of PantoAPIError.
   *
   * @param {string} message The error message.
   * @param {object} data The response object from the failed API call, if any.
   * @memberof PantoAPIError
   */
  constructor(message, data) {
    super(message);
    this.data = data;
  }
}

export { PantoAPIError };

/**
 * `PantoAPI` provides functions to interface with the Panto API.
 */
const PantoAPI = {
  /**
   * Sets the authorization token for API requests. The token will be passed in
   * all requests, using the Bearer strategy in the `Authorization` header.
   *
   * @param {string} token
   */
  setAuthorizationToken(token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  },
  /**
   * Retrieves server info, such as version.
   *
   * @returns {object} The server's info
   */
  async info() {
    const res = await axios.get('/v1/info');
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to retrieve server info', res.data);
    }

    return res.data;
  },
  /**
   * Performs a login request to the Panto API server. If the login succeeds,
   * the method returns an object containing session details:
   *
   * ```
   * {
   *     "expires": "2020-04-30T18:29:48.496713Z",
   *     "token": "hDbsb-Lz6hgO3TRKg7qmyO1tUE5MVkI4mBdbZulGPuU"
   * }
   * ```
   * An error is thrown if the login fails.
   *
   * @param {string} username the user's identifier
   * @param {string} password the user's password
   * @returns {object} An object containing the session token and expiration date.
   */
  async login(username, password) {
    const res = await axios.post('/v1/login', {
      username,
      password
    });
    if (res.status !== 200) {
      throw new PantoAPIError('Login failed', res.data);
    }

    return res.data;
  },
  /**
   * Attempts to retrieve the session token using the cookie if set in the browser.
   *
   * @returns {object} An object containing the session token and expiration date.
   */
  async getSessionToken() {
    const res = await axios.get('/v1/token');
    if (res.status !== 200) {
      throw new PantoAPIError("Couldn't refresh token", res.data);
    }

    return res.data;
  },
  /**
   * Fetches the list of organizations.
   *
   * @returns {Array} A list of organizations
   */
  async listOrganizations() {
    const res = await axios.get('/v1/organizations');
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to retrieve organizations', res.data);
    }
    return res.data?.organizations;
  },
  /**
   * Fetches the list of agents for an organization.
   *
   * @param {string} organization The name of the organization
   * @returns {Array} A list of agents
   */
  async listAgents(organization) {
    const res = await axios.get(`/v1/${organization}/agents`);
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to retrieve agents', res.data);
    }
    return res.data?.agents;
  },
  /**
   * Fetches the list of probe configurations for an agent.
   *
   * @param {string} agent The name of the agent
   * @returns {Array} A list of probe configurations
   */
  async listProbeConfigurations(agent) {
    const res = await axios.get(`/v1/${agent}/probeconfigurations`, {
      params: {
        with_target: true,
        page_size: 100
      }
    });
    if (res.status !== 200) {
      throw new PantoAPIError(
        'Failed to retrieve probe configurations',
        res.data
      );
    }
    return res.data?.probe_configurations;
  },
  /**
   * Fetches the list of probes on the server
   *
   * @returns {Array} A list of probes
   */
  async listProbes() {
    const res = await axios.get('/v1/probes');
    if (res.status !== 200) {
      throw new PantoAPIError(
        'Failed to retrieve probe configurations',
        res.data
      );
    }
    return res.data?.probes;
  },
  /**
   * Creates a new target in an organization.
   *
   * @param {string} organization The name of the organization to create the
   * target in
   * @param {object} target The details of the target to create
   * @returns {object} The newly created target
   */
  async createTarget(organization, target) {
    const res = await axios.post(`/v1/${organization}/targets`, {
      target
    });
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to create target', res.data);
    }
    return res.data;
  },
  /**
   * Creates a new check for an organization.
   *
   * @param {string} organization The name of the organization to create the
   * target in
   * @param {object} check The details of the check to create
   * @returns {object} The newly created check
   */
  async createCheck(organization, check) {
    const res = await axios.post(`/v1/${organization}/checks`, {
      check
    });
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to create check', res.data);
    }
    return res.data;
  },
  /**
   * Creates a probe configuration on an agent.
   *
   * @param {string} agent The name of the agent
   * @param {object} probeConfiguration The details of the probeConfiguration
   * to create
   * @returns {object} The newly created probe configuration
   */
  async createProbeConfiguration(agent, probeConfiguration) {
    const res = await axios.post(`/v1/${agent}/probeconfigurations`, {
      probe_configuration: probeConfiguration
    });
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to create probe configuration', res.data);
    }
    return res.data;
  },
  /**
   * Update a probe configuration on an agent.
   *
   * @param {object} probeConfiguration The details of the probeConfiguration
   * to update
   * @returns {object} The updated probe configuration
   */
  async updateProbeConfiguration(probeConfiguration) {
    const res = await axios.put(`/v1/${probeConfiguration.name}`, {
      probe_configuration: probeConfiguration
    });
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to update probe configuration', res.data);
    }
    return res.data;
  },
  /**
   * Fetches the current state of probe configurations.
   *
   * @param {String} organization The name of the probe configuration's organization
   * @param {Array} probeConfigurations A list of probe configuration names
   *
   * @returns {object} an object containing the current state of the probe configuration
   */
  async getStates(organization, probeConfigurations) {
    const res = await axios.post(`/v1/${organization}/states/query`, {
      probe_configurations: probeConfigurations,
      page_size: 100
    });
    if (res.status !== 200) {
      throw new PantoAPIError(
        'Failed to retrieve probe configuration state',
        res.data
      );
    }
    return res.data?.states;
  },
  /**
   * Fetches the list of alerts for an organization.
   *
   * @param {string} organization The name of the organization
   * @returns {Array} A list of alerts
   */
  async listAlerts(organization) {
    const res = await axios.get(`/v1/${organization}/alerts`);
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to retrieve alerts', res.data);
    }
    return res.data?.alerts;
  },
  /**
   * Updates an alert
   *
   * @param {object} alert The alert object to update
   * @returns {object} An alert object
   */
  async updateAlert(alert) {
    const res = await axios.put(`/v1/${alert.name}`, {
      alert,
      update_mask: { paths: ['checks'] }
    });
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to update alert', res.data);
    }
    return res.data;
  },
  /**
   * Fetches a channel with a name.
   *
   * @param {string} channel The name of the channel to retrieve
   * @returns {object} A channel object
   */
  async getChannel(channel) {
    const res = await axios.get(`/v1/${channel}`);
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to retrieve channel', res.data);
    }
    return res.data;
  },
  /**
   * Updates a channel's fields.
   *
   * @param {object} channel The channel object to update
   * @returns {object} A channel object
   */
  async updateChannel(channel) {
    const res = await axios.put(`/v1/${channel.name}`, {
      channel,
      update_mask: { paths: ['configuration', 'channel_type'] }
    });
    if (res.status !== 200) {
      throw new PantoAPIError('Failed to update channel', res.data);
    }
    return res.data;
  },
  /**
   * Vue API plugin loader function.
   *
   * @param {object} Vue The Vue instance.
   */
  install(Vue) {
    if (Vue.prototype.$panto) {
      throw new Error('$panto already exists on Vue prototype');
    }

    Vue.prototype.$panto = this;
  }
};

export default PantoAPI;
