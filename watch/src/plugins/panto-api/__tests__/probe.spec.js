// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';

import PantoAPI, { PantoAPIError } from '..';

const mockProbeList = require('./mock-probe-list.json');

jest.mock('axios');

describe('PantoAPI.listProbes', () => {
  test('should return probe list', async () => {
    axios.get.mockResolvedValue({
      data: mockProbeList,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.listProbes();

    expect(axios.get).toHaveBeenCalledWith('/v1/probes');
    expect(res).toEqual(mockProbeList.probes);
  });

  test('should throw on HTTP error', async () => {
    axios.get.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.listProbes()).rejects.toThrow(PantoAPIError);

    expect(axios.get).toHaveBeenCalledWith('/v1/probes');
  });
});
