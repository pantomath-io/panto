// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';

import PantoAPI, { PantoAPIError } from '..';

jest.mock('axios');

const org = 'organizations/xCM3-ZaGQoOAaJbqQjcexg';

const target = {
  address: 'https://toto.com',
  name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/targets/KOWbgbHHRzewvGmT28PrPw',
  note: '',
  tags: []
};

describe('PantoAPI.createTarget', () => {
  test('should return created target', async () => {
    axios.post.mockResolvedValue({
      data: target,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });

    const targetData = {
      address: target.address
    };
    const res = await PantoAPI.createTarget(org, targetData);

    expect(axios.post).toHaveBeenCalledWith(`/v1/${org}/targets`, {
      target: targetData
    });
    expect(res).toEqual(target);
  });

  test('should throw on HTTP error', async () => {
    axios.post.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.createTarget(org, {})).rejects.toThrow(PantoAPIError);

    expect(axios.post).toHaveBeenCalledWith(`/v1/${org}/targets`, {
      target: {}
    });
  });
});
