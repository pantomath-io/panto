// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';

import PantoAPI, { PantoAPIError } from '..';

jest.mock('axios');

const org = 'organizations/xCM3-ZaGQoOAaJbqQjcexg';

const agent = {
  display_name: 'toto',
  last_activity: '2020-05-21T23:46:55.127153Z',
  last_version: '0.9.0',
  name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ'
};

describe('PantoAPI.listAgents', () => {
  test('should return agent list', async () => {
    axios.get.mockResolvedValue({
      data: {
        next_page_token: '',
        agents: [agent]
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.listAgents(org);

    expect(axios.get).toHaveBeenCalledWith(`/v1/${org}/agents`);
    expect(res).toEqual([agent]);
  });

  test('should throw on HTTP error', async () => {
    axios.get.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.listAgents(org)).rejects.toThrow(PantoAPIError);

    expect(axios.get).toHaveBeenCalledWith(`/v1/${org}/agents`);
  });
});
