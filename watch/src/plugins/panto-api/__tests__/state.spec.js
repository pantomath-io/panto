// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';

import PantoAPI, { PantoAPIError } from '..';

jest.mock('axios');

const org = 'organizations/xCM3-ZaGQoOAaJbqQjcexg';
const probeConfigurations = [
  'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ/probeconfigurations/M65jxjmfTeq5wGMkHrLNiQ'
];
const states = {
  next_page_token: '',
  states: [
    {
      agent:
        'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ',
      check:
        'organizations/xCM3-ZaGQoOAaJbqQjcexg/checks/Ogm_VzTPTDyNLoTySPHN6g',
      children: null,
      probe_configuration:
        'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ/probeconfigurations/hpMP4iU1RG-MEozlmidrcw',
      reason: 'No results received',
      timestamp: '2020-05-26T22:53:49.263881Z',
      type: 'MISSING_DATA'
    }
  ]
};

describe('PantoAPI.getStates', () => {
  test('should return states for probe configuration', async () => {
    axios.post.mockResolvedValue({
      data: states,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.getStates(org, probeConfigurations);

    expect(axios.post).toHaveBeenCalledWith(`/v1/${org}/states/query`, {
      probe_configurations: probeConfigurations,
      page_size: 100
    });
    expect(res).toEqual(states.states);
  });

  test('should throw on HTTP error', async () => {
    axios.post.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.getStates(org, [])).rejects.toThrow(PantoAPIError);

    expect(axios.post).toHaveBeenCalledWith(`/v1/${org}/states/query`, {
      probe_configurations: [],
      page_size: 100
    });
  });
});
