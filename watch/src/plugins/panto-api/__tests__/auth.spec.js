// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';
import moment from 'moment';

import PantoAPI, { PantoAPIError } from '..';

jest.mock('axios');

const token = 'tp1NSJUKqk4Us9VkFSxkGlnooOAGesUSa8I_nFR1UKU';
const expires = moment()
  .add(24, 'h')
  .utc()
  .toISOString();

describe('PantoAPI.setAuthorizationtoken', () => {
  test('should have token set on axios authorization header', async () => {
    PantoAPI.setAuthorizationToken(token);
    expect(axios.defaults.headers.common).toHaveProperty(
      'Authorization',
      `Bearer ${token}`
    );
  });
});

describe('PantoAPI.login', () => {
  test('should return token on correct login', async () => {
    axios.post.mockResolvedValue({
      data: {
        expires,
        token
      },
      status: 200,
      statusText: 'OK'
    });

    const username = 'test@panto.app';
    const password = 'trustno1';
    const reqData = { username, password };
    const res = await PantoAPI.login(username, password);

    expect(axios.post).toHaveBeenCalledWith('/v1/login', reqData);
    expect(res).toEqual({
      expires,
      token
    });
  });

  test('should throw on HTTP error', async () => {
    axios.post.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request'
    });

    const username = 'panto@panto.app';
    const password = 'trustno1';
    const reqData = { username, password };

    expect(PantoAPI.login(username, password)).rejects.toThrow(PantoAPIError);
    expect(axios.post).toHaveBeenCalledWith('/v1/login', reqData);
  });
});

describe('PantoAPI.getSessionToken', () => {
  test('should return token', async () => {
    axios.get.mockResolvedValue({
      data: {
        expires,
        token
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.getSessionToken();

    expect(axios.get).toHaveBeenCalledWith('/v1/token');
    expect(res).toEqual({
      expires,
      token
    });
  });

  test('should throw on HTTP error', async () => {
    axios.get.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.getSessionToken()).rejects.toThrow(PantoAPIError);

    expect(axios.get).toHaveBeenCalledWith('/v1/token');
  });
});
