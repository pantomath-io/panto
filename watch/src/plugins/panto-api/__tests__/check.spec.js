// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';

import PantoAPI, { PantoAPIError } from '..';

jest.mock('axios');

const org = 'organizations/xCM3-ZaGQoOAaJbqQjcexg';

const check = {
  check: {
    name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/checks/UzONMAtZQP-JyGxNhXZuZg',
    target:
      'organizations/xCM3-ZaGQoOAaJbqQjcexg/targets/6gXfp04oQ1Oi5h6py3VJIA',
    probe: 'probes/CpXQzjOVTMKzCvPLAnXZyQ',
    state_type: '',
    state_configuration: '',
    children: null,
    type: 'threshold',
    configuration: JSON.stringify({
      states: [
        {
          conditions: [
            { field: 'status-code', operator: 'ge', reason: '', value: 400 }
          ],
          match: 'any',
          state: 'critical'
        }
      ]
    })
  }
};

describe('PantoAPI.createCheck', () => {
  test('should return created check', async () => {
    axios.post.mockResolvedValue({
      data: check,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });

    const checkData = {
      target: check.target,
      probe: check.probe,
      type: check.type,
      configuration: check.configuration
    };
    const res = await PantoAPI.createCheck(org, checkData);

    expect(axios.post).toHaveBeenCalledWith(`/v1/${org}/checks`, {
      check: checkData
    });
    expect(res).toEqual(check);
  });

  test('should throw on HTTP error', async () => {
    axios.post.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.createCheck(org, {})).rejects.toThrow(PantoAPIError);

    expect(axios.post).toHaveBeenCalledWith(`/v1/${org}/checks`, {
      check: {}
    });
  });
});
