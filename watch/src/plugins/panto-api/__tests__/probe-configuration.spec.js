// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';

import PantoAPI, { PantoAPIError } from '..';

jest.mock('axios');

const agent =
  'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ';

const probeConf = {
  check: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/checks/3AKZuY2fTXiJJnSzCOoy7w',
  configuration: '{"url":"https://twitter.com"}',
  name:
    'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ/probeconfigurations/M65jxjmfTeq5wGMkHrLNiQ',
  probe_label: 'http',
  schedule: '60s',
  target: {
    address: 'https://twitter.com',
    name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/targets/jXW0PrP2RtO_IuR9Y464aw',
    note: '',
    tags: []
  }
};

describe('PantoAPI.listProbeConfigurations', () => {
  test('should return probe configuration list', async () => {
    axios.get.mockResolvedValue({
      data: {
        next_page_token: '',
        probe_configurations: [probeConf]
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.listProbeConfigurations(agent);

    expect(axios.get).toHaveBeenCalledWith(`/v1/${agent}/probeconfigurations`, {
      params: { with_target: true, page_size: 100 }
    });
    expect(res).toEqual([probeConf]);
  });

  test('should throw on HTTP error', async () => {
    axios.get.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.listProbeConfigurations(agent)).rejects.toThrow(
      PantoAPIError
    );

    expect(axios.get).toHaveBeenCalledWith(`/v1/${agent}/probeconfigurations`, {
      params: { with_target: true, page_size: 100 }
    });
  });
});

describe('PantoAPI.createProbeConfiguration', () => {
  test('should return created probe configuration', async () => {
    axios.post.mockResolvedValue({
      data: probeConf,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });

    const probeConfData = {
      check: probeConf.check,
      configuration: probeConf.configuration,
      schedule: probeConf.schedule
    };
    const res = await PantoAPI.createProbeConfiguration(agent, probeConfData);

    expect(axios.post).toHaveBeenCalledWith(
      `/v1/${agent}/probeconfigurations`,
      { probe_configuration: probeConfData }
    );
    expect(res).toEqual(probeConf);
  });

  test('should throw on HTTP error', async () => {
    axios.post.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.createProbeConfiguration(agent, {})).rejects.toThrow(
      PantoAPIError
    );

    expect(axios.post).toHaveBeenCalledWith(
      `/v1/${agent}/probeconfigurations`,
      { probe_configuration: {} }
    );
  });
});

describe('PantoAPI.updateProbeConfiguration', () => {
  test('should return updated probe configuration', async () => {
    axios.put.mockResolvedValue({
      data: probeConf,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });

    const probeConfData = {
      name: probeConf.name,
      check: probeConf.check,
      configuration: probeConf.configuration,
      schedule: probeConf.schedule
    };
    const res = await PantoAPI.updateProbeConfiguration(probeConfData);

    expect(axios.put).toHaveBeenCalledWith(`/v1/${probeConfData.name}`, {
      probe_configuration: probeConfData
    });
    expect(res).toEqual(probeConf);
  });

  test('should throw on HTTP error', async () => {
    axios.put.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.updateProbeConfiguration({})).rejects.toThrow(
      PantoAPIError
    );

    expect(axios.put).toHaveBeenCalledWith('/v1/undefined', {
      probe_configuration: {}
    });
  });
});
