// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';

import PantoAPI, { PantoAPIError } from '..';

jest.mock('axios');

const org = {
  display_name: 'Organization',
  name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg'
};

describe('PantoAPI.listOrganizations', () => {
  test('should return organization list', async () => {
    axios.get.mockResolvedValue({
      data: {
        next_page_token: '',
        organizations: [org]
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.listOrganizations();

    expect(axios.get).toHaveBeenCalledWith('/v1/organizations');
    expect(res).toEqual([org]);
  });

  test('should throw on HTTP error', async () => {
    axios.get.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.listOrganizations()).rejects.toThrow(PantoAPIError);

    expect(axios.get).toHaveBeenCalledWith('/v1/organizations');
  });
});
