// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import axios from 'axios';

import PantoAPI, { PantoAPIError } from '..';

jest.mock('axios');

const org = 'organizations/xCM3-ZaGQoOAaJbqQjcexg';

const alert = {
  channel:
    'organizations/xCM3-ZaGQoOAaJbqQjcexg/channels/_55pJmSqRY6cxxE-2slGFg',
  checks: [
    'organizations/xCM3-ZaGQoOAaJbqQjcexg/checks/qbdNvlQFQTGnGmjOl66ZFg'
  ],
  children: null,
  configuration: '{}',
  name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/alerts/iLZst9ymReKMd4FcEMU_jg',
  type: 'state_change'
};

describe('PantoAPI.listAlerts', () => {
  test('should return alert list', async () => {
    axios.get.mockResolvedValue({
      data: {
        next_page_token: '',
        alerts: [alert]
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.listAlerts(org);

    expect(axios.get).toHaveBeenCalledWith(`/v1/${org}/alerts`);
    expect(res).toEqual([alert]);
  });

  test('should throw on HTTP error', async () => {
    axios.get.mockResolvedValue({
      data: {},
      status: 400,
      statusText: 'Bad Request',
      headers: {},
      config: {}
    });
    expect(PantoAPI.listAlerts(org)).rejects.toThrow(PantoAPIError);

    expect(axios.get).toHaveBeenCalledWith(`/v1/${org}/alerts`);
  });
});

describe('PantoAPI.updateAlert', () => {
  const mockAlert = global.mocks.mockAlert;
  test('should update and return alert', async () => {
    axios.put.mockResolvedValue({
      data: mockAlert,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.updateAlert(mockAlert);

    expect(axios.put).toHaveBeenCalled();
    expect(axios.put.mock.calls[0]).toContain(`/v1/${mockAlert.name}`);
    expect(res).toEqual(mockAlert);
  });
});

const channelName =
  'organizations/xCM3-ZaGQoOAaJbqQjcexg/channels/_55pJmSqRY6cxxE-2slGFg';

const channel = {
  channel_type: 'channeltypes/email',
  children: null,
  configuration: '{"dest":"chrales@panto.app"}',
  display_name: '',
  name: channelName
};

describe('PantoAPI.getChannel', () => {
  test('should return channel', async () => {
    axios.get.mockResolvedValue({
      data: channel,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.getChannel(channelName);

    expect(axios.get).toHaveBeenCalledWith(`/v1/${channelName}`);
    expect(res).toEqual(channel);
  });
});

describe('PantoAPI.updateChannel', () => {
  test('should update and return channel', async () => {
    axios.put.mockResolvedValue({
      data: channel,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    });
    const res = await PantoAPI.updateChannel(channel);

    expect(axios.put).toHaveBeenCalled();
    expect(axios.put.mock.calls[0]).toContain(`/v1/${channelName}`);
    expect(res).toEqual(channel);
  });
});
