// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import durationRegex from '../duration-regex';

// Test duration strings from the official Go language sources
// https://github.com/golang/go/blob/b71eafbcece175db33acfb205e9090ca99a8f984/src/time/time_test.go#L818-L873
const testStrings = [
  '0',
  '5s',
  '30s',
  '1478s',
  '-5s',
  '+5s',
  '-0',
  '+0',
  '5.0s',
  '5.6s',
  '5.s',
  '.5s',
  '1.0s',
  '1.00s',
  '1.004s',
  '1.0040s',
  '100.00100s',
  '10ns',
  '11us',
  '12µs',
  '12μs',
  '13ms',
  '14s',
  '15m',
  '16h',
  '3h30m',
  // '10.5s4m',   // Regex cannot parse unordered string yet
  '-2m3.4s',
  '1h2m3s4ms5us6ns',
  '39h9m14.425s',
  '52763797000ns',
  '0.3333333333333333333h',
  '9007199254740993ns',
  '9223372036854775807ns',
  '9223372036854775.807us',
  '9223372036s854ms775us807ns',
  '-9223372036854775807ns',
  '0.100000000000000000000h',
  '0.830103483285477580700h'
];

// Test duration strings from the official Go language sources
// https://github.com/golang/go/blob/b71eafbcece175db33acfb205e9090ca99a8f984/src/time/time_test.go#L884-L908
const errorStrings = [
  '3',
  // '-',     // Regex matches lone minus sign
  's',
  '.',
  '-.',
  '.s',
  '+.s',
  '1d'
];

describe('Duration regex', () => {
  test('matches valid duration strings', () => {
    for (const d of testStrings) {
      expect(d).toMatch(durationRegex);
    }
  });
  test('does not match invalid duration strings', () => {
    for (const d of errorStrings) {
      expect(d).not.toMatch(durationRegex);
    }
  });
});
