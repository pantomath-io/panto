// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { flattenKeys } from '@/util';

const testCases = [
  {
    input: null,
    expected: null
  },
  {
    input: undefined,
    expected: null
  },
  {
    input: '',
    expected: []
  },
  {
    input: {},
    expected: []
  },
  {
    input: { hello: 'world' },
    expected: ['hello']
  },
  {
    input: { hello: 1, world: 2 },
    expected: ['hello', 'world']
  },
  {
    input: { hello: 1, world: undefined },
    expected: ['hello']
  },
  {
    input: { hello: { world: 1 } },
    expected: ['hello.world']
  },
  {
    input: { hello: 1, toto: { world: 1 } },
    expected: ['hello', 'toto.world']
  },
  {
    input: { hello: { world: 1 }, yello: { ohyeah: '' } },
    expected: ['hello.world', 'yello.ohyeah']
  },
  {
    input: {
      hello: { world: { hello: { hello: 1 } } },
      toto: { world: 1, toto2: {} },
      barbes: 'rochechouart'
    },
    expected: ['hello.world.hello.hello', 'toto.world', 'barbes']
  }
];

describe('flattenKeys', () => {
  for (const { input, expected } of testCases) {
    const testName =
      typeof input !== 'undefined' ? JSON.stringify(input) : 'undefined';
    test(testName, () => {
      if (expected !== null) {
        const got = flattenKeys(input);
        expect(got).toBeArray();
        expect(got).toIncludeSameMembers(expected);
      } else {
        expect(() => flattenKeys(input)).toThrow();
      }
    });
  }
});
