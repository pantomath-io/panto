// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faAngleDown,
  faArrowDown,
  faArrowUp,
  faArrowLeft,
  faBan,
  faBullhorn,
  faCircleNotch,
  faListUl,
  faLock,
  faPlus,
  faQuestion,
  faSignOutAlt,
  faUser
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import * as VeeValidate from 'vee-validate';
import * as Rules from 'vee-validate/dist/rules';
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueTimers from 'vue-timers';

import App from './App.vue';
import durationRegex from './duration-regex';
import i18n from './i18n';
import EnvPlugin from './plugins/env-plugin';
import PantoAPI from './plugins/panto-api';
import router from './router';
import store from './store';
import urlRegex from './url-regex';

library.add([
  faAngleDown,
  faArrowDown,
  faArrowUp,
  faArrowLeft,
  faBan,
  faBullhorn,
  faCircleNotch,
  faListUl,
  faLock,
  faPlus,
  faQuestion,
  faSignOutAlt,
  faUser
]);

Vue.component('font-awesome-icon', FontAwesomeIcon);

VeeValidate.configure({
  defaultMessage: (field, values) => {
    values._field_ = '';
    return i18n.t(`validations.${values._rule_}`, values);
  }
});
VeeValidate.extend('required', Rules.required);
VeeValidate.extend('alpha', Rules.alpha);
VeeValidate.extend('duration', value =>
  Rules.regex.validate(value, { regex: durationRegex })
);
VeeValidate.extend('url', value =>
  Rules.regex.validate(value, { regex: urlRegex })
);
VeeValidate.extend('email', Rules.email);
VeeValidate.extend('email-list', value => {
  const addresses = value.split(',');
  return Rules.email.validate(addresses);
});
Vue.component('ValidationProvider', VeeValidate.ValidationProvider);
Vue.component('ValidationObserver', VeeValidate.ValidationObserver);

Vue.use(EnvPlugin);
Vue.use(PantoAPI);
Vue.use(VueTimers);
Vue.use(VueRouter);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app');
