// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { mount, createLocalVue } from '@vue/test-utils';
import moment from 'moment';
import VueI18n from 'vue-i18n';

import EndpointCard from '../EndpointCard';
import i18n from '@/i18n';
import * as Routes from '@/router/routes';
import { States } from '@/store/endpoint';

const mockEndpoint = {
  name: 'QFPDL6h2QJCJNn0DhPdeaA',
  url: 'https://twitter.com',
  schedule: moment.duration(60, 's')
};

const localVue = createLocalVue();
localVue.use(VueI18n);

describe('EndpointCard', () => {
  test('matches snapshot', async () => {
    for (const s of Object.values(States)) {
      const endpointCard = mount(EndpointCard, {
        localVue,
        i18n,
        propsData: {
          endpoint: { ...mockEndpoint, state: s }
        },
        mocks: { $router: { routes: { ...Routes } } },
        stubs: ['router-link', 'font-awesome-icon']
      });
      await localVue.nextTick();
      expect(endpointCard).toMatchSnapshot();
    }
  });
});
