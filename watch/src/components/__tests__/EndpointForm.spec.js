// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { mount, createLocalVue } from '@vue/test-utils';
import flushPromises from 'flush-promises';
import moment from 'moment';
import * as VeeValidate from 'vee-validate';
import VueI18n from 'vue-i18n';

import EndpointForm from '../EndpointForm';
import i18n from '@/i18n';

const mockEndpoint = {
  name: 'QFPDL6h2QJCJNn0DhPdeaA',
  url: 'https://twitter.com',
  schedule: moment.duration(60, 's')
};

const localVue = createLocalVue();
for (const rule of ['required', 'alpha', 'duration', 'url']) {
  VeeValidate.extend(rule, () => true);
}
localVue.component('ValidationProvider', VeeValidate.ValidationProvider);
localVue.component('ValidationObserver', VeeValidate.ValidationObserver);
localVue.use(VueI18n);

describe('EndpointForm', () => {
  test('matches snapshot', async () => {
    const endpointForm = mount(EndpointForm, {
      localVue,
      i18n,
      propsData: {
        endpoint: mockEndpoint
      },
      sync: false
    });

    await flushPromises();

    expect(endpointForm).toMatchSnapshot();
  });
});
