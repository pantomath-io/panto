// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { mount } from '@vue/test-utils';

import InputDropdown from '../InputDropdown';

const choices = ['toto', 'to', 'choice'];

describe('InputDropdown', () => {
  test('matches snapshot', () => {
    const inputDropdown = mount(InputDropdown, {
      propsData: {
        choices
      }
    });
    expect(inputDropdown).toMatchSnapshot();
  });

  test('displays links with choices', () => {
    const inputDropdown = mount(InputDropdown, {
      propsData: {
        choices
      }
    });
    const links = inputDropdown.findAll('a');

    expect(links).toHaveLength(choices.length);

    for (const [i, link] of links.wrappers.entries()) {
      expect(link.text()).toInclude(choices[i]);
    }
  });

  test('displays input field', () => {
    const inputDropdown = mount(InputDropdown, {
      propsData: {
        choices
      }
    });
    expect(inputDropdown.find('input').exists()).toBeTrue();
  });

  test('filter list', async () => {
    const inputDropdown = mount(InputDropdown, {
      propsData: {
        choices
      }
    });

    await inputDropdown.find('input').setValue('to');

    let links = inputDropdown.findAll('a');

    expect(links).toHaveLength(2);
    expect(links.at(0).text()).toInclude('toto');
    expect(links.at(1).text()).toInclude('to');

    await inputDropdown.find('input').setValue('a');

    links = inputDropdown.findAll('a');

    expect(links).toHaveLength(0);
  });
});
