// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { mount, createLocalVue } from '@vue/test-utils';
import VueI18n from 'vue-i18n';
import Vuex from 'vuex';

import Navbar from '../Navbar';
import i18n from '@/i18n';
import * as Routes from '@/router/routes';
import { Actions } from '@/store/auth';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);

describe('Navbar', () => {
  test('matches snapshot', () => {
    const navbar = mount(Navbar, {
      localVue,
      i18n,
      mocks: {
        $route: { name: Routes.ENDPOINTS },
        $router: { routes: Routes }
      },
      stubs: ['router-link', 'router-view', 'font-awesome-icon']
    });
    expect(navbar).toMatchSnapshot();
  });

  test('shows logout button if logged in', async () => {
    const navbar = mount(Navbar, {
      localVue,
      i18n,
      mocks: {
        $route: { name: Routes.ENDPOINTS },
        $router: { routes: Routes }
      },
      stubs: ['router-link', 'router-view', 'font-awesome-icon']
    });

    expect(navbar.find('#logout-button').exists()).toBeTrue();
  });

  test('does not show logout button on login page', async () => {
    const navbar = mount(Navbar, {
      localVue,
      i18n,
      mocks: { $route: { name: Routes.LOGIN } },
      stubs: ['router-link', 'router-view', 'font-awesome-icon']
    });

    expect(navbar.find('#logout-button').exists()).toBeFalse();
  });

  test('logout button dispatches LOGOUT action', async () => {
    const actions = {
      [Actions.LOGOUT]: jest.fn()
    };
    const store = new Vuex.Store({
      actions
    });
    const navbar = mount(Navbar, {
      localVue,
      store,
      i18n,
      mocks: {
        $route: { name: Routes.ENDPOINTS },
        $router: { routes: Routes }
      },
      stubs: ['router-link', 'router-view', 'font-awesome-icon']
    });

    const logoutButton = navbar.find('#logout-button');
    await logoutButton.trigger('click');
    expect(actions[Actions.LOGOUT]).toHaveBeenCalled();
  });
});
