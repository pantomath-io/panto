// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import VueRouter from 'vue-router';

import store from '@/store';
import * as Auth from '@/store/auth';

import * as Routes from './routes';

const routes = [
  {
    path: '/login',
    name: Routes.LOGIN,
    component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue')
  },
  {
    path: '/',
    name: Routes.ENDPOINTS,
    component: () =>
      import(/* webpackChunkName: "endpoints" */ '@/views/Endpoints.vue')
  },
  {
    path: '/endpoint/new',
    name: Routes.CREATE_ENDPOINT,
    component: () =>
      import(
        /* webpackChunkName: "create-endpoint" */ '@/views/CreateEndpoint.vue'
      )
  },
  {
    path: '/endpoint/:name',
    name: Routes.ENDPOINT_DETAILS,
    props: true,
    component: () =>
      import(
        /* webpackChunkName: "endpoint-details" */ '@/views/EndpointDetails.vue'
      )
  },
  {
    path: '/alerts',
    name: Routes.ALERTS,
    component: () =>
      import(/* webpackChunkName: "alerts" */ '@/views/Alerts.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {
  // Always navigate to login
  if (to.name === Routes.LOGIN) {
    next();
    return;
  }
  // If user is not logged in, attempt to refresh using session cookie
  // NOTE: isAfter() always returns false for an invalid moment, so take
  // advantage of that in condition.
  const sessionExpiration = store.getters.sessionExpiration;
  if (sessionExpiration.isAfter() && !store.getters.loggedIn) {
    await store.dispatch(Auth.Actions.GET_SESSION_TOKEN);
  }
  // If still not logged in, redirect to login
  if (!store.getters.loggedIn) {
    next({ name: Routes.LOGIN });
    return;
  }
  next();
});

router.routes = Routes;

export default router;
export { Routes };
