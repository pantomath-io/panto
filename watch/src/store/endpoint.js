// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { Duration } from '@icholy/duration';
import _ from 'lodash';
import moment from 'moment';

import Panto from '@/plugins/panto-api';

export const Actions = {
  LIST_ENDPOINTS: 'listEndpoints',
  CREATE_ENDPOINT: 'createEndpoint',
  UPDATE_ENDPOINT: 'updateEndpoint',
  GET_STATES: 'getStates',
  GET_HTTP_PROBE_NAME: 'getHttpProbeName'
};

export const Mutations = {
  ENDPOINTS_RECEIVED: 'endpointsReceived',
  ENDPOINT_CREATED: 'endpointCreated',
  ENDPOINT_UPDATED: 'endpointUpdated',
  STATES_RECEIVED: 'statesReceived',
  HTTP_PROBE_NAME_RECEIVED: 'httpProbeNameReceived'
};

export const States = {
  UNKNOWN: 'UNKNOWN',
  OK: 'OK',
  WARNING: 'WARNING',
  CRITICAL: 'CRITICAL',
  MISSING_DATA: 'MISSING_DATA',
  ERROR: 'ERROR'
};

function makeName(name) {
  const components = name.split('/');
  return components[components.length - 1];
}

function makeEndpoint(probeConf) {
  try {
    const schedule = moment.duration(
      Duration.parse(probeConf.schedule).milliseconds(),
      'ms'
    );
    const conf = JSON.parse(probeConf.configuration);
    return {
      name: makeName(probeConf.name),
      probeConfiguration: { name: probeConf.name },
      target: { name: probeConf.target.name },
      check: { name: probeConf.check },
      url: conf.url,
      method: conf.method,
      timeout: conf.timeout,
      body: conf.body,
      schedule: schedule,
      state: States.UNKNOWN
    };
  } catch (err) {
    return null;
  }
}

export default {
  state: () => ({
    endpoints: [],
    httpProbeName: null
  }),
  mutations: {
    endpointsReceived(state, { endpoints }) {
      state.endpoints = _.cloneDeep(endpoints);
    },
    endpointCreated(state, { endpoint }) {
      state.endpoints.push(endpoint);
    },
    endpointUpdated(state, { endpoint }) {
      const pos = state.endpoints.findIndex(e => e.name === endpoint.name);
      if (pos !== -1) {
        state.endpoints[pos] = endpoint;
      }
    },
    statesReceived(state, { states }) {
      states = _.keyBy(states, s => s.probe_configuration);
      for (const e of state.endpoints) {
        const s = states[e.probeConfiguration.name];
        if (!s) {
          e.state = States.UNKNOWN;
        } else {
          e.state = s.type;
        }
      }
    },
    httpProbeNameReceived(state, { name }) {
      state.httpProbeName = name;
    }
  },
  getters: {
    endpointByName: state => name =>
      state.endpoints?.find(e => e.name === name) || {}
  },
  actions: {
    async listEndpoints({ commit, dispatch, rootState }) {
      const probeConfs = await Panto.listProbeConfigurations(
        rootState.organization.organization.agent.name
      );
      if (!probeConfs || probeConfs.length === 0) {
        return;
      }
      const endpoints = probeConfs
        .filter(pc => pc.probe_label === 'http') // Keep only http probes
        .map(makeEndpoint) // create endpoints from probe configurations
        .filter(e => e !== null); // makeEndpoint may return null for an invalid probe
      commit(Mutations.ENDPOINTS_RECEIVED, { endpoints });
      dispatch(Actions.GET_STATES);
    },
    async getStates({ commit, state, rootState }) {
      const organization = rootState.organization.organization;
      const endpoints = state.endpoints;
      const probeConfs = endpoints.map(e => e.probeConfiguration.name);
      const states = await Panto.getStates(organization.name, probeConfs);
      if (!states || states.length === 0) {
        return;
      }

      commit(Mutations.STATES_RECEIVED, { states });
    },
    async createEndpoint({ commit, state, rootState }, { endpoint }) {
      const organization = rootState.organization.organization;
      const target = await Panto.createTarget(organization.name, {
        address: endpoint.url
      });
      if (!target) {
        throw new Error('Failed to create endpoint');
      }
      const check = await Panto.createCheck(organization.name, {
        target: target.name,
        probe: state.httpProbeName,
        type: 'threshold',
        configuration: JSON.stringify({
          states: [
            {
              state: 'critical',
              match: 'any',
              conditions: [
                {
                  field: 'status-code',
                  operator: 'ge',
                  value: 400,
                  reason: ''
                }
              ]
            }
          ]
        })
      });
      if (!check) {
        throw new Error('Failed to create endpoint.');
      }
      let alert = rootState.alert.alert;
      alert = await Panto.updateAlert({
        ...alert,
        checks: [...alert.checks, check.name]
      });
      if (!alert || !alert.checks.includes(check.name)) {
        throw new Error('Failed to connect endpoint to alert.');
      }
      const probeConfiguration = await Panto.createProbeConfiguration(
        rootState.organization.organization.agent.name,
        {
          check: check.name,
          schedule: new Duration(endpoint.schedule.asMilliseconds()).toString(),
          configuration: JSON.stringify({
            url: endpoint.url,
            method: endpoint.method,
            timeout: endpoint.timeout,
            body: endpoint.body
          })
        }
      );
      if (!probeConfiguration) {
        throw new Error('Failed to create endpoint.');
      }
      commit(Mutations.ENDPOINT_CREATED, {
        endpoint: makeEndpoint(probeConfiguration)
      });
    },
    async updateEndpoint({ commit, getters }, { endpoint_name, endpoint }) {
      const original_endpoint = getters.endpointByName(endpoint_name);
      const probeConfiguration = await Panto.updateProbeConfiguration({
        name: original_endpoint?.probeConfiguration.name,
        check: original_endpoint?.check.name,
        schedule: new Duration(endpoint.schedule.asMilliseconds()).toString(),
        configuration: JSON.stringify({
          url: endpoint.url,
          method: endpoint.method,
          timeout: endpoint.timeout,
          body: endpoint.body
        })
      });
      if (!probeConfiguration) {
        throw new Error('Failed to update endpoint.');
      }
      // HACK: a probeConfiguration does not have a Target but an endpoint
      // has one. Add the Target to API result, so makeEndpoint does not fail.
      probeConfiguration.target = original_endpoint.target;
      commit(Mutations.ENDPOINT_UPDATED, {
        endpoint: makeEndpoint(probeConfiguration)
      });
    },
    async getHttpProbeName({ commit }) {
      const probes = await Panto.listProbes();
      if (!probes) {
        return;
      }
      for (const p of probes) {
        if (p.probe_name === 'http') {
          commit(Mutations.HTTP_PROBE_NAME_RECEIVED, { name: p.name });
          return;
        }
      }
    }
  }
};
