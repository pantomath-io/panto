// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import _ from 'lodash';

import Panto from '@/plugins/panto-api';

export const Actions = {
  LIST_ALERTS: 'listAlerts',
  GET_CHANNEL: 'getChannel',
  UPDATE_CHANNEL: 'updateChannel'
};

export const Mutations = {
  ALERT_RECEIVED: 'alertReceived',
  CHANNEL_RECEIVED: 'channelReceived'
};

function makeAlert(alert) {
  // TODO: @loderunner We should normalize the alert here and make it cleaner
  // for the panto•watch app. See `makeEndpoint` for inspiration.
  return alert;
}

function makeChannel(channel) {
  return { ...channel, configuration: JSON.parse(channel.configuration) };
}

export default {
  state: () => ({
    alert: {},
    channel: {}
  }),
  mutations: {
    alertReceived(state, { alert }) {
      state.alert = _.cloneDeep(alert);
    },
    channelReceived(state, { channel }) {
      state.channel = _.cloneDeep(channel);
    }
  },
  actions: {
    async listAlerts({ commit, rootState }) {
      const organization = rootState.organization.organization;
      const alerts = await Panto.listAlerts(organization.name);

      if (!alerts || alerts.length === 0) {
        return;
      }

      commit(Mutations.ALERT_RECEIVED, { alert: makeAlert(alerts[0]) });
    },
    async getChannel({ commit, state }) {
      const channel = await Panto.getChannel(state.alert.channel);
      if (!channel) {
        return;
      }

      commit(Mutations.CHANNEL_RECEIVED, { channel: makeChannel(channel) });
    },
    async updateChannel({ commit, state }, { channelConfiguration }) {
      const channel = await Panto.updateChannel({
        name: state.channel.name,
        channel_type: 'channeltypes/email',
        configuration: JSON.stringify(channelConfiguration)
      });
      if (!channel) {
        return;
      }
      commit(Mutations.CHANNEL_RECEIVED, { channel: makeChannel(channel) });
    }
  }
};
