// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import moment from 'moment';

import Panto from '@/plugins/panto-api';

export const Actions = {
  LOGIN: 'login',
  GET_SESSION_TOKEN: 'getSessionToken',
  LOGOUT: 'logout'
};

export const Mutations = {
  LOGIN_SUCCESSFUL: 'loginSuccessful',
  LOGIN_FAILED: 'loginFailed',
  LOGGED_OUT: 'loggedOut'
};

const sessionExpirationKey = 'sessionExpiration';

export default {
  state: () => ({
    token: null
  }),
  mutations: {
    loginSuccessful(state, { token, expires }) {
      state.token = token;
      Panto.setAuthorizationToken(token);
      localStorage.setItem(sessionExpirationKey, expires);
    },
    loginFailed(state) {
      state.token = null;
      Panto.setAuthorizationToken(null);
    },
    loggedOut(state) {
      state.token = null;
      Panto.setAuthorizationToken(null);
      localStorage.removeItem(sessionExpirationKey);
    }
  },
  getters: {
    loggedIn(state) {
      return !!state.token;
    },
    sessionExpiration() {
      return moment(localStorage.getItem(sessionExpirationKey));
    }
  },
  actions: {
    async login({ commit }, { username, password }) {
      try {
        const res = await Panto.login(username, password);
        if (!res.token) {
          commit(Mutations.LOGIN_FAILED);
          return;
        }
        commit(Mutations.LOGIN_SUCCESSFUL, res);
      } catch (err) {
        commit(Mutations.LOGIN_FAILED, err);
      }
    },
    async getSessionToken({ commit }) {
      try {
        const res = await Panto.getSessionToken();
        if (!res.token) {
          commit(Mutations.LOGIN_FAILED);
          return;
        }
        commit(Mutations.LOGIN_SUCCESSFUL, res);
      } catch (err) {
        commit(Mutations.LOGIN_FAILED, err);
      }
    },
    logout({ commit }) {
      commit(Mutations.LOGGED_OUT);
    }
  }
};
