// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import moment from 'moment';

import auth, { Actions, Mutations } from '../auth';
import PantoAPI from '@/plugins/panto-api';

jest.mock('@/plugins/panto-api');

describe('Vuex store Auth module', () => {
  const token = 'tp1NSJUKqk4Us9VkFSxkGlnooOAGesUSa8I_nFR1UKU';
  const expires = moment()
    .add(24, 'h')
    .utc();

  test('mutations.loginSuccessful', () => {
    const state = {};
    auth.mutations[Mutations.LOGIN_SUCCESSFUL](state, {
      token,
      expires: expires.toISOString()
    });

    expect(state.token).toEqual(token);
  });

  test('mutations.loginFailed', () => {
    const state = { token };
    auth.mutations[Mutations.LOGIN_FAILED](state);

    expect(state.token).toBeNil();
  });

  test('mutations.LOGGED_OUT', () => {
    const state = { token };
    auth.mutations[Mutations.LOGGED_OUT](state);

    expect(state.token).toBeNil();
  });

  test('getters.loggedIn', () => {
    const state = {};
    auth.mutations[Mutations.LOGIN_SUCCESSFUL](state, {
      token,
      expires: expires.toISOString()
    });
    expect(auth.getters.loggedIn(state)).toBeTrue();

    auth.mutations[Mutations.LOGIN_FAILED](state);
    expect(auth.getters.loggedIn(state)).toBeFalse();

    auth.mutations[Mutations.LOGIN_SUCCESSFUL](state, {
      token,
      expires: expires.toISOString()
    });
    auth.mutations[Mutations.LOGIN_FAILED](state);
    expect(auth.getters.loggedIn(state)).toBeFalse();
  });

  test('getters.sessionExpiration', () => {
    const state = {};
    auth.mutations[Mutations.LOGIN_SUCCESSFUL](state, {
      token,
      expires: expires.toISOString()
    });
    expect(auth.getters.sessionExpiration(state)).toEqualMoment(expires);
  });

  const username = 'test@panto.app';
  const password = 'trustno1';
  const mockLoginResponse = {
    token,
    expires: expires.toISOString()
  };

  test('actions.login', async () => {
    PantoAPI.login.mockResolvedValue(mockLoginResponse);
    const commit = jest.fn();
    await auth.actions[Actions.LOGIN]({ commit }, { username, password });

    expect(commit).toHaveBeenCalledWith(
      Mutations.LOGIN_SUCCESSFUL,
      mockLoginResponse
    );
  });

  test('actions.login fail', async () => {
    PantoAPI.login.mockResolvedValue({});
    const commit = jest.fn();

    await auth.actions[Actions.LOGIN]({ commit }, { username, password });
    expect(commit).toHaveBeenCalledWith(Mutations.LOGIN_FAILED);

    commit.mockClear();

    PantoAPI.login.mockRejectedValue({});
    await auth.actions[Actions.LOGIN]({ commit }, { username, password });
    expect(commit).toHaveBeenCalledWith(Mutations.LOGIN_FAILED, {});
  });

  test('actions.getSessionToken', async () => {
    PantoAPI.getSessionToken.mockResolvedValue(mockLoginResponse);
    const commit = jest.fn();

    await auth.actions[Actions.GET_SESSION_TOKEN]({ commit });
    expect(commit).toHaveBeenCalledWith(
      Mutations.LOGIN_SUCCESSFUL,
      mockLoginResponse
    );
  });

  test('actions.getSessionToken fail', async () => {
    PantoAPI.getSessionToken.mockResolvedValue({});
    const commit = jest.fn();

    await auth.actions[Actions.GET_SESSION_TOKEN]({ commit });
    expect(commit).toHaveBeenCalledWith(Mutations.LOGIN_FAILED);

    commit.mockClear();

    PantoAPI.getSessionToken.mockRejectedValue({});
    await auth.actions[Actions.GET_SESSION_TOKEN]({ commit });
    expect(commit).toHaveBeenCalledWith(Mutations.LOGIN_FAILED, {});
  });

  test('actions.logout', async () => {
    const commit = jest.fn();

    await auth.actions[Actions.LOGOUT]({ commit });
    expect(commit).toHaveBeenCalledWith(Mutations.LOGGED_OUT);
  });
});
