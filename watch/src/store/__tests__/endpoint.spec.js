// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import _ from 'lodash';
import moment from 'moment';

import endpoint, { Mutations, Actions, States } from '../endpoint';
import PantoAPI from '@/plugins/panto-api';

jest.mock('@/plugins/panto-api');

describe('Vuex store Endpoint module', () => {
  const mockEndpoint = global.mocks.mockEndpoint;
  test('mutations.endpointsReceived', () => {
    const state = {};
    endpoint.mutations[Mutations.ENDPOINTS_RECEIVED](state, {
      endpoints: [mockEndpoint]
    });

    expect(state.endpoints).toEqual([mockEndpoint]);
  });

  test('mutations.endpointCreated', () => {
    const state = { endpoints: [] };
    endpoint.mutations[Mutations.ENDPOINT_CREATED](state, {
      endpoint: mockEndpoint
    });

    expect(state.endpoints).toBeArray();
    expect(state.endpoints).toContain(mockEndpoint);
  });

  test('mutations.endpointUpdated', () => {
    const state = { endpoints: [_.cloneDeep(mockEndpoint)] };
    const updatedEndpoint = _.cloneDeep(mockEndpoint);
    updatedEndpoint['url'] = 'https://about.panto.app';
    endpoint.mutations[Mutations.ENDPOINT_UPDATED](state, {
      endpoint: updatedEndpoint
    });

    expect(state.endpoints).toBeArray();
    expect(state.endpoints).toContain(updatedEndpoint);
  });

  const mockState = {
    check: mockEndpoint.check.name,
    target: mockEndpoint.target.name,
    probe_configuration: mockEndpoint.probeConfiguration.name,
    timestamp: moment()
      .utc()
      .toISOString(),
    type: States.OK,
    reason: 'OK'
  };

  test('mutations.statesReceived', () => {
    const state = { endpoints: [_.cloneDeep(mockEndpoint)] };
    endpoint.mutations[Mutations.STATES_RECEIVED](state, {
      states: [mockState]
    });

    expect(state.endpoints[0].state).toEqual(States.OK);
  });

  test('getters.endpointByName', () => {
    const state = { endpoints: [_.cloneDeep(mockEndpoint)] };
    const e = endpoint.getters.endpointByName(state)(mockEndpoint.name);

    expect(e).toEqual(mockEndpoint);
  });

  const mockProbeConfigurationsResponse = [
    {
      name:
        'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ/probeconfigurations/QFPDL6h2QJCJNn0DhPdeaA',
      check:
        'organizations/xCM3-ZaGQoOAaJbqQjcexg/checks/qbdNvlQFQTGnGmjOl66ZFg',
      configuration: '{"url":"https://twitter.com"}',
      probe_label: 'http',
      schedule: '60s',
      target: {
        name:
          'organizations/xCM3-ZaGQoOAaJbqQjcexg/targets/hnjQmuRjQyyMJeF0DRDfXA',
        address: 'https://twitter.com',
        note: '',
        tags: []
      }
    }
  ];

  const mockRootState = {
    organization: {
      organization: global.mocks.mockOrganization
    },
    alert: { alert: global.mocks.mockAlert }
  };

  test('actions.listEndpoints', async () => {
    PantoAPI.listProbeConfigurations.mockResolvedValue(
      mockProbeConfigurationsResponse
    );
    const commit = jest.fn((mutation, payload) => {
      expect(mutation).toEqual(Mutations.ENDPOINTS_RECEIVED);
      expect(payload).toMatchObject({ endpoints: [mockEndpoint] });
    });
    const dispatch = jest.fn();
    await endpoint.actions[Actions.LIST_ENDPOINTS]({
      commit,
      rootState: mockRootState,
      dispatch
    });

    expect(commit).toHaveBeenCalled();
  });

  const mockCheck = {
    children: null,
    configuration:
      '{"states":[{"conditions":[{"field":"status-code","operator":"ge","reason":"","value":400}],"match":"any","state":"critical"}]}',
    name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/checks/qbdNvlQFQTGnGmjOl66ZFg',
    probe: 'probes/CpXQzjOVTMKzCvPLAnXZyQ',
    state_configuration: '',
    state_type: '',
    target:
      'organizations/xCM3-ZaGQoOAaJbqQjcexg/targets/hnjQmuRjQyyMJeF0DRDfXA',
    type: 'threshold'
  };

  test('actions.createEndpoint', async () => {
    PantoAPI.createTarget.mockResolvedValue(
      mockProbeConfigurationsResponse[0].target
    );
    PantoAPI.createCheck.mockResolvedValue(mockCheck);
    PantoAPI.updateAlert.mockResolvedValue(global.mocks.mockAlert);
    PantoAPI.createProbeConfiguration.mockResolvedValue(
      mockProbeConfigurationsResponse[0]
    );
    const commit = jest.fn((mutation, payload) => {
      expect(mutation).toEqual(Mutations.ENDPOINT_CREATED);
      expect(payload).toMatchObject({ endpoint: mockEndpoint });
    });
    const state = {
      httpProbeName: 'http'
    };
    await endpoint.actions[Actions.CREATE_ENDPOINT](
      {
        commit,
        state,
        rootState: mockRootState
      },
      {
        endpoint: {
          probeConfiguration: mockEndpoint.probeConfiguration,
          target: mockEndpoint.target,
          check: mockEndpoint.check,
          url: mockEndpoint.url,
          schedule: mockEndpoint.schedule
        }
      }
    );

    expect(commit).toHaveBeenCalled();
  });

  const mockProbe = {
    name: 'probes/CpXQzjOVTMKzCvPLAnXZyQ',
    probe_name: 'http'
  };

  test('actions.getHttpProbeName', async () => {
    PantoAPI.listProbes.mockResolvedValue([mockProbe]);
    const commit = jest.fn();
    await endpoint.actions[Actions.GET_HTTP_PROBE_NAME]({ commit });

    expect(commit).toHaveBeenCalledWith(Mutations.HTTP_PROBE_NAME_RECEIVED, {
      name: mockProbe.name
    });
  });
});
