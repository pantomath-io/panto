// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import organization, { Mutations, Actions } from '../organization';
import PantoAPI from '@/plugins/panto-api';

jest.mock('@/plugins/panto-api');

describe('Vuex store Organization module', () => {
  test('mutations.organizationReceived', () => {
    const state = {};
    organization.mutations[Mutations.ORGANIZATION_RECEIVED](state, {
      organization: global.mocks.mockOrganization
    });

    expect(state.organization).toEqual(global.mocks.mockOrganization);
  });

  const mockOrganizationsResponse = [
    {
      display_name: 'Organization',
      name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg'
    }
  ];

  const mockAgentsResponse = [
    {
      display_name: 'toto',
      last_activity: '2020-05-26T10:09:31.919906Z',
      last_version: '0.9.0',
      name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ'
    }
  ];

  test('actions.getOrganization', async () => {
    PantoAPI.listOrganizations.mockResolvedValue(mockOrganizationsResponse);
    PantoAPI.listAgents.mockResolvedValue(mockAgentsResponse);
    const commit = jest.fn();
    await organization.actions[Actions.GET_ORGANIZATION]({ commit });

    expect(commit).toHaveBeenCalledWith(Mutations.ORGANIZATION_RECEIVED, {
      organization: global.mocks.mockOrganization
    });
  });
});
