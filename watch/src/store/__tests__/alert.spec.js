// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import alert, { Actions, Mutations } from '../alert';
import PantoAPI from '@/plugins/panto-api';

jest.mock('@/plugins/panto-api');

const mockChannelResponse = {
  channel_type: 'channeltypes/email',
  children: null,
  configuration: '{"dest":"chrales@panto.app"}',
  display_name: '',
  name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/channels/_55pJmSqRY6cxxE-2slGFg'
};

const mockChannel = {
  ...mockChannelResponse,
  configuration: { dest: 'chrales@panto.app' }
};

const mockRootState = {
  organization: {
    organization: global.mocks.mockOrganization
  }
};

describe('Vuex store Alert module', () => {
  test('mutations.alertReceived', () => {
    const state = {};
    alert.mutations[Mutations.ALERT_RECEIVED](state, {
      alert: global.mocks.mockAlert
    });

    expect(state.alert).toEqual(global.mocks.mockAlert);
  });

  test('mutations.channelReceived', () => {
    const state = {};
    alert.mutations[Mutations.CHANNEL_RECEIVED](state, {
      channel: mockChannel
    });

    expect(state.channel).toEqual(mockChannel);
  });

  test('actions.listAlerts', async () => {
    PantoAPI.listAlerts.mockResolvedValue([global.mocks.mockAlert]);
    const commit = jest.fn((mutation, payload) => {
      expect(mutation).toEqual(Mutations.ALERT_RECEIVED);
      expect(payload).toMatchObject({ alert: global.mocks.mockAlert });
    });
    await alert.actions[Actions.LIST_ALERTS]({
      commit,
      rootState: mockRootState
    });

    expect(commit).toHaveBeenCalled();
  });

  test('actions.getChannel', async () => {
    PantoAPI.getChannel.mockResolvedValue(mockChannelResponse);
    const commit = jest.fn();
    await alert.actions[Actions.GET_CHANNEL]({
      commit,
      state: { alert: global.mocks.mockAlert }
    });

    expect(commit).toHaveBeenCalledWith(Mutations.CHANNEL_RECEIVED, {
      channel: mockChannel
    });
  });

  test('actions.updateChannel', async () => {
    PantoAPI.updateChannel.mockResolvedValue(mockChannelResponse);
    const commit = jest.fn();
    await alert.actions[Actions.UPDATE_CHANNEL](
      {
        commit,
        state: { channel: mockChannel }
      },
      {
        channelConfiguration: mockChannel.configuration,
        channel_type: 'channeltypes/email'
      }
    );

    expect(commit).toHaveBeenCalledWith(Mutations.CHANNEL_RECEIVED, {
      channel: mockChannel
    });
  });
});
