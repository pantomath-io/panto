// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import _ from 'lodash';

import Panto from '@/plugins/panto-api';

export const Actions = {
  GET_ORGANIZATION: 'getOrganization'
};

export const Mutations = {
  ORGANIZATION_RECEIVED: 'organizationReceived'
};

function makeOrganization(org) {
  const organization = {
    name: org.name,
    displayName: org.display_name
  };
  if (org.agent) {
    organization.agent = { name: org.agent.name };
  }
  return organization;
}

export default {
  state: () => ({
    organization: null
  }),
  mutations: {
    organizationReceived(state, { organization }) {
      state.organization = _.cloneDeep(organization);
    }
  },
  actions: {
    async getOrganization({ commit }) {
      const orgs = await Panto.listOrganizations();
      if (!orgs || orgs.length === 0) {
        return;
      }
      const org = orgs[0];
      const agents = await Panto.listAgents(org.name);
      if (agents && agents.length > 0) {
        org.agent = agents[0];
      }
      commit(Mutations.ORGANIZATION_RECEIVED, {
        organization: makeOrganization(org)
      });
    }
  }
};
