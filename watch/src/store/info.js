// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import _ from 'lodash';

import Panto from '@/plugins/panto-api';

export const Actions = {
  GET_INFO: 'getInfo'
};

export const Mutations = {
  INFO_RECEIVED: 'infoReceived'
};

export default {
  state: () => ({
    info: {
      version: '',
      long_version: ''
    }
  }),
  getters: {
    info(state) {
      return state.info;
    }
  },
  mutations: {
    infoReceived(state, { info }) {
      state.info = _.cloneDeep(info);
    }
  },
  actions: {
    async getInfo({ commit }) {
      const info = await Panto.info();
      commit(Mutations.INFO_RECEIVED, { info });
    }
  }
};
