// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
const veevalidate = require('vee-validate/dist/locale/en.json');

export default {
  ...veevalidate.messages,
  url: 'The {_field_} field must be a valid URL',
  duration: 'The {_field_} field must be a valid duration',
  'email-list':
    'The {_field_} field must be a comma-separated list of valid e-mail addresses'
};
