// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import validations from './validations';

const alertForm = require('./alert-form.json');
const createEndpoint = require('./create-endpoint.json');
const endpointCard = require('./endpoint-card.json');
const endpointDetails = require('./endpoint-details.json');
const endpointForm = require('./endpoint-form.json');
const endpoints = require('./endpoints.json');
const login = require('./login.json');
const navbar = require('./navbar.json');

export default {
  login,
  endpoints,
  'endpoint-card': endpointCard,
  'endpoint-details': endpointDetails,
  'create-endpoint': createEndpoint,
  'endpoint-form': endpointForm,
  'alert-form': alertForm,
  navbar,
  validations
};
