// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import _ from 'lodash';

import i18n from '@/i18n';
import { flattenKeys } from '@/util';

describe('I18n tests', () => {
  // Recursively extract translation keys in a flat array (grouped by locale)
  const localeKeys = {};
  for (const [locale, messages] of Object.entries(i18n.messages)) {
    localeKeys[locale] = flattenKeys(messages);
  }

  // "Flip" the object: key by translation keys, flat array of locales
  // E.g. { en: ['hello', 'world'], fr: ['hello', 'toto'] }
  //      => { hello: ['en', 'fr'], 'world': ['en'], 'toto': ['fr'] }
  const keyLocales = {};
  for (const [locale, keys] of Object.entries(localeKeys)) {
    for (const key of keys) {
      if (keyLocales[key]) {
        keyLocales[key].push(locale);
      } else {
        keyLocales[key] = [locale];
      }
    }
  }

  // Test presence/absence of keys
  const allLocales = Object.keys(i18n.messages);
  for (const [key, locales] of Object.entries(keyLocales)) {
    test(`'${key}'`, () => {
      expect(locales).toEqual(expect.arrayContaining(allLocales));
    });
  }
});
