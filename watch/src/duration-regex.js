// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// An implementation of the go-style string duration: 3h50m20s
// https://golang.org/pkg/time/#ParseDuration

const r = String.raw;

const floatRegexp = r`((\d*\.\d+)|(\d+\.?))`;

/* eslint-disable prettier/prettier */
export default new RegExp(
  '^(' + // start of string
  '([-+]?)' + // match optional  sign
  '((0)' + // match single 0
  '|' + // OR 
  '(' + (  // begin chain of units
    `(${floatRegexp}h)?` + // hours
    `(${floatRegexp}m)?` + // minutes
    `(${floatRegexp}s)?` + // seconds
    `(${floatRegexp}ms)?` + // milliseconds
    `((${floatRegexp}us)|(${floatRegexp}\u00b5s)|(${floatRegexp}\u03bcs))?` + // microseconds
    `(${floatRegexp}ns)?` // nanoseconds
  ) + ')' + // end chain of units
  '))$' // end of string
, 'u');
