// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Recursively extracts all nested keys as a flat array of dot-separated key
// strings
// E.g. { login: { signIn: 'Sign In', signOut: 'Sign Out' }}
//      => ['login.signIn', 'login.signOut']
export function flattenKeys(obj, prefix) {
  prefix = prefix ?? '';
  const keys = [];
  for (const [k, v] of Object.entries(obj)) {
    if (typeof v === 'object') {
      keys.push(...flattenKeys(v, `${prefix}${k}.`));
    } else if (typeof v !== 'undefined') {
      keys.push(`${prefix}${k}`);
    }
  }
  return keys;
}
