// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';
import Vuex from 'vuex';

import CreateEndpoint from '../CreateEndpoint';
import i18n from '@/i18n';
import { Actions } from '@/store/endpoint';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);

describe('CreateEndpoint', () => {
  test('matches snapshot', () => {
    const store = new Vuex.Store({
      actions: {
        [Actions.GET_HTTP_PROBE_NAME]: jest.fn()
      }
    });
    const createEndpoint = shallowMount(CreateEndpoint, {
      localVue,
      store,
      i18n
    });
    expect(createEndpoint).toMatchSnapshot();
  });

  test('submitting form dispatches CREATE_ENDPOINT action', async () => {
    const action = jest.fn();
    const store = new Vuex.Store({
      actions: {
        [Actions.GET_HTTP_PROBE_NAME]: jest.fn(),
        [Actions.CREATE_ENDPOINT]: action
      }
    });
    const createEndpoint = shallowMount(CreateEndpoint, {
      localVue,
      store,
      i18n,
      stubs: ['endpoint-form']
    });
    const endpointForm = createEndpoint.findComponent({ name: 'EndpointForm' });
    expect(endpointForm.exists()).toBeTrue();

    endpointForm.vm.$emit('submit');
    expect(action).toHaveBeenCalled();
  });
});
