// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { createLocalVue, shallowMount } from '@vue/test-utils';
import * as VeeValidate from 'vee-validate';
import VueI18n from 'vue-i18n';
import Vuex from 'vuex';

import Alerts from '../Alerts';
import i18n from '@/i18n';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);
localVue.component('ValidationProvider', VeeValidate.ValidationProvider);
localVue.component('ValidationObserver', VeeValidate.ValidationObserver);

describe('Alerts', () => {
  test('matches snapshot', () => {
    const store = new Vuex.Store({
      state: {
        organization: {}
      }
    });
    const alerts = shallowMount(Alerts, {
      localVue,
      store,
      i18n,
      props: { name: global.mocks.mockEndpoint.name }
    });
    expect(alerts).toMatchSnapshot();
  });
});
