// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';
import Vuex from 'vuex';

import Login from '../Login';
import i18n from '@/i18n';
import { Actions } from '@/store/auth';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);

describe('Login', () => {
  test('matches snapshot', () => {
    const login = shallowMount(Login, {
      localVue,
      store: new Vuex.Store(),
      i18n,
      stubs: ['font-awesome-icon']
    });

    expect(login).toMatchSnapshot();
  });

  test('dispatches LOGIN action on form submit', () => {
    const loginAction = jest.fn();
    const store = new Vuex.Store({
      actions: {
        [Actions.LOGIN]: loginAction
      }
    });
    const login = shallowMount(Login, {
      localVue,
      store,
      i18n,
      stubs: ['font-awesome-icon']
    });

    const form = login.find('form');
    expect(form.exists()).toBeTrue();

    form.trigger('submit');
    expect(loginAction).toHaveBeenCalled();
    expect(loginAction.mock.calls[0].length).toBeGreaterThanOrEqual(2);
    expect(loginAction.mock.calls[0][1]).toContainKeys([
      'username',
      'password'
    ]);
  });
});
