// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';
import Vuex from 'vuex';

import EndpointDetails from '../EndpointDetails';
import i18n from '@/i18n';
import { Actions } from '@/store/endpoint';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);

describe('EndpointDetails', () => {
  test('matches snapshot', () => {
    const store = new Vuex.Store({
      state: {
        organization: {}
      },
      getters: {
        endpointByName: () => () => global.mocks.mockEndpoint
      }
    });
    const endpointDetails = shallowMount(EndpointDetails, {
      localVue,
      store,
      i18n,
      props: { name: global.mocks.mockEndpoint.name }
    });
    expect(endpointDetails).toMatchSnapshot();
  });

  test('submitting form dispatches UPDATE_ENDPOINT action', async () => {
    const action = jest.fn();
    const store = new Vuex.Store({
      state: {
        organization: {
          organization: global.mocks.mockOrganization
        }
      },
      getters: {
        endpointByName: () => () => global.mocks.mockEndpoint
      },
      actions: {
        [Actions.UPDATE_ENDPOINT]: action,
        [Actions.LIST_ENDPOINTS]: jest.fn()
      }
    });
    const endpointDetails = shallowMount(EndpointDetails, {
      localVue,
      store,
      i18n,
      props: { name: global.mocks.mockEndpoint.name },
      stubs: ['endpoint-form']
    });
    const endpointForm = endpointDetails.findComponent({
      name: 'EndpointForm'
    });
    expect(endpointForm.exists()).toBeTrue();

    endpointForm.vm.$emit('submit');
    expect(action).toHaveBeenCalled();
  });
});
