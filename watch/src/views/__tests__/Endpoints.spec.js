// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';
import Vuex from 'vuex';

import Endpoints from '../Endpoints';
import i18n from '@/i18n';
import { Routes } from '@/router';
import { Actions } from '@/store/endpoint';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);

describe('Endpoints', () => {
  test('matches snapshot', () => {
    const store = new Vuex.Store({
      state: {
        organization: { organization: {} },
        endpoint: { endpoints: [] }
      },
      actions: {
        [Actions.LIST_ENDPOINTS]: jest.fn()
      }
    });
    const endpoints = shallowMount(Endpoints, {
      localVue,
      store,
      i18n,
      mocks: { $router: { routes: Routes } },
      stubs: ['font-awesome-icon', 'router-link']
    });
    expect(endpoints).toMatchSnapshot();
  });

  test('matches snapshot', () => {
    const { mockEndpoint } = global.mocks;
    const store = new Vuex.Store({
      state: {
        organization: { organization: {} },
        endpoint: {
          endpoints: [
            {
              ...mockEndpoint,
              probeConfiguration: {
                name:
                  'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ/probeconfigurations/QFPDL6h2QJCJNn0DhPdeaA'
              }
            },
            {
              ...mockEndpoint,
              probeConfiguration: {
                name:
                  'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ/probeconfigurations/QFPDL6h2QJCJNn0DhPdeaB'
              }
            },
            {
              ...mockEndpoint,
              probeConfiguration: {
                name:
                  'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ/probeconfigurations/QFPDL6h2QJCJNn0DhPdeaC'
              }
            },
            {
              ...mockEndpoint,
              probeConfiguration: {
                name:
                  'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ/probeconfigurations/QFPDL6h2QJCJNn0DhPdeaD'
              }
            }
          ]
        }
      },
      actions: {
        [Actions.LIST_ENDPOINTS]: jest.fn()
      }
    });
    const endpoints = shallowMount(Endpoints, {
      localVue,
      store,
      i18n,
      mocks: { $router: { routes: Routes } },
      stubs: ['font-awesome-icon', 'router-link']
    });
    expect(endpoints).toMatchSnapshot();
  });
});
