import {
  EXPECTED_COLOR,
  RECEIVED_COLOR,
  matcherErrorMessage,
  matcherHint,
  printExpected,
  printReceived,
  printWithType,
  stringify
} from 'jest-matcher-utils';
import moment from 'moment';

// Global Vue test utils config
global.mocks = {
  mockOrganization: {
    displayName: 'Organization',
    name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg',
    agent: {
      name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ'
    }
  },
  mockEndpoint: {
    name: 'QFPDL6h2QJCJNn0DhPdeaA',
    probeConfiguration: {
      name:
        'organizations/xCM3-ZaGQoOAaJbqQjcexg/agents/IPh0ffZcTV6FbwaEUR2ZhQ/probeconfigurations/QFPDL6h2QJCJNn0DhPdeaA'
    },
    target: {
      name:
        'organizations/xCM3-ZaGQoOAaJbqQjcexg/targets/hnjQmuRjQyyMJeF0DRDfXA'
    },
    check: {
      name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/checks/qbdNvlQFQTGnGmjOl66ZFg'
    },
    url: 'https://twitter.com',
    schedule: moment.duration(60, 's')
  },
  mockAlert: {
    channel:
      'organizations/xCM3-ZaGQoOAaJbqQjcexg/channels/_55pJmSqRY6cxxE-2slGFg',
    checks: [
      'organizations/xCM3-ZaGQoOAaJbqQjcexg/checks/qbdNvlQFQTGnGmjOl66ZFg'
    ],
    children: null,
    configuration: '{}',
    name: 'organizations/xCM3-ZaGQoOAaJbqQjcexg/alerts/iLZst9ymReKMd4FcEMU_jg',
    type: 'state_change'
  }
};

// Jest matcher for Moment
expect.extend({
  toEqualMoment(received, expected) {
    const matcherName = 'toEqualMoment';

    if (!moment.isMoment(received)) {
      return {
        pass: false,
        message: () =>
          matcherErrorMessage(
            matcherHint(matcherName, undefined, undefined),
            `${RECEIVED_COLOR('received')} value must be of type Moment`,
            printWithType('Received', received, printReceived)
          )
      };
    }

    if (!moment.isMoment(expected)) {
      return {
        pass: false,
        message: () =>
          matcherErrorMessage(
            matcherHint(matcherName, undefined, undefined),
            `${EXPECTED_COLOR('expected')} value must be of type Moment`,
            printWithType('Expected', expected, printExpected)
          )
      };
    }

    const pass = expected.isSame(received);

    const message = pass
      ? () =>
          matcherHint(matcherName, undefined, undefined) +
          '\n\n' +
          `Expected: not ${printExpected(expected)}` +
          (stringify(expected) !== stringify(received)
            ? `\nReceived:     ${printReceived(received)}`
            : '')
      : () =>
          matcherHint(matcherName, undefined, undefined) +
          '\n\n' +
          `Expected: ${printExpected(expected)}` +
          (stringify(expected) !== stringify(received)
            ? `\nReceived:     ${printReceived(received)}`
            : '');

    return { message, pass };
  }
});
