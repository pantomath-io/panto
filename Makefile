# Copyright 2017 Pantomath SAS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PKG_BASE = "gitlab.com/pantomath-io/panto"
BINDIR ?= "$(CURDIR)/bin"
VERSION := $(shell cat VERSION)
GITHASH = $(shell git rev-parse --short HEAD)
GO_FILES := $(shell find . -name '*.go' | grep -v '/vendor/')
PROTO_FILES := $(shell find . -name '*.proto' | grep -v '/vendor/')
GOOGLEAPIS_PATH="$(shell go list -f '{{ .Dir }}' -m github.com/googleapis/googleapis)"

.PHONY: all clean api coverage coveragehtml fmt lint notice test race msan help

all: api panto panto-agent panto-ctl panto-watch panto-web notice

clean: ## Clean all sub-targets
	@rm -rf "${BINDIR}"
	@rm -rf coverage

api: ## Generate API files from protobuf
	@$(MAKE) -C api build

panto: api ## Build the Panto server
	@go build -v -o "${BINDIR}/panto" -tags="json1" -ldflags="-s -w -X main.version=${VERSION} -X main.githash=${GITHASH}" ${PKG_BASE}/cmd/panto

panto-agent: api panto-agent-protobuf ## Build the Panto agent
	@go build -v -o "${BINDIR}/panto-agent" -tags="json1" -ldflags="-s -w -X main.version=${VERSION} -X main.githash=${GITHASH}" ${PKG_BASE}/cmd/panto-agent

panto-ctl: api ## Build the Panto client
	@go build -v -o "${BINDIR}/panto-ctl" -tags="json1" -ldflags="-s -w -X main.version=${VERSION} -X main.githash=${GITHASH}" ${PKG_BASE}/cmd/panto-ctl

panto-watch: ## Build the Panto watch UI
	@$(MAKE) -C watch build

panto-web: ## Build the Panto web UI
	@$(MAKE) -C web build

panto-agent-protobuf: api ## Build the protobuf dedicated to the Panto agent
	@echo $@
	@protoc -I${PWD} \
		-I${GOOGLEAPIS_PATH} \
		--go_out=. \
		agent/local_config.proto
	@protoc -I${PWD} \
		-I${GOOGLEAPIS_PATH} \
		--go_out=. \
		agent/local_spooler.proto

fmt: ## Format Go files
	@gofmt -s -w ${GO_FILES}

files: ## List all Go files
	@echo ${GO_FILES}

lint: ## Run the linter on all Go and protobuf files
	@golint -set_exit_status ./...
	@set -e; for proto_file in ${PROTO_FILES}; do \
		clang-format -style=Google $${proto_file} | cmp $${proto_file}; \
	done

notice: ## Generate the NOTICE file
	@./check_license.sh > NOTICE

test: ## Run tests
	@go test -short -tags="json1" ${PKG_BASE}/...
	@$(MAKE) -C web test
	@$(MAKE) -C watch test

race: ## Run tests with race detector
	@go test -race -short -tags="json1" ${PKG_BASE}/...

msan: ## Run tests with memory sanitizer
	@go test -msan -short -tags="json1" ${PKG_BASE}/...

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

coverage: ## Generate code coverage
	@./coverage.sh

coveragehtml: ## Generate code coverage report
	@./coverage.sh html

packages: ## Generate packages page index
	@sed -e "s/%VERSION%/${VERSION}/g" www/_packages-index-template.html > www/packages/index.html
