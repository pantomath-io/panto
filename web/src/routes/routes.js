// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
import LoginLayout from '../components/Dashboard/Layout/LoginLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'
// Dashboard pages
import Pulse from 'src/components/Dashboard/Views/Pulse/Pulse.vue'

import Dashboard from 'src/components/Dashboard/Views/Dashboard/Dashboard.vue'

import Environment from 'src/components/Dashboard/Views/Environment/Environment.vue'
import EnvironmentAgent from 'src/components/Dashboard/Views/Environment/EnvironmentAgent.vue'
import EnvironmentAgentProbe from 'src/components/Dashboard/Views/Environment/EnvironmentAgentProbe.vue'

import Alerts from 'src/components/Dashboard/Views/Alerts/Alerts.vue'

// Pages
import Login from 'src/components/Dashboard/Views/Pages/Login.vue'


let loginPage = {
  path: '/login',
  component: LoginLayout,
  children: [
    {
      path: '/login',
      name: 'Login',
      meta: {auth: false},
      component: Login
    }
  ]
}

const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/dashboard'
  },
  loginPage,
  {
    path: '/pulse',
    component: DashboardLayout,
    children: [
      {
        path: '/pulse:organization(/organizations/[^/]+?)',
        name: 'Pulse',
        meta: {auth: false},
        component: Pulse
      }
    ]
  },
  {
    path: '/dashboard',
    component: DashboardLayout,
    children: [
      {
        path: '/dashboard/:organization(organizations/[^/]+?)',
        name: 'Dashboard',
        meta: {auth: false},
        component: Dashboard
      },
      {
        path: '/dashboard/:target(organizations/[^/]+?/targets/[^/]+?)',
        pathToRegexOptions: {strict: true},
        name: 'DashboardTarget',
        meta: {auth: false},
        component: Dashboard
      }
    ]
  },
  {
    path: '/environment',
    component: DashboardLayout,
    children: [
      {
        path: '/environment/:organization(organizations/[^/]+?)',
        pathToRegexOptions: {strict: true},
        name: 'Environment',
        meta: {auth: false},
        component: Environment,
        children: [
          {
            path: '/environment/:agent(organizations/[^/]+?/agents/[^/]+?)',
            pathToRegexOptions: {strict: true},
            name: 'EnvironmentAgent',
            meta: {auth: false},
            component: EnvironmentAgent
          },
          {
            path: '/environment/:agent(organizations/[^/]+?/agents/[^/]+?)/install',
            pathToRegexOptions: {strict: true},
            name: 'EnvironmentAgentProbe',
            meta: {auth: false},
            component: EnvironmentAgentProbe
          },
          {
            path: '/environment/:probe_configuration(organizations/[^/]+?/agents/[^/]+?/probeconfigurations/[^/]+?)',
            pathToRegexOptions: {strict: true},
            name: 'EnvironmentAgentProbeConfiguration',
            meta: {auth: false},
            component: EnvironmentAgentProbe
          }
        ]
      }
    ]
  },
  {
    path: '/alerts',
    component: DashboardLayout,
    children: [
      {
        path: '/alerts/:organization(organizations/[^/]+?)',
        pathToRegexOptions: {strict: true},
        name: 'Alerts',
        meta: {auth: false},
        component: Alerts
      }
    ]
  },
  {path: '*', component: NotFound}
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
