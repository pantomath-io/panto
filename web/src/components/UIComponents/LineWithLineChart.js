import Chart from 'chart.js'
import { generateChart } from 'vue-chartjs'

Chart.defaults.lineWithLine = Object.assign({}, Chart.defaults.line)
Chart.controllers.lineWithLine = Chart.controllers.line.extend({
  draw: function (ease) {
    Chart.controllers.line.prototype.draw.call(this, ease)

    if (this.chart.lineTime !== null) {
      const t = this.chart.lineTime
      const x = this.chart.scales['x-axis-0'].getPixelForOffset(t)
      const topY = this.chart.scales['y-axis-0'].top
      const bottomY = this.chart.scales['y-axis-0'].bottom

      // draw line
      const ctx = this.chart.ctx
      ctx.save()
      ctx.beginPath()
      ctx.moveTo(x, topY)
      ctx.lineTo(x, bottomY)
      ctx.lineWidth = 1
      ctx.strokeStyle = '#666666'
      ctx.stroke()
      ctx.restore()
    }
  }
})

const LineWithLine = generateChart('line-with-line', 'lineWithLine')

export default {
  name: 'line-with-line-chart',
  extends: LineWithLine,
  props: {
    chartData: {},
    chartOptions: {
      type: Object
    },
    lineTime: {
      type: Number,
      default: null
    }
  },
  mounted () {
    const vm = this
    this.addPlugin({
      id: 'TooltipDetectionPlugin',
      afterEvent (chart, event) {
        if (['mouseout', 'mousemove'].includes(event.type)) {
          const tooltipIsActive = chart.tooltip && chart.tooltip._active && chart.tooltip._active.length > 0
          if (event.type === 'mousemove') {
            const lineX = tooltipIsActive ? chart.tooltip._active[0].tooltipPosition().x : event.x
            const scale = chart.scales['x-axis-0']
            if (lineX > scale.left && lineX < scale.right) {
              const lineTime = scale.getValueForPixel(lineX)
              vm.$emit('update:lineTime', lineTime.valueOf())
            } else {
              vm.$emit('update:lineTime', null)
            }
          } else if (event.type === 'mouseout') {
            vm.$emit('update:lineTime', null)
          }
        }
      },
      beforeRender (chart) {
        chart.lineTime = vm.lineTime
      }
    })
  },
  methods: {
    render () {
      this.renderChart(this.chartData, this.chartOptions)
    }
  },
  watch: {
    lineTime () {
      this.$nextTick(() => this.$data._chart.render())
    }
  }
}
