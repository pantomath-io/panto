// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import moment from 'moment'

// Checks if all of object's own properties are equal, recursing on nested objects
const deepEqual = function (obj1, obj2) {
  if (obj1 === obj2) {
    return true
  }
  const type1 = typeof obj1
  const type2 = typeof obj2
  if (type1 !== type2) {
    return false
  }
  if (type1 === 'object') {
    return Object.entries(obj1).every(([k, v]) => deepEqual(obj2[k], v))
  } else {
    return false
  }
}

// Checks if any of object's own properties are not equal, recursing on nested objects
const deepNotEqual = function (obj1, obj2) {
  if (obj1 === obj2) {
    return false
  }
  const type1 = typeof obj1
  const type2 = typeof obj2
  if (type1 !== type2) {
    return true
  }
  if (type1 === 'object') {
    return Object.entries(obj1).some(([k, v]) => deepNotEqual(obj2[k], v))
  } else {
    return true
  }
}

export default {
  isEmpty (obj) {
    if (obj === undefined || obj === null) {
      return true
    }
    return Object.keys(obj).length === 0 && obj.constructor === Object
  },
  isFunction (f) {
    return !!f &&
      (Object.prototype.toString.call(f) === '[object Function]' ||
        typeof f === 'function' ||
        f instanceof Function)
  },
  toPlainObject (obj) {
    return Object.assign({}, obj)
  },
  formatGolangTypes (value, type) {
    if (value === undefined || value === null) {
      return
    }
    const vType = typeof value
    // Go playground to test types
    // https://play.golang.org/p/QibPd7u9jbF
    if (type === 'time.Duration') {
      switch (vType) {
        case 'number':
          return Math.floor(value)
        case 'string':
          const r = parseInt(value)
          if (!isNaN(r)) {
            return r
          }
      }
    } else if (['float', 'float32', 'float64'].includes(type)) {
      switch (vType) {
        case 'number':
          return value
        case 'string':
          const r = parseFloat(value)
          if (!isNaN(r)) {
            return r
          }
      }
    } else if (['int', 'int32', 'int64', 'uint', 'uint32', 'uint64'].includes(type)) {
      switch (vType) {
        case 'number':
          return Math.floor(value)
        case 'string':
          const r = parseInt(value)
          if (!isNaN(r)) {
            return r
          }
      }
    } else if (type === 'bool') {
      switch (vType) {
        case 'boolean':
          return value
        case 'string':
          if (value.toLowerCase() === 'true') {
            return true
          } else if (value.toLowerCase() === 'false') {
            return false
          }
      }
    } else if (type === 'string') {
      switch (vType) {
        case 'string':
          return value
        default:
          return value.toString()
      }
    }
    console.error(`failed to convert (${typeof value})${value} to ${type}`)
  },
  convertGolangProtobufPTypes (value) {
    if (value === 'NaNs') {
      return null
    }
    return value
  },
  fromNow (value) {
    return moment(value).fromNow()
  },
  formatDate (value, format) {
    return moment(value).format(format || 'DD-MM-YYYY')
  },
  env (key) {
    if (process.env[key]) {
      return process.env[key]
    }
    return ''
  },
  each (obj, callback) {
    Object.keys(obj).forEach(function (key) {
      callback(key, obj[key])
    })
  },
  static_url (resource, random) {
    return `${process.env.STATIC_BASE_URL}/${resource}` + (random === true ? '?_d=' + (new Date()) : '')
  },
  api_url (resource) {
    return `${process.env.API_BASE_URL}/${resource}`
  },
  pagination (headers) {
    const pagination = {}
    this.each(headers, function (k, v) {
      if (k.indexOf('x-pagination-') === 0) {
        if (v === 'True') {
          v = true
        } else if (v === 'False') {
          v = false
        } else {
          v = parseInt(v)
        }
        pagination[k.split('x-pagination-')[1]] = v
      }
    })
    return pagination
  },
  formatCheckLabel (name, checks) {
    if (!checks) {
      return name
    }
    for (const check of checks) {
      if (check.name === name) {
        if (check.children &&
          check.children.probe &&
          check.children.target) {
          return `${check.children.probe.display_name} - ${check.children.target.address}`
        } else {
          return name
        }
      }
    }
    return name
  },
  deepEqual,
  deepNotEqual
}
