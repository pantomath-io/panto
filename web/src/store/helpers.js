import Vue from 'vue'
import helpers from '../helpers'

const lock = {}

async function listResource (method, url, filters, callback) {
  // Try "lock" to see if ongoing listResource for this URL
  if (lock[url]) {
    return
  }
  try {
    lock[url] = true
    let nextPageToken
    while (nextPageToken !== '') {
      let response = {}
      if (method === 'get') {
        response = await Vue.axios.get(
          url,
          { params: { ...filters, page_token: nextPageToken } }
        )
      } else if (method === 'post') {
        response = await Vue.axios.post(
          url,
          JSON.stringify({ ...filters, page_token: nextPageToken })
        )
      }

      if (response.data.next_page_token === nextPageToken) {
        // Something went wrong: next page is this page. This could end up in an infinite loop, breaking out with an error
        throw new Error('Detected paging loop')
      }

      if (helpers.isFunction(callback)) {
        callback(response)
      }
      nextPageToken = response.data.next_page_token
    }
  } catch (err) {
    throw err
  } finally {
    // Always unlock, even in case of an exception
    lock[url] = false
  }
}

export {
  listResource
}
