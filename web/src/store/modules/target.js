// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import * as types from '../mutation-types'
import { listResource } from '../helpers'

// initial state
const state = {
  targets: [],
  target: {}
}

// getters
const getters = {
  targets: state => state.targets,
  target: state => state.target
}

// actions
const actions = {
  listTargets ({ commit }, args) {
    commit(types.RECEIVE_TARGETS, [])
    return listResource(
      'get',
      `${args.organization}/targets`,
      (args && args.filters) ? args.filters : {},
      response => commit(types.APPEND_TARGETS, response.data.targets)
    ).then(() => {
      commit(types.RECEIVE_TARGETS, state.targets)
      return state.targets
    })
  },
  getTarget ({ commit }, args) {
    return Vue.axios.get(`${args.target}`, {
      params: {
        children_mask: args.children_mask
      }
    }).then((response) => {
      commit(types.RECEIVE_TARGET, { response })
      return response.data
    })
  },
  setTarget ({ commit }, args) {
    commit(types.SET_TARGET, { target: args.target })
    return args.target
  },
  createTarget ({ commit }, args) {
    return Vue.axios.post(`/${args.organization_name}/targets`, JSON.stringify({
      target: { 'address': args.address }
    })).then((response) => {
      commit(types.RECEIVE_CREATED_TARGET, { response })
      return response.data
    })
  },
  updateTarget ({ commit }, args) {
    return Vue.axios.put(args.target.name, JSON.stringify({
      target: args.target,
      update_mask: args.update_mask
    })).then((response) => {
      commit(types.RECEIVE_UPDATED_TARGET, { response })
      return response.data
    })
  },
  deleteTarget ({ commit }, args) {
    return Vue.axios.delete(`/${args.target}`, {}).then(() => {
      commit(types.DELETE_TARGET)
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_TARGETS] (state, targets) {
    if (targets) {
      state.targets = targets
    } else {
      state.targets = []
    }
  },
  [types.APPEND_TARGETS] (state, targets) {
    if (targets) {
      state.targets.push(...targets)
    }
  },
  [types.SET_TARGET] (state, { target }) {
    if (target) {
      state.target = target
    } else {
      state.target = undefined
    }
  },
  [types.RECEIVE_TARGET] (state, { response }) {
    state.target = response.data
  },
  [types.RECEIVE_CREATED_TARGET] (state, { response }) {
    state.target = response.data
  },
  [types.RECEIVE_UPDATED_TARGET] (state, { response }) {
    state.target = response.data
  },
  [types.DELETE_TARGET] (state) {
    state.target = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
