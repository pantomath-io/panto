// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import * as types from '../mutation-types'
import { listResource } from '../helpers'

// initial state
const state = {
  probeConfigurations: [],
  probeConfiguration: {}
}

// getters
const getters = {
  probeConfigurations: state => state.probeConfigurations,
  probeConfiguration: state => state.probeConfiguration
}

// actions
const actions = {
  QueryProbeConfiguration ({ commit }, args) {
    commit(types.RECEIVE_PROBE_CONFIGURATIONS, [])
    return listResource(
      'post',
      `${args.organization}/probeconfigurations/query`,
      (args && args.filters) ? args.filters : {},
      response => commit(types.APPEND_PROBE_CONFIGURATIONS, response.data.probe_configurations)
    ).then(() => {
      commit(types.RECEIVE_PROBE_CONFIGURATIONS, state.probeConfigurations)
      return state.probeConfigurations
    })
  },
  listProbeConfigurations ({ commit }, args) {
    commit(types.RECEIVE_PROBE_CONFIGURATIONS, [])
    return listResource(
      'get',
      `${args.parent}/probeconfigurations`,
      args.filters,
      response => commit(types.APPEND_PROBE_CONFIGURATIONS, response.data.probe_configurations)
    ).then(() => {
      commit(types.RECEIVE_PROBE_CONFIGURATIONS, state.probeConfigurations)
      return state.probeConfigurations
    })
  },
  getProbeConfiguration ({ commit }, args) {
    return Vue.axios.get(`${args.probe}`, {}).then((response) => {
      commit(types.RECEIVE_PROBE_CONFIGURATION, { response })
    })
  },
  setProbeConfiguration ({ commit }, probeConfiguration) {
    commit(types.SET_PROBE_CONFIGURATION, { probeConfiguration })
  },
  createProbeConfiguration ({ commit }, args) {
    return Vue.axios.post(`${args.agent.name}/probeconfigurations`, JSON.stringify({
      probe_configuration: {
        check: args.check.name,
        configuration: JSON.stringify(args.configuration),
        probe_label: args.probe.display_name,
        schedule: args.schedule
      }
    })).then((response) => {
      commit(types.RECEIVE_CREATED_PROBE_CONFIGURATION, { response })
    })
  },
  updateProbeConfiguration ({ commit }, args) {
    return Vue.axios.put(`${args.probeConfiguration.name}`, JSON.stringify({
      probe_configuration: {
        check: args.check.name,
        configuration: JSON.stringify(args.configuration),
        probe_label: args.probe.display_name,
        schedule: args.schedule
      }
    })).then((response) => {
      commit(types.RECEIVE_UPDATED_PROBE_CONFIGURATION, { response })
    })
  },
  deleteProbeConfiguration ({ commit }, args) {
    return Vue.axios.delete(`/${args.probeConfiguration}`, {}).then((response) => {
      commit(types.DELETE_PROBE_CONFIGURATION)
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_PROBE_CONFIGURATIONS] (state, probeConfigurations) {
    if (probeConfigurations) {
      for (const pc of probeConfigurations) {
        pc.configuration = JSON.parse(pc.configuration)
      }
      state.probeConfigurations = probeConfigurations
    } else {
      state.probeConfigurations = []
    }
  },
  [types.APPEND_PROBE_CONFIGURATIONS] (state, probeConfigurations) {
    if (probeConfigurations) {
      state.probeConfigurations.push(...probeConfigurations)
    }
  },
  [types.RECEIVE_PROBE_CONFIGURATION] (state, { response }) {
    if (response.data.configuration) {
      response.data.configuration = JSON.parse(response.data.configuration)
    }
    state.probeConfiguration = response.data
  },
  [types.SET_PROBE_CONFIGURATION] (state, { probeConfiguration }) {
    state.probeConfiguration = probeConfiguration
  },
  [types.RECEIVE_UPDATED_PROBE_CONFIGURATION] (state, { response }) {
    if (response.data.configuration) {
      response.data.configuration = JSON.parse(response.data.configuration)
    }
    state.probeConfiguration = response.data
  },
  [types.RECEIVE_CREATED_PROBE_CONFIGURATION] (state, { response }) {
    if (response.data.configuration) {
      response.data.configuration = JSON.parse(response.data.configuration)
    }
    state.probeConfiguration = response.data
  },
  [types.DELETE_PROBE_CONFIGURATION] (state) {
    state.probeConfiguration = undefined
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
