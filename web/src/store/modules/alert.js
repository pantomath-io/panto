import Vue from 'vue'
import * as types from '../mutation-types'
import { listResource } from '../helpers'

// initial state
const state = {
  alerts: [],
  alert: {},
  alertDefinitions: {
    'state_change': { label: 'State Change', tooltip: 'Alert when state changes' },
    'state_recurrence': { label: 'State Recurrence', tooltip: 'Alert when the same state occurs several times' },
    'state_flap': { label: 'State Flap', tooltip: 'Alert when states alternate several times' }
  }
}

// getters
const getters = {
  alerts: state => state.alerts,
  alert: state => state.alert,
  alertDefinitions: state => state.alertDefinitions
}

// actions
const actions = {
  listAlerts ({ state, commit }, args) {
    commit(types.RECEIVE_ALERTS, [])
    return listResource(
      'get',
      `${args.organization}/alerts`,
      (args && args.filters) ? args.filters : {},
      response => commit(types.APPEND_ALERTS, response.data.alerts)
    ).then(() => {
      commit(types.RECEIVE_ALERTS, state.alerts)
      return state.alerts
    })
  },
  getAlert ({ commit }, args) {
    return Vue.axios.get(`${args.alert}`, {}).then(response => {
      commit(types.RECEIVE_ALERT, { response })
    })
  },
  setAlert ({ commit }, alert) {
    commit(types.SET_ALERT, { alert })
  },
  createAlert ({ commit }, args) {
    return Vue.axios.post(`/${args.organization}/alerts`, {
      alert: {
        channel: args.alert.channel,
        type: args.alert.type,
        checks: args.alert.checks,
        configuration: args.alert.configuration
      }
    }).then((response) => {
      commit(types.RECEIVE_CREATED_ALERT, { response })
    })
  },
  updateAlert ({ commit }, args) {
    return Vue.axios.put(`/${args.alert.name}`, JSON.stringify({
      alert: { ...args.alert },
      update_mask: args.update_mask
    })).then((response) => {
      commit(types.RECEIVE_ALERT, { response })
    })
  },
  deleteAlert ({ commit }, args) {
    return Vue.axios.delete(`/${args.alert.name}`, {}).then(response => {
      commit(types.DELETE_ALERT, { response })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_ALERTS] (state, alerts) {
    if (alerts) {
      state.alerts = alerts
    } else {
      state.alerts = []
    }
  },
  [types.APPEND_ALERTS] (state, alerts) {
    if (alerts) {
      state.alerts.push(...alerts)
    }
  },
  [types.RECEIVE_ALERT] (state, { response }) {
    state.alert = response.data
  },
  [types.SET_ALERT] (state, { alert }) {
    state.alert = alert
  },
  [types.RECEIVE_CREATED_ALERT] (state, { response }) {
    state.alert = response.data
  },
  [types.DELETE_ALERT] (state) {
    state.alert = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
