import Vue from 'vue'
import * as types from '../mutation-types'

// initial state
const state = {
  info: {}
}

// getters
const getters = {
  info: state => state.info
}

// actions
const actions = {
  getInfo ({ state, commit }, args) {
    Vue.axios.get('info').then((response) => {
      commit(types.RECEIVE_INFO, { response })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_INFO] (state, { response }) {
    state.info = response.data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
