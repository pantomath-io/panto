import Vue from 'vue'
import * as types from '../mutation-types'

// initial state
const state = {
  notifications: []
}

// getters
const getters = {
  notifications: state => state.notifications
}

// actions
const actions = {
  queryNotifications ({ commit }, args) {
    return Vue.axios.post(`${args.organization}/notifications/query`, JSON.stringify(
      args.filters
    )).then((response) => {
      commit(types.RECEIVE_NOTIFICATIONS, { response })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_NOTIFICATIONS] (state, { response }) {
    state.notifications = response.data.notifications
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
