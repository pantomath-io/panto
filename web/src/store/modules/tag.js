// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import * as types from '../mutation-types'
import { listResource } from '../helpers'

// initial state
const state = {
  tags: [],
  tag: {},
  tag_error: null
}

// getters
const getters = {
  tags: state => state.tags,
  tag: state => state.tag,
  tag_error: state => state.tag_error
}

// actions
const actions = {
  listTags ({ commit }, args) {
    commit(types.RECEIVE_TAGS, [])
    return listResource(
      'get',
      `${args.organization}/tags`,
      (args && args.filters) ? args.filters : {},
      response => commit(types.APPEND_TAGS, response.data.tags)
    ).then(() => {
      commit(types.RECEIVE_TAGS, state.tags)
      return state.tags
    })
  },
  getTag ({ commit }, args) {
    Vue.axios.get(`${args.tag}`, {}).then((response) => {
      commit(types.RECEIVE_TAG, { response })
    })
      .catch(error => {
        commit(types.RECEIVE_TAG_ERROR, { error })
      })
  },
  createTag ({ commit }, args) {
    Vue.axios.post(`/${args.organization}/tags`, JSON.stringify({
      tag: {
        display_name: args.display_name,
        note: args.note,
        color: args.color
      }
    })).then((response) => {
      commit(types.RECEIVE_CREATED_TAG, { response })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_TAGS] (state, tags) {
    if (tags) {
      state.tags = tags
    } else {
      state.tags = []
    }
  },
  [types.APPEND_TAGS] (state, tags) {
    if (tags) {
      state.tags.push(...tags)
    }
  },
  [types.RECEIVE_TAG] (state, { response }) {
    state.tag = response.data
  },
  [types.RECEIVE_TAG_ERROR] (state, { error }) {
    state.tag_error = error
  },
  [types.RECEIVE_CREATED_TAG] (state, { response }) {
    state.tag = response.data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
