import Vue from 'vue'
import * as types from '../mutation-types'
import { listResource } from '../helpers'

// initial state
const state = {
  channels: [],
  channel: {},
  channel_error: null
}

// getters
const getters = {
  channels: state => state.channels,
  channel: state => state.channel,
  channel_error: state => state.channel_error
}

// actions
const actions = {
  listChannels ({ commit }, args) {
    commit(types.RECEIVE_CHANNELS, [])
    return listResource(
      'get',
      `${args.organization}/channels`,
      (args && args.filters) ? args.filters : {},
      response => commit(types.APPEND_CHANNELS, response.data.channels)
    ).then(() => {
      commit(types.RECEIVE_CHANNELS, state.channels)
      return state.channels
    })
  },
  getChannel ({ commit }, args) {
    return Vue.axios.get(`${args.channel}`, {}).then(response => {
      commit(types.RECEIVE_CHANNEL, { response })
    })
      .catch(error => {
        commit(types.RECEIVE_CHANNEL_ERROR, { error })
      })
  },
  createChannel ({ commit }, args) {
    return Vue.axios.post(`/${args.organization}/channels`, JSON.stringify({
      channel: args.channel
    })).then(response => {
      commit(types.RECEIVE_CREATED_CHANNEL, { response })
    })
  },
  updateChannel ({ commit }, args) {
    return Vue.axios.put(`/${args.channel.name}`, JSON.stringify({
      channel: args.channel,
      update_mask: args.update_mask
    })).then(response => {
      commit(types.RECEIVE_CREATED_CHANNEL, { response })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_CHANNELS] (state, channels) {
    if (channels) {
      state.channels = channels
    } else {
      state.channels = []
    }
  },
  [types.APPEND_CHANNELS] (state, channels) {
    if (channels) {
      state.channels.push(...channels)
    }
  },
  [types.RECEIVE_CHANNEL] (state, { response }) {
    console.log(response.data)
    state.channel = response.data
  },
  [types.RECEIVE_CHANNEL_ERROR] (state, { error }) {
    state.channel_error = error
  },
  [types.RECEIVE_CREATED_CHANNEL] (state, { response }) {
    state.channel = response.data
  },
  [types.RECEIVE_UPDATED_CHANNEL] (state, { response }) {
    state.channel = response.data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
