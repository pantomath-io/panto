// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import * as types from '../mutation-types'
import { listResource } from '../helpers'

// initial state
const state = {
  agent: {},
  agents: []
}

// getters
const getters = {
  agents: state => state.agents,
  agent: state => state.agent
}

// actions
const actions = {
  listAgents ({ commit }, args) {
    commit(types.RECEIVE_AGENTS, [])
    return listResource(
      'get',
      `${args.organization_name}/agents`,
      (args && args.filters) ? args.filters : {},
      response => commit(types.APPEND_AGENTS, response.data.agents)
    ).then(() => {
      commit(types.RECEIVE_AGENTS, state.agents)
      return state.agents
    })
  },
  getAgent ({ commit }, args) {
    return Vue.axios.get(`/${args.agent_name}`).then((response) => {
      commit(types.RECEIVE_AGENT, { response })
      return response.data
    })
  },
  setAgent ({ commit }, agent) {
    commit(types.SET_AGENT, { agent })
    return agent
  },
  createAgent ({ commit }, args) {
    return Vue.axios.post(`/${args.organization_name}/agents`, JSON.stringify({
      agent: { 'display_name': args.display_name }
    })).then((response) => {
      commit(types.RECEIVE_CREATED_AGENT, { response })
      return response.data
    })
  },
  updateAgent ({ commit }, args) {
    return Vue.axios.put(`/${args.name}`, JSON.stringify({
      agent: {
        'display_name': args.display_name
      }
    })).then((response) => {
      commit(types.RECEIVE_AGENT, { response })
      return response.data
    })
  },
  deleteAgent ({ commit }, args) {
    return Vue.axios.delete(`/${args.agent}`, {}).then((response) => {
      commit(types.DELETE_AGENT)
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_AGENTS] (state, agents) {
    if (agents) {
      state.agents = agents
    } else {
      state.agents = []
    }
  },
  [types.APPEND_AGENTS] (state, agents) {
    if (agents) {
      state.agents.push(...agents)
    }
  },
  [types.RECEIVE_AGENT] (state, { response }) {
    state.agent = response.data
  },
  [types.RECEIVE_CREATED_AGENT] (state, { response }) {
    state.agent = response.data
  },
  [types.SET_AGENT] (state, { agent }) {
    state.agent = agent
  },
  [types.DELETE_AGENT] (state) {
    state.agent = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
