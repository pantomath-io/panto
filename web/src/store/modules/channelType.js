import * as types from '../mutation-types'
import { listResource } from '../helpers'

// initial state
const state = {
  channelTypes: []
}

// getters
const getters = {
  channelTypes: state => state.channelTypes
}

// actions
const actions = {
  listChannelTypes ({ commit }) {
    commit(types.RECEIVE_CHANNEL_TYPES, [])
    return listResource(
      'get',
      `/channeltypes`,
      {},
      response => commit(types.APPEND_CHANNEL_TYPES, response.data.channeltypes)
    ).then(() => {
      commit(types.RECEIVE_CHANNEL_TYPES, state.channelTypes)
      return state.channelTypes
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_CHANNEL_TYPES] (state, channelTypes) {
    if (channelTypes) {
      state.targets = channelTypes
    } else {
      state.targets = []
    }
  },
  [types.APPEND_CHANNEL_TYPES] (state, channelTypes) {
    if (channelTypes) {
      state.channelTypes.push(...channelTypes)
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
