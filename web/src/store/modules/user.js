// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import * as types from '../mutation-types'

// initial state
const state = {
  auth_token: '',
  status: ''
}

// getters
const getters = {
  authToken: state => state.auth_token,
  isAuthenticated: state => !!state.auth_token,
  authStatus: state => state.status
}

// actions
const actions = {
  loginUser ({ commit }, args) {
    return new Promise((resolve, reject) => {
      commit(types.AUTH_REQUEST)
      Vue.axios.post(`/login`, JSON.stringify({
        username: args.username,
        password: args.password
      }), {withCredentials: true}).then((response) => {
        let authToken = response.data
        Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + authToken.token
        Vue.axios.defaults.withCredentials = true
        commit(types.AUTH_SUCCESS, authToken.token)
        resolve()
      }).catch((error) => {
        delete Vue.axios.defaults.headers.common['Authorization']
        Vue.axios.defaults.withCredentials = false
        commit(types.AUTH_ERROR)
        reject(error)
      })
    })
  },
  logoutUser ({ commit }) {
    delete Vue.axios.defaults.headers.common['Authorization']
    Vue.axios.defaults.withCredentials = false
    commit(types.AUTH_RESET)
  },
  loadAuth ({ dispatch, commit, state, getters }) {
    return new Promise((resolve, reject) => {
      if (getters.isAuthenticated) {
        resolve()
      } else if (state.status === 'error') {
        dispatch('logoutUser')
        reject(state.status)
      } else {
        commit(types.AUTH_RELOAD)
        Vue.axios.get(`/token`, {withCredentials: true}
        ).then((response) => {
          let authToken = response.data
          Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + authToken.token
          Vue.axios.defaults.withCredentials = true
          commit(types.AUTH_SUCCESS, authToken.token)
          resolve()
        }).catch((error) => {
          dispatch('logoutUser')
          reject(error)
        })
      }
    })
  }
}

// mutations
const mutations = {
  [types.AUTH_REQUEST] (state) {
    state.status = 'loading'
  },
  [types.AUTH_SUCCESS] (state, token) {
    state.status = 'success'
    state.auth_token = token
  },
  [types.AUTH_RESET] (state) {
    state.status = ''
    state.auth_token = ''
  },
  [types.AUTH_ERROR] (state) {
    state.status = 'error'
  },
  [types.AUTH_RELOAD] (state) {
    state.status = 'reload'
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
