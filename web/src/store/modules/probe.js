// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import * as types from '../mutation-types'
import { listResource } from '../helpers'

// initial state
const state = {
  probes: [],
  probe: {}
}

// getters
const getters = {
  probes: state => state.probes,
  probe: state => state.probe
}

// actions
const actions = {
  listProbes ({ commit }, args) {
    commit(types.RECEIVE_PROBES, [])
    return listResource(
      'get',
      `/probes`,
      (args && args.filters) ? args.filters : {},
      response => commit(types.APPEND_PROBES, response.data.probes)
    ).then(() => {
      commit(types.RECEIVE_PROBES, state.probes)
      return state.probes
    })
  },
  setProbes ({ commit }, probes) {
    commit(types.SET_PROBES, { probes })
    return probes
  },
  getProbe ({ commit }, args) {
    return Vue.axios.get(`${args.probe}`, {}).then((response) => {
      commit(types.RECEIVE_PROBE, { response })
      return response.data
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_PROBES] (state, probes) {
    if (probes) {
      state.probes = probes
    } else {
      state.probes = []
    }
  },
  [types.APPEND_PROBES] (state, probes) {
    if (probes) {
      state.probes.push(...probes)
    }
  },
  [types.RECEIVE_PROBE] (state, { response }) {
    state.probe = response.data
  },
  [types.SET_PROBES] (state, { probes }) {
    state.probes = probes
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
