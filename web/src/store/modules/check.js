import Vue from 'vue'
import * as types from '../mutation-types'
import { listResource } from '../helpers'

// initial state
const state = {
  checks: [],
  check: {}
}

// getters
const getters = {
  checks: state => state.checks,
  check: state => state.check
}

// actions
const actions = {
  listChecks ({ state, commit }, args) {
    commit(types.RECEIVE_CHECKS, [])
    return listResource(
      'get',
      `${args.organization}/checks`,
      (args && args.filters) ? args.filters : {},
      response => commit(types.APPEND_CHECKS, response.data.checks)
    ).then(() => {
      commit(types.RECEIVE_CHECKS, state.checks)
      return state.checks
    })
  },
  getCheck ({ state, commit }, args) {
    return Vue.axios.get(`${args.check}`, {
      params: { children_mask: args.children_mask }
    }).then((response) => {
      commit(types.RECEIVE_CHECK, { response })
      return response.data
    })
  },
  createCheck ({ state, commit }, args) {
    return Vue.axios.post(`/${args.organization}/checks`, JSON.stringify({
      check: {
        target: args.target,
        probe: args.probe,
        type: args.type,
        configuration: JSON.stringify(args.configuration)
      }
    })).then((response) => {
      commit(types.RECEIVE_CREATED_CHECK, { response })
      return response.data
    })
  },
  updateCheck ({ state, commit }, args) {
    return Vue.axios.put(`/${args.name}`, JSON.stringify({
      check: {
        target: args.target_name,
        type: args.type,
        configuration: JSON.stringify(args.configuration)
      },
      update_mask: {
        paths: ['target', 'type', 'configuration']
      }
    })).then((response) => {
      commit(types.RECEIVE_CHECK, { response })
      return response.data
    })
  },
  setCheck ({ state, commit }, check) {
    commit(types.SET_CHECK, { check })
    return check
  }
}

// mutations
const mutations = {
  [types.RECEIVE_CHECKS] (state, checks) {
    if (checks) {
      checks.forEach((check) => {
        if (check.configuration) {
          check.configuration = JSON.parse(check.configuration)
        }
      })
      state.checks = checks
    } else {
      state.checks = []
    }
  },
  [types.APPEND_CHECKS] (state, checks) {
    if (checks) {
      state.checks.push(...checks)
    }
  },
  [types.RECEIVE_CHECK] (state, { response }) {
    if (response.data.configuration) {
      response.data.configuration = JSON.parse(response.data.configuration)
    }
    state.check = response.data
  },
  [types.RECEIVE_CREATED_CHECK] (state, { response }) {
    if (response.data.configuration) {
      response.data.configuration = JSON.parse(response.data.configuration)
    }
    state.check = response.data
  },
  [types.SET_CHECK] (state, { check }) {
    state.check = check
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
