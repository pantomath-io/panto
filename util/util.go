// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package util

import (
	"encoding/base64"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
	"testing"

	"github.com/golang/protobuf/proto"
	"github.com/google/uuid"
	"github.com/mitchellh/go-homedir"
	logging "github.com/op/go-logging"
	"google.golang.org/grpc/metadata"
)

var log = logging.MustGetLogger("main")

// LiveTest tests if the environment variable for live testing is set. "Live" testing means that the unit tests will
// rely on an existing instance of the TSDB at localhost:8086
func LiveTest(t *testing.T) bool {
	if t != nil {
		t.Helper()
	}
	liveEnv := strings.ToLower(os.Getenv("LIVE_TEST"))
	return liveEnv == "y" || liveEnv == "yes"
}

// Base64Encode encodes a UUID to a url-safe base64 string with no padding.
func Base64Encode(u uuid.UUID) string {
	return base64.RawURLEncoding.EncodeToString(u[:])
}

// Base64Decode decodes a url-safe base64 string with no padding to a UUID.
func Base64Decode(s string) (u uuid.UUID, err error) {
	b, err := base64.RawURLEncoding.DecodeString(s)
	if err != nil {
		return
	}
	u, err = uuid.FromBytes(b)
	return
}

// GetJoinedMetadata extracts a value from gRPC metadata, for a (case-insensitive) key, if it exists, and joins all the values in
// one string. Returns ("", false) if the key does not exist.
func GetJoinedMetadata(md metadata.MD, k string) (string, bool) {
	k = strings.ToLower(k)
	vals, ok := md[k]
	if !ok {
		return "", false
	}
	return strings.Join(vals, ""), true
}

// NextPageToken creates a `next_page_token` for a paginated response from a request, and adds additional data.
// `req` must be a protobuf message containing a `PageToken` field. Returns a URL-safe base64 encoded string.
func NextPageToken(req proto.Message, a ...interface{}) (string, error) {
	log.Debugf("Generating next page token for %s", req)

	req = stripToken(req)

	buf := proto.NewBuffer([]byte{})

	// Write the protobuf message
	err := buf.EncodeMessage(req)
	if err != nil {
		return "", fmt.Errorf("failed to encode request to buffer")
	}

	// Write the extra data
	for _, d := range a {
		switch v := d.(type) {
		case int:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case int8:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case int16:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case int32:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case int64:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint8:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint16:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint32:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint64:
			if err = buf.EncodeVarint(v); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case bool:
			if v {
				if err = buf.EncodeVarint(1); err != nil {
					return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
				}
			} else {
				if err = buf.EncodeVarint(0); err != nil {
					return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
				}
			}
		case float32:
			if err = buf.EncodeFixed32(uint64(math.Float32bits(v))); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case float64:
			if err = buf.EncodeFixed64(math.Float64bits(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case string:
			if err = buf.EncodeStringBytes(v); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case []byte:
			if err = buf.EncodeRawBytes(v); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case proto.Message:
			if err = buf.EncodeMessage(v); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		default:
			return "", fmt.Errorf("can't encode type %T into token", v)
		}
	}

	return base64.RawURLEncoding.EncodeToString(buf.Bytes()), nil
}

// ValidatePageToken validates a page token against a request. It decodes the given token, verifies it matches the
// request, and stores the extra data into successive arguments. Arguments must be pointers. Behavior is undefined
// if the arguments do not match the encoded buffer. Returns true if the token matches the request.
func ValidatePageToken(token string, req proto.Message, a ...interface{}) (bool, error) {
	dec, err := base64.RawURLEncoding.DecodeString(token)
	if err != nil {
		return false, err
	}

	req = stripToken(req)

	buf := proto.NewBuffer(dec)

	// Read input and compare
	in := proto.Clone(req)
	in.Reset()
	if err = buf.DecodeMessage(in); err != nil {
		return false, fmt.Errorf("failed to decode message (%s)", err)
	}
	if !proto.Equal(req, in) {
		return false, nil
	}

	// Read extra data
	for _, d := range a {
		switch ptr := d.(type) {
		case *int:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int(v)
		case *int8:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int8(v)
		case *int16:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int16(v)
		case *int32:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int32(v)
		case *int64:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int64(v)
		case *uint:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = uint(v)
		case *uint8:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = uint8(v)
		case *uint16:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = uint16(v)
		case *uint32:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = uint32(v)
		case *uint64:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = v
		case *bool:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = (v == 1)
		case *float32:
			v, err := buf.DecodeFixed32()
			if err != nil {
				return false, err
			}
			*ptr = math.Float32frombits(uint32(v))
		case *float64:
			v, err := buf.DecodeFixed64()
			if err != nil {
				return false, err
			}
			*ptr = math.Float64frombits(v)
		case *string:
			v, err := buf.DecodeStringBytes()
			if err != nil {
				return false, err
			}
			*ptr = v
		case []byte:
			v, err := buf.DecodeRawBytes(false)
			if err != nil {
				return false, err
			}
			copy(ptr, v)
		case *[]byte:
			v, err := buf.DecodeRawBytes(true)
			if err != nil {
				return false, err
			}
			*ptr = v
		case proto.Message:
			err := buf.DecodeMessage(ptr)
			if err != nil {
				return false, err
			}
		default:
			if reflect.ValueOf(req).Kind() != reflect.Ptr {
				return false, fmt.Errorf("can't store data into non-pointer type %T", d)
			}
			return false, fmt.Errorf("can't encode type %T into token", d)
		}
	}

	return true, err
}

func stripToken(req proto.Message) proto.Message {
	// Clone the request to avoid overwriting input argument
	req = proto.Clone(req)
	in := reflect.ValueOf(req).Elem()

	// Set PageToken field to empty string
	for i := 0; i < in.NumField(); i++ {
		f := in.Type().Field(i)
		if f.Name == "PageToken" && f.Type == reflect.TypeOf("") {
			in.Field(i).Set(reflect.ValueOf(""))
			return req
		}
	}

	return req
}

// Contains returns true if a string slice contains the given string, false otherwise
func Contains(l []string, s string) bool {
	for _, i := range l {
		if i == s {
			return true
		}
	}
	return false
}

// ExpandAbs expands ~ components in paths, if any, then calls filepath.Abs, returning
// an absolute path with no ~ components. If path is empty, just return an empty path.
func ExpandAbs(path string) (string, error) {
	if len(path) == 0 {
		return path, nil
	}
	var err error
	path, err = homedir.Expand(path)
	if err != nil {
		return "", err
	}
	path, err = filepath.Abs(path)
	if err != nil {
		return "", err
	}

	return path, nil
}

// compile the regexp for AddressHasPort function at init time
var portRegexp = regexp.MustCompile(`:\d+$`)

// AddressHasPort markes sure the provided address has the host:port format
func AddressHasPort(address string) bool {
	return portRegexp.MatchString(address)
}
