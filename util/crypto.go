// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package util

import (
	"crypto/rand"

	"golang.org/x/crypto/argon2"
)

// Salt is the type for hashed password salt
type Salt [32]byte

// Hash is the type for hashed password
type Hash [32]byte

const (
	argon2Iterations  = 1
	argon2Memory      = 64 * 1024
	argon2Parallelism = 4
	argon2KeyLength   = 32
	argon2SaltLength  = 32
)

// GenerateSalt creates a new salt to hash a password
func GenerateSalt() (Salt, error) {
	var s Salt

	_, err := rand.Read(s[:])

	return s, err
}

// HashPassword encodes a password using argon2
func HashPassword(password string, salt Salt) (hash Hash, err error) {
	hashSlice := argon2.IDKey([]byte(password), salt[:], argon2Iterations, argon2Memory, argon2Parallelism, argon2KeyLength)
	copy(hash[:], hashSlice)

	return
}
