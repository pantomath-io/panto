// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package util

import (
	"bytes"
	"encoding/hex"
	"testing"
)

func TestGenerateSalt(t *testing.T) {
	s, err := GenerateSalt()
	if err != nil {
		t.Errorf("unable to generate the salt %v", err)
	}

	emptySalt := Salt{}
	if s == emptySalt {
		t.Errorf("generated salt is empty")
	}
}

func TestHashPassword(t *testing.T) {
	var salt Salt

	testPwd := "helloworld"
	expected := struct {
		hash string
		salt string
	}{
		hash: "8074fbaf8af8a0200c7dcb0c89a2740c0cbff9d309d64cb5a75e0fe2cd9e0b4e",
		salt: "3d2438f53d68a77ceb66050b01aa9628c958845989f97a56003272fe06c2c29d",
	}

	hash, err := hex.DecodeString(expected.hash)
	if err != nil {
		t.Error("Unable to decode hash")
	}
	saltSlice, err := hex.DecodeString(expected.salt)
	if err != nil {
		t.Error("Unable to decode salt")
	}
	copy(salt[:], saltSlice)

	h, err := HashPassword(testPwd, salt)
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}
	hashSlice := make([]byte, len(h))
	copy(hashSlice, h[:])

	if !bytes.Equal(hashSlice, hash) {
		t.Errorf("hash not equals (%v != %v)", h, hash)
	}
}
