// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package util

import (
	"encoding/json"
	"testing"
)

func TestUnmarshalAndValidateParameters(t *testing.T) {
	{
		// valid test
		proposed := []byte(`{
			"foo": "bar"
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "string",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err != nil {
			t.Fatalf("unable to validate parameters: %s", err)
		}
	}

	{
		proposed := []byte(`{
			"foo": "bar"
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "string",
				Mandatory: true,
			},
			{
				Name:      "foofoo",
				Type:      "int",
				Mandatory: false,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err != nil {
			t.Fatalf("unable to validate string parameters: %s", err.Error())
		}
	}

	{
		proposed := []byte(`{
				"foo": 42
			}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "int",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err != nil {
			t.Fatalf("unable to validate int parameters: %s", err.Error())
		}
	}

	{
		proposed := []byte(`{
			"foo": 42
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "time.Duration",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err != nil {
			t.Fatalf("unable to validate duration parameters: %s", err.Error())
		}
	}
	{
		proposed := []byte(`{
			"foo": "42m"
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "time.Duration",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err != nil {
			t.Fatalf("unable to validate duration parameters: %s", err.Error())
		}
	}
	{
		proposed := []byte(`{
			"foo": "42p"
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "time.Duration",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err == nil {
			t.Fatalf("shoudln't be able to validate invalid duration parameters")
		}
	}

	{
		proposed := []byte(`{
			"foo": true
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "bool",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err != nil {
			t.Fatalf("unable to validate duration parameters: %s", err.Error())
		}

	}

	// type mismatch
	{
		proposed := []byte(`{
			"foo": "bar"
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "int",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err == nil {
			t.Fatalf("should report parameter type mismatch")
		}

	}

	// missing mandatory
	{
		proposed := []byte(`{
			"bar": "foo"
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "string",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err == nil {
			t.Fatalf("should report missing mandatory parameter")
		}
	}

	// unknown parameter
	{
		proposed := []byte(`{
			"foo": "bar"
		}`)

		ref := []Parameter{
			{
				Name:      "bar",
				Type:      "string",
				Mandatory: false,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err == nil {
			t.Fatalf("should report unknown parameter")
		}

	}

	// unknown type
	{
		proposed := []byte(`{
			"foo": "bar"
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "qhdsjflqehjfklfhjkl",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err == nil {
			t.Fatalf("should report unknown type")
		}

	}

	// null values should be filtered as undefined (#384)
	{
		proposed := []byte(`{
			"foo": null
		}`)

		ref := []Parameter{
			{
				Name: "foo",
				Type: "bool",
			},
		}

		conf, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err != nil {
			t.Fatalf("unexpected error: %s", err)
		}
		if _, ok := conf["foo"]; ok {
			t.Fatalf("null value for \"foo\" should be filtered out as undefined")
		}
	}

	{
		proposed := []byte(`{
			"foo": null
		}`)

		ref := []Parameter{
			{
				Name:      "foo",
				Type:      "bool",
				Mandatory: true,
			},
		}

		_, err := UnmarshalAndValidateParameters(proposed, ref)
		t.Log(err)
		if err == nil {
			t.Fatalf("should report null mandatory parameter")
		}
	}
}

func TestMarshal(t *testing.T) {
	// valid test
	{
		in := map[string]interface{}{
			"foo": true,
		}
		_, err := MarshalJSON(in)
		if err != nil {
			t.Fatalf("unexpected error: %s", err)
		}
	}

	// nil values should be filtered out
	{
		in := map[string]interface{}{
			"foo": nil,
		}
		out, err := MarshalJSON(in)
		if err != nil {
			t.Fatalf("unexpected error: %s", err)
		}
		var got map[string]interface{}
		err = json.Unmarshal(out, &got)
		if err != nil {
			t.Fatalf("unexpected error: %s", err)
		}
		if _, ok := got["foo"]; ok {
			t.Fatalf("nil value for \"foo\" should have been filtered out as undefined")
		}
	}
}

func TestUnmarshalParameter(t *testing.T) {
	const testCaseCount = 4
	cases := [testCaseCount][]byte{
		[]byte(`{"name":"foo","type":"int","description":"Foo parameter","mandatory":true,"placeholder":4}`),
		[]byte(`{"name":"foo","type":"float","description":"Foo parameter","mandatory":false,"default":4}`),
		[]byte(`{"name":"foo","type":"string","description":"Foo parameter","mandatory":false,"default":"hello"}`),
		[]byte(`{"name":"foo","type":"int","description":"Foo parameter","mandatory":false,"default":1.4}`),
	}
	expect := [testCaseCount]*Parameter{
		{"foo", "int", "Foo parameter", true, int64(4), nil},
		{"foo", "int", "Foo parameter", false, nil, float64(4)},
		{"foo", "string", "Foo parameter", false, nil, "hello"},
		nil,
	}
	for i := 0; i < testCaseCount; i++ {
		var p Parameter
		err := json.Unmarshal(cases[i], &p)
		if expect[i] == nil && err == nil {
			t.Fatalf("expected error, got %#v", p)
		} else if expect[i] != nil && err != nil {
			t.Fatalf("expected %#v, got error: %s", *expect[i], err)
		}
	}
}
