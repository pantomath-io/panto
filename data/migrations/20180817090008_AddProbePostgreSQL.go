// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090008, Down20180817090008)
}

// Up20180817090008 adds probe PostgreSQL
func Up20180817090008(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
INSERT INTO ` + "`" + `probe` + "`" + ` (uuid, label, display_name, description, configuration_template, metrics, graphs)
VALUES (
	'478eb9c0-d5b2-4bba-8e05-89ae7ba8d998',
	'postgresql',
	'PostgreSQL',
	'PostgreSQL probe returns the metrics from the statistics collector command of a PostgreSQL server.',
	'[{"name":"address","type":"string","description":"TCP address of the PostgreSQL server (host:port)","mandatory":true,"placeholder":"localhost:5432"},{"name":"username","type":"string","description":"Username for the server connection","mandatory":false,"default":"postgres"},{"name":"password","type":"string","description":"Password for the server connection","mandatory":false,"default":"password"},{"name":"sslmode","type":"string","description":"SSL Mode of the PostgreSQL server (see https://www.postgresql.org/docs/10/static/libpq-ssl.html)","mandatory":false,"default":"disable"}]',
	'{"metrics":[{"name":"current_connections","type":"uint","description":"Number of backends currently connected to this database"},{"name":"max_connections","type":"uint","description":"The maximum number of client connections allowed."},{"name":"shared_buffer_hits","type":"uint","description":"Number of times disk blocks were found already in the buffer cache, so that a read was not necessary"},{"name":"shared_buffer_reads","type":"uint","description":"Number of disk blocks read"},{"name":"temp_files_count","type":"uint","description":"Number of temporary files created by queries"},{"name":"temp_file_bytes","type":"uint","description":"Total amount of data written to temporary files by queries"},{"name":"rows_returned","type":"uint","description":"Number of rows returned by queries"},{"name":"rows_fetched","type":"uint","description":"Number of rows fetched by queries"},{"name":"rows_inserted","type":"uint","description":"Number of rows inserted by queries"},{"name":"rows_updated","type":"uint","description":"Number of rows updated by queries"},{"name":"rows_deleted","type":"uint","description":"Number of rows deleted by queries"},{"name":"deadlocks","type":"uint","description":"Number of deadlocks detected"},{"name":"index_size","type":"uint","description":"Total size of index on disk"},{"name":"table_size","type":"uint","description":"Total size of table on disk"},{"name":"toast_size","type":"uint","description":"Total size of toast on disk"},{"name":"n_dead_tup","type":"uint","description":"Estimated number of dead rows"},{"name":"n_live_tup","type":"uint","description":"Estimated number of live rows"},{"name":"checkpoints_requested","type":"uint","description":"Number of requested checkpoints that have been performed"},{"name":"checkpoints_scheduled","type":"uint","description":"Number of scheduled checkpoints that have been performed"},{"name":"buffers_backend","type":"uint","description":"Number of buffers written directly by a backend"},{"name":"buffers_background","type":"uint","description":"Number of buffers written by the background writer"},{"name":"buffers_checkpoint","type":"uint","description":"Number of buffers written during checkpoints"}]}',
	'{"graphs":[{"title":"Connections","type":"lines","metrics":["current_connections","max_connections"],"tags":[]},{"title":"Buffer cache","type":"lines","metrics":["shared_buffer_hits","shared_buffer_reads"],"tags":[]},{"title":"Temporary files","type":"lines","metrics":["temp_file_bytes"],"tags":[]},{"title":"Rows","type":"lines","metrics":["rows_returned","rows_fetched","rows_inserted","rows_updated","rows_deleted"],"tags":[]},{"title":"Sizes","type":"lines","metrics":["index_size","table_size","toast_size"],"tags":[]},{"title":"Checkpoints","type":"lines","metrics":["checkpoints_requested","checkpoints_scheduled"],"tags":[]},{"title":"Writes","type":"stacked-lines","metrics":["buffers_backend","buffers_background","buffers_checkpoint"],"tags":[]}]}');
`)
	if err != nil {
		return err
	}
	return nil
}

// Down20180817090008 removes probe PostgreSQL
func Down20180817090008(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
DELETE FROM ` + "`" + `probe` + "`" + ` WHERE uuid='478eb9c0-d5b2-4bba-8e05-89ae7ba8d998';
`)
	if err != nil {
		return err
	}
	return nil
}
