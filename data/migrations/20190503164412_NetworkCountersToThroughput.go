// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20190503164412, Down20190503164412)
}

// Up20190503164412 changes the network probe description to reflect the fact that absolute counters become throughput
// metrics
func Up20190503164412(qe goose.QueryExecer) error {
	_, err := qe.Exec(`UPDATE probe SET ` +
		`configuration_template='[{"description":"A comma-separated list of interfaces to collect information about. If the list is empty, return info about all interfaces. If the parameter is missing, return global information for all interfaces.","mandatory":false,"name":"interfaces","type":"string","default":""},{"description":"interval over which network throughput is calculated","mandatory":false,"name":"interval","type":"time.Duration","default":1000000000}]',` +
		`metrics='{"metrics":[{"name":"bytes-sent","type":"uint64","description":"Number of bytes sent through the interface over the interval"},{"name":"bytes-received","type":"uint64","description":"Number of bytes received through the interface over the interval"},{"name":"packets-sent","type":"uint64","description":"Number of network packets sent through the interface over the interval"},{"name":"packets-received","type":"uint64","description":"Number of network packets receives through the interface over the interval"},{"name":"error-in","type":"uint64","description":"Number of errors while receiving through the interface over the interval"},{"name":"error-out","type":"uint64","description":"Number of errors while sending through the interface over the interval"},{"name":"tcp-connections","type":"uint64","description":"Number of open TCP connections on this interface"},{"name":"udp-connections","type":"uint64","description":"Number of open UDP connections on this interface"}],"tags":[{"name":"interface","description":"The network interface, e.g. ` + "`" + `\"eth0\"` + "`" + `"}]}',` +
		`graphs='{"graphs":[{"title":"Network Throughput","type":"lines","format":"bytespersec","metrics":[{"name":"bytes-sent","color":"#c7f209"},{"name":"bytes-received","color":"#ef3d10"}],"tags":["interface"]},{"title":"Network Throughput (in packets)","type":"lines","format":"bignumber","metrics":[{"name":"packets-sent","color":"#c7f209"},{"name":"packets-received","color":"#ef3d10"}],"tags":["interface"]},{"title":"Network errors","type":"lines","format":"bignumber","metrics":[{"name":"error-in","color":"#c7f209"},{"name":"error-out","color":"#ef3d10"}],"tags":["interface"]},{"title":"Network connections","type":"stacked-lines","format":"bignumber","metrics":[{"name":"tcp-connections","color":"#22b2f4"},{"name":"udp-connections","color":"#ffd314"}],"tags":["interface"]}]}'` +
		`WHERE uuid='667f9df2-0540-4801-9a71-da43e656020c'`,
	)
	return err
}

// Down20190503164412 restores the previous state of the network probe description
func Down20190503164412(qe goose.QueryExecer) error {
	_, err := qe.Exec(`UPDATE probe SET ` +
		`configuration_template='[{"description":"A comma-separated list of interfaces to collect information about. If the list is empty, return info about all interfaces. If the parameter is missing, return global information for all interfaces.","mandatory":false,"name":"interfaces","type":"string","default":""}]',` +
		`metrics='{"metrics":[{"name":"bytes-sent","type":"uint64","description":"Number of bytes sent through the interface"},{"name":"bytes-received","type":"uint64","description":"Number of bytes received through the interface"},{"name":"packets-sent","type":"uint64","description":"Number of network packets sent through the interface"},{"name":"packets-received","type":"uint64","description":"Number of network packets receives through the interface"},{"name":"error-in","type":"uint64","description":"Number of errors while receiving through the interface"},{"name":"error-out","type":"uint64","description":"Number of errors while sending through the interface"},{"name":"tcp-connections","type":"uint64","description":"Number of open TCP connections on this interface"},{"name":"udp-connections","type":"uint64","description":"Number of open UDP connections on this interface"}],"tags":[{"name":"interface","description":"The network interface, e.g. ` + "`" + `\"eth0\"` + "`" + `"}]}',` +
		`graphs='{"graphs":[{"title":"Network Throughput","type":"lines","format":"byte","metrics":[{"name":"bytes-sent","color":"#c7f209"},{"name":"bytes-received","color":"#ef3d10"}],"tags":["interface"]},{"title":"Network Throughput (in packets)","type":"lines","format":"bignumber","metrics":[{"name":"packets-sent","color":"#c7f209"},{"name":"packets-received","color":"#ef3d10"}],"tags":["interface"]},{"title":"Network errors","type":"lines","format":"bignumber","metrics":[{"name":"error-in","color":"#c7f209"},{"name":"error-out","color":"#ef3d10"}],"tags":["interface"]},{"title":"Network connections","type":"stacked-lines","format":"bignumber","metrics":[{"name":"tcp-connections","color":"#22b2f4"},{"name":"udp-connections","color":"#ffd314"}],"tags":["interface"]}]}'` +
		`WHERE uuid='667f9df2-0540-4801-9a71-da43e656020c'`,
	)

	return err
}
