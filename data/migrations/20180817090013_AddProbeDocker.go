// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090013, Down20180817090013)
}

// Up20180817090013 adds probe PostgreSQL
func Up20180817090013(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
INSERT INTO ` + "`" + `probe` + "`" + ` (uuid, label, display_name, description, configuration_template, metrics, graphs)
VALUES (
	'c845fcee-e6ff-46ac-983e-49f440b50c2e',
	'docker',
	'Docker',
	'Docker probe returns the metrics from a Docker Engine.',
	'[{"name":"address","type":"string","description":"Docker engine address, which can be TCP (e.g. tcp://host:port) or socket based (e.g. unix:///absollute/path/to/socket)","mandatory":true,"placeholder":"unix:///var/run/docker.sock"}]',
	'{"metrics": [{"name": "container-count-created","type": "int64","description": "the number of container with state *created* in the Docker daemon"},{"name": "container-count-running","type": "int64","description": "the number of container with state *running* in the Docker daemon"},{"name": "container-count-paused","type": "int64","description": "the number of container with state *paused* in the Docker daemon"},{"name": "container-count-restarting","type": "int64","description": "the number of container with state *restarting* in the Docker daemon"},{"name": "container-count-removing","type": "int64","description": "the number of container with state *removing* in the Docker daemon"},{"name": "container-count-exited","type": "int64","description": "the number of container with state *exited* in the Docker daemon"},{"name": "container-count-dead","type": "int64","description": "the number of container with state *dead* in the Docker daemon"},{"name": "cpu-totalusage","type": "int64","description": "Total CPU time consumed (container name is a tag)"},{"name": "mem-usage","type": "float64","description": "current res_counter usage for memory (container name is a tag)"},{"name": "network-bytes-received","type": "int64","description": "Bytes received (container name is a tag)"},{"name": "network-errors-received","type": "int64","description": "Received errors (container name is a tag)"},{"name": "network-dropped-received","type": "int64","description": "Incoming packets dropped (container name is a tag)"},{"name": "network-bytes-sent","type": "int64","description": "Bytes sent (container name is a tag)"},{"name": "network-errors-sent","type": "int64","description": "Sent errors (container name is a tag)"},{"name": "network-dropped-sent","type": "int64","description": "Outgoing packets dropped (container name is a tag)"},{"name": "network-count","type": "int64","description": "the number of networks in the Docker daemon"},{"name": "volume-count","type": "int64","description": "the number of volumes in the Docker daemon"},{"name": "volume-size","type": "int64","description": "the number of bytes used by all the volumes in the Docker daemon"},{"name": "image-count","type": "int64","description": "the number of images in the Docker daemon"},{"name": "image-size","type": "int64","description": "the number of bytes used by all the images in the Docker daemon"}],"tags": [{"name": "name","description": "the name of the container"},{"name": "interface","description": "the name of the container interface"}]}',
	'{"graphs": [{"title": "CPU usage per container","type": "stacked-lines","format": "bignumber","metrics": [{"name": "cpu-totalusage","color": "#22b2f4"}],"tags": ["name"]},{"title": "Memory usage per container","type": "stacked-lines","format": "byte","metrics": [{"name": "mem-usage","color": "#22b2f4"}],"tags": ["name"]},{"title": "Network throughput per container","type": "lines","format": "byte","metrics": [{"name": "network-bytes-received","color": "#c7f209"},{"name": "network-bytes-sent","color": "#ef3d10"}],"tags": ["name","interface"]},{"title": "Network errors per container","type": "lines","format": "bignumber","metrics": [{"name": "network-errors-received","color": "#c7f209"},{"name": "network-errors-sent","color": "#ef3d10"}],"tags": ["name","interface"]},{"title": "Containers by state","type": "lines","format": "default","metrics": [{"name": "container-count-created","color": "#22b2f4"},{"name": "container-count-running","color": "#c7f209"},{"name": "container-count-paused","color": "#ffd314"},{"name": "container-count-restarting","color": "#18ffff"},{"name": "container-count-removing","color": "#f57c00"},{"name": "container-count-exited","color": "#fb8b24"},{"name": "container-count-dead","color": "#ef3d10"}],"tags": []},{"title": "Images","type": "lines","format": "default","metrics": [{"name": "image-count","color": "#22b2f4"}],"tags": []},{"title": "Images size","type": "lines","format": "byte","metrics": [{"name": "image-size","color": "#22b2f4"}],"tags": []},{"title": "Volumes","type": "lines","format": "default","metrics": [{"name": "volume-count","color": "#22b2f4"}],"tags": []},{"title": "Volumes size","type": "lines","format": "byte","metrics": [{"name": "volume-size","color": "#22b2f4"}],"tags": []},{"title": "Networks","type": "lines","format": "default","metrics": [{"name": "network-count","color": "#22b2f4"}],"tags": []}]}');
`)
	if err != nil {
		return err
	}
	return nil
}

// Down20180817090013 removes probe PostgreSQL
func Down20180817090013(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
DELETE FROM ` + "`" + `probe` + "`" + ` WHERE uuid='c845fcee-e6ff-46ac-983e-49f440b50c2e';
`)
	if err != nil {
		return err
	}
	return nil
}
