// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20190314152937, Down20190314152937)
}

// Up20190314152937 adds Disk I/O probe and renames Disk usage probe
func Up20190314152937(qe goose.QueryExecer) error {
	_, err := qe.Exec(`INSERT INTO probe (uuid,label,display_name,description,configuration_template,metrics,graphs)` +
		`VALUES ('bc0e8fb6-2095-4a5d-8bde-db362f7c8866','disk-io','Disk I/O',` +
		`'Disk I/O probes disk throughput statistics',
		'[{"description":"interval over which Disk I/O is calculated","mandatory":false,"name":"interval","type":"time.Duration","default":1000000000}]',
		'{"metrics":[{"name":"iops","type":"float32","description":"Average I/O operations per second"},{"name":"bytes-read","type":"float32","description":"Average bytes read per second"},{"name":"bytes-written","type":"float32","description":"Average bytes written per second"},{"name":"latency","type":"int64","description":"Average time for I/O requests issued to the device to be served, calculated as the time spent doing I/O divided by the # of Iops serverd during the interval"}],"tags":[{"name":"device","description":"The name of the device, e.g. ` + "`sda`" + `"}]}',
		'{"graphs":[{"title":"Iops","type":"line","format":"bignumber","metrics":[{"name":"iops","color":"#c7f209"}],"tags":["device"]},{"title":"Read/Write","type":"line","format":"bytespersec","metrics":[{"name":"bytes-read","color":"#c7f209"},{"name":"bytes-written","color":"#ef3d10"}],"tags":["device"]},{"title":"Latency","type":"line","format":"duration","metrics":[{"name":"latency","color":"#c7f209"}],"tags":["device"]}]}')`)
	if err != nil {
		return err
	}
	_, err = qe.Exec(
		`UPDATE probe ` +
			`SET label='disk-usage',display_name='Disk Usage',description='Disk Usage probes disk usage statistics' ` +
			`WHERE uuid='f83680d1-86b6-410e-933d-7db11baecce6'`)
	if err != nil {
		return err
	}
	return nil
}

// Down20190314152937 deletes Disk I/O probe and renames Disk usage probe
func Down20190314152937(qe goose.QueryExecer) error {
	_, err := qe.Exec(`DELETE FROM probe WHERE uuid='bc0e8fb6-2095-4a5d-8bde-db362f7c8866'`)
	if err != nil {
		return err
	}
	_, err = qe.Exec(
		`UPDATE probe ` +
			`SET label='disk' ` +
			`WHERE uuid='f83680d1-86b6-410e-933d-7db11baecce6'`)
	if err != nil {
		return err
	}
	return nil
}
