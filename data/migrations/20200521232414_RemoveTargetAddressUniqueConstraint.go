// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20200521232414, Down20200521232414)
}

// Up20200521232414 removes the UNIQUE constraint from the target address
func Up20200521232414(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
CREATE TABLE target_tmp (
    id           INTEGER   NOT NULL
                           PRIMARY KEY AUTOINCREMENT
                           UNIQUE,
    uuid         CHAR (36) NOT NULL
                           UNIQUE,
    address      TEXT      NOT NULL,
    note         TEXT,
    organization INTEGER,
    ts_created   TIMESTAMP DEFAULT (strftime('%s', 'now') ) 
                           NOT NULL,
    ts_updated   TIMESTAMP,
    ts_deleted   TIMESTAMP,
    FOREIGN KEY (
        organization
    )
    REFERENCES organization (id) 
);
INSERT INTO target_tmp SELECT * FROM target;
DROP TABLE target;
ALTER TABLE target_tmp RENAME TO target;
CREATE INDEX target_id_idx ON target(id);
CREATE INDEX target_uuid_idx ON target(uuid);
`)
	if err != nil {
		return err
	}
	return nil
}

// Down20200521232414 restores the UNIQUE constraint from the target address
func Down20200521232414(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
CREATE TABLE target_tmp (
    id           INTEGER   NOT NULL
                           PRIMARY KEY AUTOINCREMENT
                           UNIQUE,
    uuid         CHAR (36) NOT NULL
                           UNIQUE,
	address      TEXT      NOT NULL
                           UNIQUE,
    note         TEXT,
    organization INTEGER,
    ts_created   TIMESTAMP DEFAULT (strftime('%s', 'now') ) 
                           NOT NULL,
    ts_updated   TIMESTAMP,
    ts_deleted   TIMESTAMP,
    FOREIGN KEY (
        organization
    )
    REFERENCES organization (id) 
);
INSERT INTO target_tmp SELECT * FROM target;
DROP TABLE target;
ALTER TABLE target_tmp RENAME TO target;
CREATE INDEX target_id_idx ON target(id);
CREATE INDEX target_uuid_idx ON target(uuid);
CREATE INDEX target_address_idx ON target(address);
`)
	if err != nil {
		return err
	}
	return nil
}
