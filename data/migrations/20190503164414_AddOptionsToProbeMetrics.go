// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20190503164414, Down20190503164414)
}

// Up20190503164414 adds the option Accumulator for the Network probe
func Up20190503164414(qe goose.QueryExecer) error {
	_, err := qe.Exec(`UPDATE probe SET ` +
		`metrics='{"metrics": [{"name": "bytes-sent","type": "float64","description": "Rate of bytes sent per second through the interface","options": {"accumulator": true}},{"name": "bytes-received","type": "float64","description": "Rate of bytes received per second through the interface","options": {"accumulator": true}},{"name": "packets-sent","type": "float64","description": "Rate of network packets sent per seconds through the interface","options": {"accumulator": true}},{"name": "packets-received","type": "float64","description": "Rate of network packets received per second through the interface","options": {"accumulator": true}},{"name": "error-in","type": "float64","description": "Rate of errors per second while receiving through the interface","options": {"accumulator": true}},{"name": "error-out","type": "float64","description": "Rate of errors per second while sending through the interface","options": {"accumulator": true}},{"name": "tcp-connections","type": "uint64","description": "Number of open TCP connections on this interface"},{"name": "udp-connections","type": "uint64","description": "Number of open UDP connections on this interface"}],"tags": [{"name": "interface","description": "The network interface, e.g. \"eth0\""}]}'` +
		`WHERE uuid='667f9df2-0540-4801-9a71-da43e656020c'`)
	if err != nil {
		return err
	}
	return nil
}

// Down20190503164414 removes the option Accumulator from the Network probe
func Down20190503164414(qe goose.QueryExecer) error {
	_, err := qe.Exec(`UPDATE probe SET ` +
		`metrics='{"metrics":[{"name":"bytes-sent","type":"uint64","description":"Number of bytes sent through the interface"},{"name":"bytes-received","type":"uint64","description":"Number of bytes received through the interface"},{"name":"packets-sent","type":"uint64","description":"Number of network packets sent through the interface"},{"name":"packets-received","type":"uint64","description":"Number of network packets receives through the interface"},{"name":"error-in","type":"uint64","description":"Number of errors while receiving through the interface"},{"name":"error-out","type":"uint64","description":"Number of errors while sending through the interface"},{"name":"tcp-connections","type":"uint64","description":"Number of open TCP connections on this interface"},{"name":"udp-connections","type":"uint64","description":"Number of open UDP connections on this interface"}],"tags":[{"name":"interface","description":"The network interface, e.g. \"eth0\""}]}'` +
		`WHERE uuid='667f9df2-0540-4801-9a71-da43e656020c'`)
	if err != nil {
		return err
	}
	return nil
}
