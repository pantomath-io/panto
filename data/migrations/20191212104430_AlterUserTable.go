// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20191212104430, Down20191212104430)
}

// Up20191212104430 updates the user table to remove explicit "name" and "type" columns, and adds them to an unstructured
// JSON column "details".
func Up20191212104430(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
		CREATE TABLE tmp_user (
			id INTEGER,
			uuid CHAR(36),
			email VARCHAR(100),
			passwd_hash VARCHAR(64),
			passwd_salt VARCHAR(64),
			name VARCHAR(128),
			TYPE VARCHAR(32),
			organization INTEGER,
			client INTEGER,
			ts_created TIMESTAMP,
			ts_updated TIMESTAMP,
			ts_deleted TIMESTAMP
		);
		INSERT INTO tmp_user 
			SELECT id,uuid,email,passwd_hash,passwd_salt,name,type,organization,client,ts_created,ts_updated,ts_deleted
			FROM user;
		DROP TABLE user;
		CREATE TABLE user (
			id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
			uuid CHAR(36) NOT NULL UNIQUE,
			email VARCHAR(100) NOT NULL UNIQUE,
			passwd_hash CHAR(64) NOT NULL,
			passwd_salt CHAR(64) NOT NULL,
			details TEXT NOT NULL DEFAULT '{}',
			organization INTEGER NOT NULL,
			client INTEGER,
			ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
			ts_updated TIMESTAMP,
			ts_deleted TIMESTAMP,
			FOREIGN KEY (organization) REFERENCES organization (id) ,
			FOREIGN KEY (client) REFERENCES client (id)
		);
		CREATE INDEX user_id_idx ON user(id);
		CREATE INDEX user_uuid_idx ON user(uuid);
		CREATE INDEX user_email_idx ON user(email);
		INSERT INTO user(id,uuid,email,passwd_hash,passwd_salt,organization,client,ts_created,ts_updated,ts_deleted) 
			SELECT id,uuid,email,passwd_hash,passwd_salt,organization,client,ts_created,ts_updated,ts_deleted
			FROM tmp_user;
		UPDATE user
			SET details=json_object('fullname',(SELECT name FROM tmp_user WHERE id=user.id),'type',(SELECT type FROM tmp_user WHERE id=user.id));
		DROP TABLE tmp_user;
	`)
	if err != nil {
		return err
	}
	return nil
}

// Down20191212104430 restores the type and name column to the user table, from the JSON details
func Down20191212104430(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
		CREATE TABLE tmp_user (
			id INTEGER,
			uuid CHAR(36),
			email VARCHAR(100),
			passwd_hash CHAR(64),
			passwd_salt CHAR(64),
			details TEXT,
			organization INTEGER,
			client INTEGER,
			ts_created TIMESTAMP,
			ts_updated TIMESTAMP,
			ts_deleted TIMESTAMP
		);
		INSERT INTO tmp_user 
			SELECT id,uuid,email,passwd_hash,passwd_salt,details,organization,client,ts_created,ts_updated,ts_deleted
			FROM user;
		DROP TABLE user;
		CREATE TABLE user (
			id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
			uuid CHAR(36) NOT NULL UNIQUE,
			email VARCHAR(100) NOT NULL UNIQUE,
			passwd_hash VARCHAR(64) NOT NULL,
			passwd_salt VARCHAR(64) NOT NULL,
			name VARCHAR(128),
			type VARCHAR(32),
			organization INTEGER NOT NULL,
			client INTEGER,
			ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
			ts_updated TIMESTAMP,
			ts_deleted TIMESTAMP,
			FOREIGN KEY (organization) REFERENCES organization (id) ,
			FOREIGN KEY (client) REFERENCES client (id)
		);
		CREATE INDEX user_id_idx ON user(id);
		CREATE INDEX user_uuid_idx ON user(uuid);
		CREATE INDEX user_email_idx ON user(email);
		INSERT INTO user(id,uuid,email,passwd_hash,passwd_salt,organization,client,ts_created,ts_updated,ts_deleted)
			SELECT id,uuid,email,passwd_hash,passwd_salt,organization,client,ts_created,ts_updated,ts_deleted
			FROM tmp_user;
		UPDATE user
			SET name = json_extract((SELECT details FROM tmp_user WHERE id=user.id),'$.fullname'),
			type = json_extract((SELECT details FROM tmp_user WHERE id=user.id),'$.type');
		DROP TABLE tmp_user;
	`)
	if err != nil {
		return err
	}
	return nil
}
