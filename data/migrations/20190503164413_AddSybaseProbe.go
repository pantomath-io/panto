// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20190503164413, Down20190503164413)
}

// Up20190503164413 adds Sybase probe
func Up20190503164413(qe goose.QueryExecer) error {
	_, err := qe.Exec(`INSERT INTO probe (uuid,label,display_name,description,configuration_template,metrics,graphs)` +
		`VALUES ('5b5f9cbd-0d71-4dbb-9ca1-8cf44c3a8e72','sybase','Sybase ASE',` +
		`'Sybase probe returns metrics from a Sybase ASE',
		'[{"name": "address","description": "server address","mandatory": true,"type": "string", "default": "host:port"},{"name": "login","description": "user account","type": "string","mandatory": true},{"name": "password","description": "user password","type": "string","mandatory": true},{"name": "blocked-process","description": "Seconds before a process is considered blocked","type": "int","mandatory": false,"default": 30},{"name": "long-transaction","description": "Seconds before a transaction is considered long","type": "int","mandatory": false,"default": 30}]',
		'{"metrics": [{"name": "version", "description": "The version of the server", "type": "string"},{"name": "db-count", "description": "The number of databases on the server", "type": "int"},{"name": "proc-count", "description": "The number of processes on the server", "type": "int"},{"name": "blocked-proc-count", "description": "The number of processes blocked for more than blocked-process seconds", "type": "int"},{"name": "long-transaction", "description": "The number of transactions lasting more than long-transaction seconds", "type": "int"},{"name": "segmap", "description": "The number of segmap for this database (see tag", "type": "int"},{"name": "allocated", "description": "The number of allocated MB for this database (see tag", "type": "int"},{"name": "free", "description": "The number of free MB for this database (see tag", "type": "int"}],"tags": [{"name": "dbname","description": "The name of the database of the metric, e.g. ` + "`master`" + `"}]}',
		'{"graphs": [{"title": "Databases and processes","type": "line","format": "bignumber","metrics": [{"name": "db-count","color": "#c7f209"},{"name": "proc-count","color": "#ef3d10"}]},{"title": "Latencies","type": "line","format": "bignumber","metrics": [{"name": "blocked-proc-count","color": "#c7f209"},{"name": "long-transaction","color": "#ef3d10"}]},{"title": "Storage","type": "line","format": "byte","metrics": [{"name": "allocated","color": "#c7f209"},{"name": "free","color": "#ef3d10"}],"tags": ["dbname"]}]}')`)
	if err != nil {
		return err
	}
	return nil
}

// Down20190503164413 deletes Sybase probe
func Down20190503164413(qe goose.QueryExecer) error {
	_, err := qe.Exec(`DELETE FROM probe WHERE uuid='5b5f9cbd-0d71-4dbb-9ca1-8cf44c3a8e72'`)
	if err != nil {
		return err
	}
	return nil
}
