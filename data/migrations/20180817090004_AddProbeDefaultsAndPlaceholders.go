// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090004, Down20180817090004)
}

// Up20180817090004 Add Probe defaults and placeholders to probe configuration templates
func Up20180817090004(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
-- Ping
UPDATE probe
SET configuration_template='[{"description":"Target address. An IP or a hostname.","mandatory":true,"name":"address","type":"string","placeholder":"target.pantomath.io"},{"description":"The wait time between each packet send.","mandatory":false,"name":"interval","type":"time.Duration","default":100000000},{"description":"The number of ICMP packets to send.","mandatory":false,"name":"count","type":"int","default":4},{"description":"The time to run the ping, until it exits. If the timeout occurs before the packet count has been reached, the probe exits anyway.","mandatory":false,"name":"timeout","type":"time.Duration","default":10000000000}]'
WHERE uuid='c549129c-6fc1-4950-a7f8-05f59213cb62';
-- Checksum
UPDATE probe
SET configuration_template='[{"description":"the path of the file to check","mandatory":true,"name":"path","type":"string","placeholder":"/etc/passwd"}]'
WHERE uuid='40aaade2-f58a-46a8-9406-0424101ff62e';
-- CPU
UPDATE probe
SET configuration_template='[{"description":"` + "`" + `true` + "`" + ` collects utilization for each CPU/core, ` + "`" + `false` + "`" + ` collects utilization averaged over all CPUs/cores","mandatory":true,"name":"per-cpu","type":"bool","placeholder":true},{"description":"interval over which CPU utilization is calculated","mandatory":false,"name":"interval","type":"time.Duration","default":1000000000}]'
WHERE uuid='b05bb378-0a8c-4bab-b703-36f334a7c513';
-- HTTP
UPDATE probe
SET configuration_template='[{"description":"HTTP request method ("` + "`" + `GET"` + "`" + `, "` + "`" + `POST"` + "`" + `, etc.)","mandatory":false,"name":"method","type":"string","default":"GET"},{"description":"HTTP request URL","mandatory":true,"name":"url","type":"string","placeholder":"http://yourdomain.com"},{"description":"HTTP body of the request","mandatory":false,"name":"body","type":"string","default":""},{"description":"duration before the HTTP request times out","mandatory":false,"name":"timeout","type":"time.Duration","default":0}]'
WHERE uuid='0a95d0ce-3395-4cc2-b30a-f3cb0275d9c9';
-- Network
UPDATE probe
SET configuration_template='[{"description":"A comma-separated list of interfaces to collect information about. If the list is empty, return info about all interfaces. If the parameter is missing, return global information for all interfaces.","mandatory":false,"name":"interfaces","type":"string","default":""}]'
WHERE uuid='667f9df2-0540-4801-9a71-da43e656020c';
-- Process
UPDATE probe
SET configuration_template='[{"description":"A comma-separated list of the names of the processes to collect info about. A process name is usually the name of the executable that was launched.","mandatory":true,"name":"names","type":"string","placeholder":""}]'
WHERE uuid='d1148d83-d49c-4d9b-b05d-7496e01feb5a';
-- Redis
UPDATE probe
SET configuration_template='[{"description":"the address of the target redis server to gather metric from","mandatory":true,"name":"address","type":"string","placeholder":"localhost:6379"}]'
WHERE uuid='ad7fd7d8-a38d-4337-a16a-cee1f415faf3';
-- InfluxDB
UPDATE probe
SET configuration_template='[{"description":"the address of the target influxdb server to collect metrics about","mandatory":true,"name":"address","type":"string","placeholder":"http://localhost:8086"}]'
WHERE uuid='92709d8f-640e-4699-ba15-a760fb62fb5a';
-- TLS
UPDATE probe
SET configuration_template='[{"description":"address of the TLS host","mandatory":true,"name":"host","type":"string","placeholder":"yourdomain.com:443"}]'
WHERE uuid='01ef6bca-7f39-41e5-8e7f-126842f89ba9';
-- NTP
UPDATE probe
SET configuration_template='[{"description":"address of an NTP server","mandatory":true,"name":"address","type":"string","placeholder":"pool.ntp.org"}]'
WHERE uuid='ae94ddd1-8e99-466d-bcd8-a4fd43ee56ef';
-- Memcached
UPDATE probe
SET configuration_template='[{"description":"address of a memcached server","mandatory":true,"name":"address","type":"string","placeholder":"localhost:11211"}]'
WHERE uuid='50fdbc01-0a04-473d-845e-16ae7d9d6c0a';
-- MySQL
UPDATE probe
SET configuration_template='[{"description":"MySQL server address (host:port)","mandatory":true,"name":"address","type":"string","placeholder":"localhost:3306"},{"description":"MySQL user account","mandatory":true,"name":"login","type":"string","placeholder":"mysql"},{"description":"MySQL user password","mandatory":true,"name":"password","type":"string","placeholder":"••••••••"}]'
WHERE uuid='a29e3925-569d-492f-b2e5-1003a8d7ac68';
-- Elasticsearch
UPDATE probe
SET configuration_template='[{"description":"address of an Elasticsearch server","mandatory":true,"name":"address","type":"string","placeholder":"localhost:9200"},{"description":"duration before the HTTP request times out","mandatory":false,"name":"timeout","type":"time.Duration","default":0}]'
WHERE uuid='c6d84e99-b6d0-4e89-b847-83085ff0f318';
-- nginx
UPDATE probe
SET configuration_template='[{"name":"url","type":"string","description":"URL of the Nginx server status page","mandatory":true,"placeholder":"http://localhost:8080/basic_status"},{"name":"timeout","type":"time.Duration","description":"Duration before the status request times out","mandatory":false,"default":0}]'
WHERE uuid='d96648a9-49fd-4e08-b086-de096d4196a6';
-- PHP-FHM
UPDATE probe
SET configuration_template='[{"name":"address","type":"string","description":"TCP address of the PHP-FPM server","mandatory":true,"placeholder":"localhost:9000"},{"name":"url","type":"string","description":"Path of the builtin status page","mandatory":false,"default":"/status"}]'
WHERE uuid='dbc7b160-f44d-457f-9361-d6aae66f645f';
	`)
	if err != nil {
		return err
	}
	return nil
}

// Down20180817090004 Add Probe defaults and placeholders to probe configuration templates
func Down20180817090004(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
-- Ping
UPDATE probe
SET configuration_template='[{"description":"Target address. An IP or a hostname.","mandatory":true,"name":"address","type":"string"},{"description":"The wait time between each packet send.","mandatory":false,"name":"interval","type":"time.Duration"},{"description":"The number of ICMP packets to send.","mandatory":false,"name":"count","type":"int"},{"description":"The time to run the ping, until it exits. If the timeout occurs before the packet count has been reached, the probe exits anyway.","mandatory":false,"name":"timeout","type":"time.Duration"}]'
WHERE uuid='c549129c-6fc1-4950-a7f8-05f59213cb62';
-- Checksum
UPDATE probe
SET configuration_template='[{"description":"the path of the file to check","mandatory":true,"name":"path","type":"string"}]'
WHERE uuid='40aaade2-f58a-46a8-9406-0424101ff62e';
-- CPU
UPDATE probe
SET configuration_template='[{"description":"` + "`" + `true` + "`" + ` collects utilization for each CPU/core, ` + "`" + `false` + "`" + ` collects utilization averaged over all CPUs/cores","mandatory":true,"name":"per-cpu","type":"bool"},{"description":"interval over which CPU utilization is calculated","mandatory":false,"name":"interval","type":"time.Duration"}]'
WHERE uuid='b05bb378-0a8c-4bab-b703-36f334a7c513';
-- HTTP
UPDATE probe
SET configuration_template='[{"description":"HTTP request method (` + "`" + `GET` + "`" + `, ` + "`" + `POST` + "`" + `, etc.)","mandatory":false,"name":"method","type":"string"},{"description":"HTTP request URL","mandatory":true,"name":"url","type":"string"},{"description":"HTTP body of the request","mandatory":false,"name":"body","type":"string"},{"description":"duration before the HTTP request times out","mandatory":false,"name":"timeout","type":"time.Duration"}]'
WHERE uuid='0a95d0ce-3395-4cc2-b30a-f3cb0275d9c9';
-- Network
UPDATE probe
SET configuration_template='[{"description":"A comma-separated list of interfaces to collect information about. If the list is empty, return info about all interfaces. If the parameter is missing, return global information for all interfaces.","mandatory":false,"name":"interfaces","type":"string"}]'
WHERE uuid='667f9df2-0540-4801-9a71-da43e656020c';
-- Process
UPDATE probe
SET configuration_template='[{"description":"A comma-separated list of the names of the processes to collect info about. A process name is usually the name of the executable that was launched.","mandatory":true,"name":"names","type":"string"}]'
WHERE uuid='d1148d83-d49c-4d9b-b05d-7496e01feb5a';
-- Redis
UPDATE probe
SET configuration_template='[{"description":"the address of the target redis server to gather metric from","mandatory":true,"name":"address","type":"string"}]'
WHERE uuid='ad7fd7d8-a38d-4337-a16a-cee1f415faf3';
-- InfluxDB
UPDATE probe
SET configuration_template='[{"description":"the address of the target influxdb server to collect metrics about","mandatory":true,"name":"address","type":"string"}]'
WHERE uuid='92709d8f-640e-4699-ba15-a760fb62fb5a';
-- TLS
UPDATE probe
SET configuration_template='[{"description":"URL of the TLS host","mandatory":true,"name":"host","type":"string"}]'
WHERE uuid='01ef6bca-7f39-41e5-8e7f-126842f89ba9';
-- NTP
UPDATE probe
SET configuration_template='[{"description":"address of an NTP server","mandatory":true,"name":"address","type":"string"}]'
WHERE uuid='ae94ddd1-8e99-466d-bcd8-a4fd43ee56ef';
-- Memcached
UPDATE probe
SET configuration_template='[{"description":"address of a memcached server","mandatory":true,"name":"address","type":"string"}]'
WHERE uuid='50fdbc01-0a04-473d-845e-16ae7d9d6c0a';
-- MySQL
UPDATE probe
SET configuration_template='[{"description":"MySQL server address (host:port)","mandatory":true,"name":"address","type":"string"},{"description":"MySQL user account","mandatory":true,"name":"login","type":"string"},{"description":"MySQL user password","mandatory":true,"name":"password","type":"string"}]'
WHERE uuid='a29e3925-569d-492f-b2e5-1003a8d7ac68';
-- Elasticsearch
UPDATE probe
SET configuration_template='[{"description":"address of an Elasticsearch server","mandatory":true,"name":"address","type":"string"},{"description":"duration before the HTTP request times out","mandatory":false,"name":"timeout","type":"time.Duration"}]'
WHERE uuid='c6d84e99-b6d0-4e89-b847-83085ff0f318';
-- nginx
UPDATE probe
SET configuration_template='[{"name": "url", "type": "string", "description": "Full URL of the Nginx server status", "mandatory": true},
{"name": "timeout", "type": "time.Duration", "description": "Duration before the status request times out (default value: 0)", "mandatory": false}]'
WHERE uuid='d96648a9-49fd-4e08-b086-de096d4196a6';
-- PHP-FHM
UPDATE probe
SET configuration_template='[{"name": "address", "type": "string", "description": "TCP address of the PHP-FPM server", "mandatory": true},{"name": "url", "type": "string", "description": "Path of the builtin status page (default value: /status)", "mandatory": false}]'
WHERE uuid='dbc7b160-f44d-457f-9361-d6aae66f645f';
	`)
	if err != nil {
		return err
	}
	return nil
}
