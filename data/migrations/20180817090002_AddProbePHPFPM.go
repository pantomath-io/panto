// Copyright 2018 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20180817090002, Down20180817090002)
}

// Up20180817090002 Add probe PHP-FHM
func Up20180817090002(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
INSERT INTO ` + "`" + `probe` + "`" + ` (uuid, label, display_name, description, configuration_template, metrics, graphs)
VALUES (
	'dbc7b160-f44d-457f-9361-d6aae66f645f',
	'PHPFPM',
	'PHP-FPM',
	'PHP-FPM probe returns the values of the stub status module of a PHP-FPM server.',
	'[{"name": "address", "type": "string", "description": "TCP address of the PHP-FPM server", "mandatory": true},{"name": "url", "type": "string", "description": "Path of the builtin status page (default value: /status)", "mandatory": false}]',
	'{"metrics":[{"name":"listen_queue","type":"int","description":"the number of request in the queue of pending connections."},{"name":"idle_processes","type":"int","description":"the number of idle processes."},{"name":"active_processes","type":"int","description":"the number of active processes."},{"name":"slow_requests","type":"int","description":"Enable php-fpm slow-log before you consider this. If this value is non-zero you may have slow php processes."}]}',
	'{"graphs":[{"title":"Listen queue","type":"lines","metrics":["listen_queue"],"tags":[]},{"title":"Processes","type":"stacked-lines","metrics":["idle_processes","active_processes"],"tags":[]},{"title":"Slow requests","type":"lines","metrics":["slow_requests"],"tags":[]}]}');
`)
	if err != nil {
		return err
	}
	return nil
}

// Down20180817090002 Add probe PHP-FHM
func Down20180817090002(qe goose.QueryExecer) error {
	_, err := qe.Exec(`
DELETE FROM ` + "`" + `probe` + "`" + ` WHERE uuid='dbc7b160-f44d-457f-9361-d6aae66f645f';
`)
	if err != nil {
		return err
	}
	return nil
}
