// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20191212104431, Down20191212104431)
}

// Up20191212104431 adds the option Accumulator for the Network probe
func Up20191212104431(qe goose.QueryExecer) error {
	_, err := qe.Exec(`UPDATE probe SET ` +
		`metrics='{"metrics":[{"name":"busy","type":"float32","description":"% of utilization for a specific CPU/core"}],"tags":[{"name":"cpu","description":"number of the CPU usage result, \"all\" for average over all CPUs/cores"}]}'` +
		`WHERE uuid='b05bb378-0a8c-4bab-b703-36f334a7c513'`)
	if err != nil {
		return err
	}
	return nil
}

// Down20191212104431 removes the option Accumulator from the Network probe
func Down20191212104431(qe goose.QueryExecer) error {
	_, err := qe.Exec(`UPDATE probe SET ` +
		`metrics='{"metrics":[{"name":"usage","type":"float32","description":"% of utilization for a specific CPU/core"}],"tags":[{"name":"cpu","description":"number of the CPU usage result, \"all\" for average over all CPUs/cores"}]}'` +
		`WHERE uuid='b05bb378-0a8c-4bab-b703-36f334a7c513'`)
	if err != nil {
		return err
	}
	return nil
}
