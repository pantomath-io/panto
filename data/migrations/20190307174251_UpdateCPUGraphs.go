// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package migrations

import (
	"github.com/loderunner/goose"
)

func init() {
	goose.AddMigration(Up20190307174251, Down20190307174251)
}

// Up20190307174251 adds the IOWait usage metric to the default CPU probe graph
func Up20190307174251(qe goose.QueryExecer) error {
	// This code is executed when the migration is applied.
	_, err := qe.Exec(
		`UPDATE probe ` +
			`SET graphs='{"graphs":[{"title":"CPU usage","type":"lines","format":"percent","metrics":[{"name":"busy","color":"#22b2f4"},{"name":"iowait","color":"#c7f209"}],"tags":["cpu"]}]}' ` +
			`WHERE uuid='b05bb378-0a8c-4bab-b703-36f334a7c513'`)
	if err != nil {
		return err
	}
	return nil
}

// Down20190307174251 removes the IOWait usage metric from the default CPU probe graph
func Down20190307174251(qe goose.QueryExecer) error {
	// This code is executed when the migration is rolled back.
	_, err := qe.Exec(
		`UPDATE probe ` +
			`SET graphs='{"graphs":[{"title":"CPU usage","type":"lines","format":"percent","metrics":[{"name":"usage","color":"#22b2f4"}],"tags":["cpu"]}]}' ` +
			`WHERE uuid='b05bb378-0a8c-4bab-b703-36f334a7c513'`)
	if err != nil {
		return err
	}
	return nil
}
