-- Copyright 2017 Pantomath SAS
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--   http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

INSERT INTO `agent` (`uuid`,`display_name`,`ts_configuration`, `organization`) VALUES
    ('a0ab8e27-b11a-4ee4-a47b-cd73de56160d','agent0.pantomath.io',1524059337,1),
    ('66a52d20-ae35-49d6-8028-9cd7f34f4313','agent1.pantomath.io',1524059337,1),
    ('a73a429d-ab30-467b-a091-21552235cc96','agent2.pantomath.io',1524059337,1),
    ('4bae6c07-f4aa-4daf-a584-52fe1a2a6cfe','agent3.pantomath.io',1524059337,1),
    ('0b8e69c4-a508-4a2f-8d2b-685956c6411d','agent4.pantomath.io',1524059337,1),
    ('fcf0b5ef-1c4a-41fd-9bf1-3c513b2aea2f','agent5.pantomath.io',1524059337,1),
    ('56d6f7c6-e825-459b-880e-7df966c3b2fe','agent6.pantomath.io',1524059337,1),
    ('c22de42c-3cc2-49ff-9ba8-fa1d9ed89e53','agent7.pantomath.io',1524059337,1),
    ('31409edd-c749-473b-9a70-0e5e0c8c2db0','agent8.pantomath.io',1524059337,1),
    ('d0b7ea51-9c89-41fb-b9c1-428d5904ab64','agent9.pantomath.io',1524059337,1);
INSERT INTO `alert` (`uuid`,`type`,`configuration`, `channel`) VALUES
    ('06d9becb-0060-419f-b45f-8d557189314e','state_recurrence','{"recurrence":1}',1),
    ('9ff0f4b3-0e9d-4533-9a0e-82beeea32616','state_recurrence','{"recurrence":3}',1),
    ('88c2bd60-b514-431b-ba6c-18a3bbb77ccc','state_recurrence','{"recurrence":6}',1),
    ('a618aedd-59d6-42dc-89fa-f930af1559a5','state_recurrence','{"recurrence":7}',1),
    ('67625788-c80a-4ae4-8188-58c943447d31','state_recurrence','{"recurrence":1}',1),
    ('e0438546-d013-4ae1-bd49-24ac8a473985','state_recurrence','{"recurrence":5}',1),
    ('f18900ac-fbe5-4c01-9a86-ffa6e36589f5','state_recurrence','{"recurrence":5}',1),
    ('8e458374-284b-4bd0-b449-29c10363902a','state_recurrence','{"recurrence":2}',1),
    ('c00ee717-52cb-4a2c-bc03-6a8a3806e963','state_recurrence','{"recurrence":0}',1),
    ('671a8581-3ba0-41f4-b04c-4e478aa6a379','state_recurrence','{"recurrence":0}',1);
INSERT INTO `channel` (`uuid`,`display_name`, `type`,`configuration`,`organization`) VALUES
    ('023055aa-cf61-4c1d-a987-6116c83a0cb9','E-mail: dest0@pantomath.io', 'email','{"dest":"dest0@pantomath.io"}',1),
    ('fe7f2abc-cc96-4ced-999c-b09435f61030','E-mail: dest1@pantomath.io', 'email','{"dest":"dest1@pantomath.io"}',1),
    ('870d6452-89ed-4840-8778-12e6c549172f','E-mail: dest2@pantomath.io', 'email','{"dest":"dest2@pantomath.io"}',1),
    ('12bbbc4f-9880-4453-bf6e-bf1d51feee71','E-mail: dest3@pantomath.io', 'email','{"dest":"dest3@pantomath.io"}',1),
    ('664cb25a-3656-4294-8ddc-8fd21eb62fd0','E-mail: dest4@pantomath.io', 'email','{"dest":"dest4@pantomath.io"}',1),
    ('476929b2-f55c-4f56-a397-490d889c922d','E-mail: dest5@pantomath.io', 'email','{"dest":"dest5@pantomath.io"}',1),
    ('ce559704-4eb4-4ac5-8f0a-a9b2c9f226c7','E-mail: dest6@pantomath.io', 'email','{"dest":"dest6@pantomath.io"}',1),
    ('b6714fae-0bc2-4eed-94a7-e008f9a6be17','E-mail: dest7@pantomath.io', 'email','{"dest":"dest7@pantomath.io"}',1),
    ('c79a05b0-f62b-42b8-a7b1-a4fcfcbd730d','E-mail: dest8@pantomath.io', 'email','{"dest":"dest8@pantomath.io"}',1),
    ('598d4373-bcda-48c2-a3e3-b0e6b338d91f','E-mail: dest9@pantomath.io', 'email','{"dest":"dest9@pantomath.io"}',1);
INSERT INTO `organization` (`uuid`,`name`,`client`) VALUES
    ('2930820b-ad21-4605-83b4-69cbc5bfc8f5','organization0',1),
    ('cad5dfad-8776-4c15-a838-5b640090e849','organization1',1),
    ('cf65eaaf-318c-444d-a7fc-59c8c56e450b','organization2',1),
    ('67b1e1e8-c4fa-4c0a-956b-1d79174c2e78','organization3',1),
    ('94fdc720-61df-4d2a-99be-e315b8738c5a','organization4',1),
    ('e7d8b8a4-ac07-4fe3-b328-907ef57b3f37','organization5',1),
    ('0c65f55d-cba9-4779-8263-6145dc0e1e8b','organization6',1),
    ('6ce54fe1-a331-4e41-b87e-3a870587226e','organization7',1),
    ('7d6c560f-c2d2-44fc-b7e8-8f068b9bbca5','organization8',1),
    ('ac61eff9-13ed-4b8d-99d1-104d50d11d08','organization9',1);
INSERT INTO `tag` (`slug`,`display_name`,`note`,`color`,`organization`) VALUES
    ('tag-0','Tag 0','dkrxz qwncmwkvoof','#1af725',1),
    ('tag-1','Tag 1','paqhcsdvwa apafpkngqbi','#23a819',1),
    ('tag-2','Tag 2','dwxovvccttswf btppbwcvzpqviivcdr','#17a91c',1),
    ('tag-3','Tag 3','odpvkgdcrhf mhwfmmystsdkhngccr','#33be51',1),
    ('tag-4','Tag 4','jpjlr unggpiwjdcjlf','#d44d2c',1),
    ('tag-5','Tag 5','uaptjf izmyjwlldbk','#d94dcb',1),
    ('tag-6','Tag 6','mqxbsmrqptenyn zndxxznchnxsyoex','#125e2c',1),
    ('tag-7','Tag 7','vkyixuvsiihd fkvaavojtzoef','#e65ae3',1),
    ('tag-8','Tag 8','bdeeiqzcnouyyf uvgvheytyvtasoht','#7dbe7f',1),
    ('tag-9','Tag 9','ppqraripdmml srcbtefiffpcym','#1b5052',1);
INSERT INTO `target` (`uuid`,`address`,`note`,`organization`) VALUES
    ('c0249d5a-ec4c-454e-84fc-686dc88ad2a6','target0.pantomath.io','txpsjklml agcamxrjuewcusgzkl',1),
    ('7664b7b1-1cea-41b3-b2b9-5f61a8812ec6','target1.pantomath.io','tpxbvfbt eoetiinuqayzu',1),
    ('39f9b629-10f7-4557-83e3-e977ae35f5ea','target2.pantomath.io','njggrim tapojqittqfloaw',1),
    ('6389c72e-d116-4b36-acb6-3990990dfd5d','target3.pantomath.io','nntcrlqj shkoyswiftwurm',1),
    ('71723b55-4680-4a22-a0f9-8af82380baf5','target4.pantomath.io','ipogevta prdmytdciwzvkafpcd',1),
    ('94baff73-86b5-4e58-83bb-f67f97d7ec34','target5.pantomath.io','cjjktjfc jcxdpjmthhhcqla',1),
    ('d78b3df5-9571-4e57-bd4d-be337f953889','target6.pantomath.io','buiqk zundajnvriledmmbko',1),
    ('8b15bbf2-4023-477c-85b9-2253e03b9e76','target7.pantomath.io','nlowuq eixowjstuzadni',1),
    ('9267544e-52b6-4a1f-9783-6fa6d270bb87','target8.pantomath.io','ubvtn hbfcbpndiijx',1),
    ('64eecce5-6931-4ca9-82e0-8fb490f59d47','target9.pantomath.io','fzuefqhu khriutqcyztcuj',1);
INSERT INTO `target_probe` (`uuid`,`target`,`probe`,`state_type`,`state_configuration`) VALUES
    ('14a6c5b5-05a0-4ec2-ad36-a3c43fe5d473',1,1,'threshold','{}'),
    ('a77a3c9b-540e-4b07-83e8-95a6f281f19a',1,1,'threshold','{}'),
    ('1b41ee6a-42e9-460e-a618-5f34a9d9113c',1,1,'threshold','{}'),
    ('26a78653-2866-4c17-83ef-5ec22530622a',1,1,'threshold','{}'),
    ('df398bc0-1535-4576-abbf-abf06e418b18',1,1,'threshold','{}'),
    ('b06199b2-9577-496f-b92a-24d803cd008d',1,1,'threshold','{}'),
    ('5ed6d51b-affc-4d5c-9949-18e05faf033f',1,1,'threshold','{}'),
    ('ba54b6fb-8f94-4835-ad12-969742caf131',1,1,'threshold','{}'),
    ('e1ae2269-ea2e-4ca7-8f74-2b7070ace93e',1,1,'threshold','{}'),
    ('a9728ad7-5bad-4d37-996f-a1061d87dadb',1,1,'threshold','{}');
INSERT INTO `target_probe_agent` (`target_probe`,`agent`,`uuid`,`probe_configuration`,`schedule`) VALUES
    (1,1,'76047891-c417-42b1-b3af-af6d1bb974b8','{"address":"target0.pantomath.io"}',1057884408109),
    (1,1,'0f459ccb-0e82-4e57-9acf-9fa72cb82fe9','{"address":"target1.pantomath.io"}',1988293295263),
    (1,1,'aa5e09e5-dfe3-4b47-89e3-447a8cd1aea3','{"address":"target2.pantomath.io"}',3607659460211),
    (1,1,'b869c72d-cc4b-42d2-a768-c3f183d00433','{"address":"target3.pantomath.io"}',2200656167454),
    (1,1,'a63a4d78-ea88-4e7d-91e7-cc6f66dd7bfb','{"address":"target4.pantomath.io"}',437328399536),
    (1,1,'0d7de4ba-1dca-4581-8c82-faa4c09ff8f4','{"address":"target5.pantomath.io"}',4342716133564),
    (1,1,'3d038a07-6f9a-4479-bc6d-9ab4cf90087b','{"address":"target6.pantomath.io"}',3834272397602),
    (1,1,'2b8e1a6d-c64a-4324-b1a6-f9df69a076d2','{"address":"target7.pantomath.io"}',921611317431),
    (1,1,'087f5226-0650-4a30-9249-85b862935537','{"address":"target8.pantomath.io"}',3255049353082),
    (1,1,'e2d595a5-5c5e-4b94-81d5-46b772c9eb16','{"address":"target9.pantomath.io"}',3800956973254),
    (1,2,'222e4d41-c8e2-44ee-ad7d-d07f1d9edc82','{"address":"target10.pantomath.io"}',3240540762237),
    (2,1,'1535460e-86b5-4557-2866-83e31535ec4c','{"address":"target10.pantomath.io"}',3240540716745);
INSERT INTO `target_probe_alert` (`target_probe`,`alert`) VALUES
    (1,1),
    (1,2),
    (1,3),
    (1,4),
    (1,5),
    (1,6),
    (1,7),
    (1,8),
    (1,9),
    (1,10);
INSERT INTO `user` (`uuid`,`email`,`passwd_hash`,`passwd_salt`,`details`,`organization`) VALUES
    ('8c470774-1dca-11ea-b5c9-d70fe87a28e2', 'hello@world.com',
    -- Argon2 hash of "helloworld"
    '8074fbaf8af8a0200c7dcb0c89a2740c0cbff9d309d64cb5a75e0fe2cd9e0b4e',
    '3d2438f53d68a77ceb66050b01aa9628c958845989f97a56003272fe06c2c29d',
    '{}',1);