# Contributing guidelines

By contributing to Pantomath SAS, You accept and agree to the following terms and conditions for Your present and future Contributions submitted to Pantomath SAS. Except for the license granted herein to Pantomath SAS and recipients of software distributed by Pantomath SAS, You reserve all right, title, and interest in and to Your Contributions. All Contributions are subject to the following [Developer Certificate of Origin](DCO) and [License](LICENSE) terms.

All Documentation content that resides under the [docs/ directory](docs/) of this repository is licensed under Creative Commons: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

> **Disclaimer:** This guide is a constant work in progress. It starts (2017-07-13) as an unordered collection of notes about coding conventions and common practices. Once this gets out of hand, we'll need to start organizing it.

## Coding Style

Order of priority of style references:

1. `gofmt`
2. [Effective Go](https://golang.org/doc/effective_go.html)
2. [Code review comments](https://github.com/golang/go/wiki/CodeReviewComments)
3. [This document](#)

### Naming

----

#### Variable name length

According to the Go [code review comments guidelines](https://github.com/golang/go/wiki/CodeReviewComments#variable-names):

> The basic rule: the further from its declaration that a name is used, the more descriptive the name must be. For a method receiver, one or two letters is sufficient. Common variables such as loop indices and readers can be a single letter (`i`, `r`). More unusual things and global variables need more descriptive names.

The Panto code base follows a more specific rule. Single-letter variables are only accepted for:

* commonly used local variable names, such as `i` or `n` for iteration integers, or `s` for a local string
* method receivers

Other cases should have a more descriptive name.

#### Collections

As a general rule, collection variables (such as arrays, lists or maps) should be named using a plural noun.

```go
var highScores []int32    // ok
var highScore  []int32    // not ok

type Student struct {
    firstName, lastName string
    grades map[string]float32  // maps grades to classes
}
```

#### Capitalization

Panto uses mixed case names, as described in [Effective Go](https://golang.org/doc/effective_go.html#mixed-caps). In case one of the words is an acronym or an otherwise full-caps word, all other letters follow capitalization of the first letter.

Example:

```go
// in this example, URL is an acronym that can be fully capitalized or not according to context 

type URLContainer struct {
	url string // not exported, for some reason
}

func (c *URLContainer) GetURL() string {
	return c.url
}

urlString := fmt.Sprintf("%s://%s:%d%s", scheme, host, port, path)
container := URLContainer{url: urlString}
```

## Conventions

### Imports

----

Imports in the Panto source files are in the following order.

1. Standard Go library
2. Panto imports
3. External imports

To keep these blocks separated and not let `go fmt` reorder imports, it is necessary to insert an empty line between each block.

Example:

```go
import (
    "bytes"
    "fmt"
    "net/http"

    "gitlab.com/pantomath-io/panto/api"

    "github.com/op/go-logging"
    "github.com/spf13/pflag"
    "github.com/spf13/viper"
)
```


<!-- Panto also uses [dep](https://github.com/golang/dep) to manage dependencies. All imports should be added to the [Gopkg.toml](https://github.com/golang/dep/blob/master/docs/Gopkg.toml.md) file, using `dep ensure -add` command.
 -->

### Message capitalization

----

String messages should be capitalized with the following rules:

* Error message (`error`): all lower case
* Log (`CRITICAL`/`ERROR`/`DEBUG`/`INFO`): sentence case
* Failed test messages: all lower case

### Logging

----

Panto uses [go-logging](https://github.com/op/go-logging) for logging. Extensive documentation can be found [here](http://godoc.org/github.com/op/go-logging).

Package `logging` defines 6 logging [levels](http://godoc.org/github.com/op/go-logging#Level).
These levels are used in the following manner in Panto.

#### `CRITICAL`

This logging level is used in the worst possible case: an error occured that the program cannot recover from, and might start displaying dangerous behavior. Examples include losing important user data or corrupting a database. This is always followed by an exit for failure. Therefore, this logging level is used exclusively by calling `log.Fatal` or `log.Fatalf`, which quits after logging the error message.

#### `ERROR`

This logging level is used for errors that don't require instantly quitting the program. Examples include failing to connect to a server, or reading an invalid configuration file. This designates important cases, and implies the user should do something to fix it.

#### `WARNING`

This logging level is used for lesser failures, indicating that the program is running well, but displayed some unexpected behavior that could lead to a failure or prompting for some user interaction or diagnostic to ensure everything is functioning.

#### `NOTICE`

This logging level is not used in Panto. Use `INFO` instead.

#### `INFO`

This logging level is used to inform the user of an action the program performed.
This must not be used to indicate failures, even minor ones. Examples may include connecting to a server, performing a request, receiving a signal from the operating system. 

#### `DEBUG`

This logging level is used in development. This outputs all info from all different parts of the program. Examples include outputting the contents of a server response, the contents read from a configuration file from the filesystem or timing information from a long-running operation. As a rule of thumb, use this logging level if you would `printf` it in C, during development. This log mode can get very verbose and may also display some sensitive information, and is not recommended for users in production. **Caveat:** beware not to output any critically sensitive data such as passwords or cryptographic keys. These should _never_ be output under any circumstances.

### Errors

As is the convention in Go, if a function fails to execute, it should return an `error`. The `ERROR` logging level should never be used in this case.

Semantically, returning an error means the responsibility of this error is handed over to the caller. Handling that error should happen in a higher layer, and that includes logging. The risk is that every layer can log the error, leading to some verbosity and redundancy in logs.

Here is a rule of thumb for handling/logging errors.

1. If your function returns an error, do not log it.
2. If your function receives an error from calling a function:
  1. if you forward the error to the caller, do not log it
  2. if you handle the error and are the last one to own it, log the error
3. The _exception_ to this is to use the `DEBUG` log level. An error can trigger a `DEBUG` log at any level.

Example:

```go
// OpenConnection connects to a host on the default port. If the connection
// times out, retry up to 10 times.
func OpenConnection(host string) (net.Conn, error) {
    const (
        retries     = 10
        defaultPort = 9999
    )

    for i := 0; i < retries; i++ {
        conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", host, defaultPort))
        if err != nil {
            if opErr, ok := err.(net.Error); ok && opErr.Timeout() {
                // Error is handled at this level
                // => Apply rule 2.2: log it
                log.Warningf("timed out connecting to %s", host)
            } else {
                // Not a timeout, forward the error
                // => Apply rule 2.1: do not log it
                //
                // Rule 3: optionally log to DEBUG
                log.Debugf("could not connect to %s: %s", host, err)
                return nil, err
            }
        } else {
            // No error, return connection (and log something)
            log.Infof("established connection to %s", host)
            return conn, err
        }
    }

    // Too many attempts. Stop retrying, return an error.
    // Apply rule 1: do not log it
    return nil, fmt.Errorf("timeout connecting to %s, aborting after %d attempts", host, retries)
}

// SendMessage sends an arbitrary message followed by a CRLF over the connection.
// It is meant to be called in a goroutine, so it doesn't report any errors.
func SendMessage(conn net.Conn, message string) {
    message = fmt.Sprintf("%s\r\n", message)
    _, err := conn.Write([]byte(message))

    if err != nil {
        // Error is handled at this level
        // => Apply rule 2.2: log it
        log.Errorf("couldn't write to connection: %s", err)
    }
}

func main() {
    configureLogger()

    conn, err := OpenConnection("localhost")
    if err != nil {
        // Received error from callee, handle it by exiting the program
        // => Apply rule 2.2: log it
        log.Errorf("couldn't open connection: %s", err)
        os.Exit(1)
    }

    for {
        _, ok := <-time.Tick(1 * time.Second)
        if !ok {
            break
        }

        // Send message asynchronously
        go SendMessage(conn, "Hello World!")
    }
}
```

The logs look similar to this:

```
12:34:01.738 [DEBU] OpenConnection: could not connect to localhost: dial tcp [::1]:9999: getsockopt: connection refused
12:34:01.738 [ERRO] main: couldn't open connection: dial tcp [::1]:9999: getsockopt: connection refused
12:34:17.818 [INFO] OpenConnection: established connection to localhost
12:34:27.823 [ERRO] SendMessage: couldn't write to connection: write tcp 127.0.0.1:50873->127.0.0.1:9999: write: broken pipe
12:34:28.823 [ERRO] SendMessage: couldn't write to connection: write tcp 127.0.0.1:50873->127.0.0.1:9999: write: broken pipe
12:34:29.823 [ERRO] SendMessage: couldn't write to connection: write tcp 127.0.0.1:50873->127.0.0.1:9999: write: broken pipe
```

#### Special case

Errors that will be displayed to the user "as-is" (such as error messages in API handler responses), should be properly capitalized.

```go
func (s *server) GetResource(req *GetResourceRequest) (*Resource, error) {
    res := GetResourceFromDatabase(req.ResourceID)
    if res == nil {
        return nil, "Resource not found"
    }
    return res, nil
}
```

### Testing

----

Panto uses Go's standard library [`testing`](https://golang.org/pkg/testing/) package, in conjunction with the [`go test` tool](https://golang.org/cmd/go/#hdr-Test_packages).

There are two main ways for a test to fail in this package.

* [`T.Fail`](https://golang.org/pkg/testing/#T.Fail) and its companion functions [`T.Error`](https://golang.org/pkg/testing/#T.Error) and [`T.Errorf`](https://golang.org/pkg/testing/#T.Errorf). (According to the documentation: `Error` is equivalent to `Log` followed by `Fail`.)
* [`T.FailNow`](https://golang.org/pkg/testing/#T.FailNow) and its companion functions [`T.Fatal`](https://golang.org/pkg/testing/#T.Fatal) and [`T.Fatalf`](https://golang.org/pkg/testing/#T.Fatalf).

The former marks the test as failed, optionally logs a message, and continues executing the test. The latter marks the test as failed, optionally logs a message, and stops execution of the test. Execution will continue at the next test or benchmark.

In some cases, a failed check invalidates the rest of the test. In other cases, a check can fail but other tests still be useful. Distinguishing between both cases is important in building useful and explicit tests.

For example, if a test is checking validity for each field of a `struct`, each failure conveys important information. It is important to keep the test returning after the first failure. On the other hand, some failures make the rest of the test irrelevant. For example, if an initial HTTP request fails, there is no need to perform specific checks on the received data.

Panto uses `Fail`/`Error`/`Errorf` for failed tests that don't make the rest irrelevant, and `FailNow`/`Fatal`/`Fatalf` in other cases. Following is a working example of this convention.

```go
type Save struct {
    Version int
    Name string
}

func TestLoadFromFile(t *testing.T) {
    f, err := os.Open("save.json")

    if err != nil {
        // If the file couldn't be opened, the whole test is void.
        // Fail and exit
        t.Fatalf("failed to open save file: %s", err)
    }

    // Reading from file and Unmarshaling JSON are also fatal errors.

    if save.Version < 0  || save.Version > 5 {
        // Invalid version number, but the rest of the unmarshaled data
        // should still be tested.
        // Fail and continue.
        t.Errorf("invalid version number: %d", save.version)
    }

    if save.Name == "" {
        t.Error("saved name is empty")
    }
}
```

### SQL

#### Inner joins

SQL inner joins should always be explicit, not handled with commas. See [this post](https://mariadb.com/kb/en/library/comma-vs-join/) for the rationale.

Example:

```SQL
SELECT *
FROM
  clients
  INNER JOIN orders ON clients.id = orders.clientId
  INNER JOIN phoneNumbers ON clients.id = phoneNumbers.clientId
WHERE
  orderPlaced >= NOW() - INTERVAL 2 WEEK;
```
