#!/bin/bash
### BEGIN INIT INFO
# Provides:          panto
# Required-Start:    $local_fs $remote_fs $network $syslog $named
# Required-Stop:     $local_fs $remote_fs $network $syslog $named
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# X-Interactive:     true
# Short-Description: Start/stop panto
### END INIT INFO

# Source function library.
[[ -f /lib/lsb/init-functions ]] || exit 1;
. /lib/lsb/init-functions

PANTO_BIN="/usr/bin/panto"
PANTO_PID="/var/run/panto.pid"
PANTO_OPTS="--conf /etc/panto/panto.yaml"

start() {
	start-stop-daemon --start --pidfile $PANTO_PID -m --startas $PANTO_BIN --background -- $PANTO_OPTS
	log_success_msg "sucessfully started panto"
}

stop() {
	start-stop-daemon --stop --pidfile $PANTO_PID
	log_success_msg "sucessfully stopped panto"
}

status() {
	start-stop-daemon --status --pidfile $PANTO_PID

	case "$?" in
		0)
			state="running"
			;;
		1|3)
			state="stopped"
			;;
		*)
			state="unknown"
	esac
	log_success_msg "panto is: $state"
}

case "$1" in 
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		start
		;;
	status)
		status
		;;
	*)
		echo "Usage: $0 {start|stop|restart|status}"
esac

exit 0 
