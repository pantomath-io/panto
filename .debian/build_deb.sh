#!/bin/bash
#
# Copyright 2020 Pantomath SAS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Generate deb archives from skeletons

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(dirname "$0")

readonly PANTO_VERSION="PANTO_VERSION"

### functions
# displays nice and shiny help message (on STDERR)
# parameters: none
# return: nothing (exit)
_help() {
	helpmessage="${PROGNAME} builds deb packages for Panto\n\n";
	helpmessage=$helpmessage"usage : ${PROGDIR}/${PROGNAME} [-h] -v VERSION -p PACKAGE\n\n";
	helpmessage=$helpmessage" required:\n";
	helpmessage=$helpmessage"  -v VERSION\tVersion (in SemVer) to build\n";
	helpmessage=$helpmessage"  -p PACKAGE\tPackage to build (panto or panto-agent)\n";
	helpmessage=$helpmessage"  -c CONFIGURATION\tConfiguration files root path (default: ../conf)\n";
	helpmessage=$helpmessage"  -b BINARIES\tBinary files root path (default: ../bin)\n";
	helpmessage=$helpmessage"\n optional:\n";
	helpmessage=$helpmessage"  -h\t\tthis help screen\n";
	echo -e "$helpmessage" >&2;
	exit 1;
}

# error output and exit (on STDERR)
# parameters: _printError "code" "message"
#  - code: integer, the exit code
#  - message: string with the message to print
# return: nothing (exit)
_printError() {
	local code="$1";
	shift;
	local message="";

	# get the message
	while [ -n "$1" ] ; do
		message="${message} $1" ;
		shift ;
	done

	# print it
	echo -e "\033[01;31m[!] ${message}\033[0m" >&2 ;

	### exit
	exit "${code}";
}

# replaces a pattern in a file
# parameters:
#  file: full path of the file to modify
#  search: pattern to search
#  replace: string to replace the pattern with
# return: nothing
_replace_in_file() {
  if [[ ! -f "$1" ]]; then
    _printError 3 "  !!! $1 is not a valid file.";
  fi

  sed -i -e "s/$2/$3/g" "$1" || _printError 4 "unable to replace $2 by $3 in $1";
}

# creates the root directory for the package
# parameters:
#  package: name of the package
#  path: path of the package root
# return: nothing
_create_package_root() {
  if [[ -z "$1" ]]; then
    _printError 3 "  !! Missing package to create root.";
  fi
  if [[ -z "$2" ]]; then
    _printError 3 "  !! Missing package path to create.";
  fi

  readonly source="$1_amd64"
  readonly destination="$2"

  if [[ -d "${destination}" ]]; then
    ls -lah "${destination}" >&2;
    _printError 3 "  !! Package root already exists.";
  fi

  cp -r "${source}" "${destination}" || _printError 3 "unable to copy ${source} to ${destination}";
}

# adds a file to the package
# parameters
#  source: path of the file to add
#  destination: path of the file in the package
# return: nothing
_add_file() {
  if [[ -z "$1" ]]; then
    _printError 3 "  !! Missing path for source.";
  fi
  if [[ -z "$2" ]]; then
    _printError 3 "  !! Missing path for destination.";
  fi
  if [[ -f "$2" ]]; then
    ls -lah "$2" >&2;
    _printError 3 "  !! Destination already exists.";
  fi

  cp "$1" "$2" || _printError 3 "unable to copy $1 to $2";
}

# builds the deb package
# parameters
#  package: name of the package
#  version: version of the package
#  confroot: Configuration folder root
#  binroot: Binary folder root
# return: nothing
_build_package() {
  readonly package="${1}"
  readonly version="${2}"
  readonly confroot="${3}"
  readonly binroot="${4}"
  readonly package_root="${package}_${version}-1_amd64"

  # create the working folder
  _create_package_root "${package}" "${package_root}";
  echo -e "  - created package folder: ${package_root}";

  # set the right version in the templated files
  _replace_in_file "${package_root}/DEBIAN/control" "${PANTO_VERSION}" "${version}";
  echo -e "  - updated version in control file";
  gunzip "${package_root}/usr/share/doc/${package}/changelog.Debian.gz" \
    || _printError 4 "unable to uncompress changelog.Debian";
  _replace_in_file "${package_root}/usr/share/doc/${package}/changelog.Debian" "${PANTO_VERSION}" "${version}";
  gzip -n --best "${package_root}/usr/share/doc/${package}/changelog.Debian" \
    || _printError 4 "unable to compress changelog.Debian";
  echo -e "  - updated version in changelog file";

  # add binaries
  if [[ "${package}" = "panto" ]]; then
    _add_file "${binroot}/panto" "${package_root}/usr/bin/panto";
    _add_file "${binroot}/panto-ctl" "${package_root}/usr/bin/panto-ctl";
  elif [[ "${package}" = "panto-agent" ]]; then
    _add_file "${binroot}/panto-agent" "${package_root}/usr/bin/panto-agent";
  fi
  echo -e "  - added binaries";

  # add configurations
  if [[ "${package}" = "panto" ]]; then
    _add_file "${confroot}/panto.yaml" "${package_root}/etc/panto/panto.yaml";
  elif [[ "${package}" = "panto-agent" ]]; then
    _add_file "${confroot}/panto-agent.yaml" "${package_root}/etc/panto/panto-agent.yaml";
  fi
  echo -e "  - added configuration files";

  # clean .gitignore files
  find "${package_root}" -type f -iname '.gitignore' -delete \
    || _printError 4 "unable to remove .gitignore files";

  fakeroot dpkg-deb --build "${package_root}" || _printError 4 "unable to build to package";
  echo -e "  - package built";

  # clean build directory
  rm -rf "${package_root}" || _printError 4 "unable to remove ${package_root}";
  echo -e "  - package sandbox cleaned";
}

main() {
  ### get option(s)
  while getopts hv:p:c:b: opt ; do
    case "${opt}" in
      "h")	mayday=1 ;;
      "v")  version="${OPTARG}" ;;
      "p")  package="${OPTARG}" ;;
      "c")  confroot="${OPTARG}" ;;
      "b")  binroot="${OPTARG}" ;;
      *) echo -e "Unknown argument. Exiting." >&2; exit 1 ;;
    esac
  done

  # check for help
  if [[ "${mayday}" -gt "0" ]]; then
    _help;
  fi

  # check version is specified
  if [[ -z "${version}" ]]; then
  	echo -e "Missing version. Exiting." >&2;
    exit 1;
  fi

  # check package is specified and valid
  if [[ -z "${package}" ]]; then
  	echo -e "Missing package. Exiting." >&2;
    exit 1;
  elif [[ "${package}" != "panto" && "${package}" != "panto-agent" ]]; then
  	echo -e "Invalid package. Exiting." >&2;
    exit 2;
  fi

  # check configuration files root is specified
  if [[ -z "${confroot}" ]]; then
    confroot="../conf";
  fi
  if [[ ! -d "${confroot}" ]]; then
    echo -e "Conf folder is not a folder. Exiting." >&2
    exit 1;
  fi

  # check bin files root is specified
  if [[ -z "${binroot}" ]]; then
    binroot="../bin";
  fi
  if [[ ! -d "${binroot}" ]]; then
    echo -e "Bin folder is not a folder. Exiting." >&2
    exit 1;
  fi

  echo -e "Building package ${package}-${version}";
  _build_package "${package}" "${version}" "${confroot}" "${binroot}";
}

main "$@"
