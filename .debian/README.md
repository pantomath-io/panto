# Panto - Debian

This folder contains everything needed to build debian packages and maintain apt repository for panto project.

## Initial setup

### `.deb` packages

The [`panto_amd64`](.debian/panto_amd64) and [`panto-agent_amd64`](.debian/panto-agent_amd64) directories contain the basic skeleton to build .deb packages for `panto` and `panto-agent`, respectively.
They are still empty shells, and need the proper binaries to be injected, and some files updated (with template variables, like `PANTO_VERSION`) before being compiled in a `.deb` files. See [Updating section](#updating).

### `apt` repository

The [`debian` folder](.debian/debian) is the root of the `apt` repository, and contain a control file.
The `deb` files will be placed in that root and the `reprepro` tool will be used to add packages to the repository. The result (a full `apt` repository) can then be uploaded on an HTTPS server. Note that the PGP key used to sign the packages need to be shared, so the `apt` user can check the packages.

## <a name="updating"></a> Updating

### Creating a new `.deb` package

A helper script is provided to build the packages during the CI (and the CD): `build_deb.sh`:

```shell
$ ./build_deb.sh -h
build_deb.sh builds deb packages for Panto

usage : ./build_deb.sh [-h] -v VERSION -p PACKAGE

 required:
  -v VERSION    Version (in SemVer) to build
  -p PACKAGE    Package to build (panto or panto-agent)

 optional:
  -h            this help screen
```

The script wraps up the following actions:

* update the `<package>/DEBIAN/control` files (search-and-replace PANTO_VERSION)
* update the `<package>/usr/share/doc/<package>/changelog.Debian.gz` files (search-and-replace PANTO_VERSION)
* inject binaries
* inject configuration files
* call `dpkg-deb` to build the package

If everything goes as expected, the result should be a `.deb` file.

You can test the quality of the resulting `.deb` package with tools like `lintian`.

### Adding a `.deb` package to the `apt` repository

A helper script is provided to add new packages to the `apt` repository, during the CD: `add_package.sh`:

```shell
$ ./add_to_repo.sh -h
add_to_repo.sh add a deb package to an apt repository, and sync it

usage : ./add_to_repo.sh [-h] -l LOCAL -r REPOSITORY -p PACKAGE

 required:
  -l LOCAL      Local path for the repository
  -r REPOSITORY Remote repository (format: user@hostname:/full/path)
  -p PACKAGE    Package to add (i.e. panto_0.1.0-1_amd64.deb)

 optional:
  -h            this help screen
```

The script wraps up the following actions:

* add the package in the repository
* update all the corresponding files (Packages, Release)
* sign the packages with the PGP key

If everything goes as expected, the result should be an `apt` repository, to be served over HTTPS.

## License

Copyright 2017-19 Pantomath SAS

The Panto source code is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).

By contributing to Pantomath SAS, You accept and agree to the following terms and conditions for Your present and future Contributions submitted to Pantomath SAS. Except for the license granted herein to Pantomath SAS and recipients of software distributed by Pantomath SAS, You reserve all right, title, and interest in and to Your Contributions. All Contributions are subject to the following [Developer Certificate of Origin](DCO) and [License](LICENSE) terms.

All Documentation content that resides under the [`docs` directory](docs) of this repository is licensed under Creative Commons: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
