#!/bin/bash
#
# Copyright 2020 Pantomath SAS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e;

readonly DATA_DIR="/var/lib/panto"
readonly LOG_DIR="/var/log/panto"
readonly SCRIPT_DIR="/usr/lib/panto-agent/scripts"
readonly PANTO_AGENT_SERVICE="panto-agent"
readonly PANTO_AGENT_USER="panto"
readonly PANTO_AGENT_GROUP="panto"

function install_init {
  cp -f "${SCRIPT_DIR}/init.sh" "/etc/init.d/${PANTO_AGENT_SERVICE}";
  chmod +x "/etc/init.d/${PANTO_AGENT_SERVICE}";
}

function install_systemd {
  cp -f "${SCRIPT_DIR}/panto-agent.service" "/lib/systemd/system/${PANTO_AGENT_SERVICE}.service";
  systemctl enable ${PANTO_AGENT_SERVICE};
}

function install_update_rcd {
  update-rc.d ${PANTO_AGENT_SERVICE} defaults;
}

function install_chkconfig {
  chkconfig --add ${PANTO_AGENT_SERVICE};
}

# Create user / group if not exist
if ! grep "${PANTO_AGENT_GROUP}:" /etc/group &>/dev/null; then
  groupadd -r ${PANTO_AGENT_GROUP};
fi

if ! id ${PANTO_AGENT_USER} &>/dev/null; then
  useradd -g ${PANTO_AGENT_GROUP} -s /bin/false -d "${DATA_DIR}" -M -r ${PANTO_AGENT_USER};
fi

# Fix permissions
chown -R -L ${PANTO_AGENT_USER}:${PANTO_AGENT_GROUP} "${DATA_DIR}";
chown -R -L ${PANTO_AGENT_USER}:${PANTO_AGENT_GROUP} "${LOG_DIR}";

# Distribution-specific logic
if [[ -f "/etc/debian_version" ]]; then
  # Debian/Ubuntu logic
  if command -v systemctl &>/dev/null; then
      install_systemd;
  else
    # Assuming sysv
    install_init;
    install_update_rcd;
  fi
fi

exit 0;