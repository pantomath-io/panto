#!/bin/bash
### BEGIN INIT INFO
# Provides:          panto-agent
# Required-Start:    $local_fs $remote_fs $network $syslog $named
# Required-Stop:     $local_fs $remote_fs $network $syslog $named
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# X-Interactive:     true
# Short-Description: Start/stop panto-agent
### END INIT INFO

# Source function library.
[[ -f /lib/lsb/init-functions ]] || exit 1;
. /lib/lsb/init-functions

PANTOAGENT_BIN="/usr/bin/panto-agent"
PANTOAGENT_PID="/var/run/panto-agent.pid"
PANTOAGENT_OPTS="--conf /etc/panto/panto-agent.yaml"

start() {
	start-stop-daemon --start --pidfile $PANTOAGENT_PID -m --startas $PANTOAGENT_BIN --background -- $PANTOAGENT_OPTS
	log_success_msg "sucessfully started panto-agent"
}

stop() {
	start-stop-daemon --stop --pidfile $PANTOAGENT_PID
	log_success_msg "sucessfully stopped panto-agent"
}

status() {
	start-stop-daemon --status --pidfile $PANTOAGENT_PID

	case "$?" in
		0)
			state="running"
			;;
		1|3)
			state="stopped"
			;;
		*)
			state="unknown"
	esac
	log_success_msg "panto-agent is: $state"
}

case "$1" in 
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		start
		;;
	status)
		status
		;;
	*)
		echo "Usage: $0 {start|stop|restart|status}"
esac

exit 0 
