#!/bin/bash
#
# Copyright 2020 Pantomath SAS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Add a deb file to a remote apt repository

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(dirname "$0")
readonly DISTRIBUTIONS_FILE="apt/distributions"
readonly REPREPRO="$( command -v reprepro )"

### functions
# displays nice and shiny help message (on STDERR)
# parameters: none
# return: nothing (exit)
_help() {
	helpmessage="${PROGNAME} add a deb package to an apt repository, and sync it\n\n";
	helpmessage=$helpmessage"usage : ${PROGDIR}/${PROGNAME} [-h] -l LOCAL -r REPOSITORY -p PACKAGE\n\n";
	helpmessage=$helpmessage" required:\n";
	helpmessage=$helpmessage"  -l NAME\tRepository name (i.e. debian)\n";
	helpmessage=$helpmessage"  -r REMOTE\tRemote repository (format: user@hostname:/full/path)\n";
	helpmessage=$helpmessage"  -p PACKAGE\tPackage to add (i.e. panto_0.1.0-1_amd64.deb)\n";
	helpmessage=$helpmessage"\n optional:\n";
	helpmessage=$helpmessage"  -h\t\tthis help screen\n";
	echo -e "$helpmessage" >&2;
	exit 1;
}

# error output and exit (on STDERR)
# parameters: _printError "code" "message"
#  - code: integer, the exit code
#  - message: string with the message to print
# return: nothing (exit)
_printError() {
	local code="$1";
	shift;
	local message="";

	# get the message
	while [ -n "$1" ] ; do
		message="${message} $1" ;
		shift ;
	done

	# print it
	echo -e "\033[01;31m[!] ${message}\033[0m" >&2 ;

	### exit
	exit "${code}";
}

# rsync a folder from/to a local path
# parameters:
#  source: source of the sync (could be local or remote)
#  destination: source of the sync (could be local or remote)
# return: nothing
_rsync_folder() {
  if [[ -z "$1" ]]; then
    _printError 3 "  !! Missing path for source.";
  fi
  if [[ -z "$2" ]]; then
    _printError 3 "  !! Missing path for destination.";
  fi

	if ! rsync -az -e ssh "$1" "$2"; then
    _printError 3 "Unable to rsync $1 to $2";
  fi
}

# adds a file to the package
# parameters
#  source: path of the file to add
#  destination: path of the file in the package
# return: nothing
_add_file() {
  if [[ -z "$1" ]]; then
    _printError 3 "  !! Missing path for source.";
  fi
  if [[ -z "$2" ]]; then
    _printError 3 "  !! Missing path for destination.";
  fi

  dest_path=$( dirname "$2" )
  if [[ ! -d "${dest_path}" ]]; then
    mkdir -p "${dest_path}";
  fi

  if ! cp -f "$1" "$2"; then
    _printError 3 "unable to copy $1 to $2";
  fi
}

# checks if a package exists in the repo
# parameters:
#   root: root of the repository
#   packname: name of the package
#   version: version of the package
# return: 1 if the package is found, 0 if not
_check_package_in_repo() {
  local root="${1}"
  local packname="${2}"
  local version="${3}"

  retour=$( ${REPREPRO} -b "${root}" -C main -A amd64 ls "${packname}" | grep -c "${version}" );
  return "${retour}"
}

# add the package to the repo
# parameters
#  remote_repo: remote repository (user@hostname:/full/path)
#  repo_name: name of the repository (/full/path)
#  package: deb package (panto_0.1.0-1_amd64.deb)
_add_repo() {
  readonly remote_repo="${1}"
  readonly repo_name="${2}"
  readonly package="${3}"
  local packname
  local version
  packname="$( echo ${package} | sed -e 's/^\([^_]*\)_.*/\1/g' )"
  version="$( echo ${package} | sed -e 's/^[^_]*_\([^_]*\)_.*/\1/g' )"

  # get the remote repository
  _rsync_folder "${remote_repo}/${repo_name}" .;
  echo -e "  - received remote repository: ${remote_repo}/${repo_name}";

  # copy our distributions file
  _add_file "${DISTRIBUTIONS_FILE}" "${repo_name}/conf/distributions";
  echo -e "  - added ${DISTRIBUTIONS_FILE} in ${repo_name}";

  # test if the package already exists
  if _check_package_in_repo "${repo_name}" "${packname}" "${version}"; then
    "${REPREPRO}" -C main -V -b "${repo_name}" remove stable "${packname}" \
      || _printError 4 "unable to remove ${packname} from repo";
    echo -e "  - removed previous ${packname} from ${repo_name}";
  else
    echo -e "  - no previous ${packname} in ${repo_name} to remove";
  fi

  # add the package(s) to the local repo
  cd "${repo_name}" || _printError 4 "unable to enter the repo";
  "${REPREPRO}" -C main -V -b . includedeb stable "../${package}" \
    || _printError 4 "unable to add ${package} in repo";
  cd - || _printError 4 "unable to leave the repo";
  echo -e "  - added ${package} in ${repo_name}";

  # sign the package(s) in the local repo
  cd "${repo_name}" || _printError 4 "unable to enter the repo";
  "${REPREPRO}" --ask-passphrase export || _printError 4 "unable to sign ${repo_name}";
  cd - || _printError 4 "unable to leave the repo";
  echo -e "  - signed ${package} in ${repo_name}";

  # send the repository to remote
  _rsync_folder "${repo_name}" "${remote_repo}/";
  echo -e "  - sent ${repo_name} to ${remote_repo}";

  # cleaning the local repository
  rm -rf "${repo_name}" || _printError 3 "unable to remove ${repo_name}";
  echo -e "  - removed ${repo_name}";
}

main () {
  ### get option(s)
  while getopts hl:r:p: opt ; do
    case "${opt}" in
      "h")	mayday=1 ;;
      "l")  repo_name="${OPTARG}" ;;
      "r")  remote_repo="${OPTARG}" ;;
      "p")  package="${OPTARG}" ;;
      *)    echo -e "Unknown argument. Exiting." >&2; exit 1 ;;
    esac
  done

  # check for help
  if [[ "${mayday}" -gt "0" ]]; then
    _help;
  fi

  # check dependencies
  if [[ -z "${REPREPRO}" ]]; then
  	_printError 1 "Missing reprepro tool. Exiting.";
  fi

  # check local repo is specified
  if [[ -z "${repo_name}" ]]; then
  	_printError 1 "Missing local repository. Exiting.";
  fi

  # check remote repo is specified
  if [[ -z "${remote_repo}" ]]; then
  	_printError 1 "Missing remote repository. Exiting.";
  fi

  # check package is specified and valid
  if [[ -z "${package}" ]]; then
  	_printError 1 "Missing package. Exiting.";
  elif [[ ! -f "${package}" ]]; then
  	_printError 2 "Invalid package. Exiting.";
  fi

  echo -e "Adding package ${package} to ${remote_repo}";
  _add_repo "${remote_repo}" "${repo_name}" "${package}";
}

main "$@"
