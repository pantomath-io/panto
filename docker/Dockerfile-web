# Copyright 2017 Pantomath SAS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM debian:buster-slim
LABEL authors="Pantomath Team <maintainers@pantomath.io>"

# The PANTO_ADDRESS environment variable is the base URL for the HTTP REST API.
# WARNING: The address will be used *by the client* inside the browser. Make sure
# you use a publicly visible address.
ENV PANTO_ADDRESS="http://localhost:7576"

# create directories
RUN mkdir -p /var/www/panto
RUN mkdir -p /var/log/panto

# download the caddy binary
RUN apt-get update                             && \
    apt-get install --no-install-recommends -yq   \
    curl                                          \
    ca-certificates                            && \
    apt-get autoremove -y                      && \
    rm -rf /var/lib/apt/lists/*.deb            && \
    cd /tmp \
    && curl -sL -O https://github.com/caddyserver/caddy/releases/download/v1.0.4/caddy_v1.0.4_linux_amd64.tar.gz \
    && mkdir -p caddy \
    && tar xz -C /tmp/caddy -f caddy_v1.0.4_linux_amd64.tar.gz \
    && cp /tmp/caddy/caddy /usr/bin/caddy \
    && cd / && rm -rf /tmp/caddy*

# deploy the web application
COPY web/dist /var/www/panto
COPY docker/Caddyfile /etc/panto/Caddyfile

# share Panto web ports
EXPOSE 8080

# run the server
CMD /usr/bin/caddy -conf=/etc/panto/Caddyfile

# Cleanup tmp directories after build
RUN rm -rf /tmp/* /var/tmp/*