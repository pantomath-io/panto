# Copyright 2017 Pantomath SAS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM debian:buster-slim
LABEL authors="Pantomath Team <maintainers@pantomath.io>"

ENV PANTO_SERVER_ADDRESS="localhost:7575"

# add ca certificates
RUN apt-get update                             && \
    apt-get install --no-install-recommends -yq   \
    ca-certificates                            && \
    apt-get autoremove -y                      && \
    rm -rf /var/lib/apt/lists/*.deb

# add intermediate certificate for Gandi
RUN mkdir -p /usr/local/share/ca-certificates/gandi.net
COPY docker/GandiStandardSSLCA2.crt /usr/local/share/ca-certificates/gandi.net/
RUN update-ca-certificates

# create data directories
RUN mkdir -p /var/lib/panto
RUN mkdir -p /var/log/panto

# deploy the binary
COPY bin/panto-agent /usr/bin/panto-agent

# run the agent
CMD panto-agent --conf=/etc/panto/panto-agent.yaml $PANTO_SERVER_ADDRESS
