### Problem description

(Describe the problem, including screenshots if available)

### Environment

#### Panto versions

* Panto server version: (Hover over the Panto logo in the top left corner or run `panto -V` on the server)
* Panto agent version: (Run `panto-agent -V` on the agent)
* Panto web version: (Hover over the Panto logo in the top left corner)

#### Local environment

* Operating system:
* Browser and version:

### Reproduction

1. Describe the steps to reproduce the issue
2. Including preliminary setup
3. And any relevant information

#### Expected behavior

(What did you expect to see?)

#### Observed behavior

(What did you see instead?)

/label ~"Bug"