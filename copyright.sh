#!/bin/bash
#
# Copyright 2017 Pantomath SAS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Test presence of copyright in every source file

readonly COUNT_MISSING_FILES=$( grep -r -L -E "Copyright 20(1[7-9]|20) Pantomath SAS" . | grep -v \
	-e "^./.debian/" \
	-e "^./.git" \
	-e "^./ansible/local.serverlist" \
	-e "^./ansible/roles" \
	-e "^./api/panto.pb*" \
	-e "^./api/panto.swagger.json" \
	-e "^./bin/" \
	-e "^./conf/" \
	-e "^./data/panto*" \
	--regexp "^\.\/data/.*\.dump" \
	-e "^./docker/" \
	-e "^./docs/" \
	-e "^./go.sum" \
	-e "^./log/" \
	-e "^./vagrant" \
	-e "^./web/dist" \
	-e "^./web/node_modules" \
	-e "^./web/static/fonts" \
	-e "^./web/static/css" \
	-e "^./web/static/.*" \
	-e "^./web/src/assets/font" \
	-e "^./web/src/assets/less/bootstrap" \
	-e "^./web/src/assets/less/plugin" \
	-e "^./web/test/e2e" \
	-e "^./web/test/unit/.es*" \
	-e "^./web/test/unit/coverage/lcov*" \
	-e "^./web/.*" \
	-e "^./web/yarn.lock" \
	-e "^./web/package-lock.json" \
	-e "^./watch/dist" \
	-e "^./watch/node_modules" \
	-e "^./watch/test/e2e" \
	-e "^./watch/test/unit/.es*" \
	-e "^./watch/test/unit/coverage/lcov*" \
	-e "^./watch/.*" \
	-e "^./watch/*.json" \
	-e "^./watch/package-lock.json" \
	-e "^./www/about/image" \
	-e "^./DCO" \
	-e "^./NOTICE" \
	-e "^./VERSION" \
	-e ".pb.go$" \
	-e ".gitignore$" \
	-e ".dockerignore$" \
	-e ".md$" \
	-e ".png$" \
	-e "debug$" \
	-e ".tar.gz$" \
	-e "^./data/fixtures/003_DeveloperFixtures.influx" )

if [ "${COUNT_MISSING_FILES}" == "" ]; then
	exit 0
else
	echo "${COUNT_MISSING_FILES}" | tr " " "\n";
	exit 1
fi
