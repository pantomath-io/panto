# Panto

__Develop__ [![build status](https://gitlab.com/pantomath-io/panto/badges/develop/pipeline.svg)](https://gitlab.com/pantomath-io/panto/commits/develop) [![coverage report](https://gitlab.com/pantomath-io/panto/badges/develop/coverage.svg)](https://gitlab.com/pantomath-io/panto/commits/develop)

__Master__ [![build status](https://gitlab.com/pantomath-io/panto/badges/master/pipeline.svg)](https://gitlab.com/pantomath-io/panto/commits/master) [![coverage report](https://gitlab.com/pantomath-io/panto/badges/master/coverage.svg)](https://gitlab.com/pantomath-io/panto/commits/master)

[Panto](https://panto.app) is a platform for monitoring your infrastructure, servers and applications in production. Agents collect metrics about your systems in real time, and report their health and performance to the server. The server stores metrics, evaluates the state according to user-defined rules, and, in case the state is considered dysfunctional, dispatches alerts to your teams.

## TL;DR

You want to try Panto like, now? Here is the quickest way:

```shell
$ curl -o docker-compose.yml 'https://gitlab.com/pantomath-io/panto/raw/master/docker/docker-compose.yml?inline=false'
$ docker-compose up
$ docker exec -it panto-server /usr/bin/panto-ctl init data
```

## Downloading Panto

There are plenty of way to install Panto in your environment, check out the [detailed documentation](docs/site-docs/002-Detailed-Documentation.md) for your prefered methods.

The recommended setup to run a `panto-sever` instance is to use the [Docker Compose](https://docs.docker.com/compose/) version. After making sure `docker-compose` is installed, download the [`docker-compose.yml`](https://gitlab.com/pantomath-io/panto/raw/master/docker/docker-compose.yml) file to your computer, you can edit it to adapt the configuration to your environment.

Type:

```shell
$ docker-compose pull
```

from the same directory as the `docker-compose.yml` file, and you'll get the very latest stable version of Panto, ready to use.

## Building Panto from source

After installing the [prerequisites](docs/BUILD.md#prerequisites), clone the repository and run `make` from the repository root. The built executables will be in the `bin` directory. The built web client will be in the `web/dist` directory.

```shell
git clone https://gitlab.com/pantomath-io/panto
cd panto
make
```

Detailed build instructions can be found [here](docs/BUILD.md).

## Running Panto

### Server

Now the `docker-compose.yml` is confiugred, type:

```shell
$ docker-compose up
```

When running for the first time, you need to initialize the persistent database. Use the following command, once the docker compose is up:

```shell
$ docker exec -it panto-server /usr/bin/panto-ctl init data
```

You will be asked for your `Organization`, and to create your first `User`.

⚠️ Note: The `panto-agent` container of this `docker-compose` might not be working until you [configured](docs/site-docs/002-Detailed-Documentation.md#agent-initialization) it.

To learn more about configuration options, use `panto --help` or see the [documentation](docs/site-docs/002-Detailed-Documentation.md#panto).


### Web client

The web client is a static web application that runs in the browser. The client is the main frontend to display metrics, create targets, configure alerts, etc. Panto uses [Caddy](https://caddyserver.com/) to serve the files. This part is included in the `docker-compose` setup, so you don't have to run it manually, but you will need to configure a [proxy](#proxy).

To learn more about setup and configuration options, see the [documentation](002-Detailed-Documentation.md#web-client).

### Agent

The Panto agent should be run from the targets you want to monitor. It is part of the docker-compose setup, but needs to be configured before it is launched. Follow the [Agent initialization procedure](docs/sites-docs/002-Detailed-Documentation.md#agent-initialization) to properly start your Agent

Detailed configuration file and command-line options can be found [here](docs/site-docs/002-Detailed-Documentation.md#executables)

### Docker

Docker images are available for the panto agent and server. See the Panto [registry](https://gitlab.com/pantomath-io/panto/container_registry). Detailed documentation [here](docs/site-docs/002-Detailed-Documentation.md#docker).


## Documentation

See the [official documentation site](https://about.panto.app).

## License

Copyright 2017-19 Pantomath SAS

The Panto source code is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).

By contributing to Pantomath SAS, You accept and agree to the following terms and conditions for Your present and future Contributions submitted to Pantomath SAS. Except for the license granted herein to Pantomath SAS and recipients of software distributed by Pantomath SAS, You reserve all right, title, and interest in and to Your Contributions. All Contributions are subject to the following [Developer Certificate of Origin](DCO) and [License](LICENSE) terms.

All Documentation content that resides under the [`docs` directory](docs) of this repository is licensed under Creative Commons: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
