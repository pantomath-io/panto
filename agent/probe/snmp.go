// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"
	"strings"
	"time"

	"github.com/soniah/gosnmp"
)

//------------
// SNMP probe
//------------

// SNMP probes OIDs values
type SNMP struct {
	client gosnmp.GoSNMP
	oids   []string
}

const (
	// SNMP configuration parameters

	// SNMPParamAddress : (mandatory) the address of the target SNMP server to get metric from (string)
	SNMPParamAddress = "address"
	// SNMPParamPort : (optional, default: 161) the port of the target SNMP server to get metric from (float64)
	SNMPParamPort = "port"
	// SNMPParamCommunity : (optional, default: public) the community of the target SNMP server to get metric from (string)
	SNMPParamCommunity = "community"
	// SNMPParamTimeout (optional, default: 10s) : duration before the SNMP request times out (time.Duration)
	SNMPParamTimeout = "timeout"
	// SNMPParamOID (mandatory) : A comma-separated list of OIDs to get (string)
	SNMPParamOID = "oid"

	// SNMP results

	// SNMPResultValue : Value for the SNMP OID (interface)
	SNMPResultValue = "value"

	// SNMP result tags

	// SNMPTagOID : The requested OID, e.g. `"1.3.6.1.2.1.1.4.0"`
	SNMPTagOID = "oid"
)

// Name returns the name of the probe "SNMP"
func (s *SNMP) Name() string {
	return "SNMP"
}

// Configure configures the SNMP probe
func (s *SNMP) Configure(config map[string]interface{}) error {
	var ok bool

	// Check for mandatory address field
	var address string
	if _, ok := config[SNMPParamAddress]; !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", SNMPParamAddress)
	}
	address, ok = config[SNMPParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", SNMPParamAddress, config[SNMPParamAddress])
	}

	// Check for optional port
	var port float64
	if _, ok = config[SNMPParamPort]; !ok {
		port = 161
	} else {
		port, ok = config[SNMPParamPort].(float64)
		if !ok {
			return fmt.Errorf("\"%s\" field is not a number: %v", SNMPParamPort, config[SNMPParamPort])
		}
	}

	// Check for optional community field
	var community string
	if _, ok := config[SNMPParamCommunity]; !ok {
		community = "public"
	} else {
		community, ok = config[SNMPParamCommunity].(string)
		if !ok {
			return fmt.Errorf("\"%s\" field is not string: %v", SNMPParamCommunity, config[SNMPParamCommunity])
		}
	}

	// Check for optional timeout
	var timeout time.Duration
	if _, ok = config[SNMPParamTimeout]; !ok {
		timeout = 10 * time.Second
	} else {
		timeout, ok = config[SNMPParamTimeout].(time.Duration)
		if !ok {
			return fmt.Errorf("\"%s\" field is not time.Duration: %v", SNMPParamTimeout, config[SNMPParamTimeout])
		}
	}

	// Check for mandatory OID list
	if _, ok := config[SNMPParamOID]; !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", SNMPParamOID)
	}

	if oids, ok := config[SNMPParamOID].(string); ok {
		s.oids = strings.Split(oids, ",")
	} else {
		return fmt.Errorf("\"%s\" parameter is not string: %v", SNMPParamOID, config[SNMPParamOID])
	}

	s.client.Target = address
	s.client.Port = uint16(port)
	s.client.Community = community
	s.client.Timeout = timeout
	s.client.Version = gosnmp.Version2c
	s.client.Retries = 3
	s.client.ExponentialTimeout = true
	s.client.MaxOids = gosnmp.MaxOids

	return nil
}

// Execute runs the probe and gathers disk information
func (s *SNMP) Execute() ([]*Result, error) {
	log.Debug("Connecting SNMP agent")
	if err := s.client.Connect(); err != nil {
		return nil, fmt.Errorf("Unable to connect the SNMP agent: %s", err)
	}
	defer s.client.Conn.Close()

	result, err := s.client.Get(s.oids)
	if err != nil {
		return nil, fmt.Errorf("Unable to GET OIDs from the SNMP agent: %s", err)
	}

	res := make([]*Result, len(result.Variables))

	for i, variable := range result.Variables {
		log.Debugf("result: %#v", variable)

		var value interface{}

		if variable.Value == nil {
			value = nil
		} else {
			switch variable.Type {
			case gosnmp.Boolean:
				value = variable.Value.(bool)
			case gosnmp.Integer:
				value = variable.Value.(int32)
			case gosnmp.OctetString, gosnmp.BitString:
				value = variable.Value.([]byte)
			case gosnmp.Null:
				value = nil
			case gosnmp.ObjectDescription:
				value = variable.Value.(string)
			case gosnmp.IPAddress:
				value = variable.Value.(string)
			case gosnmp.Counter32, gosnmp.Gauge32, gosnmp.Uinteger32:
				value = variable.Value.(uint32)
			case gosnmp.TimeTicks:
				value = variable.Value.(uint)
			case gosnmp.OpaqueFloat:
				value = variable.Value.(float32)
			case gosnmp.OpaqueDouble:
				value = variable.Value.(float64)
			case gosnmp.NsapAddress:
				if len(variable.Value.([]byte)) == 1 && (variable.Value.([]byte)[0]) == 0 {
					value = "0"
				} else {
					value = variable.Value.(string)
				}
			case gosnmp.Counter64:
				value = variable.Value.(uint64)
			case gosnmp.NoSuchObject, gosnmp.NoSuchInstance, gosnmp.EndOfMibView, gosnmp.EndOfContents:
				log.Warningf("Value type is an error: %v", variable)
				continue
			default:
				log.Warningf("Unable to parse value type: %v", variable)
				continue
			}
		}

		res[i] = &Result{
			Fields: map[string]interface{}{
				SNMPResultValue: value,
			},
			Tags: map[string]string{
				SNMPTagOID: variable.Name,
			},
		}
	}

	return res, nil
}
