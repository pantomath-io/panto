// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"
	"time"
)

// HTTP is a probe that sends an HTTP request
// and gathers data on the response.
type HTTP struct {
	req     *http.Request
	timeout time.Duration
	content string
}

const (
	// HTTPResponseContentNone : No content response
	HTTPResponseContentNone = "none"
	// HTTPResponseContentHeadersOnly : Only send HTTP headers from response
	HTTPResponseContentHeadersOnly = "headers-only"
	// HTTPResponseContentBodyOnly : Only send HTTP body from response
	HTTPResponseContentBodyOnly = "body-only"
	// HTTPResponseContentFull : Send complete HTTP response
	HTTPResponseContentFull = "full"
)

const (
	// HTTP configuration parameters

	// HTTPParamMethod (optional, default `"GET"`) : HTTP request method (`"GET"`, `"POST"`, etc.)
	HTTPParamMethod = "method"
	// HTTPParamURL : HTTP request URL (`string`)
	HTTPParamURL = "url"
	// HTTPParamBody (optional) : HTTP Body of the request (`string`)
	HTTPParamBody = "body"
	// HTTPParamTimeout (optional) : duration before the HTTP request times out (`time.Duration`)
	HTTPParamTimeout = "timeout"
	// HTTPParamResponseContent : content from the response to be submitted in the results (`string`)
	HTTPParamResponseContent = "response-content"

	// HTTP results

	// HTTPResultRoundTripTime : time between sending the HTTP request and receiving the response, in nanoseconds
	// (`int64`)
	HTTPResultRoundTripTime = "rtt"
	// HTTPResultStatusCode : the HTTP response's status code (`int`)
	HTTPResultStatusCode = "status-code"
	// HTTPResultResponseContent : the HTTP response's content (depending on the probe's configuration) (`string`)
	HTTPResultResponseContent = "response-content"
)

// Name returns the name of the probe "ping"
func (h *HTTP) Name() string {
	return "http"
}

// Configure configures the HTTP probe and prepares the
// HTTP request.
func (h *HTTP) Configure(config map[string]interface{}) error {
	h.req = nil

	// Check for optional method field
	var method string
	_, ok := config[HTTPParamMethod]
	if !ok {
		method = http.MethodGet
	} else {
		method, ok = config[HTTPParamMethod].(string)
		if !ok {
			return fmt.Errorf("\"%s\" field is not string: %v", HTTPParamMethod, config[HTTPParamMethod])
		}
	}

	// Check for mandatory URL field
	var url string
	_, ok = config[HTTPParamURL]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", HTTPParamURL)
	}
	url, ok = config[HTTPParamURL].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", HTTPParamURL, config[HTTPParamURL])
	}

	// Check for optional method field
	var body string
	_, ok = config[HTTPParamBody]
	if ok {
		body, ok = config[HTTPParamBody].(string)
		if !ok {
			return fmt.Errorf("\"%s\" field is not bytes.Buffer: %v", HTTPParamBody, config[HTTPParamBody])
		}
	}

	// Check for optional timeout
	_, ok = config[HTTPParamTimeout]
	if !ok {
		h.timeout = 0
	} else {
		t, ok := config[HTTPParamTimeout].(float64)
		if !ok {
			return fmt.Errorf("\"%s\" field is not time.Duration: (%T)%v", HTTPParamTimeout, config[HTTPParamTimeout], config[HTTPParamTimeout])
		}
		h.timeout = time.Duration(t)
	}

	// Check for optional response content parameter
	_, ok = config[HTTPParamResponseContent]
	if !ok {
		h.content = HTTPResponseContentNone
	} else {
		h.content, ok = config[HTTPParamResponseContent].(string)
		if !ok {
			return fmt.Errorf("\"%s\" field is not string: %v", HTTPParamResponseContent, config[HTTPParamResponseContent])
		}
	}

	var err error
	if body == "" {
		h.req, err = http.NewRequest(method, url, nil)
	} else {
		h.req, err = http.NewRequest(method, url, strings.NewReader(body))
	}
	if err != nil {
		return err
	}

	return nil
}

// Execute sends an HTTP request to the target
// and synchronously waits for the reply.
func (h *HTTP) Execute() ([]*Result, error) {
	client := &http.Client{Timeout: h.timeout}
	defer client.CloseIdleConnections()
	if h.req == nil {
		return nil, fmt.Errorf("probe has no HTTP request, probably from an invalid configuration")
	}

	log.Debugf("Performing HTTP request: %s %s", h.req.Method, h.req.URL)
	begin := time.Now()
	res, err := client.Do(h.req)
	end := time.Now()

	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	rtt := end.Sub(begin)

	result := map[string]interface{}{
		HTTPResultRoundTripTime: rtt.Nanoseconds(),
		HTTPResultStatusCode:    res.StatusCode,
	}

	switch h.content {
	case HTTPResponseContentNone:
		result[HTTPResultResponseContent] = ""

	case HTTPResponseContentHeadersOnly:
		var contentBuf bytes.Buffer
		res.Header.Write(&contentBuf)
		result[HTTPResultResponseContent] = contentBuf.String()

	case HTTPResponseContentBodyOnly:
		var contentBuf bytes.Buffer
		contentBuf.ReadFrom(res.Body)
		result[HTTPResultResponseContent] = contentBuf.String()

	case HTTPResponseContentFull:
		var contentBuf bytes.Buffer
		res.Write(&contentBuf)
		result[HTTPResultResponseContent] = contentBuf.String()

	default:
		return nil, fmt.Errorf("%s configuration parameter is missing", HTTPResultResponseContent)
	}

	return []*Result{{Fields: result}}, nil
}
