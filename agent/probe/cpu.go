// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"
	"strconv"
	"time"

	"github.com/shirou/gopsutil/cpu"
)

//-----------
// CPU Probe
//-----------

// CPU probes CPU usage
type CPU struct {
	perCPU   bool
	interval time.Duration
}

const (

	// CPU configuration parameters

	// CPUParamPerCPU : (optional, default `true`) set to `false` to collect utilization % averaged over all CPUs/cores (`bool`)
	CPUParamPerCPU = "per-cpu"
	// CPUParamInterval : (optional, default 1 second) interval over which CPU utilization is calculated
	CPUParamInterval = "interval"

	// CPU result fields

	// CPUResultBusy :  % of time spent running (sum of all "non-idle" times) (`float32`)
	CPUResultBusy = "busy"
	// CPUResultIdle : % of time spent idle (`float32`)
	CPUResultIdle = "idle"
	// CPUResultUser : % of time spent in user mode (`float32`)
	CPUResultUser = "user"
	// CPUResultSystem : % of time spent in system mode, which is the time spent executing kernel code (`float32`)
	CPUResultSystem = "system"
	// CPUResultNice : % of time spent in user mode with low priority (nice) (`float32`)
	CPUResultNice = "nice"
	// CPUResultIowait : % of time waiting for I/O to complete (`float32`)
	CPUResultIowait = "iowait"
	// CPUResultIrq : % of time servicing interrupts (`float32`)
	CPUResultIrq = "irq"
	// CPUResultSoftirq : % of time servicing softirqs (`float32`)
	CPUResultSoftirq = "softirq"

	// CPU result tags

	// CPUTagCPU : name of the CPU result, "all" for average over all CPUs/cores
	CPUTagCPU = "cpu"
)

// Name returns the name of the probe "cpu"
func (c *CPU) Name() string {
	return "cpu"
}

// Configure configures the CPU probe
func (c *CPU) Configure(config map[string]interface{}) error {
	// Check for optional  argument
	_, ok := config[CPUParamPerCPU]
	if !ok {
		c.perCPU = true
	} else {
		c.perCPU, ok = config[CPUParamPerCPU].(bool)
		if !ok {
			return fmt.Errorf("\"%s\" field is not bool: %v", CPUParamPerCPU, config[CPUParamPerCPU])
		}
	}

	// Check for optional interval argument
	interval, ok := config[CPUParamInterval]
	if ok {
		switch i := interval.(type) {
		case float64:
			interval = time.Duration(i)
		}
		c.interval, ok = interval.(time.Duration)
		if !ok {
			return fmt.Errorf("\"%s\" field is not time.Duration: %v", CPUParamInterval, config[CPUParamInterval])
		}
	}
	if c.interval <= 0 {
		c.interval = 1 * time.Second
	}

	return nil
}

// Execute runs the CPU probe and returns details on the CPU utilization
func (c *CPU) Execute() ([]*Result, error) {
	cpuTimesStart, err := cpu.Times(c.perCPU)
	if err != nil {
		return nil, err
	}
	log.Debugf("CPU probe gathered interval start info: %s", cpuTimesStart)

	time.Sleep(c.interval)

	cpuTimesEnd, err := cpu.Times(c.perCPU)
	if err != nil {
		return nil, err
	}
	log.Debugf("CPU probe gathered interval end info: %s", cpuTimesEnd)

	if len(cpuTimesStart) != len(cpuTimesEnd) {
		return nil, fmt.Errorf(
			"length mismatch: received %d CPU times at start of interval, and %d at end",
			len(cpuTimesStart),
			len(cpuTimesEnd),
		)
	}

	cpuTimes := make([]cpu.TimesStat, len(cpuTimesStart))
	for i := 0; i < len(cpuTimesStart); i++ {
		if cpuTimes[i].CPU == "" {
			cpuTimes[i].CPU = strconv.Itoa(i)
		}
		cpuTimes[i].User = cpuTimesEnd[i].User - cpuTimesStart[i].User
		cpuTimes[i].System = cpuTimesEnd[i].System - cpuTimesStart[i].System
		cpuTimes[i].Idle = cpuTimesEnd[i].Idle - cpuTimesStart[i].Idle
		cpuTimes[i].Nice = cpuTimesEnd[i].Nice - cpuTimesStart[i].Nice
		cpuTimes[i].Iowait = cpuTimesEnd[i].Iowait - cpuTimesStart[i].Iowait
		cpuTimes[i].Irq = cpuTimesEnd[i].Irq - cpuTimesStart[i].Irq
		cpuTimes[i].Softirq = cpuTimesEnd[i].Softirq - cpuTimesStart[i].Softirq
		cpuTimes[i].Steal = cpuTimesEnd[i].Steal - cpuTimesStart[i].Steal
		cpuTimes[i].Guest = cpuTimesEnd[i].Guest - cpuTimesStart[i].Guest
		cpuTimes[i].GuestNice = cpuTimesEnd[i].GuestNice - cpuTimesStart[i].GuestNice
		cpuTimes[i].Stolen = cpuTimesEnd[i].Stolen - cpuTimesStart[i].Stolen
	}

	log.Debugf("CPU probe gathered info over interval: %s", cpuTimes)

	res := make([]*Result, len(cpuTimes))
	if !c.perCPU {
		t := cpuTimes[0]
		total := t.Total()
		res[0] = &Result{
			Fields: map[string]interface{}{
				CPUResultBusy:    float32(1-t.Idle/total) * 100,
				CPUResultUser:    float32(t.User/total) * 100,
				CPUResultSystem:  float32(t.System/total) * 100,
				CPUResultIdle:    float32(t.Idle/total) * 100,
				CPUResultNice:    float32(t.Nice/total) * 100,
				CPUResultIowait:  float32(t.Iowait/total) * 100,
				CPUResultIrq:     float32(t.Irq/total) * 100,
				CPUResultSoftirq: float32(t.Softirq/total) * 100,
			},
			Tags: map[string]string{
				CPUTagCPU: "all",
			},
		}
	} else {
		for i, t := range cpuTimes {
			total := t.Total()
			res[i] = &Result{
				Fields: map[string]interface{}{
					CPUResultBusy:    float32(1-t.Idle/total) * 100,
					CPUResultUser:    float32(t.User/total) * 100,
					CPUResultSystem:  float32(t.System/total) * 100,
					CPUResultIdle:    float32(t.Idle/total) * 100,
					CPUResultNice:    float32(t.Nice/total) * 100,
					CPUResultIowait:  float32(t.Iowait/total) * 100,
					CPUResultIrq:     float32(t.Irq/total) * 100,
					CPUResultSoftirq: float32(t.Softirq/total) * 100,
				},
				Tags: map[string]string{
					CPUTagCPU: t.CPU,
				},
			}
		}
	}

	log.Debugf("CPU probe returning reults: %s", res)

	return res, nil
}
