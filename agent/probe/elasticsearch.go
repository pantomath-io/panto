// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// Elasticsearch probes a elasticsearch store
type Elasticsearch struct {
	timeout time.Duration
	req     map[string]*http.Request
}

const (
	// ElasticsearchURLClusterHealth : very simple status on the health of the cluster
	ElasticsearchURLClusterHealth = "cluster_health"
	// ElasticsearchURLClusterStats : statistics from a cluster wide perspective
	ElasticsearchURLClusterStats = "cluster_stats"
)

// ElasticsearchURLs is the list of endpoints to request
var ElasticsearchURLs = map[string]string{
	ElasticsearchURLClusterHealth: "_cluster/health",
	ElasticsearchURLClusterStats:  "_cluster/stats",
}

const (

	// Elasticsearch configuration parameters

	// ElasticsearchParamAddress : (mandatory) the address of the target elasticsearch server to gather metric from (string)
	ElasticsearchParamAddress = "address"
	// ElasticsearchParamTimeout (optional) : duration before the HTTP request times out (default value: 0)(`time.Duration`)
	ElasticsearchParamTimeout = "timeout"

	// Elasticsearch results

	// ElasticsearchResultClusterName : ES cluster name (string)
	ElasticsearchResultClusterName = "cluster-name"
	// ElasticsearchResultStatus : Health level of the cluster (string)
	ElasticsearchResultStatus = "status"
	// ElasticsearchResultTimedOut : (boolean)
	ElasticsearchResultTimedOut = "timed-out"
	// ElasticsearchResultNumberOfNodes : Number of nodes in the cluster (float64)
	ElasticsearchResultNumberOfNodes = "number-of-nodes"
	// ElasticsearchResultNumberOfDataNodes : Number of data nodes in the cluster (float64)
	ElasticsearchResultNumberOfDataNodes = "number-of-data-nodes"
	// ElasticsearchResultActivePrimaryShards : Number of primary shards in the cluster (float64)
	ElasticsearchResultActivePrimaryShards = "active-primary-shards"
	// ElasticsearchResultActiveShards : Number of active shards in the cluster (float64)
	ElasticsearchResultActiveShards = "active-shards"
	// ElasticsearchResultRelocatingShards : Number of shards being relocated in the cluster (float64)
	ElasticsearchResultRelocatingShards = "relocating-shards"
	// ElasticsearchResultInitializingShards : Number of shards being initialized in the cluster (float64)
	ElasticsearchResultInitializingShards = "initializing-shards"
	// ElasticsearchResultUnassignedShards : Number of shards currently unassigned in the cluster (float64)
	ElasticsearchResultUnassignedShards = "unassigned-shards"
	// ElasticsearchResultDelayedUnassignedShards : Number of unassigned shards which allocation is delayed in the cluster (float64)
	ElasticsearchResultDelayedUnassignedShards = "delayed-unassigned-shards"
	// ElasticsearchResultNumberOfPendingTasks : Number of pending task in the cluster (float64)
	ElasticsearchResultNumberOfPendingTasks = "number-of-pending-tasks"
	// ElasticsearchResultNumberOfInFlightFetch : Number of in-flight fetch operations (float64)
	ElasticsearchResultNumberOfInFlightFetch = "number-of-in-flight-fetch"
	// ElasticsearchResultTaskMaxWaitingInQueueMillis : Maximum number of milliseconds a task is waiting in queue (float64)
	ElasticsearchResultTaskMaxWaitingInQueueMillis = "task-max-waiting-in-queue-millis"
	// ElasticsearchResultIndicesCount : Count of indices in the cluster (float64)
	ElasticsearchResultIndicesCount = "indices-count"
	// ElasticsearchResultDocsCount : Count of documents in the cluster (float64)
	ElasticsearchResultDocsCount = "docs-count"
	// ElasticsearchResultDocsDeleted : Count of deleted documents in the cluster (float64)
	ElasticsearchResultDocsDeleted = "docs-deleted"
	// ElasticsearchResultStoreSize : Size of the cluster storage (float64)
	ElasticsearchResultStoreSize = "store-size"
	// ElasticsearchResultQCMemory : Memory allocated to the cluster query cache(float64)
	ElasticsearchResultQCMemory = "querycache-memory"
	// ElasticsearchResultQCCountHit : Number of hits on the cluster query cache (float64)
	ElasticsearchResultQCCountHit = "querycache-count-hit"
	// ElasticsearchResultQCCountMiss : Number of miss on the cluster query cache (float64)
	ElasticsearchResultQCCountMiss = "querycache-count-miss"
	// ElasticsearchResultQCCountCache : Number of cache operations on the cluster query cache(float64)
	ElasticsearchResultQCCountCache = "querycache-count-cache"
	// ElasticsearchResultQCEvictions : Number of evictions from the cluster query cache (float64)
	ElasticsearchResultQCEvictions = "querycache-evictions"
	// ElasticsearchResultJVMHeapMax : JVM heap max size on the cluster (float64)
	ElasticsearchResultJVMHeapMax = "jvm-heap-max"
	// ElasticsearchResultJVMHeapUsed : JVM heap used size on the cluster (float64)
	ElasticsearchResultJVMHeapUsed = "jvm-heap-used"
	// ElasticsearchResultJVMThreads : Number of JVM threads on the cluster (float64)
	ElasticsearchResultJVMThreads = "jvm-threads"
)

// Name returns the name of the probe "elasticsearch"
func (es *Elasticsearch) Name() string {
	return "elasticsearch"
}

// Configure configures the elasticsearch probe
func (es *Elasticsearch) Configure(config map[string]interface{}) (err error) {
	es.req = make(map[string]*http.Request)

	// Check for mandatory address field
	var address, url string
	_, ok := config[ElasticsearchParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", ElasticsearchParamAddress)
	}
	address, ok = config[ElasticsearchParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", ElasticsearchParamAddress, config[ElasticsearchParamAddress])
	}

	for k, uri := range ElasticsearchURLs {
		// build the health check URL
		url = fmt.Sprintf("http://%s/%s", address, uri)

		es.req[k], err = http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			log.Errorf("unable to create GET request: %s", err)
		}
	}

	// Check for optional timeout
	_, ok = config[ElasticsearchParamTimeout]
	if !ok {
		es.timeout = 0
	} else {
		es.timeout, ok = config[ElasticsearchParamTimeout].(time.Duration)
		if !ok {
			return fmt.Errorf("\"%s\" field is not time.Duration: %v", ElasticsearchParamTimeout, config[ElasticsearchParamTimeout])
		}
	}

	return nil
}

// Execute runs the probe and gathers information about the elasticsearch server
func (es *Elasticsearch) Execute() ([]*Result, error) {
	fields := make(map[string]interface{})

	client := &http.Client{Timeout: es.timeout}
	defer client.CloseIdleConnections()
	if len(es.req) == 0 {
		return nil, fmt.Errorf("probe has no HTTP request, probably from an invalid configuration")
	}

	for k, req := range es.req {
		log.Debugf("Performing Elasticsearch request: %s %s", req.Method, req.URL)
		res, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return nil, fmt.Errorf("unable to get cluster status result: %v", err)
		}
		res.Body.Close()

		stats, err := scanElasticsearchStats(body)
		if err != nil {
			return nil, fmt.Errorf("unable to parse output: %v", err)
		}

		gatherElasticsearchFields(fields, k, stats)
	}

	log.Debugf("returned result: %v", fields)
	return []*Result{{Fields: fields}}, nil
}

func scanElasticsearchStats(r []byte) (stats map[string]interface{}, err error) {
	stats = make(map[string]interface{})

	if err = json.Unmarshal(r, &stats); err != nil {
		return nil, fmt.Errorf("couldn't unmarshal cluster status: %s", err)
	}

	return
}

func gatherElasticsearchFields(fields map[string]interface{}, key string, stats map[string]interface{}) {

	switch key {
	case ElasticsearchURLClusterHealth:
		if s, ok := stats["cluster_name"]; ok {
			if n, ok := s.(string); ok {
				fields[ElasticsearchResultClusterName] = n
			}
		}
		if s, ok := stats["status"]; ok {
			if n, ok := s.(string); ok {
				fields[ElasticsearchResultStatus] = n
			}
		}
		if s, ok := stats["timed_out"]; ok {
			if n, ok := s.(bool); ok {
				fields[ElasticsearchResultTimedOut] = n
			}
		}
		if s, ok := stats["number_of_nodes"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultNumberOfNodes] = n
			}
		}
		if s, ok := stats["number_of_data_nodes"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultNumberOfDataNodes] = n
			}
		}
		if s, ok := stats["active_primary_shards"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultActivePrimaryShards] = n
			}
		}
		if s, ok := stats["active_shards"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultActiveShards] = n
			}
		}
		if s, ok := stats["relocating_shards"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultRelocatingShards] = n
			}
		}
		if s, ok := stats["initializing_shards"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultInitializingShards] = n
			}
		}
		if s, ok := stats["unassigned_shards"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultUnassignedShards] = n
			}
		}
		if s, ok := stats["delayed_unassigned_shards"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultDelayedUnassignedShards] = n
			}
		}
		if s, ok := stats["number_of_pending_tasks"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultNumberOfPendingTasks] = n
			}
		}
		if s, ok := stats["number_of_in_flight_fetch"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultNumberOfInFlightFetch] = n
			}
		}
		if s, ok := stats["task_max_waiting_in_queue_millis"]; ok {
			if n, ok := s.(float64); ok {
				fields[ElasticsearchResultTaskMaxWaitingInQueueMillis] = n
			}
		}

	case ElasticsearchURLClusterStats:
		if i, ok := stats["indices"]; ok {
			if m, ok := i.(map[string]interface{}); ok {
				if val, ok := m["count"]; ok {
					if n, ok := val.(float64); ok {
						fields[ElasticsearchResultIndicesCount] = n
					}
				}
				if val, ok := m["docs"]; ok {
					if m, ok := val.(map[string]interface{}); ok {
						if val, ok := m["count"]; ok {
							if n, ok := val.(float64); ok {
								fields[ElasticsearchResultDocsCount] = n
							}
						}
						if val, ok := m["deleted"]; ok {
							if n, ok := val.(float64); ok {
								fields[ElasticsearchResultDocsDeleted] = n
							}
						}
					}
				}
				if val, ok := m["store"]; ok {
					if m, ok := val.(map[string]interface{}); ok {
						if val, ok := m["size_in_bytes"]; ok {
							if n, ok := val.(float64); ok {
								fields[ElasticsearchResultStoreSize] = n
							}
						}
					}
				}
				if val, ok := m["query_cache"]; ok {
					if m, ok := val.(map[string]interface{}); ok {
						if val, ok := m["memory_size_in_bytes"]; ok {
							if n, ok := val.(float64); ok {
								fields[ElasticsearchResultQCMemory] = n
							}
						}
						if val, ok := m["hit_count"]; ok {
							if n, ok := val.(float64); ok {
								fields[ElasticsearchResultQCCountHit] = n
							}
						}
						if val, ok := m["miss_count"]; ok {
							if n, ok := val.(float64); ok {
								fields[ElasticsearchResultQCCountMiss] = n
							}
						}
						if val, ok := m["cache_count"]; ok {
							if n, ok := val.(float64); ok {
								fields[ElasticsearchResultQCCountCache] = n
							}
						}
						if val, ok := m["evictions"]; ok {
							if n, ok := val.(float64); ok {
								fields[ElasticsearchResultQCEvictions] = n
							}
						}
					}
				}
			}
		}
		if i, ok := stats["nodes"]; ok {
			if m, ok := i.(map[string]interface{}); ok {
				if val, ok := m["jvm"]; ok {
					if m, ok := val.(map[string]interface{}); ok {
						if val, ok := m["mem"]; ok {
							if m, ok := val.(map[string]interface{}); ok {
								if val, ok := m["heap_used_in_bytes"]; ok {
									if n, ok := val.(float64); ok {
										fields[ElasticsearchResultJVMHeapUsed] = n
									}
								}
								if val, ok := m["heap_max_in_bytes"]; ok {
									if n, ok := val.(float64); ok {
										fields[ElasticsearchResultJVMHeapMax] = n
									}
								}
							}
						}
						if val, ok := m["threads"]; ok {
							if n, ok := val.(float64); ok {
								fields[ElasticsearchResultJVMThreads] = n
							}
						}
					}
				}
			}
		}
	}
}
