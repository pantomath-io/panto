// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"bytes"
	"io"
	"reflect"
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Docker)(nil)

func TestDockerName(t *testing.T) {
	r := Docker{}
	if r.Name() != "docker" {
		t.Error("Docker probe name is not \"docker\"")
	}
}

func TestDockerConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Docker{}
	err := r.Configure(map[string]interface{}{DockerParamAddress: viper.GetString("docker.address")})
	if err != nil {
		t.Errorf("couldn't configure Docker probe: %s", err)
	}
}

func TestDockerExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Docker{}

	err := r.Configure(map[string]interface{}{DockerParamAddress: viper.GetString("docker.address")})
	if err != nil {
		t.Fatalf("couldn't configure Docker probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Docker probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}

	expectedKeys := map[string]interface{}{
		DockerResultContainerCountCreated:    reflect.Int64,
		DockerResultContainerCountRunning:    reflect.Int64,
		DockerResultContainerCountPaused:     reflect.Int64,
		DockerResultContainerCountRestarting: reflect.Int64,
		DockerResultContainerCountRemoving:   reflect.Int64,
		DockerResultContainerCountExited:     reflect.Int64,
		DockerResultContainerCountDead:       reflect.Int64,
	}
	for _, result := range res {
		for k, field := range result.Fields {
			kind, ok := expectedKeys[k]
			if !ok {
				continue
			}
			if kind != reflect.TypeOf(field).Kind() {
				t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
				continue
			}
		}
	}
}

type nopCloser struct {
	io.Reader
}

func (nopCloser) Close() error { return nil }

func TestDockerParseContainerStats(t *testing.T) {
	stats, err := parseContainerStats(nopCloser{bytes.NewBufferString(testDockerStat)})
	if err != nil {
		t.Errorf("unable to parse container stats: %s", err)
	}

	t.Logf("%v", stats)

	// merge all the results in a single map
	fields := make(map[string]interface{}, 0)
	for _, result := range stats {
		for k, v := range result.Fields {
			fields[k] = v
		}
	}

	expectedKeys := map[string]interface{}{
		DockerResultContainerCPUTotalUsage:          reflect.Float64,
		DockerResultContainerMemorylUsage:           reflect.Float64,
		DockerResultContainerNetworkErrorReceived:   reflect.Float64,
		DockerResultContainerNetworkReceived:        reflect.Float64,
		DockerResultContainerNetworkDroppedReceived: reflect.Float64,
		DockerResultContainerNetworkSent:            reflect.Float64,
		DockerResultContainerNetworkErrorSent:       reflect.Float64,
		DockerResultContainerNetworkDroppedSent:     reflect.Float64,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in docker results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in docker results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

// A mock of a docker container stats
const testDockerStat = `{
    "read": "2018-10-11T15:37:30.213467Z",
    "preread": "2018-10-11T15:37:29.2069851Z",
    "pids_stats": {
        "current": 12
    },
    "blkio_stats": {
        "io_service_bytes_recursive": [
            {
                "major": 8,
                "minor": 0,
                "op": "Read",
                "value": 18411520
            },
            {
                "major": 8,
                "minor": 0,
                "op": "Write",
                "value": 568520704
            },
            {
                "major": 8,
                "minor": 0,
                "op": "Sync",
                "value": 568520704
            },
            {
                "major": 8,
                "minor": 0,
                "op": "Async",
                "value": 18411520
            },
            {
                "major": 8,
                "minor": 0,
                "op": "Total",
                "value": 586932224
            }
        ],
        "io_serviced_recursive": [
            {
                "major": 8,
                "minor": 0,
                "op": "Read",
                "value": 247
            },
            {
                "major": 8,
                "minor": 0,
                "op": "Write",
                "value": 55173
            },
            {
                "major": 8,
                "minor": 0,
                "op": "Sync",
                "value": 55173
            },
            {
                "major": 8,
                "minor": 0,
                "op": "Async",
                "value": 247
            },
            {
                "major": 8,
                "minor": 0,
                "op": "Total",
                "value": 55420
            }
        ],
        "io_queue_recursive": [],
        "io_service_time_recursive": [],
        "io_wait_time_recursive": [],
        "io_merged_recursive": [],
        "io_time_recursive": [],
        "sectors_recursive": []
    },
    "num_procs": 0,
    "storage_stats": {},
    "cpu_stats": {
        "cpu_usage": {
            "total_usage": 1262719254695,
            "percpu_usage": [
                628187352591,
                634531902104
            ],
            "usage_in_kernelmode": 420970000000,
            "usage_in_usermode": 468130000000
        },
        "system_cpu_usage": 876783980000000,
        "online_cpus": 2,
        "throttling_data": {
            "periods": 0,
            "throttled_periods": 0,
            "throttled_time": 0
        }
    },
    "precpu_stats": {
        "cpu_usage": {
            "total_usage": 1262718101819,
            "percpu_usage": [
                628186289214,
                634531812605
            ],
            "usage_in_kernelmode": 420970000000,
            "usage_in_usermode": 468130000000
        },
        "system_cpu_usage": 876781980000000,
        "online_cpus": 2,
        "throttling_data": {
            "periods": 0,
            "throttled_periods": 0,
            "throttled_time": 0
        }
    },
    "memory_stats": {
        "usage": 48242688,
        "max_usage": 165122048,
        "stats": {
            "active_anon": 19853312,
            "active_file": 2695168,
            "cache": 23810048,
            "dirty": 8192,
            "hierarchical_memory_limit": 9223372036854771712,
            "hierarchical_memsw_limit": 9223372036854771712,
            "inactive_anon": 3407872,
            "inactive_file": 21114880,
            "mapped_file": 14741504,
            "pgfault": 291946,
            "pgmajfault": 157,
            "pgpgin": 369131,
            "pgpgout": 358150,
            "rss": 23261184,
            "rss_huge": 2097152,
            "total_active_anon": 19853312,
            "total_active_file": 2695168,
            "total_cache": 23810048,
            "total_dirty": 8192,
            "total_inactive_anon": 3407872,
            "total_inactive_file": 21114880,
            "total_mapped_file": 14741504,
            "total_pgfault": 291946,
            "total_pgmajfault": 157,
            "total_pgpgin": 369131,
            "total_pgpgout": 358150,
            "total_rss": 23261184,
            "total_rss_huge": 2097152,
            "total_unevictable": 0,
            "total_writeback": 0,
            "unevictable": 0,
            "writeback": 0
        },
        "limit": 2096058368
    },
    "name": "/goofy_hodgkin",
    "id": "9cf1cc9df4e6ad3a3a781d84a8e08b6861acc7bfa8fc15e60e20a9946ab4981c",
    "networks": {
        "eth0": {
            "rx_bytes": 16683477,
            "rx_packets": 66303,
            "rx_errors": 0,
            "rx_dropped": 0,
            "tx_bytes": 21940773,
            "tx_packets": 61266,
            "tx_errors": 0,
            "tx_dropped": 0
        }
    }
}
`
