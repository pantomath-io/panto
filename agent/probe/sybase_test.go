// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if Sybase probe do not conform to Probe interface
var _ Probe = (*Sybase)(nil)

func TestSybaseName(t *testing.T) {
	r := Sybase{}
	if r.Name() != "sybase" {
		t.Error("Sybase probe name is not \"sybase\"")
	}
}

func TestSybaseConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Sybase{}
	err := r.Configure(map[string]interface{}{
		SybaseParamAddress:  viper.GetString("sybase.address"),
		SybaseParamLogin:    viper.GetString("sybase.login"),
		SybaseParamPassword: viper.GetString("sybase.password"),
	})
	if err != nil {
		t.Errorf("couldn't configure Sybase probe: %s", err)
	}
}

func TestSybaseExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Sybase{}

	err := r.Configure(map[string]interface{}{
		SybaseParamAddress:  viper.GetString("sybase.address"),
		SybaseParamLogin:    viper.GetString("sybase.login"),
		SybaseParamPassword: viper.GetString("sybase.password"),
	})
	if err != nil {
		t.Fatalf("couldn't configure Sybase probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Sybase probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}
}
