// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"
	"time"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*DiskIO)(nil)

func TestDiskIOName(t *testing.T) {
	d := DiskIO{}
	if d.Name() != "disk-io" {
		t.Error("Disk Usage probe name is not \"disk-io\"")
	}
}

func TestDiskIOConfigure(t *testing.T) {
	disk := DiskIO{}

	config := map[string]interface{}{}

	err := disk.Configure(config)
	if err != nil {
		t.Fatalf("couldn't configure Disk IO probe: %s", err)
	}
	expectInterval := 1 * time.Second
	if disk.interval != expectInterval {
		t.Fatalf("default value for interval is %s, expected %s", disk.interval, expectInterval)
	}

	config = map[string]interface{}{
		DiskIOParamInterval: 42 * time.Millisecond,
	}

	err = disk.Configure(config)
	if err != nil {
		t.Fatalf("couldn't configure Disk probe: %s", err)
	}
	if disk.interval != config[DiskIOParamInterval] {
		t.Fatalf("invalid \"interval\" value: expected %s, got %s", config[DiskIOParamInterval], disk.interval)
	}
}

func TestDiskIOExecute(t *testing.T) {
	d := DiskIO{}

	res, err := d.Execute()
	if err != nil {
		t.Errorf("couldn't execute Disk I/O probe: %s", err)
	}
	if len(res) == 0 {
		t.Errorf("no items in results")
	}

	expectedKeys := map[string]interface{}{
		DiskIOResultIops:         reflect.Float32,
		DiskIOResultBytesRead:    reflect.Float32,
		DiskIOResultBytesWritten: reflect.Float32,
		DiskIOResultLatency:      reflect.Int64,
	}
	for _, r := range res {
		for k := range expectedKeys {
			if _, ok := r.Fields[k]; !ok {
				t.Errorf("missing key \"%s\" in load results", k)
			}
		}
		for k, field := range r.Fields {
			kind, ok := expectedKeys[k]
			if !ok {
				t.Errorf("unexpected key \"%s\" in load results", k)
				continue
			}
			if kind != reflect.TypeOf(field).Kind() {
				t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
				continue
			}
		}

		dev, ok := r.Tags[DiskIOTagDevice]
		if !ok {
			t.Errorf("Missing tag %s", DiskIOTagDevice)
		}
		if dev == "" {
			t.Errorf("Empty disk device")
		}
	}
}
