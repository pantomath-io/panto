// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"
	"time"
)

// Compile error if Dummy does not conform to Probe interface
var _ Probe = (*Ping)(nil)

func TestPingName(t *testing.T) {
	p := &Ping{}

	if p.Name() != "ping" {
		t.Error("ping probe name is not \"ping\"")
	}
}

func TestPingConfigure(t *testing.T) {
	p := Ping{}

	err := p.Configure(map[string]interface{}{})
	if err == nil {
		t.Errorf("Unexpected success when configuring ping probe.")
	}

	err = p.Configure(map[string]interface{}{
		PingParamAddress: "127.0.0.1",
	})
	if err != nil {
		t.Errorf("Failed to configure ping probe: %s", err)
	}

	err = p.Configure(map[string]interface{}{
		PingParamAddress: 12345,
	})
	if err == nil {
		t.Errorf("Unexpected success when configuring ping probe.")
	}

	err = p.Configure(map[string]interface{}{
		PingParamAddress:  "127.0.0.1",
		PingParamInterval: 1 * time.Second,
		PingParamCount:    5,
		PingParamTimeout:  30 * time.Second,
	})
	if err != nil {
		t.Errorf("Failed to configure ping probe: %s", err)
	}
}

func TestPingExecute(t *testing.T) {
	p := Ping{}

	err := p.Configure(map[string]interface{}{
		PingParamAddress: "127.0.0.1",
	})
	if err != nil {
		t.Errorf("Failed to configure ping probe: %s", err)
	}

	res, err := p.Execute()
	if err != nil {
		t.Errorf("Failed to execute ping probe: %s", err)
	}
	if len(res) != 1 {
		t.Errorf("Unexpected results count: got %d, expected %d", len(res), 1)
	} else {
		expectedKeys := map[string]interface{}{
			PingResultSent:   reflect.Int,
			PingResultRecv:   reflect.Int,
			PingResultMin:    reflect.Int64,
			PingResultMax:    reflect.Int64,
			PingResultAvg:    reflect.Int64,
			PingResultStdDev: reflect.Int64,
		}
		for k := range expectedKeys {
			if _, ok := res[0].Fields[k]; !ok {
				t.Errorf("missing key \"%s\" in checksum results", k)
			}
		}
		for k, field := range res[0].Fields {
			kind, ok := expectedKeys[k]
			if !ok {
				t.Errorf("unexpected key \"%s\" in checksum results", k)
				continue
			}
			if kind != reflect.TypeOf(field).Kind() {
				t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
				continue
			}
		}
	}
	for _, r := range res {
		t.Logf("%v", *r)
	}

	err = p.Configure(map[string]interface{}{
		PingParamAddress:  "8.8.8.8",
		PingParamInterval: 900 * time.Millisecond,
		PingParamCount:    10,
		PingParamTimeout:  60 * time.Second,
	})
	if err != nil {
		t.Errorf("Failed to configure ping probe: %s", err)
	}

	res, err = p.Execute()
	if err != nil {
		t.Errorf("Failed to execute ping probe: %s", err)
	}
	for _, r := range res {
		t.Logf("%v", *r)
	}
}

// Non-regression test for issue #165
func TestPingRepeated(t *testing.T) {
	p := Ping{}

	err := p.Configure(map[string]interface{}{
		PingParamAddress: "127.0.0.1",
	})
	if err != nil {
		t.Fatalf("Failed to configure ping probe: %s", err)
	}

	res1, err := p.Execute()
	if err != nil {
		t.Fatalf("Failed to execute ping probe: %s", err)
	}
	if len(res1) != 1 {
		t.Fatalf("Unexpected results count: got %d, expected %d", len(res1), 1)
	}
	if res1[0].Fields[PingResultSent] == 0 {
		t.Skip("Unable to send ping. Skipping test...")
	}

	res2, err := p.Execute()
	if err != nil {
		t.Fatalf("Failed to execute ping probe: %s", err)
	}
	if len(res2) != 1 {
		t.Fatalf("Unexpected results count: got %d, expected %d", len(res2), 1)
	}

	// Compare results between first and second execution.
	// Expects:
	//     * Same number of sent packets
	//     * Different statistics
	if res1[0].Fields[PingResultSent] != res2[0].Fields[PingResultSent] ||
		res1[0].Fields[PingResultAvg] == res2[0].Fields[PingResultAvg] ||
		res1[0].Fields[PingResultMin] == res2[0].Fields[PingResultMin] ||
		res1[0].Fields[PingResultMax] == res2[0].Fields[PingResultMax] ||
		res1[0].Fields[PingResultStdDev] == res2[0].Fields[PingResultStdDev] {
		t.Fatalf("Unexpected result after 2 probe runs:\n\t1st run: %s\n\t2nd run: %s", res1[0].Fields, res2[0].Fields)
	}
}
