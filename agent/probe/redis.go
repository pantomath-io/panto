// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"bufio"
	"fmt"
	"strconv"
	"strings"

	"github.com/go-redis/redis"
)

// Redis probes a redis store
type Redis struct {
	addr string
}

const (

	// Redis configuration parameters

	// RedisParamAddress : (mandatory) the address of the target redis server to gather metric from (string)
	RedisParamAddress = "address"

	// Redis results

	// RedisResultConnectedClients : The number of clients currently connected to this Redis server (uint64)
	RedisResultConnectedClients = "connected-clients"
	// RedisResultBlockedClients : The number of clients pending on a blocking call (uint64)
	RedisResultBlockedClients = "blocked-clients"
	// RedisResultUsedMemory : The amout of memory allocated by this Redis server, in bytes (uint64)
	RedisResultUsedMemory = "used-memory"
	// RedisResultMemoryFragmentationRatio : The ratio between the memory allocated by Redis and the memory as seen by
	// the operating system. See Redis INFO documentation for details. (float32)
	RedisResultMemoryFragmentationRatio = "mem-frag-ratio"
	// RedisResultCacheHitRatio : The cache hit ratio is the ratio between the # cache hits and the # of key requests.
	// (float32)
	RedisResultCacheHitRatio = "cache-hit-ratio"
	// RedisResultUptime : The time since the Redis server was launched, in seconds (uint64)
	RedisResultUptime = "uptime"
	// RedisResultChangesSinceLastSave : The number of changes since the last time the database was saved to disk. The
	// number of changes that would be lost upon restart. (uint64)
	RedisResultChangesSinceLastSave = "changes-since-last-save"
	// RedisResultLastSaveTime : The UNIX timestamp of the last time the database was saved to disk. (uint64)
	RedisResultLastSaveTime = "last-save-time"
	// RedisResultOpsPerSec : The number of commands processed by the Redis server per second. (uint64)
	RedisResultOpsPerSec = "ops-per-sec"
	// RedisResultRejectedConnections : The number of connections rejected because of the maximum connections limit. (uint64)
	RedisResultRejectedConnections = "rejected-connections"
	// RedisResultInputKbps : The incoming bandwith usage of the Redis server, in kilobytes per second. (uint64)
	RedisResultInputKbps = "input-kbps"
	// RedisResultOutputKbps : The outgoing bandwith usage of the Redis server, in kilobytes per second. (uint64)
	RedisResultOutputKbps = "output-kbps"
	// RedisResultExpiredKeys : Number of keys that have been removed when reaching their expiration date.(uint64)
	RedisResultExpiredKeys = "expired-keys"
	// RedisResultEvictedKeys : Number of keys removed (evicted) due to reaching maximum memory.(uint64)
	RedisResultEvictedKeys = "evicted-keys"
	// RedisResultMasterLastIO : Time in seconds since the last interaction with the master Redis server (uint64)
	RedisResultMasterLastIO = "master-last-io"
	// RedisResultMasterLinkStatus : The current status of the link to the master Redis server (string)
	RedisResultMasterLinkStatus = "master-link-status"
	// RedisResultMasterLinkDownSince : The time in seconds since the link between master and slave is down (uint64)
	RedisResultMasterLinkDownSince = "master-link-down-since"
	// RedisResultConnectedSlaves : The number of slave instances connected to the master Redis server (uint64)
	RedisResultConnectedSlaves = "connected-slaves"
)

// Name returns the name of the probe "redis"
func (r *Redis) Name() string {
	return "redis"
}

// Configure configures the redis probe
func (r *Redis) Configure(config map[string]interface{}) error {
	var ok bool

	if r.addr, ok = config[RedisParamAddress].(string); !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", RedisParamAddress, config[RedisParamAddress])
	}

	return nil
}

// Execute runs the probe and gathers information about the redis server
func (r *Redis) Execute() ([]*Result, error) {
	var client *redis.Client

	client = redis.NewClient(&redis.Options{Addr: r.addr})

	info, err := client.Info().Result()
	if err != nil {
		return nil, err
	}

	stats, err := scanInfo(info)
	if err != nil {
		return nil, err
	}

	fields := gatherFields(stats)

	return []*Result{{Fields: fields}}, nil
}

func scanInfo(info string) (stats map[string]string, err error) {
	stats = make(map[string]string)

	scanner := bufio.NewScanner(strings.NewReader(info))
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		m := strings.Split(line, ":")
		if len(m) == 2 {
			stats[m[0]] = m[1]
		}
	}
	if err = scanner.Err(); err != nil {
		stats = nil
	}
	return
}

func gatherFields(stats map[string]string) map[string]interface{} {
	fields := make(map[string]interface{})

	if s, ok := stats["uptime_in_seconds"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultUptime] = n
		}
	}
	if s, ok := stats["connected_clients"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultConnectedClients] = n
		}
	}
	if s, ok := stats["blocked_clients"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultBlockedClients] = n
		}
	}
	if s, ok := stats["used_memory"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultUsedMemory] = n
		}
	}
	if s, ok := stats["mem_fragmentation_ratio"]; ok {
		if n, err := strconv.ParseFloat(s, 32); err == nil {
			fields[RedisResultMemoryFragmentationRatio] = float32(n)
		}
	}
	if s, ok := stats["rdb_changes_since_last_save"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultChangesSinceLastSave] = n
		}
	}
	if s, ok := stats["rdb_last_save_time"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultLastSaveTime] = n
		}
	}
	if s, ok := stats["instantaneous_ops_per_sec"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultOpsPerSec] = n
		}
	}
	if s, ok := stats["rejected_connections"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultRejectedConnections] = n
		}
	}
	if s, ok := stats["instantaneous_input_kbps"]; ok {
		if n, err := strconv.ParseFloat(s, 32); err == nil {
			fields[RedisResultInputKbps] = float32(n)
		}
	}
	if s, ok := stats["instantaneous_output_kbps"]; ok {
		if n, err := strconv.ParseFloat(s, 32); err == nil {
			fields[RedisResultOutputKbps] = float32(n)
		}
	}
	if s, ok := stats["expired_keys"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultExpiredKeys] = n
		}
	}
	if s, ok := stats["evicted_keys"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultEvictedKeys] = n
		}
	}
	if s, ok := stats["connected_slaves"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[RedisResultConnectedSlaves] = n
			if n > 0 {
				// if there are slaves, there is a master
				if s, ok := stats["master_last_io_seconds_ago"]; ok {
					if n, err := strconv.ParseUint(s, 10, 64); err == nil {
						fields[RedisResultMasterLastIO] = n
					}
				}
				if s, ok := stats["master_link_status"]; ok {
					fields[RedisResultMasterLinkStatus] = s
				}
				if s, ok := stats["master_link_down_since_seconds"]; ok {
					if n, err := strconv.ParseUint(s, 10, 64); err == nil {
						fields[RedisResultMasterLinkDownSince] = n
					}
				}
			} else {
				// when there is no slave, there is no master
				fields[RedisResultMasterLastIO] = 0
				fields[RedisResultMasterLinkStatus] = "-"
				fields[RedisResultMasterLinkDownSince] = 0
			}
		}
	}
	{
		s1, ok1 := stats["keyspace_hits"]
		s2, ok2 := stats["keyspace_misses"]
		if ok1 && ok2 {
			n1, err1 := strconv.ParseUint(s1, 10, 64)
			n2, err2 := strconv.ParseUint(s2, 10, 64)
			if err1 == nil && err2 == nil {
				if (float32(n1) + float32(n2)) != 0.0 {
					fields[RedisResultCacheHitRatio] = float32(n1) / (float32(n1) + float32(n2))
				} else {
					fields[RedisResultCacheHitRatio] = 0
				}
			}
		}
	}

	return fields
}
