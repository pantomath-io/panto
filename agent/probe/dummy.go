// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

// Dummy is a test probe that accepts any configuration,
// does nothing with it, and returns an empty map as result
type Dummy struct{}

// Name returns the name of the probe "dummy"
func (d *Dummy) Name() string {
	return "dummy"
}

// Configure takes an arbitrary map of options and ignores them.
func (d *Dummy) Configure(config map[string]interface{}) error {
	return nil
}

// Execute does nothing and returns an empty map
func (d *Dummy) Execute() ([]*Result, error) {
	return []*Result{
		{
			Fields: map[string]interface{}{"hello": "world"},
			Tags:   map[string]string{},
		},
	}, nil
}
