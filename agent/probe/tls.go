// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"crypto/sha256"
	"crypto/tls"
	"fmt"
	"strings"
	"time"
)

// TLS is a probe that opens a TLS connection
// and gathers data on the certificate.
type TLS struct {
	host string
}

const (
	// TLS configuration parameters

	// TLSParamHost : TLS request URL (`string`)
	TLSParamHost = "host"

	// TLS results

	// TLSExpiresIn : time before the TLS certificate expires, in nanoseconds (`int64`)
	TLSExpiresIn = "expires-in"
	// TLSChainExpiresIn : time before any TLS certificate in the chain expires, in nanoseconds (`int64`)
	TLSChainExpiresIn = "chain-expires-in"
	// TLSHash : the TLS certificate's hash (`string`)
	TLSHash = "hash"
	// TLSSignatureList : list of the TLS certificate's signature algorithm in the full chain (`string`)
	TLSSignatureList = "signature-list"
)

// Name returns the name of the probe "tls"
func (h *TLS) Name() string {
	return "tls"
}

// Configure configures the TLS probe and prepares the TLS request.
func (h *TLS) Configure(config map[string]interface{}) error {
	// Check for mandatory URL field
	_, ok := config[TLSParamHost]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", TLSParamHost)
	}
	h.host, ok = config[TLSParamHost].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", TLSParamHost, config[TLSParamHost])
	}

	return nil
}

// Execute sends an TLS request to the target
// and synchronously waits for the reply.
func (h *TLS) Execute() ([]*Result, error) {
	var client *tls.Conn
	SigAlgo := make(map[string]struct{})

	result := map[string]interface{}{
		TLSExpiresIn:      int64(0),
		TLSChainExpiresIn: int64(0),
		TLSHash:           "",
		TLSSignatureList:  "",
	}

	log.Debugf("Dialing TLS connection: %s", h.host)
	client, err := tls.Dial("tcp", h.host, nil)
	if err != nil {
		return nil, fmt.Errorf("unable to dial \"%s\": %v", h.host, err)
	}
	defer client.Close()

	for _, chain := range client.ConnectionState().VerifiedChains {
		for i, cert := range chain {

			certExpiration := time.Until(cert.NotAfter).Nanoseconds()
			if result[TLSChainExpiresIn].(int64) == 0 || (result[TLSChainExpiresIn].(int64) > 0 && certExpiration < result[TLSChainExpiresIn].(int64)) {
				result[TLSChainExpiresIn] = certExpiration
			}
			if i == 0 {
				result[TLSExpiresIn] = certExpiration

				// calculate certificate's hash
				hash := sha256.New()
				hash.Write(cert.RawTBSCertificate)
				result[TLSHash] = fmt.Sprintf("%x", hash.Sum(nil))
			}

			// keep the signature algorithm for all the chain but the root certificate
			if i < len(chain)-1 {
				SigAlgo[cert.SignatureAlgorithm.String()] = struct{}{}
			}
		}
	}

	// get a list of unique algorithms in the result
	uniqSigAlgo := make([]string, 0, len(SigAlgo))
	for algo := range SigAlgo {
		uniqSigAlgo = append(uniqSigAlgo, algo)
	}
	result[TLSSignatureList] = strings.Join(uniqSigAlgo[:], ",")

	return []*Result{{Fields: result}}, nil
}
