// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"

	"github.com/shirou/gopsutil/disk"
	"gitlab.com/pantomath-io/panto/util"
)

//------------
// DiskUsage probe
//------------

// DiskUsage probes disk usage statistics
type DiskUsage struct{}

// list of FSType to filter from results
var filteredFSType = []string{
	"autofs",
	"binfmt_misc",
	"cgroup",
	"debugfs",
	"devfs",
	"devpts",
	"devtmpfs",
	"dfsfuse_DFS",
	"hugetlbfs",
	"mqueue",
	"nullfs",
	"proc",
	"pstore",
	"securityfs",
	"sysfs",
	"tmpfs",
	"udev",
}

const (
	// DiskUsage configuration parameters

	// DiskUsageParamInterval : (optional, default 1 second) interval over which disk utilization is calculated
	DiskUsageParamInterval = "interval"

	// DiskUsage results

	// DiskUsageResultFree : Free space on the disk, in bytes (`uint64`)
	DiskUsageResultFree = "free"
	// DiskUsageResultUsed : Used space on the disk, in bytes (`uint64`)
	DiskUsageResultUsed = "used"
	// DiskUsageResultUsedPercent : Percentage of disk space left, `free / (free + used)` (`float32`)
	DiskUsageResultUsedPercent = "used-percent"
	// DiskUsageResultInodesFree : Number of free inodes on the disk (`uint64`)
	DiskUsageResultInodesFree = "inodes-free"
	// DiskUsageResultInodesUsed : Number of used inodes on the disk (`uint64`)
	DiskUsageResultInodesUsed = "inodes-used"
	// DiskUsageResultInodesUsedPercent : Percentage of inodes left, `free / (free + used)` (`float32`)
	DiskUsageResultInodesUsedPercent = "inodes-used-percent"

	// Disk result tags

	// DiskUsageTagPath : The mount point of the disk, e.g. `"/"`
	DiskUsageTagPath = "path"
)

// Name returns the name of the probe "disk"
func (d *DiskUsage) Name() string {
	return "disk-usage"
}

// Configure configures the disk probe
func (d *DiskUsage) Configure(config map[string]interface{}) error {
	return nil
}

// Execute runs the probe and gathers disk information
func (d *DiskUsage) Execute() ([]*Result, error) {
	partitions, err := disk.Partitions(true)
	if err != nil {
		return nil, fmt.Errorf("couldn't gather all partitions: %s", err)
	}

	res := make([]*Result, 0, len(partitions))
	for _, p := range partitions {
		if util.Contains(filteredFSType, p.Fstype) {
			log.Debugf("filtering fstype %s (%s) from result", p.Fstype, p.Mountpoint)
			continue
		}
		log.Debugf("%s -> %s", p.Mountpoint, p.Fstype)
		stats, err := disk.Usage(p.Mountpoint)
		if err != nil {
			log.Errorf("Couldn't collect disk usage stats for %s", p.Mountpoint)
			continue
		}
		res = append(res,
			&Result{
				Fields: map[string]interface{}{
					DiskUsageResultFree:              stats.Free,
					DiskUsageResultUsed:              stats.Used,
					DiskUsageResultUsedPercent:       float32(stats.UsedPercent),
					DiskUsageResultInodesFree:        stats.InodesFree,
					DiskUsageResultInodesUsed:        stats.InodesUsed,
					DiskUsageResultInodesUsedPercent: float32(stats.InodesUsedPercent),
				},
				Tags: map[string]string{
					DiskUsageTagPath: p.Mountpoint,
				},
			},
		)
	}
	return res, nil
}
