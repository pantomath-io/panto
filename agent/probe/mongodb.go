// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"context"
	"fmt"
	"strings"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"
)

// MongoDB probes an influxtb mdb
type MongoDB struct {
	client    *mongo.Client
	databases []string
}

const (
	// MongoDB configuration parameters

	// MongoDBParamAddress : (mandatory) the address of the target mongodb server to gather metrics from (string)
	MongoDBParamAddress = "address"
	// MongoDBParamDatabases : (optional) the comma separated list of databases to gather metrics from (string)
	MongoDBParamDatabases = "databases"
	// MongoDBParamUsername : (optional) a username to authenticate to the MongoDB server (string)
	MongoDBParamUsername = "username"
	// MongoDBParamPassword : (optional) a password to authenticate to the MongoDB server (string)
	MongoDBParamPassword = "password"

	// MongoDB results

	// ServerStats

	// MongoDBResultServerUptime : The number of seconds that the current MongoDB process has been active (int64)
	MongoDBResultServerUptime = "uptime"

	// MongoDBResultServerConnectionsCurrent : The number of incoming connections from clients to the database server (int32)
	MongoDBResultServerConnectionsCurrent = "connections_current"
	// MongoDBResultServerConnectionsAvailable : The number of unused incoming connections available (int32)
	MongoDBResultServerConnectionsAvailable = "connections_available"

	// MongoDBResultServerGlobalLockActiveClientsReaders : The number of the active client connections performing read operations (int32)
	MongoDBResultServerGlobalLockActiveClientsReaders = "gl_clients_readers"
	// MongoDBResultServerGlobalLockActiveClientWriters : The number of active client connections performing write operations (int32)
	MongoDBResultServerGlobalLockActiveClientWriters = "gl_clients_writers"

	// MongoDBResultServerNetworkIn : The number of bytes that reflects the amount of network traffic received by this database (int64)
	MongoDBResultServerNetworkIn = "network_in"
	// MongoDBResultServerNetworkOut : The number of bytes that reflects the amount of network traffic sent from this database (int64)
	MongoDBResultServerNetworkOut = "network_out"

	// MongoDBResultServerOpsInsert : The total number of insert operations received since the mongod instance last started (int64)
	MongoDBResultServerOpsInsert = "ops_insert"
	// MongoDBResultServerOpsQuery : The total number of queries operations received since the mongod instance last started (int64)
	MongoDBResultServerOpsQuery = "ops_query"
	// MongoDBResultServerOpsUpdate : The total number of update operations received since the mongod instance last started (int64)
	MongoDBResultServerOpsUpdate = "ops_update"
	// MongoDBResultServerOpsDelete : The total number of delete operations received since the mongod instance last started (int64)
	MongoDBResultServerOpsDelete = "ops_delete"
	// MongoDBResultServerOpsGetmore : The total number of getmore operations received since the mongod instance last started (int64)
	MongoDBResultServerOpsGetmore = "ops_getmore"
	// MongoDBResultServerOpsCommand : The total number of command operations received since the mongod instance last started (int64)
	MongoDBResultServerOpsCommand = "ops_command"

	// MongoDBResultServerMemResident : The value of mem.resident is roughly equivalent to the amount of RAM, in megabytes (MB), currently used by the database process (int32)
	MongoDBResultServerMemResident = "mem_resident"
	// MongoDBResultServerMemVirtual : mem.virtual displays the quantity, in megabytes (MB), of virtual memory used by the mongod process. (int32)
	MongoDBResultServerMemVirtual = "mem_virtual"
)

// Name returns the name of the probe "mongodb"
func (mdb *MongoDB) Name() string {
	return "mongodb"
}

// Configure configures the mongodb probe
func (mdb *MongoDB) Configure(config map[string]interface{}) (err error) {
	var ok bool
	uri := new(strings.Builder)
	fmt.Fprint(uri, "mongodb://")

	// Check for optional username field
	var username string
	_, ok = config[MongoDBParamUsername]
	if ok {
		username, ok = config[MongoDBParamUsername].(string)
		if !ok {
			return fmt.Errorf("\"%s\" field is not string: %v", MongoDBParamUsername, config[MongoDBParamUsername])
		}
		fmt.Fprintf(uri, "%s", username)
	}
	// Check for optional password field
	var password string
	_, ok = config[MongoDBParamPassword]
	if ok {
		password, ok = config[MongoDBParamPassword].(string)
		if !ok {
			return fmt.Errorf("\"%s\" field is not string: %v", MongoDBParamPassword, config[MongoDBParamPassword])
		}
		fmt.Fprintf(uri, ":%s", password)
	}

	// Check for mandatory address field
	var addr string
	_, ok = config[MongoDBParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", MongoDBParamAddress)
	}
	if addr, ok = config[MongoDBParamAddress].(string); !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", MongoDBParamAddress, config[MongoDBParamAddress])
	}
	fmt.Fprintf(uri, "@%s", addr)

	// Check for optional databases field
	var dbs string
	_, ok = config[MongoDBParamDatabases]
	if ok {
		dbs, ok = config[MongoDBParamDatabases].(string)
		if !ok {
			return fmt.Errorf("\"%s\" field is not string: %v", MongoDBParamDatabases, config[MongoDBParamDatabases])
		}
		mdb.databases = strings.Split(dbs, ",")
	}

	mdb.client, err = mongo.NewClient(uri.String())
	if err != nil {
		return err
	}

	return nil
}

// Execute runs the probe and gathers information about the mongodb server
func (mdb *MongoDB) Execute() ([]*Result, error) {
	err := mdb.client.Connect(context.TODO())
	if err != nil {
		return nil, err
	}
	defer mdb.client.Disconnect(context.Background())

	log.Debug("Query to mongo: serverStatus")
	stats, err := mdb.client.Database("admin").RunCommand(context.Background(), bson.NewDocument(bson.EC.Boolean("serverStatus", true)))
	if err != nil {
		return nil, err
	}
	if l, err := stats.Keys(false); err != nil || len(l) < 1 {
		return nil, fmt.Errorf("error getting the stats: %s", err)
	}

	fields := scanStatsMongo(stats)
	log.Debugf("fields: %v", fields)

	return []*Result{{Fields: fields}}, nil
}

func scanStatsMongo(res bson.Reader) (fields map[string]interface{}) {
	fields = make(map[string]interface{})

	if e, err := res.Lookup("uptimeMillis"); err == nil {
		if v, ok := e.Value().Int64OK(); ok {
			fields[MongoDBResultServerUptime] = v
		}
	}

	if d, err := res.Lookup("connections"); err == nil {
		if doc, ok := d.Value().MutableDocumentOK(); ok {
			if e, err := doc.LookupErr("current"); err == nil {
				if v, ok := e.Int32OK(); ok {
					fields[MongoDBResultServerConnectionsCurrent] = v
				}
			}
			if e, err := doc.LookupErr("available"); err == nil {
				if v, ok := e.Int32OK(); ok {
					fields[MongoDBResultServerConnectionsAvailable] = v
				}
			}
		}
	}

	if d, err := res.Lookup("globalLock"); err == nil {
		if doc, ok := d.Value().MutableDocumentOK(); ok {
			if queue, err := doc.LookupErr("currentQueue"); err == nil {
				if sdoc, ok := queue.MutableDocumentOK(); ok {
					if e, err := sdoc.LookupErr("readers"); err == nil {
						if v, ok := e.Int32OK(); ok {
							fields[MongoDBResultServerGlobalLockActiveClientsReaders] = v
						}
					}
					if e, err := sdoc.LookupErr("writers"); err == nil {
						if v, ok := e.Int32OK(); ok {
							fields[MongoDBResultServerGlobalLockActiveClientWriters] = v
						}
					}
				}
			}
		}
	}

	if d, err := res.Lookup("network"); err == nil {
		if doc, ok := d.Value().MutableDocumentOK(); ok {
			if e, err := doc.LookupErr("bytesIn"); err == nil {
				if v, ok := e.Int64OK(); ok {
					fields[MongoDBResultServerNetworkIn] = v
				}
			}
			if e, err := doc.LookupErr("bytesOut"); err == nil {
				if v, ok := e.Int64OK(); ok {
					fields[MongoDBResultServerNetworkOut] = v
				}
			}
		}
	}

	if d, err := res.Lookup("opcounters"); err == nil {
		if doc, ok := d.Value().MutableDocumentOK(); ok {
			if e, err := doc.LookupErr("insert"); err == nil {
				if v, ok := e.Int64OK(); ok {
					fields[MongoDBResultServerOpsInsert] = v
				}
			}
			if e, err := doc.LookupErr("query"); err == nil {
				if v, ok := e.Int64OK(); ok {
					fields[MongoDBResultServerOpsQuery] = v
				}
			}
			if e, err := doc.LookupErr("update"); err == nil {
				if v, ok := e.Int64OK(); ok {
					fields[MongoDBResultServerOpsUpdate] = v
				}
			}
			if e, err := doc.LookupErr("delete"); err == nil {
				if v, ok := e.Int64OK(); ok {
					fields[MongoDBResultServerOpsDelete] = v
				}
			}
			if e, err := doc.LookupErr("getmore"); err == nil {
				if v, ok := e.Int64OK(); ok {
					fields[MongoDBResultServerOpsGetmore] = v
				}
			}
			if e, err := doc.LookupErr("command"); err == nil {
				if v, ok := e.Int64OK(); ok {
					fields[MongoDBResultServerOpsCommand] = v
				}
			}
		}
	}

	if d, err := res.Lookup("mem"); err == nil {
		if doc, ok := d.Value().MutableDocumentOK(); ok {
			if e, err := doc.LookupErr("resident"); err == nil {
				if v, ok := e.Int32OK(); ok {
					fields[MongoDBResultServerMemResident] = v
				}
			}
			if e, err := doc.LookupErr("virtual"); err == nil {
				if v, ok := e.Int32OK(); ok {
					fields[MongoDBResultServerMemVirtual] = v
				}
			}
		}
	}

	return
}
