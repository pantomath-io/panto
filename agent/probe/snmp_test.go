// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*SNMP)(nil)

func TestSNMPName(t *testing.T) {
	r := SNMP{}
	if r.Name() != "SNMP" {
		t.Error("SNMP probe name is not \"SNMP\"")
	}
}

func TestSNMPConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := SNMP{}
	err := r.Configure(map[string]interface{}{
		SNMPParamAddress: viper.GetString("snmp.address"),
		SNMPParamPort:    viper.GetFloat64("snmp.port"),
		SNMPParamOID:     viper.GetString("snmp.oid"),
	})
	if err != nil {
		t.Errorf("couldn't configure SNMP probe: %s", err)
	}
}

func TestSNMPExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := SNMP{}
	err := r.Configure(map[string]interface{}{
		SNMPParamAddress: viper.GetString("snmp.address"),
		SNMPParamOID:     viper.GetString("snmp.oid"),
	})
	if err != nil {
		t.Errorf("couldn't configure SNMP probe: %s", err)
	}

	res, err := r.Execute()
	if err != nil {
		t.Fatalf("unexpected error executing SNMP probe: %s", err)
	}

	expectedKeys := map[string]interface{}{
		SNMPResultValue: reflect.Interface,
	}
	expectedTags := map[string]interface{}{
		SNMPTagOID: reflect.String,
	}
	for k := range expectedKeys {
		if _, ok := res[0].Fields[k]; !ok {
			t.Fatalf("missing \"%s\" field in SNMP results", k)
		}
	}

	for k, tag := range res[0].Tags {
		kind, ok := expectedTags[k]
		if !ok {
			t.Errorf("unexpected tag \"%s\" in SNMP results", k)
			continue
		}
		if kind != reflect.TypeOf(tag).Kind() {
			t.Errorf("invalid type for tag %s, expected %s, got %s", k, kind, reflect.TypeOf(tag).Kind())
			continue
		}
	}

}
