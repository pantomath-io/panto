// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"strings"
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Memcached)(nil)

func TestMemcachedName(t *testing.T) {
	r := Memcached{}
	if r.Name() != "memcached" {
		t.Error("Memcached probe name is not \"memcached\"")
	}
}

func TestMemcachedConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Memcached{}
	err := r.Configure(map[string]interface{}{MemcachedParamAddress: viper.GetString("memcached.address")})
	if err != nil {
		t.Errorf("couldn't configure Memcached probe: %s", err)
	}
}

func TestMemcachedExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Memcached{}

	err := r.Configure(map[string]interface{}{MemcachedParamAddress: viper.GetString("memcached.address")})
	if err != nil {
		t.Fatalf("couldn't configure Memcached probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Memcached probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}

	stats, err := scanStats(strings.NewReader(testStat))
	if err != nil {
		t.Fatalf("Failed to scan STAT: %s", err)
	}
	if len(stats) != 59 {
		t.Fatalf("Expected %d results, got %d", 59, len(stats))
	}
	t.Logf("Stats: %v", stats)

	fields := gatherFieldsMemcached(stats)
	if err != nil {
		t.Fatalf("Failed to parse stats: %v", err)
	}

	t.Logf("Fields: %v", fields)
}

func TestMemcachedGatherFieldsMemcached(t *testing.T) {
	stats, _ := scanStats(strings.NewReader(testStat))
	fields := gatherFieldsMemcached(stats)

	t.Logf("Fields: %v", fields)

	expectedKeys := map[string]interface{}{
		MemcachedResultUptime:          reflect.Uint64,
		MemcachedResultCurrConnections: reflect.Uint64,
		MemcachedResultReservedFDs:     reflect.Uint64,
		MemcachedResultCmdGet:          reflect.Uint64,
		MemcachedResultCmdSet:          reflect.Uint64,
		MemcachedResultCmdFlush:        reflect.Uint64,
		MemcachedResultCmdTouch:        reflect.Uint64,
		MemcachedResultGetHits:         reflect.Uint64,
		MemcachedResultGetMisses:       reflect.Uint64,
		MemcachedResultGetExpired:      reflect.Uint64,
		MemcachedResultGetFlushed:      reflect.Uint64,
		MemcachedResultDeleteMisses:    reflect.Uint64,
		MemcachedResultDeleteHits:      reflect.Uint64,
		MemcachedResultBytesRead:       reflect.Uint64,
		MemcachedResultBytesWritten:    reflect.Uint64,
		MemcachedResultBytes:           reflect.Uint64,
		MemcachedResultCurrItems:       reflect.Uint64,
		MemcachedResultEvictions:       reflect.Uint64,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in redis results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in redis results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}

// A mock of a memcached STAT command
const testStat = `STAT pid 475
STAT uptime 62119
STAT time 1528985701
STAT version 1.4.33
STAT libevent 2.0.21-stable
STAT pointer_size 64
STAT rusage_user 11.064000
STAT rusage_system 9.052000
STAT curr_connections 1
STAT total_connections 8
STAT connection_structures 2
STAT reserved_fds 20
STAT cmd_get 0
STAT cmd_set 0
STAT cmd_flush 0
STAT cmd_touch 0
STAT get_hits 0
STAT get_misses 0
STAT get_expired 0
STAT get_flushed 0
STAT delete_misses 0
STAT delete_hits 0
STAT incr_misses 0
STAT incr_hits 0
STAT decr_misses 0
STAT decr_hits 0
STAT cas_misses 0
STAT cas_hits 0
STAT cas_badval 0
STAT touch_hits 0
STAT touch_misses 0
STAT auth_cmds 0
STAT auth_errors 0
STAT bytes_read 61
STAT bytes_written 7886
STAT limit_maxbytes 67108864
STAT accepting_conns 1
STAT listen_disabled_num 0
STAT time_in_listen_disabled_us 0
STAT threads 4
STAT conn_yields 0
STAT hash_power_level 16
STAT hash_bytes 524288
STAT hash_is_expanding 0
STAT malloc_fails 0
STAT log_worker_dropped 0
STAT log_worker_written 0
STAT log_watcher_skipped 0
STAT log_watcher_sent 0
STAT bytes 0
STAT curr_items 0
STAT total_items 0
STAT expired_unfetched 0
STAT evicted_unfetched 0
STAT evictions 0
STAT reclaimed 0
STAT crawler_reclaimed 0
STAT crawler_items_checked 0
STAT lrutail_reflocked 0
`
