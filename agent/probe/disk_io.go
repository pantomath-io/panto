// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"
	"time"

	"github.com/shirou/gopsutil/disk"
)

//------------
// DiskIO probe
//------------

// DiskIO probes disk I/O statistics
type DiskIO struct {
	interval time.Duration
}

const (
	// DiskIO configuration parameters

	// DiskIOParamInterval : (optional, default 1 second) interval over which disk utilization is calculated
	DiskIOParamInterval = "interval"

	// DiskIO results

	// DiskIOResultIops : Average I/O operations per second (`float32`)
	DiskIOResultIops = "iops"
	// DiskIOResultBytesRead : Average bytes read per second (`float32`)
	DiskIOResultBytesRead = "bytes-read"
	// DiskIOResultBytesWritten : Average bytes written per second (`float32`)
	DiskIOResultBytesWritten = "bytes-written"
	// DiskIOResultLatency : Average time for I/O requests issued to the device to be served, calculated as the time spent
	// doing I/O divided by the # of Iops serverd during the interval (`time.Duration`)
	DiskIOResultLatency = "latency"

	// Disk result tags

	// DiskIOTagDevice : The name of the device, e.g. "sda"
	DiskIOTagDevice = "device"
)

// Name returns the name of the probe "disk"
func (d *DiskIO) Name() string {
	return "disk-io"
}

// Configure configures the disk probe
func (d *DiskIO) Configure(config map[string]interface{}) error {
	// Check for optional interval argument
	interval, ok := config[DiskIOParamInterval]
	if ok {
		switch i := interval.(type) {
		case float64:
			interval = time.Duration(i)
		}
		d.interval, ok = interval.(time.Duration)
		if !ok {
			return fmt.Errorf(
				"\"%s\" field is not time.Duration: %v",
				DiskIOParamInterval,
				config[DiskIOParamInterval])
		}
	}
	if d.interval <= 0 {
		d.interval = 1 * time.Second
	}
	return nil
}

// Execute runs the probe and gathers disk information
func (d *DiskIO) Execute() ([]*Result, error) {
	ioStart, err := disk.IOCounters()
	if err != nil {
		return nil, fmt.Errorf("couldn't get disk I/O counters at start of interval: %s", err)
	}

	time.Sleep(d.interval)

	ioEnd, err := disk.IOCounters()
	if err != nil {
		return nil, fmt.Errorf("couldn't get disk I/O counters at end of interval: %s", err)
	}

	res := make([]*Result, 0, len(ioStart))
	for dev, start := range ioStart {
		end, ok := ioEnd[dev]
		if !ok {
			// Device only found in start stats, skip it
			continue
		}
		// total # of I/O operations operation during the interval
		iops := float32((end.ReadCount + end.WriteCount) - (start.ReadCount + start.WriteCount))
		var latency float32
		if iops > 0 {
			latency = float32((end.ReadTime+end.WriteTime)-(start.ReadTime+start.WriteTime)) / float32(iops)
		}
		itvSeconds := float32(d.interval.Seconds())

		res = append(res,
			&Result{
				Fields: map[string]interface{}{
					DiskIOResultIops:         iops / itvSeconds,
					DiskIOResultBytesRead:    float32((end.ReadBytes - start.ReadBytes)) / itvSeconds,
					DiskIOResultBytesWritten: float32((end.WriteBytes - start.WriteBytes)) / itvSeconds,
					DiskIOResultLatency:      (time.Duration(latency) * time.Millisecond).Nanoseconds(),
				},
				Tags: map[string]string{
					DiskIOTagDevice: dev,
				},
			},
		)
	}
	return res, nil
}
