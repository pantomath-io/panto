// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	// Go MySQL Driver is an implementation of Go's database/sql/driver interface
	_ "github.com/go-sql-driver/mysql"
)

// Mysql is a probe that request  stats from a Mysql server.
type Mysql struct {
	conn     *sql.DB
	address  string
	login    string
	password string
}

const (
	// Mysql configuration parameters

	// MysqlParamAddress : Mysql server (host:port) (`string`)
	MysqlParamAddress = "address"
	// MysqlParamLogin : Mysql user account (`string`)
	MysqlParamLogin = "login"
	// MysqlParamPassword : Mysql user password (`string`)
	MysqlParamPassword = "password"

	// Mysql results

	// MysqlResultAbortedClients : The number of connections that were aborted because the client died without closing the connection properly.
	MysqlResultAbortedClients = "aborted-clients"
	// MysqlResultAbortedConnects :    The number of failed attempts to connect to the MySQL server.
	MysqlResultAbortedConnects = "aborted-connects"
	// MysqlResultBinlogCacheUse : The number of transactions that used the binary log cache.
	MysqlResultBinlogCacheUse = "binlog-cache-use"
	// MysqlResultBinlogStmtCacheUse : The number of nontransactional statements that used the binary log statement cache.
	MysqlResultBinlogStmtCacheUse = "binlog-stmt-cache-use"
	// MysqlResultBytesReceived : The number of bytes received from all clients.
	MysqlResultBytesReceived = "bytes-received"
	// MysqlResultBytesSent : The number of bytes sent to all clients.
	MysqlResultBytesSent = "bytes-sent"
	// MysqlResultComBegin : The number of times begin statement has been executed.
	MysqlResultComBegin = "com-begin"
	// MysqlResultComChangeDb : The number of times change-db statement has been executed.
	MysqlResultComChangeDb = "com-change-db"
	// MysqlResultComChangeMaster : The number of times change-master statement has been executed.
	MysqlResultComChangeMaster = "com-change-master"
	// MysqlResultComCommit : The number of times commit statement has been executed.
	MysqlResultComCommit = "com-commit"
	// MysqlResultComCreateDb : The number of times create-db statement has been executed.
	MysqlResultComCreateDb = "com-create-db"
	// MysqlResultComDelete : The number of times delete statement has been executed.
	MysqlResultComDelete = "com-delete"
	// MysqlResultComDeleteMulti : The number of times delete-multi statement has been executed.
	MysqlResultComDeleteMulti = "com-delete-multi"
	// MysqlResultComInsert : The number of times insert statement has been executed.
	MysqlResultComInsert = "com-insert"
	// MysqlResultComRollback : The number of times rollback statement has been executed.
	MysqlResultComRollback = "com-rollback"
	// MysqlResultComSelect : The number of times select statement has been executed.
	MysqlResultComSelect = "com-select"
	// MysqlResultComStmtExecute : The number of times stmt-execute statement has been executed.
	MysqlResultComStmtExecute = "com-stmt-execute"
	// MysqlResultComStmtFetch : The number of times stmt-fetch statement has been executed.
	MysqlResultComStmtFetch = "com-stmt-fetch"
	// MysqlResultComTruncate : The number of times truncate statement has been executed.
	MysqlResultComTruncate = "com-truncate"
	// MysqlResultComUpdate : The number of times update statement has been executed.
	MysqlResultComUpdate = "com-update"
	// MysqlResultConnectionErrorsAccept : The number of errors that occurred during calls to accept() on the listening port.
	MysqlResultConnectionErrorsAccept = "connection-errors-accept"
	// MysqlResultConnectionErrorsInternal : The number of connections refused due to internal errors in the server, such as failure to start a new thread or an out-of-memory condition.
	MysqlResultConnectionErrorsInternal = "connection-errors-internal"
	// MysqlResultConnectionErrorsMaxConnections : The number of connections refused because the server max-connections limit was reached.
	MysqlResultConnectionErrorsMaxConnections = "connection-errors-max-connections"
	// MysqlResultConnectionErrorsPeerAddress : The number of errors that occurred while searching for connecting client IP addresses.
	MysqlResultConnectionErrorsPeerAddress = "connection-errors-peer-address"
	// MysqlResultConnectionErrorsSelect : The number of errors that occurred during calls to select() or poll() on the listening port.
	MysqlResultConnectionErrorsSelect = "connection-errors-select"
	// MysqlResultConnectionErrorsTcpwrap : The number of connections refused by the libwrap library.
	MysqlResultConnectionErrorsTcpwrap = "connection-errors-tcpwrap"
	// MysqlResultConnections : The number of connection attempts (successful or not) to the MySQL server.
	MysqlResultConnections = "connections"
	// MysqlResultCreatedTmpDiskTables : The number of internal on-disk temporary tables created by the server while executing statements.
	MysqlResultCreatedTmpDiskTables = "created-tmp-disk-tables"
	// MysqlResultCreatedTmpTables : The number of internal temporary tables created by the server while executing statements.
	MysqlResultCreatedTmpTables = "created-tmp-tables"
	// MysqlResultFlushCommands : The number of times the server flushes tables, whether because a user executed a FLUSH TABLES statement or due to internal server operation.
	MysqlResultFlushCommands = "flush-commands"
	// MysqlResultHandlerReadFirst : The number of times the first entry in an index was read. If this value is high, it suggests that the server is doing a lot of full index scans.
	MysqlResultHandlerReadFirst = "handler-read-first"
	// MysqlResultHandlerReadKey : The number of requests to read a row based on a key. If this value is high, it is a good indication that your tables are properly indexed for your queries.
	MysqlResultHandlerReadKey = "handler-read-key"
	// MysqlResultHandlerReadLast : The number of requests to read the last key in an index.
	MysqlResultHandlerReadLast = "handler-read-last"
	// MysqlResultHandlerReadNext : The number of requests to read the next row in key order.
	MysqlResultHandlerReadNext = "handler-read-next"
	// MysqlResultHandlerReadPrev : The number of requests to read the previous row in key order.
	MysqlResultHandlerReadPrev = "handler-read-prev"
	// MysqlResultHandlerReadRnd : The number of requests to read a row based on a fixed position. This value is high if you are doing a lot of queries that require sorting of the result.
	MysqlResultHandlerReadRnd = "handler-read-rnd"
	// MysqlResultHandlerReadRndNext : The number of requests to read the next row in the data file. This value is high if you are doing a lot of table scans.
	MysqlResultHandlerReadRndNext = "handler-read-rnd-next"
	// MysqlResultInnodbBufferPoolPagesData : The number of pages in the InnoDB buffer pool containing data.
	MysqlResultInnodbBufferPoolPagesData = "innodb-buffer-pool-pages-data"
	// MysqlResultInnodbBufferPoolPagesDirty : The current number of dirty pages in the InnoDB buffer pool.
	MysqlResultInnodbBufferPoolPagesDirty = "innodb-buffer-pool-pages-dirty"
	// MysqlResultInnodbBufferPoolPagesFlushed : The number of requests to flush pages from the InnoDB buffer pool.
	MysqlResultInnodbBufferPoolPagesFlushed = "innodb-buffer-pool-pages-flushed"
	// MysqlResultInnodbBufferPoolPagesFree : The number of free pages in the InnoDB buffer pool.
	MysqlResultInnodbBufferPoolPagesFree = "innodb-buffer-pool-pages-free"
	// MysqlResultInnodbBufferPoolPagesMisc : The number of pages in the InnoDB buffer pool that are busy because they have been allocated for administrative overhead, such as row locks or the adaptive hash index.
	MysqlResultInnodbBufferPoolPagesMisc = "innodb-buffer-pool-pages-misc"
	// MysqlResultInnodbDataFsyncs : The number of fsync() operations so far.
	MysqlResultInnodbDataFsyncs = "innodb-data-fsyncs"
	// MysqlResultInnodbDataReads : The total number of data reads (OS file reads).
	MysqlResultInnodbDataReads = "innodb-data-reads"
	// MysqlResultInnodbDataWrites : The total number of data writes.
	MysqlResultInnodbDataWrites = "innodb-data-writes"
	// MysqlResultInnodbLogWaits : The number of times that the log buffer was too small and a wait was required for it to be flushed before continuing.
	MysqlResultInnodbLogWaits = "innodb-log-waits"
	// MysqlResultInnodbLogWrites : The number of physical writes to the InnoDB redo log file.
	MysqlResultInnodbLogWrites = "innodb-log-writes"
	// MysqlResultInnodbPageSize : InnoDB page size (default 16KB).
	MysqlResultInnodbPageSize = "innodb-page-size"
	// MysqlResultInnodbPagesRead : The number of pages read from the InnoDB buffer pool by operations on InnoDB tables.
	MysqlResultInnodbPagesRead = "innodb-pages-read"
	// MysqlResultInnodbPagesWritten : The number of pages written by operations on InnoDB tables.
	MysqlResultInnodbPagesWritten = "innodb-pages-written"
	// MysqlResultInnodbRowLockTimeMax : The maximum time to acquire a row lock for InnoDB tables, in milliseconds.
	MysqlResultInnodbRowLockTimeMax = "innodb-row-lock-time-max"
	// MysqlResultInnodbRowLockWaits : The number of times operations on InnoDB tables had to wait for a row lock.
	MysqlResultInnodbRowLockWaits = "innodb-row-lock-waits"
	// MysqlResultKeyBlocksUnused : The number of unused blocks in the MyISAM key cache.
	MysqlResultKeyBlocksUnused = "key-blocks-unused"
	// MysqlResultKeyBlocksUsed : The number of used blocks in the MyISAM key cache.
	MysqlResultKeyBlocksUsed = "key-blocks-used"
	// MysqlResultKeyReads : The number of physical reads of a key block from disk into the MyISAM key cache.
	MysqlResultKeyReads = "key-reads"
	// MysqlResultKeyWrites : The number of physical writes of a key block from the MyISAM key cache to disk.
	MysqlResultKeyWrites = "key-writes"
	// MysqlResultOpenFiles : The number of files that are open. This count includes regular files opened by the server.
	MysqlResultOpenFiles = "open-files"
	// MysqlResultOpenStreams : The number of streams that are open (used mainly for logging).
	MysqlResultOpenStreams = "open-streams"
	// MysqlResultOpenTables : The number of tables that are open.
	MysqlResultOpenTables = "open-tables"
	// MysqlResultPreparedStmtCount : The current number of prepared statements.
	MysqlResultPreparedStmtCount = "prepared-stmt-count"
	// MysqlResultQueries : The number of statements executed by the server.
	MysqlResultQueries = "queries"
	// MysqlResultSelectFullJoin : The number of joins that perform table scans because they do not use indexes.
	MysqlResultSelectFullJoin = "select-full-join"
	// MysqlResultSelectFullRangeJoin : The number of joins that used a range search on a reference table.
	MysqlResultSelectFullRangeJoin = "select-full-range-join"
	// MysqlResultSlowQueries : The number of queries that have taken more than long-query-time seconds.
	MysqlResultSlowQueries = "slow-queries"
	// MysqlResultUptime : The number of seconds that the server has been up.
	MysqlResultUptime = "uptime"
)

// Name returns the name of the probe "mysql"
func (db *Mysql) Name() string {
	return "mysql"
}

// Configure configures the Mysql probe and prepares the Mysql request.
func (db *Mysql) Configure(config map[string]interface{}) (err error) {
	// Check for mandatory server field
	_, ok := config[MysqlParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", MysqlParamAddress)
	}
	db.address, ok = config[MysqlParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", MysqlParamAddress, config[MysqlParamAddress])
	}
	// Check for mandatory server field
	_, ok = config[MysqlParamLogin]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", MysqlParamLogin)
	}
	db.login, ok = config[MysqlParamLogin].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", MysqlParamLogin, config[MysqlParamLogin])
	}
	// Check for mandatory server field
	_, ok = config[MysqlParamPassword]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", MysqlParamPassword)
	}
	db.password, ok = config[MysqlParamPassword].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", MysqlParamPassword, config[MysqlParamPassword])
	}

	return nil
}

// Execute sends an Mysql stats request to the server and synchronously waits for the reply.
func (db *Mysql) Execute() ([]*Result, error) {
	var err error

	dsn := fmt.Sprintf("%s:%s@tcp(%s)/", db.login, db.password, db.address)

	db.conn, err = sql.Open("mysql", dsn)
	if err != nil {
		return nil, fmt.Errorf("unable to open connection to MySQL server: %v", err)
	}
	defer db.conn.Close()

	err = db.conn.Ping()
	if err != nil {
		return nil, fmt.Errorf("connection to %s is not working: %v", db.address, err)
	}

	log.Debugf("Requesting MySQL stats: %s", db.address)

	res, err := db.conn.Query("SHOW STATUS")
	if err != nil {
		return nil, fmt.Errorf("unable to get stats from \"%s\": %v", db.address, err)
	}

	stats, err := scanMysqlStats(res)
	if err != nil {
		return nil, fmt.Errorf("unable to parse output: %v", err)
	}

	fields := gatherMysqlFields(stats)

	log.Debugf("returning %v", fields)
	return []*Result{{Fields: fields}}, nil
}

func scanMysqlStats(r *sql.Rows) (stats map[string]string, err error) {
	stats = make(map[string]string)

	for r.Next() {
		var line struct {
			key   string
			value string
		}
		err := r.Scan(
			&line.key,
			&line.value,
		)
		if err != nil {
			continue
		}

		stats[line.key] = line.value
	}
	err = r.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return stats, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

func gatherMysqlFields(stats map[string]string) map[string]interface{} {
	fields := make(map[string]interface{})

	for k, v := range stats {
		switch strings.ToLower(k) {
		case "aborted_clients":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultAbortedClients] = n
			}
		case "aborted_connects":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultAbortedConnects] = n
			}
		case "binlog_cache_use":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultBinlogCacheUse] = n
			}
		case "binlog_stmt_cache_use":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultBinlogStmtCacheUse] = n
			}
		case "bytes_received":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultBytesReceived] = n
			}
		case "bytes_sent":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultBytesSent] = n
			}
		case "com_begin":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComBegin] = n
			}
		case "com_change_db":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComChangeDb] = n
			}
		case "com_change_master":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComChangeMaster] = n
			}
		case "com_commit":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComCommit] = n
			}
		case "com_create_db":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComCreateDb] = n
			}
		case "com_delete":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComDelete] = n
			}
		case "com_delete_multi":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComDeleteMulti] = n
			}
		case "com_insert":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComInsert] = n
			}
		case "com_rollback":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComRollback] = n
			}
		case "com_select":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComSelect] = n
			}
		case "com_stmt_execute":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComStmtExecute] = n
			}
		case "com_stmt_fetch":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComStmtFetch] = n
			}
		case "com_truncate":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComTruncate] = n
			}
		case "com_update":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultComUpdate] = n
			}
		case "connection_errors_accept":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultConnectionErrorsAccept] = n
			}
		case "connection_errors_internal":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultConnectionErrorsInternal] = n
			}
		case "connection_errors_max_connections":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultConnectionErrorsMaxConnections] = n
			}
		case "connection_errors_peer_address":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultConnectionErrorsPeerAddress] = n
			}
		case "connection_errors_select":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultConnectionErrorsSelect] = n
			}
		case "connection_errors_tcpwrap":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultConnectionErrorsTcpwrap] = n
			}
		case "connections":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultConnections] = n
			}
		case "created_tmp_disk_tables":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultCreatedTmpDiskTables] = n
			}
		case "created_tmp_tables":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultCreatedTmpTables] = n
			}
		case "flush_commands":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultFlushCommands] = n
			}
		case "handler_read_first":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultHandlerReadFirst] = n
			}
		case "handler_read_key":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultHandlerReadKey] = n
			}
		case "handler_read_last":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultHandlerReadLast] = n
			}
		case "handler_read_next":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultHandlerReadNext] = n
			}
		case "handler_read_prev":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultHandlerReadPrev] = n
			}
		case "handler_read_rnd":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultHandlerReadRnd] = n
			}
		case "handler_read_rnd_next":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultHandlerReadRndNext] = n
			}
		case "innodb_buffer_pool_pages_data":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbBufferPoolPagesData] = n
			}
		case "innodb_buffer_pool_pages_dirty":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbBufferPoolPagesDirty] = n
			}
		case "innodb_buffer_pool_pages_flushed":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbBufferPoolPagesFlushed] = n
			}
		case "innodb_buffer_pool_pages_free":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbBufferPoolPagesFree] = n
			}
		case "innodb_buffer_pool_pages_misc":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbBufferPoolPagesMisc] = n
			}
		case "innodb_data_fsyncs":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbDataFsyncs] = n
			}
		case "innodb_data_reads":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbDataReads] = n
			}
		case "innodb_data_writes":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbDataWrites] = n
			}
		case "innodb_log_waits":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbLogWaits] = n
			}
		case "innodb_log_writes":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbLogWrites] = n
			}
		case "innodb_page_size":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbPageSize] = n
			}
		case "innodb_pages_read":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbPagesRead] = n
			}
		case "innodb_pages_written":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbPagesWritten] = n
			}
		case "innodb_row_lock_time_max":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbRowLockTimeMax] = n
			}
		case "innodb_row_lock_waits":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultInnodbRowLockWaits] = n
			}
		case "key_blocks_unused":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultKeyBlocksUnused] = n
			}
		case "key_blocks_used":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultKeyBlocksUsed] = n
			}
		case "key_reads":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultKeyReads] = n
			}
		case "key_writes":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultKeyWrites] = n
			}
		case "open_files":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultOpenFiles] = n
			}
		case "open_streams":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultOpenStreams] = n
			}
		case "open_tables":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultOpenTables] = n
			}
		case "prepared_stmt_count":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultPreparedStmtCount] = n
			}
		case "queries":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultQueries] = n
			}
		case "select_full_join":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultSelectFullJoin] = n
			}
		case "select_full_range_join":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultSelectFullRangeJoin] = n
			}
		case "slow_queries":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultSlowQueries] = n
			}
		case "uptime":
			if n, err := strconv.ParseUint(v, 10, 64); err == nil {
				fields[MysqlResultUptime] = n
			}
		}
	}

	return fields
}
