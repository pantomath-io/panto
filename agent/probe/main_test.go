// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"math/rand"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

func TestMain(m *testing.M) {
	setUp()        // Setup for tests
	res := m.Run() // Run the actual tests
	tearDown()     // Teardown after running the tests
	os.Exit(res)
}

func setUp() {
	// Check if this is live testing
	if util.LiveTest(nil) {
		rand.Seed(time.Now().UnixNano())

		viper.BindEnv("redis.address", "REDIS_ADDRESS")
		viper.SetDefault("redis.address", "redis:6379")

		viper.BindEnv("influxdb.address", "INFLUXDB_ADDRESS")
		viper.SetDefault("influxdb.address", "http://influxdb:8086")

		viper.BindEnv("memcached.address", "MEMCACHED_ADDRESS")
		viper.SetDefault("memcached.address", "memcached:11211")

		viper.BindEnv("mysql.address", "MYSQL_ADDRESS")
		viper.SetDefault("mysql.address", "mysql")
		viper.BindEnv("mysql.login", "MYSQL_LOGIN")
		viper.SetDefault("mysql.login", "root")
		viper.BindEnv("mysql.password", "MYSQL_PASSWORD")
		viper.SetDefault("mysql.password", "password")

		viper.BindEnv("postgresql.address", "POSTGRESQL_ADDRESS")
		viper.SetDefault("postgresql.address", "postgres")
		viper.BindEnv("postgresql.login", "POSTGRESQL_LOGIN")
		viper.SetDefault("postgresql.login", "postgres")
		viper.BindEnv("postgresql.password", "POSTGRESQL_PASSWORD")
		viper.SetDefault("postgresql.password", "password")
		viper.BindEnv("postgresql.sslmode", "POSTGRESQL_SSLMODE")
		viper.SetDefault("postgresql.sslmode", "disable")

		viper.BindEnv("elasticsearch.address", "ELASTICSEARCH_ADDRESS")
		viper.SetDefault("elasticsearch.address", "elasticsearch:9200")

		viper.BindEnv("nginx.url", "NGINX_URL")
		viper.SetDefault("nginx.url", "http://nginx/basic_status")

		viper.BindEnv("PHPFPM.address", "PHPFPM_ADDRESS")
		viper.SetDefault("PHPFPM.address", "phpfpm:9000")
		viper.BindEnv("PHPFPM.url", "PHPFPM_URL")
		viper.SetDefault("PHPFPM.url", "/status")

		viper.BindEnv("mongodb.address", "MONGODB_ADDRESS")
		viper.SetDefault("mongodb.address", "mongo:27017")

		viper.BindEnv("docker.address", "DOCKER_ADDRESS")
		viper.SetDefault("docker.address", "tcp://docker:2375")

		viper.BindEnv("cassandra.address", "CASSANDRA_ADDRESS")
		viper.SetDefault("cassandra.address", "cassandra:8778/jolokia/read")

		viper.BindEnv("snmp.address", "SNMP_ADDRESS")
		viper.SetDefault("snmp.address", "snmpd")
		viper.BindEnv("snmp.address", "SNMP_PORT")
		viper.SetDefault("snmp.port", 161)
		viper.BindEnv("snmp.oid", "SNMP_OID")
		viper.SetDefault("snmp.oid", "1.3.6.1.2.1.1.3.0")

		viper.BindEnv("sybase.address", "SYBASE_ADDRESS")
		viper.SetDefault("sybase.address", "sybase:5000")
		viper.BindEnv("sybase.login", "SYBASE_LOGIN")
		viper.SetDefault("sybase.login", "sa")
		viper.BindEnv("sybase.password", "SYBASE_PASSWORD")
		viper.SetDefault("sybase.password", "myPassword")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		time.Sleep(1 * time.Second)
		w.Write([]byte(responseBody))
	})
	go http.ListenAndServe("localhost:9999", nil)
}

func tearDown() {}
