// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"
	"reflect"
)

var registry map[string]reflect.Type

// Probes is the list of Probes known in the registry by default
var Probes = [...]Probe{
	&Cassandra{},
	&Checksum{},
	&CPU{},
	&DiskUsage{},
	&DiskIO{},
	&Docker{},
	&Elasticsearch{},
	&HTTP{},
	&InfluxDB{},
	&Load{},
	&Memcached{},
	&Memory{},
	&MongoDB{},
	&Mysql{},
	&Network{},
	&Nginx{},
	&NTP{},
	&PHPFPM{},
	&Ping{},
	&Postgresql{},
	&Process{},
	&Redis{},
	&SNMP{},
	&Sybase{},
	&TLS{},
	&Uptime{},
}

func init() {
	registry = make(map[string]reflect.Type)

	for _, p := range Probes {
		if err := RegisterProbe(p); err != nil {
			log.Errorf("unable to register probe: %s", err)
		}
	}
}

// RegisterProbe enters a new probe into the probe registry. A Probe is registered under the name
// it exposes with func `Name()`. It returns an error if a probe is already registered under this
// name.
//
// Example:
//
// ```
// err := RegisterProbe(&Ping{})
// ```
//
func RegisterProbe(p Probe) error {
	name := p.Name()
	if _, ok := registry[name]; ok {
		return fmt.Errorf("probe already registered for name \"%s\"", name)
	}

	// Get the concrete type behind the Probe interface
	// Synopsis:
	//   - Get a reflected Value of the probe (ValueOf())
	//   - Resolve one level of indirection to get a reflected Value of the
	//     concrete Probe implementation, e.g. Ping (Indirect())
	//   - Get the actual value it points to, untyped (Interface())
	//   - Get the type of that value (TypeOf())
	registry[name] = reflect.TypeOf(reflect.Indirect(reflect.ValueOf(p)).Interface())
	return nil
}

// NewProbe returns a new unconfigured instance of the Probe registered under `name`. It
// fails if no Probe could be found in the registry for this name.
func NewProbe(name string) (Probe, error) {
	t, ok := registry[name]
	if !ok {
		return nil, fmt.Errorf("no probe registered for name \"%s\"", name)
	}

	// Create new reflected Value of a Probe
	v := reflect.New(t)
	// Extract an actual value of type Probe
	i := v.Interface()
	// Cast it to the Probe type for return
	// (should never fail, since all types were registered using RegisterProbe)
	p, _ := i.(Probe)

	return p, nil
}
