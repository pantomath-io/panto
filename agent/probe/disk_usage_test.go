// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*DiskUsage)(nil)

func TestDiskUsageName(t *testing.T) {
	d := DiskUsage{}
	if d.Name() != "disk-usage" {
		t.Error("Disk Usage probe name is not \"disk-usage\"")
	}
}

func TestDiskUsageConfigure(t *testing.T) {
	disk := DiskUsage{}

	config := map[string]interface{}{}

	err := disk.Configure(config)
	if err != nil {
		t.Fatalf("couldn't configure Disk Usage probe: %s", err)
	}
}

func TestDiskUsageExecute(t *testing.T) {
	d := DiskUsage{}

	res, err := d.Execute()
	if err != nil {
		t.Errorf("couldn't execute Disk probe: %s", err)
	}
	if len(res) == 0 {
		t.Errorf("no items in results")
	}

	expectedKeys := map[string]interface{}{
		DiskUsageResultFree:              reflect.Uint64,
		DiskUsageResultUsed:              reflect.Uint64,
		DiskUsageResultUsedPercent:       reflect.Float32,
		DiskUsageResultInodesFree:        reflect.Uint64,
		DiskUsageResultInodesUsed:        reflect.Uint64,
		DiskUsageResultInodesUsedPercent: reflect.Float32,
	}
	for _, r := range res {
		for k := range expectedKeys {
			if _, ok := r.Fields[k]; !ok {
				t.Errorf("missing key \"%s\" in load results", k)
			}
		}
		for k, field := range r.Fields {
			kind, ok := expectedKeys[k]
			if !ok {
				t.Errorf("unexpected key \"%s\" in load results", k)
				continue
			}
			if kind != reflect.TypeOf(field).Kind() {
				t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
				continue
			}
		}
		percent := r.Fields[DiskUsageResultUsedPercent].(float32)
		if percent < 0.0 || percent > 100.0 {
			t.Fatalf("Disk usage %% is not in range [0, 100]: %f", percent)
		}
		percent = r.Fields[DiskUsageResultInodesUsedPercent].(float32)
		if percent < 0.0 || percent > 100.0 {
			t.Errorf("Disk free inodes %% is not in range [0, 100]: %f", percent)
		}

		path, ok := r.Tags[DiskUsageTagPath]
		if !ok {
			t.Errorf("Missing tag %s", DiskUsageTagPath)
		}
		if path == "" {
			t.Errorf("Empty disk path")
		}
	}
}
