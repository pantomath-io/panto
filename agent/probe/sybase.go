// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"database/sql"
	"fmt"
	"strings"

	// Go Sybase Driver is an implementation of Go's database/sql/driver interface
	_ "github.com/thda/tds"
)

// Sybase is a probe that request stats from a Sybase server.
type Sybase struct {
	address         string
	login           string
	password        string
	blockedProcess  int
	longTransaction int
}

const (
	// Sybase configuration parameters

	// SybaseParamAddress : Sybase server (host:port) (`string`)
	SybaseParamAddress = "address"
	// SybaseParamLogin : Sybase user account (`string`)
	SybaseParamLogin = "login"
	// SybaseParamPassword : Sybase user password (`string`)
	SybaseParamPassword = "password"
	// SybaseParamBlockedProcess : Number of seconds before a process is blocked (`int`) default 30
	SybaseParamBlockedProcess = "blocked-process"
	// SybaseParamLongTransactions : Number of seconds before a transaction is considered long (`int`) default 30
	SybaseParamLongTransactions = "long-transaction"

	// Sybase results

	// SybaseResultVersion : The version of the server.
	SybaseResultVersion = "version"
	// SybaseResultDatabasesCount : The number of databases on the server.
	SybaseResultDatabasesCount = "db-count"
	// SybaseResultProcessesCount : The number of processes on the server.
	SybaseResultProcessesCount = "proc-count"
	// SybaseResultBlockedProcesses : The number of processes blocked for more than SybaseParamBlockedProcess seconds.
	SybaseResultBlockedProcesses = "blocked-proc-count"
	// SybaseResultLongTransactions : The number of transactions lasting more than SybaseParamLongTransaction seconds.
	SybaseResultLongTransactions = "long-transaction"
	// SybaseResultDBSegmap : The number of segmap associated with a database.
	SybaseResultDBSegmap = "segmap"
	// SybaseResultDBAllocated : The number of allocated MB associated with a database.
	SybaseResultDBAllocated = "allocated"
	// SybaseResultDBFree : The number of free MB associated with a database.
	SybaseResultDBFree = "free"

	// Sybase result tags

	// SybaseTagDBname : The name of the database of the metric, e.g. "master"
	SybaseTagDBname = "dbname"
)

// Name returns the name of the probe "sybase"
func (db *Sybase) Name() string {
	return "sybase"
}

// Configure configures the Sybase probe and prepares the Sybase request.
func (db *Sybase) Configure(config map[string]interface{}) (err error) {
	// Check for mandatory server field
	_, ok := config[SybaseParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", SybaseParamAddress)
	}
	db.address, ok = config[SybaseParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", SybaseParamAddress, config[SybaseParamAddress])
	}
	// Check for mandatory server field
	_, ok = config[SybaseParamLogin]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", SybaseParamLogin)
	}
	db.login, ok = config[SybaseParamLogin].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", SybaseParamLogin, config[SybaseParamLogin])
	}
	// Check for mandatory server field
	_, ok = config[SybaseParamPassword]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", SybaseParamPassword)
	}
	db.password, ok = config[SybaseParamPassword].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", SybaseParamPassword, config[SybaseParamPassword])
	}
	_, ok = config[SybaseParamBlockedProcess]
	if !ok {
		db.blockedProcess = 30
	} else {
		var blockedProcess float64
		blockedProcess, ok = config[SybaseParamBlockedProcess].(float64)
		if !ok {
			return fmt.Errorf("\"%s\" field is not int: %v", SybaseParamBlockedProcess, config[SybaseParamBlockedProcess])
		}
		db.blockedProcess = int(blockedProcess)
	}
	_, ok = config[SybaseParamLongTransactions]
	if !ok {
		db.longTransaction = 30
	} else {
		var longTransaction float64
		longTransaction, ok = config[SybaseParamLongTransactions].(float64)
		if !ok {
			return fmt.Errorf("\"%s\" field is not int: %v", SybaseParamLongTransactions, config[SybaseParamLongTransactions])
		}
		db.longTransaction = int(longTransaction)
	}

	return nil
}

// Execute sends an Sybase stats request to the server and synchronously waits for the reply.
func (db *Sybase) Execute() ([]*Result, error) {
	var err error
	var results []*Result
	var conn *sql.DB

	dsn := fmt.Sprintf("tds://%s:%s@%s/?charset=utf8", db.login, db.password, db.address)

	conn, err = sql.Open("tds", dsn)
	if err != nil {
		return nil, fmt.Errorf("unable to open connection to MySQL server: %v", err)
	}
	defer conn.Close()

	err = conn.Ping()
	if err != nil {
		return nil, fmt.Errorf("connection to %s is not working: %v", db.address, err)
	}

	log.Debugf("Requesting Sybase stats: %s", db.address)
	fields := make(map[string]interface{})

	// Global metrics
	{
		res := conn.QueryRow("select @@version")
		var value string
		err := res.Scan(&value)
		if err != nil {
			return nil, fmt.Errorf("unable to get version from \"%s\": %v", db.address, err)
		}
		fields[SybaseResultVersion] = value
	}
	{
		res := conn.QueryRow("declare @databases int, @processes int select @databases = count(*) from master..sysdatabases select @processes = count(*) from master..sysprocesses where suid > 0 and spid <> @@spid select @databases as databases, @processes as processes")
		var dbCount, procCount int
		err := res.Scan(&dbCount, &procCount)
		if err != nil {
			return nil, fmt.Errorf("unable to get db/proc count from \"%s\": %v", db.address, err)
		}
		fields[SybaseResultDatabasesCount] = dbCount
		fields[SybaseResultProcessesCount] = procCount
	}
	{
		res := conn.QueryRow("select count(spid) from master..sysprocesses where suid > 0  and spid <> @@spid and status = 'lock sleep' and blocked > 0 and time_blocked > ?", db.blockedProcess)
		var proc int
		err := res.Scan(&proc)
		if err != nil {
			fields[SybaseResultBlockedProcesses] = 0
		}
		fields[SybaseResultBlockedProcesses] = proc
	}
	{
		res := conn.QueryRow("select db_name(dbid) from master..syslogshold where name not like '%replication_truncation_point%' and datediff(ss, starttime, getdate()) >= ?", db.longTransaction)
		var trx int
		err := res.Scan(&trx)
		if err != nil {
			fields[SybaseResultLongTransactions] = 0
		}
		fields[SybaseResultLongTransactions] = trx
	}

	results = append(results, &Result{Fields: fields})

	// per DB metrics
	res, err := conn.Query("select convert(char(20), db_name(dbid)) dbname, segmap, sum(size) / (1048576 / @@maxpagesize) allocatedMB, case when segmap = 4 then lct_admin(\"logsegment_freepages\", dbid) else sum(curunreservedpgs(dbid, lstart, unreservedpgs)) end / (1048576 / @@maxpagesize) as freeMB from master..sysusages where dbid in (select dbid from master..sysdatabases where (status3 & 8192 != 8192 or status3 is null) and status & 32 != 32 and status2 & (16 + 32) = 0) group by dbid, segmap")
	if err != nil {
		return nil, fmt.Errorf("unable to get stats from \"%s\": %v", db.address, err)
	}
	for res.Next() {
		var dbname string
		var segmap, allocated, free int
		err := res.Scan(&dbname, &segmap, &allocated, &free)
		if err != nil {
			continue
		}
		dbfields := map[string]interface{}{
			SybaseResultDBSegmap:    segmap,
			SybaseResultDBAllocated: allocated * 1024 * 1024,
			SybaseResultDBFree:      free * 1024 * 1024,
		}
		tags := map[string]string{
			SybaseTagDBname: strings.TrimSpace(dbname),
		}

		results = append(results, &Result{Fields: dbfields, Tags: tags})
	}
	if err = res.Close(); err != nil {
		return nil, fmt.Errorf("unable to close results after stats")
	}

	log.Debugf("returning %v", results)
	return results, nil
}
