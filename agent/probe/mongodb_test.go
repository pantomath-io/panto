// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"bytes"
	"reflect"
	"testing"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*MongoDB)(nil)

func TestMongoDBName(t *testing.T) {
	r := MongoDB{}
	if r.Name() != "mongodb" {
		t.Error("MongoDB probe name is not \"mongodb\"")
	}
}

func TestMongoDBConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := MongoDB{}
	err := r.Configure(map[string]interface{}{MongoDBParamAddress: viper.GetString("mongodb.address")})
	if err != nil {
		t.Errorf("couldn't configure MongoDB probe: %s", err)
	}
}

func TestMongoDBExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := MongoDB{}

	err := r.Configure(map[string]interface{}{MongoDBParamAddress: viper.GetString("mongodb.address")})
	if err != nil {
		t.Fatalf("couldn't configure MongoDB probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute MongoDB probe: %s", err)
	}
	if len(res) == 0 {
		t.Fatalf("no items in results")
	}
}

func TestMongoDBscanStatsMongo(t *testing.T) {
	var ec bson.ElementConstructor
	var response bson.Reader

	doc := bson.NewDocument(
		ec.Int64("uptimeMillis", 42),
		ec.SubDocumentFromElements("connections",
			ec.Int32("current", 42),
			ec.Int32("available", 42),
		),
		ec.SubDocumentFromElements("globalLock",
			ec.SubDocumentFromElements("currentQueue",
				ec.Int32("readers", 42),
				ec.Int32("writers", 42),
			),
		),
		ec.SubDocumentFromElements("network",
			ec.Int64("bytesIn", 42),
			ec.Int64("bytesOut", 42),
		),
		ec.SubDocumentFromElements("opcounters",
			ec.Int64("insert", 42),
			ec.Int64("query", 42),
			ec.Int64("update", 42),
			ec.Int64("delete", 42),
			ec.Int64("getmore", 42),
			ec.Int64("command", 42),
		),
		ec.SubDocumentFromElements("mem",
			ec.Int32("resident", 42),
			ec.Int32("virtual", 42),
		),
	)
	docByte, err := doc.MarshalBSON()
	if err != nil {
		t.Errorf("unable to convert BSON Document to slice of bytes: %s", err)
	}
	response, err = bson.NewFromIOReader(bytes.NewReader(docByte))
	if err != nil {
		t.Errorf("unable to convert io.reader to BSON Reader: %s", err)
	}

	fields := scanStatsMongo(response)

	t.Logf("Fields: %v", fields)

	expectedKeys := map[string]interface{}{
		MongoDBResultServerUptime:                         reflect.Int64,
		MongoDBResultServerConnectionsCurrent:             reflect.Int32,
		MongoDBResultServerConnectionsAvailable:           reflect.Int32,
		MongoDBResultServerGlobalLockActiveClientsReaders: reflect.Int32,
		MongoDBResultServerGlobalLockActiveClientWriters:  reflect.Int32,
		MongoDBResultServerNetworkIn:                      reflect.Int64,
		MongoDBResultServerNetworkOut:                     reflect.Int64,
		MongoDBResultServerOpsInsert:                      reflect.Int64,
		MongoDBResultServerOpsQuery:                       reflect.Int64,
		MongoDBResultServerOpsUpdate:                      reflect.Int64,
		MongoDBResultServerOpsDelete:                      reflect.Int64,
		MongoDBResultServerOpsGetmore:                     reflect.Int64,
		MongoDBResultServerOpsCommand:                     reflect.Int64,
		MongoDBResultServerMemResident:                    reflect.Int32,
		MongoDBResultServerMemVirtual:                     reflect.Int32,
	}
	for k := range expectedKeys {
		if _, ok := fields[k]; !ok {
			t.Errorf("missing key \"%s\" in mongodb results", k)
		}
	}
	for k, field := range fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in mongodb results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}
}
