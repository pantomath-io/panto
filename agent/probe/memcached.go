// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"bufio"
	"fmt"
	"io"
	"net"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Memcached is a probe that request  stats from a Memcached server.
type Memcached struct {
	address string
}

const (
	// Memcached configuration parameters

	// MemcachedParamAddress : Memcached server (`string`)
	MemcachedParamAddress = "address"

	// Memcached results

	// MemcachedResultUptime Number of seconds the Memcached server has been running since last restart. (uint64)
	MemcachedResultUptime = "uptime"
	// MemcachedResultCurrConnections Number of open connections to this Memcached server, should be the same value on all servers during normal operation. This is something like the count of mySQL's "SHOW PROCESSLIST" result rows. (uint64)
	MemcachedResultCurrConnections = "curr_connections"
	// MemcachedResultReservedFDs Number of misc fds used internally (uint64)
	MemcachedResultReservedFDs = "reserved_fds"
	// MemcachedResultCmdGet Number of "get" commands received since server startup not counting if they were successful or not. (uint64)
	MemcachedResultCmdGet = "cmd_get"
	// MemcachedResultCmdSet Number of "set" commands serviced since startup. (uint64)
	MemcachedResultCmdSet = "cmd_set"
	// MemcachedResultCmdFlush The "flush_all" command clears the whole cache and shouldn't be used during normal operation. (uint64)
	MemcachedResultCmdFlush = "cmd_flush"
	// MemcachedResultCmdTouch Cumulative number of touch reqs (uint64)
	MemcachedResultCmdTouch = "cmd_touch"
	// MemcachedResultGetHits Number of successful "get" commands (cache hits) since startup, divide them by the "cmd_get" value to get the cache hitrate. (uint64)
	MemcachedResultGetHits = "get_hits"
	// MemcachedResultGetMisses Number of failed "get" requests because nothing was cached for this key or the cached value was too old. (uint64)
	MemcachedResultGetMisses = "get_misses"
	// MemcachedResultGetExpired Number of items that have been requested but had already expired (uint64)
	MemcachedResultGetExpired = "get_expired"
	// MemcachedResultGetFlushed Number of items that have been requested but have been flushed via flush_all (uint64)
	MemcachedResultGetFlushed = "get_flushed"
	// MemcachedResultDeleteMisses Number of "delete" commands for keys not existing within the cache. These 107k failed deletes are deletions of non existent race keys (see above). (uint64)
	MemcachedResultDeleteMisses = "delete_misses"
	// MemcachedResultDeleteHits Stored keys may be deleted using the "delete" command, this system doesn't delete cached data itself, but it's using the Memcached to avoid recaching-races and the race keys are deleted once the race is over and fresh content has been cached. (uint64)
	MemcachedResultDeleteHits = "delete_hits"
	// MemcachedResultBytesRead Total number of bytes received from the network by this server. (uint64)
	MemcachedResultBytesRead = "bytes_read"
	// MemcachedResultBytesWritten Total number of bytes send to the network by this server. (uint64)
	MemcachedResultBytesWritten = "bytes_written"
	// MemcachedResultBytes Number of bytes currently used for caching items, this server currently uses ~6 MB of it's maximum allowed (limit_maxbytes) 1 GB cache size. (uint64)
	MemcachedResultBytes = "bytes"
	// MemcachedResultCurrItems Number of items currently in this server's cache. The production system of this development environment holds more than 8 million items. (uint64)
	MemcachedResultCurrItems = "curr_items"
	// MemcachedResultEvictions Number of objects removed from the cache to free up memory for new items because Memcached reached its maximum memory setting (limit_maxbytes). (uint64)
	MemcachedResultEvictions = "evictions"
)

// Name returns the name of the probe "ntp"
func (h *Memcached) Name() string {
	return "memcached"
}

// Configure configures the Memcached probe and prepares the Memcached request.
func (h *Memcached) Configure(config map[string]interface{}) (err error) {
	// Check for mandatory server field
	_, ok := config[MemcachedParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", MemcachedParamAddress)
	}
	h.address, ok = config[MemcachedParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", MemcachedParamAddress, config[MemcachedParamAddress])
	}

	return nil
}

// Execute sends an Memcached stats request to the server and synchronously waits for the reply.
func (h *Memcached) Execute() ([]*Result, error) {
	var err error
	var conn net.Conn

	log.Debugf("Dialing Memcached connection: %s", h.address)
	conn, err = net.Dial("tcp", h.address)
	if err != nil {
		return nil, fmt.Errorf("unable to connect \"%s\": %v", h.address, err)
	}
	defer conn.Close()

	// Set a deadline for reading. Read operation will fail if no data
	// is received after deadline.
	conn.SetReadDeadline(time.Now().Add(30 * time.Second))
	// send stats command to socket
	fmt.Fprintf(conn, "stats\n")

	stats, err := scanStats(conn)
	if err != nil {
		return nil, fmt.Errorf("unable to parse output: %v", err)
	}

	fields := gatherFieldsMemcached(stats)

	return []*Result{{Fields: fields}}, nil
}

func scanStats(r io.Reader) (stats map[string]string, err error) {
	stats = make(map[string]string)
	var stdLinePattern = regexp.MustCompile(
		"^STAT ([^ ]+) ([^\n]+)$",
	)

	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		// test end of transmission
		if strings.HasPrefix(strings.TrimSpace(scanner.Text()), "END") {
			break
		}

		// get pattern line
		m := stdLinePattern.FindStringSubmatch(strings.TrimSpace(scanner.Text()))
		if len(m) > 2 {
			stats[m[1]] = m[2]
		}
	}
	if err = scanner.Err(); err != nil {
		return nil, fmt.Errorf("unable to scan response: %v", err)
	}

	return
}

func gatherFieldsMemcached(stats map[string]string) map[string]interface{} {
	fields := make(map[string]interface{})

	if s, ok := stats["uptime"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultUptime] = n
		}
	}
	if s, ok := stats["curr_connections"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultCurrConnections] = n
		}
	}
	if s, ok := stats["reserved_fds"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultReservedFDs] = n
		}
	}
	if s, ok := stats["cmd_get"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultCmdGet] = n
		}
	}
	if s, ok := stats["cmd_set"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultCmdSet] = n
		}
	}
	if s, ok := stats["cmd_flush"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultCmdFlush] = n
		}
	}
	if s, ok := stats["cmd_touch"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultCmdTouch] = n
		}
	}
	if s, ok := stats["get_hits"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultGetHits] = n
		}
	}
	if s, ok := stats["get_misses"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultGetMisses] = n
		}
	}
	if s, ok := stats["get_expired"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultGetExpired] = n
		}
	}
	if s, ok := stats["get_flushed"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultGetFlushed] = n
		}
	}
	if s, ok := stats["delete_misses"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultDeleteMisses] = n
		}
	}
	if s, ok := stats["delete_hits"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultDeleteHits] = n
		}
	}
	if s, ok := stats["bytes_read"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultBytesRead] = n
		}
	}
	if s, ok := stats["bytes_written"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultBytesWritten] = n
		}
	}
	if s, ok := stats["bytes"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultBytes] = n
		}
	}
	if s, ok := stats["curr_items"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultCurrItems] = n
		}
	}
	if s, ok := stats["evictions"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[MemcachedResultEvictions] = n
		}
	}

	return fields
}
