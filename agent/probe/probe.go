// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"time"

	"github.com/op/go-logging"
)

var log = logging.MustGetLogger("main")

// Result encapsulates a result from a probe execution. A result is a collection of values
// mapped to strings and a collection of tags mapped by strings.
type Result struct {
	Timestamp          time.Time              `json:"timestamp"`
	ProbeConfiguration string                 `json:"probe_configuration,omitempty"`
	Check              string                 `json:"check,omitempty"`
	Fields             map[string]interface{} `json:"fields"`
	Tags               map[string]string      `json:"tags,omitempty"`
	Error              bool                   `json:"-"`
	ErrorMessage       string                 `json:"error_message,omitempty"`
}

// Probe a probe configuration and execution.
type Probe interface {
	Name() string
	Configure(map[string]interface{}) error
	Execute() ([]*Result, error)
}
