// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"
	"net"
	"strings"
	"time"

	"gitlab.com/pantomath-io/panto/util"

	psnet "github.com/shirou/gopsutil/net"
)

//---------------
// Network probe
//---------------

// Network probes network interface statistics
type Network struct {
	interfaces []string
	interval   time.Duration
}

const (

	// Network configuration parameters

	// NetworkParamInterfaces : A comma-separated list of interfaces to gather information about.
	// If the list is empty, return info about all interfaces. If the parameter is missing, return global information
	// for all interfaces.
	NetworkParamInterfaces = "interfaces"
	// NetworkParamInterval : (optional, default 1 second) interval over which network utilization is calculated
	NetworkParamInterval = "interval"

	// Network results

	// NetworkResultBytesSent : Number of bytes sent through the interface (uint64)
	NetworkResultBytesSent = "bytes-sent"
	// NetworkResultBytesRecv : Number of bytes received through the interface (uint64)
	NetworkResultBytesRecv = "bytes-received"
	// NetworkResultPacketsSent : Number of network packets sent through the interface (uint64)
	NetworkResultPacketsSent = "packets-sent"
	// NetworkResultPacketsRecv : Number of network packets receives through the interface (uint64)
	NetworkResultPacketsRecv = "packets-received"
	// NetworkResultErrIn : Number of errors while receiving through the interface (uint64)
	NetworkResultErrIn = "error-in"
	// NetworkResultErrOut : Number of errors while sending through the interface (uint64)
	NetworkResultErrOut = "error-out"
	// NetworkResultTCPConn : Number of open TCP connections on this interface (uint64)
	NetworkResultTCPConn = "tcp-connections"
	// NetworkResultUDPConn : Number of open UDP connections on this interface (uint64)
	NetworkResultUDPConn = "udp-connections"

	// Network result tags

	// NetworkTagInterface : The network interface, e.g. `"eth0"`
	NetworkTagInterface = "interface"
)

// Name returns the name of the probe "network"
func (n *Network) Name() string {
	return "network"
}

// Configure configures the network probe
func (n *Network) Configure(config map[string]interface{}) error {
	if _, ok := config[NetworkParamInterfaces]; ok {
		if interfaces, ok := config[NetworkParamInterfaces].(string); ok {
			n.interfaces = strings.Split(interfaces, ",")
		} else {
			return fmt.Errorf("\"%s\" parameter is not string: %v", NetworkParamInterfaces, config[NetworkParamInterfaces])
		}
	} else {
		n.interfaces = nil
	}

	// Check for optional interval argument
	interval, ok := config[DiskIOParamInterval]
	if ok {
		switch i := interval.(type) {
		case float64:
			interval = time.Duration(i)
		}
		n.interval, ok = interval.(time.Duration)
		if !ok {
			return fmt.Errorf(
				"\"%s\" field is not time.Duration: %v",
				DiskIOParamInterval,
				config[DiskIOParamInterval])
		}
	}
	if n.interval <= 0 {
		n.interval = 1 * time.Second
	}

	return nil
}

// Execute runs the probe and gathers network information
func (n *Network) Execute() ([]*Result, error) {
	if n.interfaces == nil {
		return executeNil(n.interval)
	}
	return execute(n.interval, n.interfaces)
}

func executeNil(interval time.Duration) ([]*Result, error) {
	countersStart, err := psnet.IOCounters(false)
	if err != nil {
		return nil, fmt.Errorf("couldn't get network I/O counters at start of interval: %s", err)
	}

	time.Sleep(interval)

	countersEnd, err := psnet.IOCounters(false)
	if err != nil {
		return nil, fmt.Errorf("couldn't get network I/O counters at end of interval: %s", err)
	}

	tcpConns, err := psnet.Connections("tcp")
	if err != nil {
		return nil, err
	}
	udpConns, err := psnet.Connections("udp")
	if err != nil {
		return nil, err
	}

	cStart := countersStart[0]
	cEnd := countersEnd[0]
	res := []*Result{
		{
			Fields: map[string]interface{}{
				NetworkResultBytesSent:   uint64(float64(cEnd.BytesSent-cStart.BytesSent) / float64(interval.Seconds())),
				NetworkResultBytesRecv:   uint64(float64(cEnd.BytesRecv-cStart.BytesRecv) / float64(interval.Seconds())),
				NetworkResultPacketsSent: uint64(float64(cEnd.PacketsSent-cStart.PacketsSent) / float64(interval.Seconds())),
				NetworkResultPacketsRecv: uint64(float64(cEnd.PacketsRecv-cStart.PacketsRecv) / float64(interval.Seconds())),
				NetworkResultErrIn:       uint64(float64(cEnd.Errin-cStart.Errin) / float64(interval.Seconds())),
				NetworkResultErrOut:      uint64(float64(cEnd.Errout-cStart.Errout) / float64(interval.Seconds())),
				NetworkResultTCPConn:     uint64(len(tcpConns)),
				NetworkResultUDPConn:     uint64(len(udpConns)),
			},
			Tags: map[string]string{
				NetworkTagInterface: cEnd.Name,
			},
		},
	}

	return res, nil
}

func execute(interval time.Duration, ifs []string) ([]*Result, error) {
	countersStart, err := psnet.IOCounters(false)
	if err != nil {
		return nil, fmt.Errorf("couldn't get network I/O counters at start of interval: %s", err)
	}

	time.Sleep(interval)

	countersEnd, err := psnet.IOCounters(false)
	if err != nil {
		return nil, fmt.Errorf("couldn't get network I/O counters at end of interval: %s", err)
	}

	tcpConns, err := psnet.Connections("tcp")
	if err != nil {
		return nil, err
	}
	udpConns, err := psnet.Connections("udp")
	if err != nil {
		return nil, err
	}

	tcpCountsPerIf := connCount(tcpConns)
	udpCountsPerIf := connCount(udpConns)

	res := make([]*Result, 0, len(ifs))
	for _, cStart := range countersStart {
		if len(ifs) == 0 || util.Contains(ifs, cStart.Name) {
			// Find end counters
			var found bool
			var cEnd psnet.IOCountersStat
			for _, cEnd = range countersEnd {
				if cStart.Name == cEnd.Name {
					found = true
					break
				}
			}
			if !found {
				log.Warningf("interface \"%s\" was found at start of interval, but not at end", cStart.Name)
			}

			r := Result{
				Fields: map[string]interface{}{
					NetworkResultBytesSent:   uint64(float64(cEnd.BytesSent-cStart.BytesSent) / float64(interval.Seconds())),
					NetworkResultBytesRecv:   uint64(float64(cEnd.BytesRecv-cStart.BytesRecv) / float64(interval.Seconds())),
					NetworkResultPacketsSent: uint64(float64(cEnd.PacketsSent-cStart.PacketsSent) / float64(interval.Seconds())),
					NetworkResultPacketsRecv: uint64(float64(cEnd.PacketsRecv-cStart.PacketsRecv) / float64(interval.Seconds())),
					NetworkResultErrIn:       uint64(float64(cEnd.Errin-cStart.Errin) / float64(interval.Seconds())),
					NetworkResultErrOut:      uint64(float64(cEnd.Errout-cStart.Errout) / float64(interval.Seconds())),
					NetworkResultTCPConn:     tcpCountsPerIf[cStart.Name],
					NetworkResultUDPConn:     udpCountsPerIf[cStart.Name],
				},
				Tags: map[string]string{
					NetworkTagInterface: cStart.Name,
				},
			}
			res = append(res, &r)
		}
	}

	return res, nil
}

func connCount(conns []psnet.ConnectionStat) (counts map[string]uint64) {
	allIfs, err := psnet.Interfaces()
	if err != nil {
		return
	}

	counts = make(map[string]uint64)
ConnsLoop:
	for _, c := range conns {
		cip := net.ParseIP(c.Laddr.IP)
		if cip == nil {
			log.Warningf("Could not parse connection IP %s", c.Laddr.IP)
			continue
		}
		for _, interf := range allIfs {
			for _, a := range interf.Addrs {
				ip, ipnet, err := net.ParseCIDR(a.Addr)
				if err != nil {
					log.Warningf("Couldn't parse address %s for interface %s: %s", a.Addr, interf.Name, err)
					continue
				}
				if ip.Equal(cip) || ipnet.Contains(cip) {
					counts[interf.Name]++
					continue ConnsLoop
				}
			}
		}
	}
	return
}
