// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"fmt"
	"strings"

	"gitlab.com/pantomath-io/panto/util"

	"github.com/shirou/gopsutil/process"
)

//---------------
// Process probe
//---------------

// Process probes memory usage
type Process struct {
	names []string
}

const (

	// Process configuration parameters

	// ProcessParamNames : (mandatory) The names of the processes to gather info for. A process "name" is usually
	// the name of the executable that was launched.
	ProcessParamNames = "names"

	// Process results

	// ProcessResultCommandLine : The full command-line used to launch this process (string)
	ProcessResultCommandLine = "command-line"
	// ProcessResultPID : The PID of this process (int32)
	ProcessResultPID = "pid"
	// ProcessResultCreateTime : The exact time this process started (int64)
	ProcessResultCreateTime = "create-time"
	// ProcessResultStatus : A character representing the current status of the process. R: Running, S: Sleep, T: Stop,
	// I: Idle, Z: Zombie, W: Wait, L: Lock. (string)
	ProcessResultStatus = "status"
	// ProcessResultCPU : An approximate percentage of CPU power used by this process (float64)
	ProcessResultCPU = "cpu"
	// ProcessResultMemory : An approximate percentage of RAM used by this process (float64)
	ProcessResultMemory = "memory"
	// ProcessResultRSS : The "Resident Set Size" is the amount of RAM used by this process, in bytes (uint64)
	ProcessResultRSS = "rss"
	// ProcessResultVMS : The "Virtual Memory Size" is the total size of memory addressable by this process, in bytes
	// (uint64)
	ProcessResultVMS = "vms"
	// ProcessResultThreads : The number of threads this process is currently running (uint64)
	ProcessResultThreads = "threads"
	// ProcessResultOpenFiles : The number of files currently opened by this process (uint64)
	ProcessResultOpenFiles = "files"
	// ProcessResultConnections : The number of network connections open by this process (uint64)
	ProcessResultConnections = "connections"

	// Process result tags

	// ProcessTagName : The name of this process (string)
	ProcessTagName = "name"
)

// Name returns the probe name "process"
func (p *Process) Name() string {
	return "process"
}

// Configure configures the Process probe
func (p *Process) Configure(config map[string]interface{}) error {
	if _, ok := config[ProcessParamNames]; ok {
		if names, ok := config[ProcessParamNames].(string); ok {
			p.names = strings.Split(names, ",")
		} else {
			return fmt.Errorf("\"%s\" field is not string: %v", ProcessParamNames, config[ProcessParamNames])
		}
	} else {
		return fmt.Errorf("Missing \"%s\" field", ProcessParamNames)
	}

	if len(p.names) == 0 || (len(p.names) == 1 && p.names[0] == "") {
		return fmt.Errorf("Empty list of process names")
	}

	return nil
}

// Execute runs the Process probe and returns details about processes
func (p *Process) Execute() ([]*Result, error) {
	processes, err := process.Processes()
	if err != nil {
		return nil, err
	}

	res := make([]*Result, 0)
	for _, proc := range processes {
		name, err := proc.Name()
		if err != nil {
			log.Warningf("Couldn't get process name: %s. Skipping...", err)
			continue
		}
		if p.names == nil || util.Contains(p.names, name) {
			fields := make(map[string]interface{})
			fields[ProcessResultCommandLine], err = proc.Cmdline()
			if err != nil {
				log.Warningf("Couldn't get command line for process %d(%s): %s", proc.Pid, name, err)
			}
			fields[ProcessResultPID] = proc.Pid
			fields[ProcessResultStatus], err = proc.Status()
			if err != nil {
				log.Warningf("Couldn't get status for process %d(%s): %s", proc.Pid, name, err)
			}
			cpu, err := proc.CPUPercent()
			if err != nil {
				log.Warningf("Couldn't get CPU for process %d(%s): %s", proc.Pid, name, err)
			}
			fields[ProcessResultCPU] = float32(cpu)
			fields[ProcessResultCreateTime], err = proc.CreateTime()
			if err != nil {
				log.Warningf("Couldn't get creation time for process %d(%s): %s", proc.Pid, name, err)
			}
			fields[ProcessResultMemory], err = proc.MemoryPercent()
			if err != nil {
				log.Warningf("Couldn't get memory for process %d(%s): %s", proc.Pid, name, err)
			}
			if mem, err := proc.MemoryInfo(); err == nil {
				fields[ProcessResultRSS] = mem.RSS
				fields[ProcessResultVMS] = mem.VMS
			} else {
				fields[ProcessResultRSS] = uint64(0)
				fields[ProcessResultVMS] = uint64(0)
			}
			threads, err := proc.Threads()
			if err != nil {
				log.Warningf("Couldn't get thread for process %d(%s): %s", proc.Pid, name, err)
			}
			fields[ProcessResultThreads] = uint64(len(threads))
			files, err := proc.OpenFiles()
			if err != nil {
				log.Warningf("Couldn't get open files for process %d(%s): %s", proc.Pid, name, err)
			}
			fields[ProcessResultOpenFiles] = uint64(len(files))
			conns, err := proc.Connections()
			if err != nil {
				log.Warningf("Couldn't get connections for process %d(%s): %s", proc.Pid, name, err)
			}
			fields[ProcessResultConnections] = uint64(len(conns))

			res = append(res, &Result{
				Fields: fields,
				Tags: map[string]string{
					ProcessTagName: name,
				},
			})
		}
	}

	return res, nil
}
