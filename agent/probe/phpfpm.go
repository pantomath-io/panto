// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"

	fcgiclient "github.com/tomasen/fcgi_client"
)

// PHPFPM is a probe that request  stats from a PHPFPM server.
type PHPFPM struct {
	address string
	url     string
	env     map[string]string
}

const (
	// PHPFPM configuration parameters

	// PHPFPMParamAddress : PHPFPM server (`string`)
	PHPFPMParamAddress = "address"
	// PHPFPMParamURL : PHPFPM status endpoint URL (`string`)
	PHPFPMParamURL = "url"

	// PHPFPM results

	// PHPFPMResultListenQueue the number of request in the queue of pending connections. (uint)
	PHPFPMResultListenQueue = "listen_queue"
	// PHPFPMResultIdleProcesses the number of idle processes. (uint)
	PHPFPMResultIdleProcesses = "idle_processes"
	// PHPFPMResultActiveProcesses the number of active processes. (uint)
	PHPFPMResultActiveProcesses = "active_processes"
	// PHPFPMResultSlowRequests Enable php-fpm slow-log before you consider this. If this value is non-zero you may have slow php processes. (uint)
	PHPFPMResultSlowRequests = "slow_requests"
)

// Name returns the name of the probe "ntp"
func (h *PHPFPM) Name() string {
	return "PHP-FPM"
}

// Configure configures the PHPFPM probe and prepares the PHPFPM request.
func (h *PHPFPM) Configure(config map[string]interface{}) (err error) {
	// Check for mandatory server field
	_, ok := config[PHPFPMParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", PHPFPMParamAddress)
	}
	h.address, ok = config[PHPFPMParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", PHPFPMParamAddress, config[PHPFPMParamAddress])
	}

	// Check for optionnal configuration field
	_, ok = config[PHPFPMParamURL]
	if !ok {
		h.url = "/status"
	} else {
		h.url, ok = config[PHPFPMParamURL].(string)
		if !ok {
			return fmt.Errorf("\"%s\" field is not string: %v", PHPFPMParamURL, config[PHPFPMParamURL])
		}
	}

	h.env = make(map[string]string)
	h.env["SCRIPT_NAME"] = h.url
	h.env["SCRIPT_FILENAME"] = h.url
	h.env["REQUEST_METHOD"] = "GET"

	return nil
}

// Execute sends an PHPFPM stats request to the server and synchronously waits for the reply.
func (h *PHPFPM) Execute() ([]*Result, error) {
	var err error
	var client *fcgiclient.FCGIClient

	log.Debugf("Dialing PHP-FPM server: %s", h.address)
	client, err = fcgiclient.Dial("tcp", h.address)
	if err != nil {
		return nil, fmt.Errorf("unable to connect \"%s\": %v", h.address, err)
	}
	defer client.Close()

	// send FCGI request to PHP FPM
	resp, err := client.Get(h.env)
	if err != nil {
		log.Errorf("unable to get status page: %v", err)
	}

	stats, err := scanPHPFPMStats(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("unable to parse output: %v", err)
	}

	fields := gatherFieldsPHPFPM(stats)

	return []*Result{{Fields: fields}}, nil
}

func scanPHPFPMStats(r io.Reader) (stats map[string]string, err error) {
	stats = make(map[string]string)
	var stdLinePattern = regexp.MustCompile(
		"^([^:]+):[ \t]*([^\n]+)$",
	)

	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		// test end of transmission
		// get pattern line
		m := stdLinePattern.FindStringSubmatch(strings.TrimSpace(scanner.Text()))
		if len(m) > 2 {
			stats[m[1]] = m[2]
		}
	}
	if err = scanner.Err(); err != nil {
		return nil, fmt.Errorf("unable to scan response: %v", err)
	}

	return
}

func gatherFieldsPHPFPM(stats map[string]string) map[string]interface{} {
	fields := make(map[string]interface{})

	if s, ok := stats["listen queue"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[PHPFPMResultListenQueue] = n
		}
	}
	if s, ok := stats["idle processes"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[PHPFPMResultIdleProcesses] = n
		}
	}
	if s, ok := stats["active processes"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[PHPFPMResultActiveProcesses] = n
		}
	}
	if s, ok := stats["slow requests"]; ok {
		if n, err := strconv.ParseUint(s, 10, 64); err == nil {
			fields[PHPFPMResultSlowRequests] = n
		}
	}

	return fields
}
