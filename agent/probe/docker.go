// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
)

// Docker is a probe that requests the Docker API and gathers metrics.
type Docker struct {
	address string
}

const (
	// Docker configuration parameters

	// DockerParamAddress : Docker host (`string`)
	DockerParamAddress = "address"

	// Docker results

	// DockerResultContainerCountCreated : the number of container with state *created* in the Docker daemon (state is a tag) (`int64`)
	DockerResultContainerCountCreated = "container-count-created"
	// DockerResultContainerCountRunning : the number of container with state *running* in the Docker daemon (state is a tag) (`int64`)
	DockerResultContainerCountRunning = "container-count-running"
	// DockerResultContainerCountPaused : the number of container with state *paused* in the Docker daemon (state is a tag) (`int64`)
	DockerResultContainerCountPaused = "container-count-paused"
	// DockerResultContainerCountRestarting : the number of container with state *restarting* in the Docker daemon (state is a tag) (`int64`)
	DockerResultContainerCountRestarting = "container-count-restarting"
	// DockerResultContainerCountRemoving : the number of container with state *removing* in the Docker daemon (state is a tag) (`int64`)
	DockerResultContainerCountRemoving = "container-count-removing"
	// DockerResultContainerCountExited : the number of container with state *exited* in the Docker daemon (state is a tag) (`int64`)
	DockerResultContainerCountExited = "container-count-exited"
	// DockerResultContainerCountDead : the number of container with state *dead* in the Docker daemon (state is a tag) (`int64`)
	DockerResultContainerCountDead = "container-count-dead"
	// DockerResultContainerCPUTotalUsage : Total CPU time consumed (container name is a tag) (`int64`)
	DockerResultContainerCPUTotalUsage = "cpu-totalusage"
	// DockerResultContainerMemorylUsage : current res_counter usage for memory (container name is a tag) (`float64`)
	DockerResultContainerMemorylUsage = "mem-usage"
	// DockerResultContainerNetworkReceived : Bytes received (container name is a tag) (`int64`)
	DockerResultContainerNetworkReceived = "network-bytes-received"
	// DockerResultContainerNetworkErrorReceived : Received errors (container name is a tag) (`int64`)
	DockerResultContainerNetworkErrorReceived = "network-errors-received"
	// DockerResultContainerNetworkDroppedReceived : Incoming packets dropped (container name is a tag) (`int64`)
	DockerResultContainerNetworkDroppedReceived = "network-dropped-received"
	// DockerResultContainerNetworkSent : Bytes sent (container name is a tag) (`int64`)
	DockerResultContainerNetworkSent = "network-bytes-sent"
	// DockerResultContainerNetworkErrorSent : Sent errors (container name is a tag) (`int64`)
	DockerResultContainerNetworkErrorSent = "network-errors-sent"
	// DockerResultContainerNetworkDroppedSent : Outgoing packets dropped (container name is a tag) (`int64`)
	DockerResultContainerNetworkDroppedSent = "network-dropped-sent"
	// DockerResultNetworkCount : the number of networks in the Docker daemon (`int64`)
	DockerResultNetworkCount = "network-count"
	// DockerResultVolumeCount : the number of volumes in the Docker daemon (`int64`)
	DockerResultVolumeCount = "volume-count"
	// DockerResultVolumeSize : the number of bytes used by all the volumes in the Docker daemon (`int64`)
	DockerResultVolumeSize = "volume-size"
	// DockerResultImageCount : the number of images in the Docker daemon (`int64`)
	DockerResultImageCount = "image-count"
	// DockerResultImageSize : the number of bytes used by all the images in the Docker daemon (`int64`)
	DockerResultImageSize = "image-size"

	// DockerTagContainerName : The name of a container, e.g. `"goofy_hodgkin"`
	DockerTagContainerName = "name"
	// DockerTagContainerInterface : The name of a container interface, e.g. `"eth0"`
	DockerTagContainerInterface = "interface"
)

// Name returns the name of the probe "ntp"
func (d *Docker) Name() string {
	return "docker"
}

// Configure configures the Docker probe and prepares the Docker request.
func (d *Docker) Configure(config map[string]interface{}) error {
	// Check for mandatory server field
	_, ok := config[DockerParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", DockerParamAddress)
	}
	d.address, ok = config[DockerParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", DockerParamAddress, config[DockerParamAddress])
	}

	return nil
}

// Execute sends an Docker request to the server and synchronously waits for the reply.
func (d *Docker) Execute() ([]*Result, error) {
	var err error
	var cli *client.Client
	ctx := context.Background()
	res := make([]*Result, 0)

	log.Debugf("Dialing Docker: %s", d.address)
	cli, err = client.NewClient(d.address, "", nil, nil)
	if err != nil {
		return nil, fmt.Errorf("Unable to create the Docker client: %s", err)
	}
	defer cli.Close()

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	if err != nil {
		return nil, fmt.Errorf("unable to get container list: %s", err)
	}

	countPerState := map[string]interface{}{
		DockerResultContainerCountCreated:    int64(0),
		DockerResultContainerCountRunning:    int64(0),
		DockerResultContainerCountPaused:     int64(0),
		DockerResultContainerCountRestarting: int64(0),
		DockerResultContainerCountRemoving:   int64(0),
		DockerResultContainerCountExited:     int64(0),
		DockerResultContainerCountDead:       int64(0),
	}

Containers:
	for _, container := range containers {
		log.Debugf("Container: %s / state: %s / status: %s", container.ID, container.State, container.Status)

		switch container.State {
		case "created":
			countPerState[DockerResultContainerCountCreated] = countPerState[DockerResultContainerCountCreated].(int64) + 1
		case "running":
			countPerState[DockerResultContainerCountRunning] = countPerState[DockerResultContainerCountRunning].(int64) + 1
		case "paused":
			countPerState[DockerResultContainerCountPaused] = countPerState[DockerResultContainerCountPaused].(int64) + 1
		case "restarting":
			countPerState[DockerResultContainerCountRestarting] = countPerState[DockerResultContainerCountRestarting].(int64) + 1
		case "removing":
			countPerState[DockerResultContainerCountRemoving] = countPerState[DockerResultContainerCountRemoving].(int64) + 1
		case "exited":
			countPerState[DockerResultContainerCountExited] = countPerState[DockerResultContainerCountExited].(int64) + 1
		case "dead":
			countPerState[DockerResultContainerCountDead] = countPerState[DockerResultContainerCountDead].(int64) + 1
		}
		if stats, err := cli.ContainerStats(ctx, container.ID, false); err == nil {
			resContainer, err := parseContainerStats(stats.Body)
			if err != nil {
				log.Errorf("unable to parse container stats: %s", err)
				continue Containers
			}

			res = append(res, resContainer...)
		}
	}
	res = append(res,
		&Result{
			Fields: countPerState,
			Tags:   map[string]string{},
		},
	)

	networks, err := cli.NetworkList(ctx, types.NetworkListOptions{})
	if err != nil {
		log.Errorf("unable to get network list: %s", err)
	}

	volumeDataSize := int64(0)
	volumes, err := cli.VolumeList(ctx, filters.Args{})
	if err != nil {
		log.Errorf("unable to get volume list: %s", err)
	}
	for _, volume := range volumes.Volumes {
		if data := volume.UsageData; data != nil {
			volumeDataSize += data.Size
		}
	}

	imageDataSize := int64(0)
	images, err := cli.ImageList(ctx, types.ImageListOptions{})
	if err != nil {
		log.Errorf("unable to get image list: %s", err)
	}
	for _, image := range images {
		imageDataSize += image.Size
	}

	// Report the network count
	res = append(res,
		&Result{
			Fields: map[string]interface{}{
				DockerResultNetworkCount: len(networks),
				DockerResultVolumeCount:  len(volumes.Volumes),
				DockerResultVolumeSize:   volumeDataSize,
				DockerResultImageCount:   len(images),
				DockerResultImageSize:    imageDataSize,
			},
		},
	)

	return res, nil
}

func parseContainerStats(stats io.ReadCloser) ([]*Result, error) {
	res := make([]*Result, 0)
	var containerName string
	var statStruct map[string]interface{}

	if buf, err := ioutil.ReadAll(stats); err == nil && buf != nil {
		if err = json.Unmarshal(buf, &statStruct); err != nil {
			return []*Result{}, fmt.Errorf("unable to unmarshal container stats: %s", err)
		}

		if _, ok := statStruct["name"]; ok {
			containerName = statStruct["name"].(string)
			if strings.HasPrefix(containerName, "/") {
				containerName = containerName[1:]
			}
		}

		if _, ok := statStruct["cpu_stats"]; ok {
			if statCPU, ok := statStruct["cpu_stats"].(map[string]interface{}); ok {
				if _, ok := statCPU["cpu_usage"]; ok {
					if statCPUUsage, ok := statCPU["cpu_usage"].(map[string]interface{}); ok {
						res = append(res,
							&Result{
								Fields: map[string]interface{}{
									DockerResultContainerCPUTotalUsage: statCPUUsage["total_usage"].(float64),
								},
								Tags: map[string]string{
									DockerTagContainerName: containerName,
								},
							},
						)
					}
				}
			}
		}

		if _, ok := statStruct["memory_stats"]; ok {
			if statMem, ok := statStruct["memory_stats"].(map[string]interface{}); ok {
				if _, ok := statMem["usage"]; ok {
					res = append(res,
						&Result{
							Fields: map[string]interface{}{
								DockerResultContainerMemorylUsage: statMem["usage"].(float64),
							},
							Tags: map[string]string{
								DockerTagContainerName: containerName,
							},
						},
					)
				}
			}
		}

		if _, ok := statStruct["networks"]; ok {
			if statNetworks, ok := statStruct["networks"].(map[string]interface{}); ok {
				for statNetworkName, statNetwork := range statNetworks {

					if statNet, ok := statNetwork.(map[string]interface{}); ok {
						res = append(res,
							&Result{
								Fields: map[string]interface{}{
									DockerResultContainerNetworkReceived:        statNet["rx_bytes"].(float64),
									DockerResultContainerNetworkErrorReceived:   statNet["rx_errors"].(float64),
									DockerResultContainerNetworkDroppedReceived: statNet["rx_dropped"].(float64),
									DockerResultContainerNetworkSent:            statNet["tx_bytes"].(float64),
									DockerResultContainerNetworkErrorSent:       statNet["tx_errors"].(float64),
									DockerResultContainerNetworkDroppedSent:     statNet["tx_dropped"].(float64),
								},
								Tags: map[string]string{
									DockerTagContainerName:      containerName,
									DockerTagContainerInterface: statNetworkName,
								},
							})
					}
				}
			}
		}
	}

	return res, nil
}
