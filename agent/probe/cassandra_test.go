// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"reflect"
	"testing"

	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Cassandra)(nil)

func TestCassandraName(t *testing.T) {
	r := Cassandra{}
	if r.Name() != "cassandra" {
		t.Error("Cassandra probe name is not \"cassandra\"")
	}
}

func TestCassandraConfigure(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Cassandra{}
	err := r.Configure(map[string]interface{}{CassandraParamAddress: viper.GetString("cassandra.address")})
	if err != nil {
		t.Errorf("couldn't configure Cassandra probe: %s", err)
	}
}

func TestCassandraExecute(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	r := Cassandra{}

	err := r.Configure(map[string]interface{}{CassandraParamAddress: viper.GetString("cassandra.address")})
	if err != nil {
		t.Fatalf("couldn't configure Cassandra probe: %s", err)
	}
	res, err := r.Execute()
	if err != nil {
		t.Fatalf("couldn't execute Cassandra probe: %s", err)
	}
	if len(res) <= 0 {
		t.Fatalf("Expected at least 1 results, got %d", len(res))
	}
}

func TestExtractTagFromGroup(t *testing.T) {
	validUsecases := []struct {
		group    string
		tag      string
		tagValue string
	}{
		{
			"org.apache.cassandra.metrics:keyspace=system,name=ReadLatency,scope=available_ranges,type=Table:",
			"keyspace",
			"system",
		}, {
			"org.apache.cassandra.metrics:keyspace=system,name=ReadLatency,scope=available_ranges,type=Table:",
			"type",
			"Table",
		}, {
			"org.apache.cassandra.metrics:keyspace=system,name=*,scope=available_ranges,type=Table:",
			"name",
			"*",
		},
	}

	for _, uc := range validUsecases {
		result, err := extractTagFromGroup(uc.group, uc.tag)
		if err != nil {
			t.Errorf("unexpected error: %s", err)
		}
		if result != uc.tagValue {
			t.Errorf("expected %s, got %s", uc.tagValue, result)
		}
	}

	invalidUsecases := []struct {
		group string
		tag   string
	}{
		{
			"org.apache.cassandra.metrics:keyspace=system,name=ReadLatency,scope=available_ranges,type=Table:",
			"pantomath",
		}, {
			"org.apache.cassandra.metrics:keyspace:system,name=ReadLatency,scope=available_ranges,type=Table:",
			"keyspace",
		}, {
			"",
			"type",
		},
	}

	for _, uc := range invalidUsecases {
		result, err := extractTagFromGroup(uc.group, uc.tag)
		if err == nil {
			t.Errorf("expected error, got result: %s", result)
		}
	}
}

func TestExtractCassandraResult(t *testing.T) {
	validUsecases := []struct {
		data     []byte
		ep       *cassandraEndpoint
		expected map[string]map[string]float64
	}{
		{
			[]byte(`{"request":{"mbean":"org.apache.cassandra.metrics:name=CurrentlyBlockedTasks,path=request,scope=RequestResponseStage,type=ThreadPools","type":"read"},"value":{"Count":0},"timestamp":1541786419,"status":200}`),
			&cassandraEndpoint{
				endpoint: "/org.apache.cassandra.metrics:name=CurrentlyBlockedTasks,path=request,scope=RequestResponseStage,type=ThreadPools",
				metrics: map[string]string{
					"Count": CassandraResultsTableWriteTotalLatency,
				},
			},
			map[string]map[string]float64{
				"": {
					CassandraResultsTableWriteTotalLatency: 0,
				},
			},
		}, {
			[]byte(`{"request":{"mbean":"org.apache.cassandra.metrics:keyspace=*,name=LiveDiskSpaceUsed,scope=*,type=Table","type":"read"},"value":{"org.apache.cassandra.metrics:keyspace=system_auth,name=LiveDiskSpaceUsed,scope=role_members,type=Table":{"Count":42},"org.apache.cassandra.metrics:keyspace=system,name=LiveDiskSpaceUsed,scope=IndexInfo,type=Table":{"Count":42}},"timestamp":1541673214,"status":200}`),
			&cassandraEndpoint{
				endpoint: "/org.apache.cassandra.metrics:keyspace=*,name=LiveDiskSpaceUsed,scope=*,type=Table",
				tags:     []string{"keyspace"},
				metrics: map[string]string{
					"Count": CassandraResultsTableLiveDiskSpaceUsed,
				},
			},
			map[string]map[string]float64{
				"system_auth": {
					CassandraResultsTableLiveDiskSpaceUsed: 42,
				},
				"system": {
					CassandraResultsTableLiveDiskSpaceUsed: 42,
				},
			},
		},
	}

	for _, uc := range validUsecases {
		result, err := extractCassandraResult(uc.data, uc.ep)
		if err != nil {
			t.Errorf("unexpected error: %s", err)
		}
		if !reflect.DeepEqual(result, uc.expected) {
			t.Errorf("expected result is %v, got %v", uc.expected, result)
		}
	}

	invalidUsecases := []struct {
		data     []byte
		ep       *cassandraEndpoint
		expected map[string]map[string]float64
	}{
		{ // invalid JSON in data
			[]byte(`{request: {mbean: "org.apache.cassandra.metrics:name=CurrentlyBlockedTasks,path=request,scope=RequestResponseStage,type=ThreadPools",type: "read"},value: {Count: 0},timestamp: 1541786419,status: 200}`),
			&cassandraEndpoint{
				endpoint: "/org.apache.cassandra.metrics:name=CurrentlyBlockedTasks,path=request,scope=RequestResponseStage,type=ThreadPools",
				metrics: map[string]string{
					"Count": CassandraResultsTableWriteTotalLatency,
				},
			},
			map[string]map[string]float64{
				"": {
					CassandraResultsTableWriteTotalLatency: 0,
				},
			},
		}, { // no "value" field in JSON data
			[]byte(`{"request":{"mbean":"org.apache.cassandra.metrics:name=CurrentlyBlockedTasks,path=request,scope=RequestResponseStage,type=ThreadPools","type":"read"},"values":{"Count":0},"timestamp":1541786419,"status":200}`),
			&cassandraEndpoint{
				endpoint: "/org.apache.cassandra.metrics:name=CurrentlyBlockedTasks,path=request,scope=RequestResponseStage,type=ThreadPools",
				metrics: map[string]string{
					"Count": CassandraResultsTableWriteTotalLatency,
				},
			},
			map[string]map[string]float64{
				"": {
					CassandraResultsTableWriteTotalLatency: 0,
				},
			},
		}, { // "value" field in JSON data should be a struct
			[]byte(`{"request":{"mbean":"org.apache.cassandra.metrics:name=CurrentlyBlockedTasks,path=request,scope=RequestResponseStage,type=ThreadPools","type":"read"},"value":0,"timestamp":1541786419,"status":200}`),
			&cassandraEndpoint{
				endpoint: "/org.apache.cassandra.metrics:name=CurrentlyBlockedTasks,path=request,scope=RequestResponseStage,type=ThreadPools",
				metrics: map[string]string{
					"Count": CassandraResultsTableWriteTotalLatency,
				},
			},
			map[string]map[string]float64{
				"": {
					CassandraResultsTableWriteTotalLatency: 0,
				},
			},
		},
	}

	for _, uc := range invalidUsecases {
		result, err := extractCassandraResult(uc.data, uc.ep)
		if err == nil {
			t.Errorf("expected error, but got a result: %v", result)
		}
	}
}
