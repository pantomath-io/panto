// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"net"
	"reflect"
	"strings"
	"testing"
	"time"
)

// Compile error if sysinfo probes do not conform to Probe interface
var _ Probe = (*Network)(nil)

func TestNetworkName(t *testing.T) {
	n := Network{}
	if n.Name() != "network" {
		t.Error("Network probe name is not \"network\"")
	}
}

func TestNetworkConfigure(t *testing.T) {
	n := Network{}
	err := n.Configure(map[string]interface{}{})
	if err != nil {
		t.Errorf("Failed to configure network probe: %s", err)
	}
	err = n.Configure(map[string]interface{}{
		NetworkParamInterfaces: "",
	})
	if err != nil {
		t.Errorf("Failed to configure network probe: %s", err)
	}
	err = n.Configure(map[string]interface{}{
		NetworkParamInterfaces: "eth0",
	})
	if err != nil {
		t.Errorf("Failed to configure network probe: %s", err)
	}
	err = n.Configure(map[string]interface{}{
		NetworkParamInterfaces: "eth0,eth1",
	})
	if err != nil {
		t.Errorf("Failed to configure network probe: %s", err)
	}
	if len(n.interfaces) != 2 {
		t.Errorf("Unexpected number of interfaces: got %d, expected %d", len(n.interfaces), 2)
	}
	err = n.Configure(map[string]interface{}{
		NetworkParamInterfaces: []string{"eth0"},
	})
	if err == nil {
		t.Errorf("Unexpected success when configuring network probe.")
	}
	err = n.Configure(map[string]interface{}{
		NetworkParamInterval: 6 * time.Second,
	})
	if err != nil {
		t.Errorf("Failed to configure network probe: %s", err)
	}
	if n.interval != 6*time.Second {
		t.Errorf("interval was not set to 6 seconds")
	}
}

func TestNetworkExecute(t *testing.T) {
	n := Network{}

	err := n.Configure(map[string]interface{}{})
	if err != nil {
		t.Errorf("Failed to configure network probe: %s", err)
	}
	res, err := n.Execute()
	if err != nil {
		t.Errorf("Failed to execute network probe: %s", err)
	}
	if len(res) != 1 {
		t.Error("Expected only one result for global counters")
	}
	if res[0].Tags[NetworkTagInterface] != "all" {
		t.Errorf("Unexpected \"%s\" tag found. Got %s, expected %s.", NetworkTagInterface, res[0].Tags[NetworkTagInterface], "all")
	}
	for _, r := range res {
		t.Logf("%v", *r)
	}

	// Validate types
	expectedKeys := map[string]interface{}{
		NetworkResultBytesSent:   reflect.Uint64,
		NetworkResultBytesRecv:   reflect.Uint64,
		NetworkResultPacketsSent: reflect.Uint64,
		NetworkResultPacketsRecv: reflect.Uint64,
		NetworkResultErrIn:       reflect.Uint64,
		NetworkResultErrOut:      reflect.Uint64,
		NetworkResultTCPConn:     reflect.Uint64,
		NetworkResultUDPConn:     reflect.Uint64,
	}
	for k := range expectedKeys {
		if _, ok := res[0].Fields[k]; !ok {
			t.Errorf("missing key \"%s\" in network results", k)
		}
	}
	for k, field := range res[0].Fields {
		kind, ok := expectedKeys[k]
		if !ok {
			t.Errorf("unexpected key \"%s\" in network results", k)
			continue
		}
		if kind != reflect.TypeOf(field).Kind() {
			t.Errorf("invalid type for field %s, expected %s, got %s", k, kind, reflect.TypeOf(field).Kind())
			continue
		}
	}

	err = n.Configure(map[string]interface{}{
		NetworkParamInterfaces: "",
	})
	if err != nil {
		t.Errorf("Failed to configure network probe: %s", err)
	}
	res, err = n.Execute()
	if err != nil {
		t.Errorf("Failed to execute network probe: %s", err)
	}
	for _, r := range res {
		t.Logf("%v", *r)
	}

	ifs, err := net.Interfaces()
	if err != nil {
		t.Skipf("Couldn't collect network interface list: %s. Skipping test...", err)
	}
	ifnames := make([]string, len(ifs))
	for i, interf := range ifs {
		ifnames[i] = interf.Name
	}
	err = n.Configure(map[string]interface{}{
		NetworkParamInterfaces: strings.Join(ifnames, ","),
	})
	if err != nil {
		t.Errorf("Failed to configure network probe: %s", err)
	}
	res, err = n.Execute()
	if err != nil {
		t.Errorf("Failed to execute network probe: %s", err)
	}
	for _, r := range res {
		t.Logf("%v", *r)
	}
}
