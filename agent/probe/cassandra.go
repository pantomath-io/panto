// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package probe

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"time"
)

// Cassandra probes a cassandra store
type Cassandra struct {
	baseURL string
	timeout time.Duration
}

// Jolokia endpoints and metrics mapping
type cassandraEndpoint struct {
	endpoint string            // path of the endpoint
	tags     []string          // the returned result are grouped by tags
	metrics  map[string]string // mapping between field name and result name
}

const (

	// Cassandra configuration parameters

	// CassandraParamAddress : (mandatory) the address of the jolokia bridge to cassandra server to gather metric from (string)
	CassandraParamAddress = "address"
	// CassandraParamTimeout (optional) : duration before the HTTP request times out (default value: 0)(`time.Duration`)
	CassandraParamTimeout = "timeout"

	// Cassandra results

	// CassandraResultHeapMemoryInit : Amount of heap memory in bytes that the JVM initially requests from the OS (float64)
	CassandraResultHeapMemoryInit = "heap-memory-init"
	// CassandraResultHeapMemoryCommitted : Amount of heap memory in bytes that is committed for the JVM to use (float64)
	CassandraResultHeapMemoryCommitted = "heap-memory-committed"
	// CassandraResultHeapMemoryMax : Maximum amount of heap memory in bytes that can be used for memory management (float64)
	CassandraResultHeapMemoryMax = "heap-memory-max"
	// CassandraResultHeapMemoryUsed : Amount of used heap memory in bytes (float64)
	CassandraResultHeapMemoryUsed = "heap-memory-used"
	// CassandraResultNonHeapMemoryInit : Amount of non-heap memory in bytes that the JVM initially requests from the OS (float64)
	CassandraResultNonHeapMemoryInit = "nonheap-memory-init"
	// CassandraResultNonHeapMemoryCommitted : Amount of non-heap memory in bytes that is committed for the JVM to use (float64)
	CassandraResultNonHeapMemoryCommitted = "nonheap-memory-committed"
	// CassandraResultNonHeapMemoryMax : Maximum amount of non-heap memory in bytes that can be used for memory management (float64)
	CassandraResultNonHeapMemoryMax = "nonheap-memory-max"
	// CassandraResultNonHeapMemoryUsed : Amount of used non-heap memory in bytes (float64)
	CassandraResultNonHeapMemoryUsed = "nonheap-memory-used"
	// CassandraResultConnectedClients : Number of clients connected to this nodes native protocol server (float64)
	CassandraResultConnectedClients = "connected-clients"
	// CassandraResultKeyCacheHits : Total number of cache hits for partition to sstable offsets (float64)
	CassandraResultKeyCacheHits = "key-cache-hits"
	// CassandraResultKeyCacheRequests : Total number of cache requests for partition to sstable offsets (float64)
	CassandraResultKeyCacheRequests = "key-cache-requests"
	// CassandraResultKeyCacheEntries : Total number of cache entries for partition to sstable offsets (float64)
	CassandraResultKeyCacheEntries = "key-cache-entries"
	// CassandraResultKeyCacheSize : Total size of occupied cache, in bytes, for partition to sstable offsets (float64)
	CassandraResultKeyCacheSize = "key-cache-size"
	// CassandraResultKeyCacheCapacity : Cache capacity, in bytes, for partition to sstable offsets (float64)
	CassandraResultKeyCacheCapacity = "key-cache-capacity"
	// CassandraResultRowCacheHits : Total number of cache hits for rows kept in memory (float64)
	CassandraResultRowCacheHits = "row-cache-hits"
	// CassandraResultRowCacheRequests : Total number of cache requests for rows kept in memory (float64)
	CassandraResultRowCacheRequests = "row-cache-requests"
	// CassandraResultRowCacheEntries : Total number of cache entries for rows kept in memory (float64)
	CassandraResultRowCacheEntries = "row-cache-entries"
	// CassandraResultRowCacheSize : Total size of occupied cache, in bytes, for rows kept in memory (float64)
	CassandraResultRowCacheSize = "row-cache-size"
	// CassandraResultRowCacheCapacity : Cache capacity, in bytes, for rows kept in memory (float64)
	CassandraResultRowCacheCapacity = "row-cache-capacity"
	// CassandraResultReadTotalLatency : Total read latency since starting (float64)
	CassandraResultReadTotalLatency = "read-totallatency"
	// CassandraResultWriteTotalLatency : Total write latency since starting (float64)
	CassandraResultWriteTotalLatency = "write-totallatency"
	// CassandraResultReadTimeouts : Number of timeouts encountered during read (float64)
	CassandraResultReadTimeouts = "read-timeouts"
	// CassandraResultWriteTimeouts : Number of timeouts encountered during write (float64)
	CassandraResultWriteTimeouts = "write-timeouts"
	// CassandraResultReadUnavailables : Number of unavailable exceptions  encountered during read (float64)
	CassandraResultReadUnavailables = "read-unavailables"
	// CassandraResultWriteUnavailables : Number of unavailable exceptions  encountered during write (float64)
	CassandraResultWriteUnavailables = "write-unavailables"
	// CassandraResultReadFailures : Number of failures  encountered during read (float64)
	CassandraResultReadFailures = "read-failures"
	// CassandraResultWriteFailures : Number of failures  encountered during write (float64)
	CassandraResultWriteFailures = "write-failures"
	// CassandraResultCommitLogPendingTasks : Number of commit log messages written but yet to be fsync’d (float64)
	CassandraResultCommitLogPendingTasks = "commitlog-pendingtasks"
	// CassandraResultCommitLogTotalCommitLogSize : Current size, in bytes, used by all the commit log segments (float64)
	CassandraResultCommitLogTotalCommitLogSize = "commitlog-totalsize"
	// CassandraResultCompactionCompletedTasks : Number of completed compactions since server [re]start (float64)
	CassandraResultCompactionCompletedTasks = "compaction-completedtasks"
	// CassandraResultCompactionPendingTasks : Estimated number of compactions remaining to perform (float64)
	CassandraResultCompactionPendingTasks = "compaction-pendingtasks"
	// CassandraResultCompactionBytesCompacted : Total number of bytes compacted since server [re]start (float64)
	CassandraResultCompactionBytesCompacted = "compaction-bytescompacted"
	// CassandraResultStorageLoad : Size, in bytes, of the on disk data size this node manages (float64)
	CassandraResultStorageLoad = "storage-load"
	// CassandraResultStorageExceptions : Number of internal exceptions caught (float64)
	CassandraResultStorageExceptions = "storage-exceptions"
	// CassandraResultTPCompactionExecutorActiveTasks : Number of tasks being actively worked on Compactions (float64)
	CassandraResultTPCompactionExecutorActiveTasks = "tp-compactionexecutor-activetasks"
	// CassandraResultTPAntiEntropyStageActiveTasks : Number of tasks being actively worked on Builds merkle tree for repairs (float64)
	CassandraResultTPAntiEntropyStageActiveTasks = "tp-antientropystage-activetasks"
	// CassandraResultTPCounterMutationStagePendingTasks : Number of queued tasks queued up on counter writes (float64)
	CassandraResultTPCounterMutationStagePendingTasks = "tp-countermutationstage-pendingtasks"
	// CassandraResultTPCounterMutationStageCurrentlyBlockedTasks : Number of tasks that are currently blocked due to queue saturation but on retry will become unblocked on counter writes (float64)
	CassandraResultTPCounterMutationStageCurrentlyBlockedTasks = "tp-countermutationstage-currentlyblockedtasks"
	// CassandraResultTPMutationStagePendingTasks : Number of queued tasks queued up on all other writes (float64)
	CassandraResultTPMutationStagePendingTasks = "tp-mutationstage-pendingtasks"
	// CassandraResultTPMutationStageCurrentlyBlockedTasks : Number of tasks that are currently blocked due to queue saturation but on retry will become unblocked on all other writes (float64)
	CassandraResultTPMutationStageCurrentlyBlockedTasks = "tp-mutationstage-currentlyblockedtasks"
	// CassandraResultTPReadRepairStagePendingTasks : Number of queued tasks queued up on ReadRepair (float64)
	CassandraResultTPReadRepairStagePendingTasks = "tp-readrepairstage-pendingtasks"
	// CassandraResultTPReadRepairStageCurrentlyBlockedTasks : Number of tasks that are currently blocked due to queue saturation but on retry will become unblocked on ReadRepair (float64)
	CassandraResultTPReadRepairStageCurrentlyBlockedTasks = "tp-readrepairstage-currentlyblockedtasks"
	// CassandraResultTPReadStagePendingTasks : Number of queued tasks queued up on Local reads (float64)
	CassandraResultTPReadStagePendingTasks = "tp-readstage-pendingtasks"
	// CassandraResultTPReadStageCurrentlyBlockedTasks : Number of tasks that are currently blocked due to queue saturation but on retry will become unblocked on Local read (float64)
	CassandraResultTPReadStageCurrentlyBlockedTasks = "tp-readstage-currentlyblockedtasks"
	// CassandraResultTPRequestResponseStagePendingTasks : Number of queued tasks queued up on Coordinator requests to the cluster (float64)
	CassandraResultTPRequestResponseStagePendingTasks = "tp-requestresponsestage-pendingtasks"
	// CassandraResultTPRequestResponseStageCurrentlyBlockedTasks : Number of tasks that are currently blocked due to queue saturation but on retry will become unblocked on Coordinator requests to the cluster (float64)
	CassandraResultTPRequestResponseStageCurrentlyBlockedTasks = "tp-requestresponsestage-currentlyblockedtasks"
	// CassandraResultsTableLiveDiskSpaceUsed : Disk space used by SSTables belonging to this table (in bytes) (float64)
	CassandraResultsTableLiveDiskSpaceUsed = "table-livediskspaceused"
	// CassandraResultsTableTotalDiskSpaceUsed : Total disk space used by SSTables belonging to this table, including obsolete ones waiting to be GC’d (float64)
	CassandraResultsTableTotalDiskSpaceUsed = "table-totaldiskspaceused"
	// CassandraResultsTableReadLatency : Local read latency for this keyspace (float64)
	CassandraResultsTableReadLatency = "table-readlatency"
	// CassandraResultsTableCoordinatorReadLatency : Coordinator read latency for this keyspace (float64)
	CassandraResultsTableCoordinatorReadLatency = "table-coordinatorreadlatency"
	// CassandraResultsTableWriteLatency : Local write latency for this keyspace (float64)
	CassandraResultsTableWriteLatency = "table-writelatency"
	// CassandraResultsTableReadTotalLatency : Local read latency for this keyspace since starting (float64)
	CassandraResultsTableReadTotalLatency = "table-readtotallatency"
	// CassandraResultsTableWriteTotalLatency : Local write latency for this keyspace since starting (float64)
	CassandraResultsTableWriteTotalLatency = "table-writetotallatency"

	// Cassandra result tags

	// CassandraTagKeyspace : The keyspace of the metric, e.g. "system"
	CassandraTagKeyspace = "keyspace"
)

var cassandraEndpoints = []cassandraEndpoint{
	{
		endpoint: "/java.lang:type=Memory/HeapMemoryUsage",
		metrics: map[string]string{
			"init":      CassandraResultHeapMemoryInit,
			"committed": CassandraResultHeapMemoryCommitted,
			"max":       CassandraResultHeapMemoryMax,
			"used":      CassandraResultHeapMemoryUsed,
		},
	}, {
		endpoint: "/java.lang:type=Memory/NonHeapMemoryUsage",
		metrics: map[string]string{
			"init":      CassandraResultNonHeapMemoryInit,
			"committed": CassandraResultNonHeapMemoryCommitted,
			"max":       CassandraResultNonHeapMemoryMax,
			"used":      CassandraResultNonHeapMemoryUsed,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Client,name=connectedNativeClients",
		metrics: map[string]string{
			"Value": CassandraResultConnectedClients,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=KeyCache,name=Hits",
		metrics: map[string]string{
			"Count": CassandraResultKeyCacheHits,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=KeyCache,name=Requests",
		metrics: map[string]string{
			"Count": CassandraResultKeyCacheRequests,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=KeyCache,name=Entries",
		metrics: map[string]string{
			"Value": CassandraResultKeyCacheEntries,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=KeyCache,name=Size",
		metrics: map[string]string{
			"Value": CassandraResultKeyCacheSize,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=RowCache,name=Capacity",
		metrics: map[string]string{
			"Value": CassandraResultRowCacheCapacity,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=RowCache,name=Hits",
		metrics: map[string]string{
			"Count": CassandraResultRowCacheHits,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=RowCache,name=Requests",
		metrics: map[string]string{
			"Count": CassandraResultRowCacheRequests,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=RowCache,name=Entries",
		metrics: map[string]string{
			"Value": CassandraResultRowCacheEntries,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=RowCache,name=Size",
		metrics: map[string]string{
			"Value": CassandraResultRowCacheSize,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=RowCache,name=Capacity",
		metrics: map[string]string{
			"Value": CassandraResultRowCacheCapacity,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Cache,scope=RowCache,name=Capacity",
		metrics: map[string]string{
			"Value": CassandraResultRowCacheCapacity,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ClientRequest,scope=Read,name=TotalLatency",
		metrics: map[string]string{
			"Count": CassandraResultReadTotalLatency,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ClientRequest,scope=Write,name=TotalLatency",
		metrics: map[string]string{
			"Count": CassandraResultWriteTotalLatency,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ClientRequest,scope=Read,name=Timeouts",
		metrics: map[string]string{
			"Count": CassandraResultReadTimeouts,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ClientRequest,scope=Write,name=Timeouts",
		metrics: map[string]string{
			"Count": CassandraResultWriteTimeouts,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ClientRequest,scope=Read,name=Unavailables",
		metrics: map[string]string{
			"Count": CassandraResultReadUnavailables,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ClientRequest,scope=Write,name=Unavailables",
		metrics: map[string]string{
			"Count": CassandraResultWriteUnavailables,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ClientRequest,scope=Read,name=Failures",
		metrics: map[string]string{
			"Count": CassandraResultReadFailures,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ClientRequest,scope=Write,name=Failures",
		metrics: map[string]string{
			"Count": CassandraResultWriteFailures,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=CommitLog,name=PendingTasks",
		metrics: map[string]string{
			"Value": CassandraResultCommitLogPendingTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=CommitLog,name=TotalCommitLogSize",
		metrics: map[string]string{
			"Value": CassandraResultCommitLogTotalCommitLogSize,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Compaction,name=CompletedTasks",
		metrics: map[string]string{
			"Value": CassandraResultCompactionCompletedTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Compaction,name=PendingTasks",
		metrics: map[string]string{
			"Value": CassandraResultCompactionPendingTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Compaction,name=BytesCompacted",
		metrics: map[string]string{
			"Count": CassandraResultCompactionBytesCompacted,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Storage,name=Load",
		metrics: map[string]string{
			"Count": CassandraResultStorageLoad,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Storage,name=Exceptions",
		metrics: map[string]string{
			"Count": CassandraResultStorageExceptions,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=internal,scope=CompactionExecutor,name=ActiveTasks",
		metrics: map[string]string{
			"Value": CassandraResultTPCompactionExecutorActiveTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=internal,scope=AntiEntropyStage,name=ActiveTasks",
		metrics: map[string]string{
			"Value": CassandraResultTPAntiEntropyStageActiveTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=CounterMutationStage,name=PendingTasks",
		metrics: map[string]string{
			"Value": CassandraResultTPCounterMutationStagePendingTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=CounterMutationStage,name=CurrentlyBlockedTasks",
		metrics: map[string]string{
			"Count": CassandraResultTPCounterMutationStageCurrentlyBlockedTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=MutationStage,name=PendingTasks",
		metrics: map[string]string{
			"Value": CassandraResultTPMutationStagePendingTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=MutationStage,name=CurrentlyBlockedTasks",
		metrics: map[string]string{
			"Count": CassandraResultTPMutationStageCurrentlyBlockedTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=ReadRepairStage,name=PendingTasks",
		metrics: map[string]string{
			"Value": CassandraResultTPReadRepairStagePendingTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=ReadRepairStage,name=CurrentlyBlockedTasks",
		metrics: map[string]string{
			"Count": CassandraResultTPReadRepairStageCurrentlyBlockedTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=ReadStage,name=PendingTasks",
		metrics: map[string]string{
			"Value": CassandraResultTPReadStagePendingTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=ReadStage,name=CurrentlyBlockedTasks",
		metrics: map[string]string{
			"Count": CassandraResultTPReadStageCurrentlyBlockedTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=RequestResponseStage,name=PendingTasks",
		metrics: map[string]string{
			"Value": CassandraResultTPRequestResponseStagePendingTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=ThreadPools,path=request,scope=RequestResponseStage,name=CurrentlyBlockedTasks",
		metrics: map[string]string{
			"Count": CassandraResultTPRequestResponseStageCurrentlyBlockedTasks,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Table,keyspace=*,scope=*,name=LiveDiskSpaceUsed",
		tags:     []string{"keyspace"},
		metrics: map[string]string{
			"Count": CassandraResultsTableLiveDiskSpaceUsed,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Table,keyspace=*,scope=*,name=TotalDiskSpaceUsed",
		tags:     []string{"keyspace"},
		metrics: map[string]string{
			"Count": CassandraResultsTableTotalDiskSpaceUsed,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Table,keyspace=*,scope=*,name=ReadLatency",
		tags:     []string{"keyspace"},
		metrics: map[string]string{
			"Count": CassandraResultsTableReadLatency,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Table,keyspace=*,scope=*,name=CoordinatorReadLatency",
		tags:     []string{"keyspace"},
		metrics: map[string]string{
			"Count": CassandraResultsTableCoordinatorReadLatency,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Table,keyspace=*,scope=*,name=WriteLatency",
		tags:     []string{"keyspace"},
		metrics: map[string]string{
			"Count": CassandraResultsTableWriteLatency,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Table,keyspace=*,scope=*,name=ReadTotalLatency",
		tags:     []string{"keyspace"},
		metrics: map[string]string{
			"Count": CassandraResultsTableReadTotalLatency,
		},
	}, {
		endpoint: "/org.apache.cassandra.metrics:type=Table,keyspace=*,scope=*,name=WriteTotalLatency",
		tags:     []string{"keyspace"},
		metrics: map[string]string{
			"Count": CassandraResultsTableWriteTotalLatency,
		},
	},
}

// Name returns the name of the probe "cassandra"
func (c *Cassandra) Name() string {
	return "cassandra"
}

// Configure configures the cassandra probe
func (c *Cassandra) Configure(config map[string]interface{}) (err error) {
	// Check for mandatory address field
	_, ok := config[CassandraParamAddress]
	if !ok {
		return fmt.Errorf("missing \"%s\" field in configuration", CassandraParamAddress)
	}
	c.baseURL, ok = config[CassandraParamAddress].(string)
	if !ok {
		return fmt.Errorf("\"%s\" field is not string: %v", CassandraParamAddress, config[CassandraParamAddress])
	}

	// Check for optional timeout
	_, ok = config[CassandraParamTimeout]
	if !ok {
		c.timeout = 0
	} else {
		c.timeout, ok = config[CassandraParamTimeout].(time.Duration)
		if !ok {
			return fmt.Errorf("\"%s\" field is not time.Duration: %v", CassandraParamTimeout, config[CassandraParamTimeout])
		}
	}

	return nil
}

// Execute runs the probe and gathers information about the cassandra server
func (c *Cassandra) Execute() ([]*Result, error) {
	var accumulator = map[string]map[string]float64{}

	client := &http.Client{Timeout: c.timeout}
	defer client.CloseIdleConnections()
	for _, cass := range cassandraEndpoints {
		url := fmt.Sprintf("http://%s%s", c.baseURL, cass.endpoint)

		req, err := http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			log.Errorf("unable to create GET request: %s", err)
			continue
		}

		log.Debugf("Performing Cassandra request: %s %s", req.Method, req.URL)
		res, err := client.Do(req)
		if err != nil {
			log.Errorf("unable to execute request: %s", err)
			continue
		}

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.Errorf("unable to read cassandra response to request: %v", err)
			continue
		}
		res.Body.Close()

		endpointResults, err := extractCassandraResult(body, &cass)
		if err != nil {
			log.Errorf("unable to extract result from output: %v", err)
			log.Errorf("output: %s", body)
		}

		for tag, fields := range endpointResults {
			if _, ok := accumulator[tag]; !ok {
				accumulator[tag] = make(map[string]float64)
			}
			for key, value := range fields {
				accumulator[tag][key] = value
			}
		}
	}

	results := make([]*Result, len(accumulator))
	i := 0
	for tag, metrics := range accumulator {
		tags := make(map[string]string)
		fields := make(map[string]interface{})

		for k, v := range metrics {
			fields[k] = v
		}
		if tag != "" {
			tags[CassandraTagKeyspace] = tag
			results[i] = &Result{Fields: fields, Tags: tags}
		} else {
			results[i] = &Result{Fields: fields}
		}
		i++
	}

	return results, nil
}

func extractCassandraResult(r []byte, ep *cassandraEndpoint) (map[string]map[string]float64, error) {
	var accumulator = map[string]map[string]float64{}
	values := make(map[string]interface{})

	if err := json.Unmarshal(r, &values); err != nil {
		return nil, fmt.Errorf("couldn't unmarshal result: %s", err)
	}

	s, ok := values["value"]
	if !ok {
		return nil, fmt.Errorf("Missing 'value' in result (%v)", values)
	}
	m, ok := s.(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("Wrong format for value block (%v)", values)
	}

	if len(ep.tags) <= 0 {
		fields := make(map[string]float64)
		for metric, resultKey := range ep.metrics {
			if v, ok := m[metric]; ok {
				if n, ok := v.(float64); ok {
					fields[resultKey] = n
				}
			}
		}
		accumulator[""] = fields
	} else {
		for _, tag := range ep.tags {
			for group, values := range m {
				// extract the value of the tag
				cTag, err := extractTagFromGroup(group, tag)
				if err != nil {
					log.Errorf("unable to extract value of %s from %s", tag, group)
					continue
				}
				if _, ok := accumulator[cTag]; !ok {
					accumulator[cTag] = make(map[string]float64)
				}
				// get the metrics from this group
				for metric, resultKey := range ep.metrics {
					// init the accumulator if it doesn't exists
					if _, ok := accumulator[cTag][resultKey]; !ok {
						accumulator[cTag][resultKey] = 0
					}
					if n, ok := values.(map[string]interface{}); ok {
						if v, ok := n[metric]; ok {
							if value, ok := v.(float64); ok {
								accumulator[cTag][resultKey] += value
							}
						}
					}
				}
			}
		}
	}

	return accumulator, nil
}

func extractTagFromGroup(group, tag string) (string, error) {
	pattern, err := regexp.Compile(tag + "=" + "([^,^:]*)")
	if err != nil {
		return "", fmt.Errorf("unable to compile regexp")
	}
	matches := pattern.FindStringSubmatch(group)
	if matches == nil {
		return "", fmt.Errorf("invalid group format")
	}

	return matches[1], nil
}
