// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package agent

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/pantomath-io/panto/agent/probe"
	"gitlab.com/pantomath-io/panto/api"

	"github.com/golang/protobuf/ptypes"
)

// runningProbe encapsulate a scheduled probe
type runningProbe struct {
	label              string
	configuration      map[string]interface{}
	ticker             *time.Ticker
	stopChan           chan bool
	check              string
	probeConfiguration string
}

// Scheduler is a probe scheduling engine. It contains
// a list of currently scheduled probes and handles
// executing probes at regular intervals.
type Scheduler struct {
	// Global version of the configured probes
	version string

	probes []*runningProbe

	// Scheduler uses one channel per critical operation.
	// A single goroutine for the scheduler selects a
	// single channel/operation at any time, avoiding
	// race conditions.
	addChan    chan *runningProbe
	removeChan chan string
	tickChan   chan *runningProbe

	// This is to avoid a race, see GetProbes for details
	getProbesChan chan chan []string

	resultChan chan []*probe.Result
}

// DefaultScheduler is a default scheduler, instantiated by the package
// for usage by the easy interface (package-level functions).
var DefaultScheduler *Scheduler

func init() {
	DefaultScheduler = NewScheduler()
	go DefaultScheduler.Run()
}

// Results returns the default channel of probe results to receive from.
func Results() <-chan []*probe.Result {
	return DefaultScheduler.Results()
}

// ScheduleProbe adds a probe to the default `Scheduler` to
// be fired every `interval`.
func ScheduleProbe(label string, configuration map[string]interface{}, probeConfiguration, check string, interval time.Duration) {
	DefaultScheduler.AddProbe(label, configuration, probeConfiguration, check, interval)
}

// NewScheduler creates and returns a new `Scheduler` instance.
func NewScheduler() *Scheduler {
	s := Scheduler{
		probes: []*runningProbe{},

		// Channels are unbuffered. Making them buffered could improve
		// performance and availability, but might introduce latency.
		// Also check for possible race conditions.
		addChan:       make(chan *runningProbe),
		removeChan:    make(chan string),
		tickChan:      make(chan *runningProbe),
		getProbesChan: make(chan chan []string),

		resultChan: make(chan []*probe.Result),
	}

	return &s
}

// Run starts the scheduler. This function does not return,
// it is safe to run in a goroutine.
func (s *Scheduler) Run() {
	// This select ensures that at one time, the
	// scheduler only does one critical operation.
	for {
		select {
		case rp := <-s.tickChan:
			res := executeProbe(rp)
			go func(res []*probe.Result) { s.resultChan <- res }(res)
		case rp := <-s.addChan:
			s.probes = append(s.probes, rp)
		case pc := <-s.removeChan:
			// Find probe and remove it
			for i, itProbe := range s.probes {
				if itProbe.probeConfiguration == pc {
					itProbe.ticker.Stop()
					s.probes[i] = s.probes[len(s.probes)-1]
					s.probes[len(s.probes)-1] = nil
					s.probes = s.probes[:len(s.probes)-1]
					itProbe.stopChan <- true
					break
				}
			}
		case c := <-s.getProbesChan:
			probes := make([]string, len(s.probes))
			for i, p := range s.probes {
				probes[i] = p.probeConfiguration
			}
			c <- probes
		}
	}
}

func executeProbe(rp *runningProbe) []*probe.Result {
	ts := time.Now()

	// instantiate and configure the right probe
	p, err := probe.NewProbe(rp.label)
	if err != nil {
		return []*probe.Result{
			{
				Timestamp:          ts,
				Check:              rp.check,
				ProbeConfiguration: rp.probeConfiguration,
				Error:              true,
				ErrorMessage:       err.Error(),
			},
		}
	}

	err = p.Configure(rp.configuration)
	if err != nil {
		return []*probe.Result{
			{
				Timestamp:          ts,
				Check:              rp.check,
				ProbeConfiguration: rp.probeConfiguration,
				Error:              true,
				ErrorMessage:       err.Error(),
			},
		}
	}

	res, err := p.Execute()

	// Take time at which probe finished executing
	ts = time.Now()
	if err != nil {
		return []*probe.Result{
			{
				Timestamp:          ts,
				Check:              rp.check,
				ProbeConfiguration: rp.probeConfiguration,
				Error:              true,
				ErrorMessage:       err.Error(),
			},
		}
	}

	for _, r := range res {
		r.ProbeConfiguration = rp.probeConfiguration
		r.Check = rp.check
		r.Timestamp = ts
	}
	return res
}

// AddProbe schedules a probe to be fired every `interval`. It returns a
// function to be called to terminate the probe
func (s *Scheduler) AddProbe(label string, configuration map[string]interface{}, probeConfiguration, check string, interval time.Duration) {
	rp := &runningProbe{
		label:              label,
		configuration:      configuration,
		probeConfiguration: probeConfiguration,
		check:              check,
		ticker:             time.NewTicker(interval),
		stopChan:           make(chan bool),
	}

	// Add running probe to scheduler
	s.addChan <- rp

	// Fire probe once immediately
	log.Debugf("Timer for probe %s fired at %s", rp, time.Now())
	s.tickChan <- rp

	// Receive probe's tick events in a goroutine and forward them
	// to the scheduler's main tick event channel until probe
	// is unscheduled.
	go func() {
		for {
			// Select on probe's channels in a single goroutine to
			// avoid race condition
			select {
			case now := <-rp.ticker.C:
				log.Debugf("Timer for probe %s fired at %s", rp, now)
				s.tickChan <- rp
			case <-rp.stopChan:
				// Stop signal received, exit goroutine to avoid leaking
				return
			}
		}
	}()
}

// removeProbe unschedules a running probe by stopping the ticker,
// removing from the scheduler and finally signalling the
// running probe's gorouting to terminate.
func (s *Scheduler) removeProbe(probeConfiguration string) {
	s.removeChan <- probeConfiguration
}

// GetProbes returns a copy of the Scheduler's ProbeConfiguration names.
// To avoid a race condition, the results are copied in the same
// goroutine as the add & remove critical operations and returned
// to the current context via a channel.
// Both copying and async introduce a latency & memory overhead.
// Use this method with that in mind.
func (s *Scheduler) GetProbes() []string {
	// Create an ad-hoc channel for the copy
	c := make(chan []string)
	defer close(c)
	s.getProbesChan <- c

	// Receive the copy from the channel
	return <-c
}

// Results returns a channel of probe results to receive from.
func (s *Scheduler) Results() <-chan []*probe.Result {
	return s.resultChan
}

// RemoveAllProbes flushes the current Scheduler from all the runningProbes
func (s *Scheduler) RemoveAllProbes() error {
	probes := s.GetProbes()
	for _, rp := range probes {
		s.removeProbe(rp)
	}

	return nil
}

// SetVersion stores the version of the configuration in the Scheduler
func (s *Scheduler) SetVersion(version string) error {
	s.version = version
	return nil
}

// GetVersion returns the version of the configuration in the Scheduler
func (s Scheduler) GetVersion() string {
	return s.version
}

func (s Scheduler) String() string {
	return fmt.Sprintf("[version:%s] %s", s.version, s.probes)
}

func (rp runningProbe) String() string {
	return rp.label
}

// ConfigureScheduler loads the api.ListProbeConfigurationsResponse in the Scheduler
func (s *Scheduler) ConfigureScheduler(c *api.ListProbeConfigurationsResponse, v string) error {
	// set the global version
	if err := s.SetVersion(v); err != nil {
		return err
	}

	// add every configuration to the Scheduler
	for i, probeConfig := range c.GetProbeConfigurations() {
		log.Debugf("Probe configuration %d: %#v", i, probeConfig)

		var config map[string]interface{}
		err := json.Unmarshal([]byte(probeConfig.Configuration), &config)
		if err != nil {
			log.Errorf("couldn't unmarshal JSON probe configuration: %s", err)
			continue
		}

		schedule, err := ptypes.Duration(probeConfig.GetSchedule())
		if err != nil {
			log.Errorf("invalid schedule interval: %s", err)
			continue
		}

		// Schedule the probe
		s.AddProbe(
			probeConfig.ProbeLabel,
			config,
			probeConfig.Name,
			probeConfig.GetCheck(),
			schedule,
		)
	}

	return nil
}
