// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package agent

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/pantomath-io/panto/agent/probe"
)

func TestSpooler(t *testing.T) {
	_, err := NewSpooler(0, "/path/to/dump")
	if err == nil {
		t.Errorf("creating spooler with capacity 0 expected to fail")
	}
	_, err = NewSpooler(-14, "/path/to/dump")
	if err == nil {
		t.Errorf("creating spooler with capacity 0 expected to fail")
	}
	const capacity = 32
	s, err := NewSpooler(capacity, "/path/to/dump")
	if err != nil {
		t.Errorf("Failed to create new spooler: %s", err)
	}
	if got := s.Count(); got != 0 {
		t.Errorf("Invalid count on new spooler: expected 0, got %d", got)
	}
	if got := s.Results(); len(got) != 0 {
		t.Errorf("Invalid results on new spooler: expected [], got %v", got)
	}

	// Build test results
	now := time.Now()
	results := make([]*probe.Result, capacity)
	for i := 0; i < capacity; i++ {
		results[i] = &probe.Result{Timestamp: now.Add(time.Duration(-i) * time.Hour)}
	}

	s.AddResults(results)
	if got := s.Count(); got != len(results) {
		t.Errorf("Invalid count after adding results: expected %d, got %d", len(results), got)
	}
	res := s.Results()
	if len(res) != len(results) {
		t.Errorf("Invalid number of results: expected %d, got %d", len(results), len(res))
	}
Loop1:
	for _, expect := range results {
		found := false
		for _, got := range res {
			if reflect.DeepEqual(got, expect) {
				if !found {
					found = true
				} else {
					t.Errorf("Found result twice: %v", expect)
					break Loop1
				}
			}
		}
		if !found {
			t.Errorf("Result not found: %v", expect)
		}
	}
	lastResult := &probe.Result{Timestamp: now.Add(time.Hour)}
	s.AddResults([]*probe.Result{lastResult})
	if got := s.Count(); got != capacity {
		t.Errorf("Invalid count after adding result at capacity: expected %d, got %d", capacity, got)
	}
	res = s.Results()
	if len(res) != capacity {
		t.Errorf("Invalid number of results: expected %d, got %d", capacity, len(res))
	}
Loop2:
	for i := 0; i < len(results)-1; i++ {
		expect := results[i]
		found := false
		for _, got := range res {
			if reflect.DeepEqual(got, expect) {
				if !found {
					found = true
				} else {
					t.Errorf("Found result twice: %v", expect)
					break Loop2
				}
			}
		}
		if !found {
			t.Errorf("Result not found: %v", expect)
		}
	}
	{
		found := false
		for _, got := range res {
			if reflect.DeepEqual(got, lastResult) {
				if !found {
					found = true
				} else {
					t.Errorf("Found result twice: %v", lastResult)
					break
				}
			}
		}
		if !found {
			t.Errorf("Result not found: %v", lastResult)
		}
	}
	s.Clear()
	if got := s.Count(); got != 0 {
		t.Errorf("Invalid count on cleared spooler: expected 0, got %d", got)
	}
	if got := s.Results(); len(got) != 0 {
		t.Errorf("Invalid results on cleared spooler: expected [], got %v", got)
	}
}
