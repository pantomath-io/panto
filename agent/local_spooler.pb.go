// Code generated by protoc-gen-go. DO NOT EDIT.
// source: agent/local_spooler.proto

package agent

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	api "gitlab.com/pantomath-io/panto/api"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// DumpedSpooler defines the representation of the backup file
// for the agent spooler
type DumpedSpooler struct {
	Results              []*api.Result `protobuf:"bytes,1,rep,name=Results,proto3" json:"Results,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *DumpedSpooler) Reset()         { *m = DumpedSpooler{} }
func (m *DumpedSpooler) String() string { return proto.CompactTextString(m) }
func (*DumpedSpooler) ProtoMessage()    {}
func (*DumpedSpooler) Descriptor() ([]byte, []int) {
	return fileDescriptor_652913fc52bd7953, []int{0}
}

func (m *DumpedSpooler) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DumpedSpooler.Unmarshal(m, b)
}
func (m *DumpedSpooler) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DumpedSpooler.Marshal(b, m, deterministic)
}
func (m *DumpedSpooler) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DumpedSpooler.Merge(m, src)
}
func (m *DumpedSpooler) XXX_Size() int {
	return xxx_messageInfo_DumpedSpooler.Size(m)
}
func (m *DumpedSpooler) XXX_DiscardUnknown() {
	xxx_messageInfo_DumpedSpooler.DiscardUnknown(m)
}

var xxx_messageInfo_DumpedSpooler proto.InternalMessageInfo

func (m *DumpedSpooler) GetResults() []*api.Result {
	if m != nil {
		return m.Results
	}
	return nil
}

func init() {
	proto.RegisterType((*DumpedSpooler)(nil), "agent.DumpedSpooler")
}

func init() {
	proto.RegisterFile("agent/local_spooler.proto", fileDescriptor_652913fc52bd7953)
}

var fileDescriptor_652913fc52bd7953 = []byte{
	// 115 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x4c, 0x4c, 0x4f, 0xcd,
	0x2b, 0xd1, 0xcf, 0xc9, 0x4f, 0x4e, 0xcc, 0x89, 0x2f, 0x2e, 0xc8, 0xcf, 0xcf, 0x49, 0x2d, 0xd2,
	0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x05, 0x4b, 0x49, 0xf1, 0x27, 0x16, 0x64, 0xea, 0x17,
	0x24, 0xe6, 0x95, 0xe4, 0x43, 0xc4, 0x95, 0xcc, 0xb8, 0x78, 0x5d, 0x4a, 0x73, 0x0b, 0x52, 0x53,
	0x82, 0x21, 0xca, 0x85, 0x54, 0xb9, 0xd8, 0x83, 0x52, 0x8b, 0x4b, 0x73, 0x4a, 0x8a, 0x25, 0x18,
	0x15, 0x98, 0x35, 0xb8, 0x8d, 0xb8, 0xf5, 0x12, 0x0b, 0x32, 0xf5, 0x20, 0x62, 0x41, 0x30, 0xb9,
	0x24, 0x36, 0xb0, 0x76, 0x63, 0x40, 0x00, 0x00, 0x00, 0xff, 0xff, 0xe3, 0x98, 0x0f, 0xa2, 0x73,
	0x00, 0x00, 0x00,
}
