// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package agent

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"gitlab.com/pantomath-io/panto/agent/probe"
	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/wrapany"
)

// Spooler is used to spool data from a failed report, to try again when connectivity is re-established
type Spooler struct {
	capacity int
	results  []*probe.Result
	dumpPath string
}

// NewSpooler creates and returns a Spooler that can store up to maxResults. maxResults is the maximum number of results
// spooled. Once the maximum is reached, the spooler evicts the oldest results.
func NewSpooler(capacity int, path string) (*Spooler, error) {
	if capacity <= 0 {
		return nil, fmt.Errorf("max results should be a non-zero positive integer")
	}

	s := &Spooler{
		capacity: capacity,
		results:  make([]*probe.Result, 0, capacity),
		dumpPath: path,
	}

	if path == "" {
		log.Warningf("No dump path specified for the result spooler. The result will be kept in memory only.")
	} else {
		if err := s.load(); err != nil {
			return s, fmt.Errorf("unable to load previous dump: %s", err)
		}
	}

	return s, nil
}

// Count returns the number of items currently in the Spooler.
func (s Spooler) Count() int {
	return len(s.results)
}

// Results returns the spooled results as an array.
func (s Spooler) Results() []*probe.Result {
	return s.results
}

// Clear clears all results from the Spooler.
func (s *Spooler) Clear() {
	s.results = s.results[:0]

	// dump the spooler to disk
	if err := s.dump(); err != nil {
		log.Errorf("%s", err)
	}
}

// AddResults adds a batch of results to the Spooler. If the Spooler reaches max capacity, the oldest results are
// evicted.
func (s *Spooler) AddResults(results []*probe.Result) {
	if len(s.results)+len(results) > s.capacity {
		log.Infof(
			"Result spooler is at full capacity, %d results dropped",
			len(s.results)+len(results)-s.capacity,
		)
	}
	for _, r := range results {
		s.add(r)
	}

	// dump the spooler to disk
	if err := s.dump(); err != nil {
		log.Errorf("%s", err)
	}
}

func (s *Spooler) add(res *probe.Result) {
	// Chrales 2018-03-23
	// Currently the result is implemented as an unsorted array. This is not an efficient structure for this. We could
	// keep the array sorted as we insert, or even use a binary tree or something. But keeping with the UNIX philosophy:
	// don't use fancy algorithms until you need them.
	if len(s.results) == s.capacity {
		// Spooler at max capacity, evict the oldest result
		minIdx := 0
		minTs := s.results[0].Timestamp
		for i, r := range s.results {
			if r.Timestamp.Before(minTs) {
				minIdx = i
				minTs = r.Timestamp
			}
		}
		if minIdx+1 < s.capacity {
			s.results = append(s.results[:minIdx], s.results[minIdx+1:]...)
		} else {
			s.results = s.results[:minIdx]
		}
	}
	s.results = append(s.results, res)
}

// dump saves the spooler in a protobuf file
func (s *Spooler) dump() (err error) {
	if s.dumpPath == "" {
		log.Warning("Can't dump spooler to file: no path specified")
		return nil
	}

	dump := &DumpedSpooler{
		Results: make([]*api.Result, len(s.results)),
	}

	for i, result := range s.results {
		ts, err := ptypes.TimestampProto(result.Timestamp)
		if err != nil {
			log.Errorf("invalid timestamp: %s", err)
			continue
		}

		if result.Error {
			dump.Results[i] = &api.Result{
				ProbeConfiguration: result.ProbeConfiguration,
				Check:              result.Check,
				Timestamp:          ts,
				Error:              true,
				ErrorMessage:       result.ErrorMessage,
			}
		} else {
			tags := result.Tags
			if tags == nil {
				tags = map[string]string{}
			}

			fields := make(map[string]*any.Any)

			for k, v := range result.Fields {
				fields[k], err = wrapany.Wrap(v)
				if err != nil {
					log.Errorf("Dump error - unable to convert %T value for field %s to any.Any", v, k)
					continue
				}
			}

			dump.Results[i] = &api.Result{
				ProbeConfiguration: result.ProbeConfiguration,
				Check:              result.Check,
				Timestamp:          ts,
				Fields:             fields,
				Tags:               tags,
			}
		}
	}

	// Write the message to disk.
	out, err := proto.Marshal(dump)
	if err != nil {
		return fmt.Errorf("unable to marshal spooler while dumping: %s", err)
	}

	if err = ioutil.WriteFile(s.dumpPath, out, 0644); err != nil {
		return fmt.Errorf("unable to write spooler to disk: %s", err)
	}

	return
}

// load read a dumped spooler file, and loads its content
func (s *Spooler) load() (err error) {
	if s.dumpPath == "" {
		log.Warning("Can't load spooler from dump file: no path specified")
		return nil
	}
	if _, err := os.Stat(s.dumpPath); os.IsNotExist(err) {
		log.Infof("Spooler dump file (%s) does not exist", s.dumpPath)
		return nil
	}
	log.Infof("Loading spooler dump file (%s)", s.dumpPath)

	in, err := ioutil.ReadFile(s.dumpPath)
	if err != nil {
		return fmt.Errorf("unable to read dump file: %s", err)
	}

	dump := &DumpedSpooler{}

	if err = proto.Unmarshal(in, dump); err != nil {
		return fmt.Errorf("unable to parse spooler dump file: %s", err)
	}

	var countLoaded uint8
	for _, r := range dump.Results {
		ts, err := ptypes.Timestamp(r.Timestamp)
		if err != nil {
			log.Errorf("invalid timestamp (%s)", err)
			continue
		}
		// If there is no timestamp, default to now.
		if ts.IsZero() {
			ts = time.Now()
		}

		result := &probe.Result{
			Timestamp:          ts,
			ProbeConfiguration: r.ProbeConfiguration,
			Check:              r.Check,
		}

		if !r.Error {
			fields := make(map[string]interface{})
			// Transform type Any to interface{}
			for k, v := range r.Fields {
				fields[k], err = wrapany.Unwrap(v)
				if err != nil {
					log.Errorf("couldn't unwrap field value for %s (%s)", k, err)
					continue
				}
			}
			result.Fields = fields
			result.Tags = r.Tags
		} else {
			result.Error = true
			result.ErrorMessage = r.ErrorMessage
		}

		s.add(result)
		countLoaded++
	}

	log.Infof("%d results loaded from spooler dump file", countLoaded)
	return
}
