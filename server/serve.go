// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package server manages the gRPC service and
// defines the protobuf services and message
package server

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/datapipe"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/golang/protobuf/proto"
	"github.com/google/uuid"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/op/go-logging"
	"github.com/rs/cors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"
)

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

// private type for Context keys
type organizationUUIDKey struct{}
type agentUUIDKey struct{}

// server is used to save the state and resources of the
// gRPC server.
type server struct {
	info     api.Info
	dataPipe datapipe.DataPipe
}

// allowedHeaders lists the headers to be validated by the gRPC headerMatcher and CORS
var allowedHeaders = []string{"agent-version", "content-type", "cookie", "user-agent", "authorization"}

// agentConfigurationVersionInterceptor allow to intercept a RPC call before it triggers
// the service to send the Agent Configuration Version in the metadata
func agentConfigurationVersionInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	// Get the agent from the context, where it was set by `authenticateAgent`
	agentUUID, agentOK := ctx.Value(agentUUIDKey{}).(uuid.UUID)
	organizationUUID, organizationOK := ctx.Value(organizationUUIDKey{}).(uuid.UUID)
	if agentOK && organizationOK {
		// if the agentUUID is null, it means we have no configuration version to return
		if (agentUUID == uuid.UUID{}) {
			return handler(ctx, req)
		}
		dbconf, err := services.GetDBConf()
		if err != nil {
			log.Errorf("Couldn't connect to configuration database: %s", err)
			return nil, status.Error(codes.Internal, "Internal error")
		}
		a, err := dbconf.GetAgent(organizationUUID, agentUUID)
		if err != nil {
			log.Errorf("Unknown agent %s (%s)", agentUUID, err)
			return nil, status.Error(codes.NotFound, "Unknown Agent")
		}

		// get the TsConfiguration timestamp in the metadata
		agentConfigurationVersion := uint64(a.TsConfiguration.UnixNano())
		err = grpc.SendHeader(
			ctx,
			metadata.New(map[string]string{
				"agent-configuration-version": strconv.FormatUint(agentConfigurationVersion, 10),
			}),
		)
		if err != nil {
			err := fmt.Sprintf("failed to set metadata in response header (%s)", err)
			log.Errorf(err)
			return nil, status.Error(codes.Internal, err)
		}
	} else {
		log.Warning("missing agent or organization metadata")
	}
	return handler(ctx, req)
}

// agentActivityInterceptor allows to intercept the RPC call before it triggers
// the service to log the agent activity
func agentActivityInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	// Get the agent from the context, where it was set by `authenticateAgent`
	agentUUID, agentOK := ctx.Value(agentUUIDKey{}).(uuid.UUID)
	organizationUUID, organizationOK := ctx.Value(organizationUUIDKey{}).(uuid.UUID)
	if agentOK && organizationOK {
		// if the agentUUID is null, it's not an agent
		if (agentUUID == uuid.UUID{}) {
			return handler(ctx, req)
		}

		// get details for the Agent
		dbconf, err := services.GetDBConf()
		if err != nil {
			log.Errorf("Couldn't connect to configuration database: %s", err)
			return nil, status.Error(codes.Internal, "Internal error")
		}
		a, err := dbconf.GetAgent(organizationUUID, agentUUID)
		if err != nil {
			log.Errorf("Unknown agent %s (%s)", agentUUID, err)
			return nil, status.Error(codes.NotFound, "Unknown Agent")
		}
		var agentVersion string
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			log.Warning("no request metadata: unable to get agent version")
		} else {
			if av, ok := md["agent-version"]; ok {
				agentVersion = strings.Join(av, "")
			}
		}

		ts, err := services.GetTSDB()
		if err != nil {
			log.Errorf("Couldn't find tsdb service: %s", err)
			return nil, status.Error(codes.Internal, "Internal error")
		}
		err = ts.AddActivity(tsdb.Activity{
			Timestamp:    time.Now(),
			Organization: a.OrganizationUUID,
			Agent:        a.UUID,
			Type:         info.FullMethod,
			Version:      agentVersion,
		})
		if err != nil {
			log.Errorf("Couldn't write agent activity: %s", err)
			return nil, status.Error(codes.Internal, "Internal error")
		}

	}
	return handler(ctx, req)
}

// logInterceptor logs the request after it passes through the handler. The log
// format is similar to the NCSA combined log format, replacing:
//  * the HTTP request line with the gRPC method name
//  * the HTTP return status code with the gRPC return code
//  * the size of the response in bytes with the size of the serialized protobuf message
// Apache's mod_log_config format string:
// "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\""
func logInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (res interface{}, err error) {
	// Store incoming request time
	reqTime := time.Now()

	// Call the actual handler (or next interceptor in the chain)
	res, err = handler(ctx, req)

	// Initialize all strings
	addr := "-"                                                 // The remote hostname
	logname := "-"                                              // Always "-", has something to do with identd
	username := "-"                                             // Remote user if the request was authenticated.
	timestamp := reqTime.Format("[02/Jan/2006 03:04:05 -0700]") // Time the request was received
	request := info.FullMethod                                  // The full gRPC method name
	statusCode := "-"                                           // The gRPC return status
	byteSize := "-"                                             // The size of the response in bytes
	referer := "-"                                              // The Referer header, if any
	userAgent := "-"                                            // The User-Agent header, if any

	// Get the remote peer info from the context
	if p, ok := peer.FromContext(ctx); ok {
		if tcpAddr, ok := p.Addr.(*net.TCPAddr); ok {
			addr = tcpAddr.IP.String()
		} else {
			addr = p.Addr.String()
		}
	}

	// TODO: Get the username if logged in

	if err == nil {
		statusCode = codes.OK.String()
		byteSize = strconv.Itoa(proto.Size(res.(proto.Message)))
	} else if grpcErr, ok := status.FromError(err); ok {
		statusCode = grpcErr.Code().String()
		byteSize = strconv.Itoa(proto.Size(grpcErr.Proto()))
	}

	// Get metadata from context
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if r, ok := md["referer"]; ok {
			referer = strings.Join(r, "")
		}
		if ua, ok := md["user-agent"]; ok {
			userAgent = strings.Join(ua, "")
		}
	}

	var b strings.Builder
	b.WriteString(addr)
	b.WriteString(" ")
	b.WriteString(logname)
	b.WriteString(" ")
	b.WriteString(username)
	b.WriteString(" ")
	b.WriteString(timestamp)
	b.WriteString(" ")
	b.WriteString(fmt.Sprintf("\"%s\"", request))
	b.WriteString(" ")
	b.WriteString(statusCode)
	b.WriteString(" ")
	b.WriteString(byteSize)
	b.WriteString(" ")
	b.WriteString(fmt.Sprintf("\"%s\"", referer))
	b.WriteString(" ")
	b.WriteString(fmt.Sprintf("\"%s\"", userAgent))

	log.Infof("%s", b.String())

	return
}

func dbVersionInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (res interface{}, err error) {
	// Make sure the configuration database is up to date
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbconfVersionOK, myerr := dbconf.IsVersion(db, dbconf.RequiredVersion())
	if myerr != nil {
		log.Errorf("Unable to test configuration database version: %s", myerr)
		return nil, status.Error(codes.FailedPrecondition, "Invalid database version")
	}
	if !dbconfVersionOK {
		dbconfVersion, myerr := dbconf.GetVersion(db)
		if myerr != nil {
			log.Errorf("Unable to get configuration database version: %s", myerr)
			return nil, status.Error(codes.FailedPrecondition, "Invalid database version")
		}

		log.Errorf("The database version (%d) does not match the expected version (%d).", dbconfVersion, dbconf.RequiredVersion())
		return nil, status.Error(codes.FailedPrecondition, "Invalid database version")
	}

	// Call the actual handler (or next interceptor in the chain)
	return handler(ctx, req)
}

// incomingHeaderMatcher matches HTTP headers in the request and authorizes their injection in gRPC context
func incomingHeaderMatcher(headerName string) (string, bool) {
	headerName = strings.ToLower(headerName)
	if util.Contains(allowedHeaders, headerName) {
		return headerName, true
	}

	// Header not allowed
	return headerName, false
}

// outgoingHeaderMatcher matches HTTP headers in the response and authorizes their injection HTTP header
func outgoingHeaderMatcher(headerName string) (string, bool) {
	headerName = strings.ToLower(headerName)
	if util.Contains([]string{"set-cookie"}, headerName) {
		return headerName, true
	}

	// Header not allowed
	return headerName, false
}

func startGRPCServer(conf Configuration, info api.Info) error {
	lis, err := net.Listen("tcp", conf.GRPCAddress)
	if err != nil {
		return fmt.Errorf("failed to listen: %v", err)
	}

	// Create the gRPC server with the credentials and the interceptors
	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				logInterceptor,
				dbVersionInterceptor,
				authInterceptor,
				agentConfigurationVersionInterceptor,
				agentActivityInterceptor,
				deprecationInterceptor,
			),
		),
	}

	if !conf.NoTLS {
		if conf.CertFilePath != "" && conf.KeyFilePath != "" {
			// Create the TLS credentials
			creds, err := credentials.NewServerTLSFromFile(conf.CertFilePath, conf.KeyFilePath)
			if err != nil {
				return fmt.Errorf("could not load TLS keys: %s", err)
			}
			opts = append(opts, grpc.Creds(creds))
		} else {
			return fmt.Errorf("missing certificate and key file, specify a TLS certificate or run panto with --no-tls")
		}
	}

	grpcServer := grpc.NewServer(opts...)

	s := server{
		info:     info,
		dataPipe: datapipe.New(),
	}

	api.RegisterPantoServer(grpcServer, &s)

	log.Debugf("Starting gRPC server on %s", conf.GRPCAddress)

	if err := grpcServer.Serve(lis); err != nil {
		return fmt.Errorf("failed to serve: %s", err)
	}

	return nil
}

func startRESTServer(conf Configuration) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	marshaler := runtime.JSONPb{
		EmitDefaults: true,
		OrigName:     true,
	}
	mux := runtime.NewServeMux(
		runtime.WithIncomingHeaderMatcher(incomingHeaderMatcher),
		runtime.WithOutgoingHeaderMatcher(outgoingHeaderMatcher),
		runtime.WithMarshalerOption("*", &marshaler),
	)

	var err error
	var opts []grpc.DialOption
	if conf.NoTLS {
		opts = []grpc.DialOption{grpc.WithInsecure()}
	} else if conf.CertFilePath != "" {
		creds, err := credentials.NewClientTLSFromFile(conf.CertFilePath, "")
		if err != nil {
			return fmt.Errorf("could not load TLS certificate: %s", err)
		}
		opts = []grpc.DialOption{grpc.WithTransportCredentials(creds)}
	} else {
		return fmt.Errorf("missing certificate and key file, specify a TLS certificate or run panto with --no-tls")
	}

	// Register probe result
	err = api.RegisterPantoHandlerFromEndpoint(
		ctx,
		mux,
		conf.GRPCAddress,
		opts,
	)
	if err != nil {
		return err
	}

	// Configure CORS options
	c := cors.New(cors.Options{
		AllowedOrigins:   strings.Split(conf.AllowOrigin, ","),
		AllowedMethods:   []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
		AllowedHeaders:   allowedHeaders,
		AllowCredentials: true,
	})
	handler := c.Handler(mux)

	log.Debugf("Starting HTTP/1.1 REST server on %s", conf.RESTAddress)
	return http.ListenAndServe(conf.RESTAddress, handler)
}

// Serve starts the gRPC server on a specific address
func Serve(conf Configuration, info api.Info) (err error) {
	gchan := make(chan error)
	rchan := make(chan error)

	go func() {
		gchan <- startGRPCServer(conf, info)
	}()

	go func() {
		rchan <- startRESTServer(conf)
	}()

	select {
	case err = <-gchan:
		err = fmt.Errorf("failure in gRPC server: %s", err)
	case err = <-rchan:
		err = fmt.Errorf("failure in REST server: %s", err)
	}

	// TODO: if one server fails, we should probably shutdown the other gracefully
	// This requires using more complex data structures that actually support shutdown.
	// See `http.Server`

	return err
}
