// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"regexp"
	"testing"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	durpb "github.com/golang/protobuf/ptypes/duration"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
)

func TestListProbeConfigurations(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListProbeConfigurationsRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
	}
	res, err := s.ListProbeConfigurations(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list probe configurations: %s", err)
	} else {
		if len(res.ProbeConfigurations) != 11 {
			t.Fatalf("Unexpected probe configurations count: got %d, expected %d", len(res.ProbeConfigurations), 11)
		}
		for i, c := range res.ProbeConfigurations {
			expect := fmt.Sprintf(`{"address":"target%d.pantomath.io"}`, i)
			if c.Configuration != expect {
				t.Fatalf("Unexpected probe configuration at index %d: got %v, expected %v", i, c.Configuration, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestQueryProbeConfigurations(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	{
		req := &api.QueryProbeConfigurationsRequest{
			Organization: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		}
		res, err := s.QueryProbeConfigurations(context.Background(), req)
		if err != nil {
			t.Fatalf("Failed to list probe configurations: %s", err)
		} else {
			if len(res.ProbeConfigurations) != 12 {
				t.Fatalf("Unexpected probe configurations count: got %d, expected %d", len(res.ProbeConfigurations), 12)
			}
			if res.NextPageToken != "" {
				t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
			}
		}
	}
	{
		req := &api.QueryProbeConfigurationsRequest{
			Organization: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
			Checks:       []string{"organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/FKbFtQWgTsKtNqPEP-XUcw"},
		}
		res, err := s.QueryProbeConfigurations(context.Background(), req)
		if err != nil {
			t.Fatalf("Failed to list probe configurations: %s", err)
		} else {
			if len(res.ProbeConfigurations) != 11 {
				t.Fatalf("Unexpected probe configurations count: got %d, expected %d", len(res.ProbeConfigurations), 11)
			}
			for i, c := range res.ProbeConfigurations {
				expect := fmt.Sprintf(`{"address":"target%d.pantomath.io"}`, i)
				if c.Configuration != expect {
					t.Fatalf("Unexpected probe configuration at index %d: got %v, expected %v", i, c.Configuration, expect)
				}
			}
			if res.NextPageToken != "" {
				t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
			}
		}
	}
}

func TestListProbeConfigurationsWithTarget(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListProbeConfigurationsRequest{
		Parent:     "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
		WithTarget: true,
	}
	res, err := s.ListProbeConfigurations(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list probe configurations: %s", err)
	} else {
		if len(res.ProbeConfigurations) != 11 {
			t.Fatalf("Unexpected probe configurations count: got %d, expected %d", len(res.ProbeConfigurations), 11)
		}
		expect := &api.Target{
			Name:    "organizations/KTCCC60hRgWDtGnLxb_I9Q/targets/wCSdWuxMRU6E_GhtyIrSpg",
			Address: "target0.pantomath.io",
			Note:    "txpsjklml agcamxrjuewcusgzkl",
		}
		for i, c := range res.ProbeConfigurations {
			if c.Target.Name != expect.Name {
				t.Fatalf("Unexpected probe configuration target name at index %d: got %v, expected %v", i, c.Target.Name, expect.Name)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestListProbeConfigurationsPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test pagination
	req := &api.ListProbeConfigurationsRequest{
		Parent:   "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
		PageSize: 4,
	}
	res, err := s.ListProbeConfigurations(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list probe configurations: %s", err)
	} else {
		if len(res.ProbeConfigurations) != 4 {
			t.Fatalf("Unexpected probe configurations count: got %d, expected %d", len(res.ProbeConfigurations), 4)
		}
		for i, c := range res.ProbeConfigurations {
			expect := fmt.Sprintf(`{"address":"target%d.pantomath.io"}`, i)
			if c.Configuration != expect {
				t.Fatalf("Unexpected probe configuration parameters at index %d: got %v, expected %v", i, c.Configuration, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 2
	page2Token := res.NextPageToken
	req.PageToken = res.NextPageToken
	res, err = s.ListProbeConfigurations(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list probe configurations: %s", err)
	} else {
		if len(res.ProbeConfigurations) != 4 {
			t.Fatalf("Unexpected probe configurations count: got %d, expected %d", len(res.ProbeConfigurations), 4)
		}
		for i, c := range res.ProbeConfigurations {
			expect := fmt.Sprintf(`{"address":"target%d.pantomath.io"}`, i+4)
			if c.Configuration != expect {
				t.Fatalf("Unexpected probe configuration parameters at index %d: got %v, expected %v", i, c.Configuration, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 3
	req.PageToken = res.NextPageToken
	res, err = s.ListProbeConfigurations(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list probe configurations: %s", err)
	} else {
		if len(res.ProbeConfigurations) != 3 {
			t.Fatalf("Unexpected probe configurations count: got %d, expected %d", len(res.ProbeConfigurations), 3)
		}
		for i, c := range res.ProbeConfigurations {
			expect := fmt.Sprintf(`{"address":"target%d.pantomath.io"}`, i+8)
			if c.Configuration != expect {
				t.Fatalf("Unexpected probe configuration parameters at index %d: got %v, expected %v", i, c.Configuration, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}

	// Invalid page token
	req.PageToken = page2Token[1:]
	_, err = s.ListProbeConfigurations(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed probe configurations with invalid token")
	}

	// Invalid request
	req.PageSize = 10
	req.PageToken = page2Token
	_, err = s.ListProbeConfigurations(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed probe configurations with invalid request")
	}
}

func TestGetProbeConfiguration(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test success
	req := api.GetProbeConfigurationRequest{
		Name: "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/dgR4kcQXQrGzr69tG7l0uA",
	}
	res, err := s.GetProbeConfiguration(context.Background(), &req)
	if err != nil {
		t.Errorf("Failed to retrieve probe configuration: %s", err)
	} else {
		expect := api.ProbeConfiguration{
			Name:          "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/dgR4kcQXQrGzr69tG7l0uA",
			Check:         "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/FKbFtQWgTsKtNqPEP-XUcw",
			Configuration: `{"address":"target0.pantomath.io"}`,
			ProbeLabel:    "ping",
			Schedule:      ptypes.DurationProto(1057884408109),
		}
		if !proto.Equal(res, &expect) {
			t.Errorf("Unexpected response: got %v, expected %v", *res, expect)
		}
	}

	// Test failure
	req = api.GetProbeConfigurationRequest{
		Name: "organizations/kDtJz8EqQ0C7KgOF6qwxfg/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/bhfPN3czSZCH0MDVc7gfGQ",
	}
	res, err = s.GetProbeConfiguration(context.Background(), &req)
	if err == nil {
		t.Errorf("Unexpected success retrieving probe configuration: got %v", *res)
	}
}

func TestGetProbeConfigurationWithTarget(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test success
	req := api.GetProbeConfigurationRequest{
		Name:       "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/dgR4kcQXQrGzr69tG7l0uA",
		WithTarget: true,
	}
	res, err := s.GetProbeConfiguration(context.Background(), &req)
	if err != nil {
		t.Errorf("Failed to retrieve probe configuration: %s", err)
	} else {
		expect := api.ProbeConfiguration{
			Name:          "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/dgR4kcQXQrGzr69tG7l0uA",
			Check:         "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/FKbFtQWgTsKtNqPEP-XUcw",
			Configuration: `{"address":"target0.pantomath.io"}`,
			ProbeLabel:    "ping",
			Schedule:      ptypes.DurationProto(1057884408109),
			Target: &api.Target{
				Name:    "organizations/KTCCC60hRgWDtGnLxb_I9Q/targets/wCSdWuxMRU6E_GhtyIrSpg",
				Address: "target0.pantomath.io",
				Note:    "txpsjklml agcamxrjuewcusgzkl",
			},
		}
		if !proto.Equal(res, &expect) {
			t.Errorf("Unexpected response: got %v, expected %v", *res, expect)
		}
	}
}

func TestCreateProbeConfiguration(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test success
	req := api.CreateProbeConfigurationRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
		ProbeConfiguration: &api.ProbeConfiguration{
			Check:         "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/FKbFtQWgTsKtNqPEP-XUcw",
			Configuration: `{"address":"targettino.pantomath.io"}`,
			Schedule:      ptypes.DurationProto(time.Minute),
		},
	}
	res, err := s.CreateProbeConfiguration(context.Background(), &req)
	if err != nil {
		t.Errorf("Failed to create probe configuration: %s", err)
	} else {
		if ok, _ := regexp.Match(
			"organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/[-_A-Za-z0-9]{22}",
			[]byte(res.Name),
		); !ok {
			t.Fatalf("Unexpected ProbeConfiguration name: %s", res.Name)
		}
		if res.Check != "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/FKbFtQWgTsKtNqPEP-XUcw" {
			t.Fatalf("Unexpected check: expected %s, got %s", "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/FKbFtQWgTsKtNqPEP-XUcw", res.Name)
		}
		if res.Configuration != `{"address":"targettino.pantomath.io"}` {
			t.Fatalf("Unexpected param: expected %s, got %s", `{"address":"targettino.pantomath.io"}`, res.Configuration)
		}
		if res.ProbeLabel != "ping" {
			t.Fatalf("Unexpected probe label: expected %s, got %s", "ping", res.ProbeLabel)
		}
		if !proto.Equal(res.Schedule, ptypes.DurationProto(time.Minute)) {
			t.Fatalf("Unexpected schedule: expected %v, got %v", ptypes.DurationProto(time.Minute), res.Schedule)
		}
	}

	// TODO: check agent configuration timestamp update
}

func TestCreateProbeConfiguration_WithInvalidCheck(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test success
	req := api.CreateProbeConfigurationRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
		ProbeConfiguration: &api.ProbeConfiguration{
			Check:         "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/qb0vYT7OQjmlDgaj58bHgw",
			Configuration: `{"address":"targettino.pantomath.io"}`,
			Schedule:      ptypes.DurationProto(time.Minute),
		},
	}
	_, err := s.CreateProbeConfiguration(context.Background(), &req)
	if err == nil {
		t.Fatalf("unexpected success creating ProbeConfiguration with invalid Check")
	}
}

func TestUpdateProbeConfiguration(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test success
	req := api.UpdateProbeConfigurationRequest{
		ProbeConfiguration: &api.ProbeConfiguration{
			Name:          "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/dgR4kcQXQrGzr69tG7l0uA",
			Configuration: `{"address":"target-test.pantomath.io"}`,
			Schedule:      ptypes.DurationProto(1*time.Minute + 30*time.Second),
			Check:         "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/FKbFtQWgTsKtNqPEP-XUcw",
		},
	}
	res, err := s.UpdateProbeConfiguration(context.Background(), &req)
	if err != nil {
		t.Errorf("Failed to update probe configuration: %s", err)
	} else {
		expect := &api.ProbeConfiguration{
			Name:          "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/dgR4kcQXQrGzr69tG7l0uA",
			Check:         "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/FKbFtQWgTsKtNqPEP-XUcw",
			ProbeLabel:    "ping",
			Configuration: `{"address":"target-test.pantomath.io"}`,
			Schedule:      ptypes.DurationProto(1*time.Minute + 30*time.Second),
		}
		if !proto.Equal(res, expect) {
			t.Fatalf("Unexpected ProbeConfiguration: got %s, expected %s", res, expect)
		}
	}

	// TODO: check agent configuration timestamp update
}

func TestDeleteProbeConfiguration(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test success
	req2 := &api.GetProbeConfigurationRequest{
		Name: "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/K44abcZKQySxpvnfaaB20g",
	}
	_, err := s.GetProbeConfiguration(context.Background(), req2)
	if err != nil {
		t.Fatalf("Failed to retrieve ProbeConfiguration")
	}

	req := api.DeleteProbeConfigurationRequest{
		Name: "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/K44abcZKQySxpvnfaaB20g",
	}
	_, err = s.DeleteProbeConfiguration(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to delete probe configuration: %s", err)
	}

	_, err = s.GetProbeConfiguration(context.Background(), req2)
	if err == nil {
		t.Fatalf("Unexpected success retrieving deleted ProbeConfiguration")
	}

	// TODO: check agent configuration timestamp update
}

func TestValidateProbeConfigurationName(t *testing.T) {
	{
		_, _, _, errDetail := validateProbeConfigurationName("organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ/probeconfigurations/K44abcZKQySxpvnfaaB20g")
		if errDetail != nil {
			t.Fatalf("unexpected error: %s: %s", errDetail.Field, errDetail.Description)
		}
	}

	{
		o, a, pc, errDetail := validateProbeConfigurationName("Stalingrad")
		if errDetail == nil {
			t.Fatalf("Expected error, got: %s, %s, %s", o, a, pc)
		}
	}

	{
		o, a, pc, errDetail := validateProbeConfigurationName("")
		if errDetail == nil {
			t.Fatalf("Expected error, got: %s, %s, %s", o, a, pc)
		}
	}
}

func TestValidateProbeConfigurationCheck(t *testing.T) {
	{
		_, _, errDetail := validateProbeConfigurationCheck("organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/oKuOJ7EaTuSke81z3lYWDQ")
		if errDetail != nil {
			t.Fatalf("unexpected error: %s: %s", errDetail.Field, errDetail.Description)
		}
	}

	{
		o, c, errDetail := validateProbeConfigurationCheck("Stalingrad")
		if errDetail == nil {
			t.Fatalf("Expected error, got: %s, %s", o, c)
		}
	}

	{
		o, c, errDetail := validateProbeConfigurationCheck("")
		if errDetail == nil {
			t.Fatalf("Expected error, got: %s, %s", o, c)
		}
	}
}

func TestValidateProbeConfigurationSchedule(t *testing.T) {
	{
		_, errDetail := validateProbeConfigurationSchedule(&durpb.Duration{Seconds: 1, Nanos: 0})
		if errDetail != nil {
			t.Fatalf("unexpected error: %s: %s", errDetail.Field, errDetail.Description)
		}
	}

	{
		d, errDetail := validateProbeConfigurationSchedule(&durpb.Duration{Seconds: 0, Nanos: 0})
		if errDetail == nil {
			t.Fatalf("Expected error, got: %s", d)
		}
	}
}

func TestValidateProbeConfigurationConfiguration(t *testing.T) {
	{
		_, errDetail := validateProbeConfigurationConfiguration(`{"foo":"bar"}`, []util.Parameter{
			{
				Name:        "foo",
				Type:        "string",
				Description: "",
				Mandatory:   true,
				Placeholder: "",
				Default:     "",
			},
		})
		if errDetail != nil {
			t.Fatalf("unexpected error(s): %s", errDetail)
		}
	}
}
