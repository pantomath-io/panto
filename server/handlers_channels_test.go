// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"google.golang.org/genproto/protobuf/field_mask"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
)

func TestListChannels(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListChannelsRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
	}
	res, err := s.ListChannels(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list channels: %s", err)
	} else {
		if len(res.Channels) != 10 {
			t.Fatalf("Unexpected channels count: got %d, expected %d", len(res.Channels), 10)
		}
		for i, c := range res.Channels {
			got := c.DisplayName
			expect := fmt.Sprintf("E-mail: dest%d@pantomath.io", i)
			if got != expect {
				t.Fatalf("Unexpected channel display_name at index %d: got %v, expected %v", i, got, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestListChannelsWithNesting(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListChannelsRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		ChildrenMask: &field_mask.FieldMask{
			Paths: []string{"channel_type"},
		},
	}
	res, err := s.ListChannels(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list channels: %s", err)
	} else {
		if len(res.Channels) != 10 {
			t.Fatalf("Unexpected channels count: got %d, expected %d", len(res.Channels), 10)
		}
		for i, c := range res.Channels {
			got := c.DisplayName
			expect := fmt.Sprintf("E-mail: dest%d@pantomath.io", i)
			if got != expect {
				t.Fatalf("Unexpected channel configuration at index %d: got %v, expected %v", i, got, expect)
			}
			if c.Children == nil {
				t.Fatalf("Missing expected channel_type children at index %d: got %v", i, c)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestListChannelsPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test pagination
	req := &api.ListChannelsRequest{
		Parent:   "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		PageSize: 4,
	}
	res, err := s.ListChannels(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list channels: %s", err)
	} else {
		if len(res.Channels) != 4 {
			t.Fatalf("Unexpected channels count: got %d, expected %d", len(res.Channels), 4)
		}
		for i, c := range res.Channels {
			got := c.DisplayName
			expect := fmt.Sprintf("E-mail: dest%d@pantomath.io", i)
			if got != expect {
				t.Fatalf("Unexpected channel configuration at index %d: got %v, expected %v", i, got, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 2
	page2Token := res.NextPageToken
	req.PageToken = res.NextPageToken
	res, err = s.ListChannels(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list channels: %s", err)
	} else {
		if len(res.Channels) != 4 {
			t.Fatalf("Unexpected channels count: got %d, expected %d", len(res.Channels), 4)
		}
		for i, c := range res.Channels {
			got := c.DisplayName
			expect := fmt.Sprintf("E-mail: dest%d@pantomath.io", i+4)
			if got != expect {
				t.Fatalf("Unexpected channel configuration at index %d: got %v, expected %v", i, got, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 3
	req.PageToken = res.NextPageToken
	res, err = s.ListChannels(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list channels: %s", err)
	} else {
		if len(res.Channels) != 2 {
			t.Fatalf("Unexpected channels count: got %d, expected %d", len(res.Channels), 2)
		}
		for i, c := range res.Channels {
			got := c.DisplayName
			expect := fmt.Sprintf("E-mail: dest%d@pantomath.io", i+8)
			if got != expect {
				t.Fatalf("Unexpected channel configuration at index %d: got %v, expected %v", i, got, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}

	// Invalid page token
	req.PageToken = page2Token[1:]
	_, err = s.ListChannels(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed channels with invalid token")
	}

	// Invalid request
	req.PageSize = 10
	req.PageToken = page2Token
	_, err = s.ListChannels(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed channels with invalid request")
	}
}

func TestGetChannel(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.GetChannelRequest{
		Name: "organizations/KTCCC60hRgWDtGnLxb_I9Q/channels/AjBVqs9hTB2ph2EWyDoMuQ",
	}
	res, err := s.GetChannel(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to get channel: %s", err)
	} else {
		expect := api.Channel{
			Name:          "organizations/KTCCC60hRgWDtGnLxb_I9Q/channels/AjBVqs9hTB2ph2EWyDoMuQ",
			ChannelType:   "channeltypes/email",
			DisplayName:   "E-mail: dest0@pantomath.io",
			Configuration: "{\"dest\": \"dest@pantomath.io\"}",
		}

		if res.Name != expect.Name ||
			res.ChannelType != expect.ChannelType ||
			res.DisplayName != expect.DisplayName {
			r, _ := json.Marshal(res)
			e, _ := json.Marshal(expect)
			t.Fatalf("expected %s, got %s", e, r)
		}
	}
}

func TestGetChannelWithNesting(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.GetChannelRequest{
		Name: "organizations/KTCCC60hRgWDtGnLxb_I9Q/channels/AjBVqs9hTB2ph2EWyDoMuQ",
		ChildrenMask: &field_mask.FieldMask{
			Paths: []string{"channel_type"},
		},
	}
	res, err := s.GetChannel(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to get channel: %s", err)
	} else {
		expect := api.Channel{
			Name:          "organizations/KTCCC60hRgWDtGnLxb_I9Q/channels/AjBVqs9hTB2ph2EWyDoMuQ",
			DisplayName:   "E-mail: dest0@pantomath.io",
			ChannelType:   "channeltypes/email",
			Configuration: "{\"dest\": \"dest@pantomath.io\"}",
		}

		if res.Name != expect.Name ||
			res.ChannelType != expect.ChannelType ||
			len(res.Configuration) != len(expect.Configuration) ||
			res.Children == nil ||
			res.Children.ChannelType.Name != res.ChannelType {
			r, _ := json.Marshal(res)
			e, _ := json.Marshal(expect)
			t.Fatalf("expected %s, got %s", e, r)
		}
	}
}
