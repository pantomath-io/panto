// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/base64"
	"testing"
	"time"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/empty"
)

func TestLogin(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.LoginRequest{}
	res, err := s.Login(context.Background(), req)
	if err == nil {
		t.Fatalf("expected error for missing username")
	}

	req = &api.LoginRequest{
		Username: "hello@world.com",
	}
	res, err = s.Login(context.Background(), req)
	if err == nil {
		t.Fatalf("expected error for missing password")
	}

	req = &api.LoginRequest{
		Username: "barbes@rochechouart.net",
		Password: "helloworld",
	}
	res, err = s.Login(context.Background(), req)
	if err == nil {
		t.Fatalf("expected error for user not found")
	}

	req = &api.LoginRequest{
		Username: "barbes@rochechouart.net",
		Password: "helloworld",
	}
	res, err = s.Login(context.Background(), req)
	if err == nil {
		t.Fatalf("expected error for user not found")
	}

	req = &api.LoginRequest{
		Username: "hello@world.com",
		Password: "barbesrochechouart",
	}
	res, err = s.Login(context.Background(), req)
	if err == nil {
		t.Fatalf("expected error for invalid password")
	}

	req = &api.LoginRequest{
		Username: "hello@world.com",
		Password: "helloworld",
	}
	res, err = s.Login(context.Background(), req)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	token, err := base64.RawURLEncoding.DecodeString(res.Token)
	if err != nil {
		t.Fatalf("error decoding base64 token: %s", err)
	}
	if len(token) != 32 {
		t.Fatalf("expected 32-byte token, got %d bytes", len(token))
	}
	expires, err := ptypes.Timestamp(res.Expires)
	if err != nil {
		t.Fatalf("Error converting protobuf timestamp")
	}
	if expires.IsZero() {
		t.Fatalf("unexpected zero timestamp")
	}
	if expires.Location() != time.UTC {
		t.Fatalf("timestamp is not in UTC time zone")
	}
}

func TestGetToken(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &empty.Empty{}

	ctx := context.Background()
	_, err := s.GetToken(ctx, req)
	if err == nil {
		t.Fatalf("expected error for missing session key in context")
	}

	ctx = context.WithValue(context.Background(), sessionKey{}, "toto")
	_, err = s.GetToken(ctx, req)
	if err == nil {
		t.Fatalf("expected error for invalid session in context")
	}

	prevExpiration := session1.Expires

	ctx = context.WithValue(context.Background(), sessionKey{}, session1)
	token, err := s.GetToken(ctx, req)
	if err != nil {
		t.Fatalf("Unexpected error getting token: %e", err)
	}

	sessionMgr, _ := services.GetSessionManager()
	sess, _ := sessionMgr.GetSession(session1.UUID)

	expires, _ := ptypes.TimestampProto(sess.Expires)
	expect := &api.Token{
		Token:   base64.RawURLEncoding.EncodeToString(sess.Key),
		Expires: expires,
	}

	if !proto.Equal(token, expect) {
		t.Fatalf("Expected %#v, got %#v", expect, token)
	}
	if !prevExpiration.Before(sess.Expires) {
		t.Fatalf("Session was not refreshed")
	}
}
