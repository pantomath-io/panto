// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"bytes"
	"context"
	"encoding/base64"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/session"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Login returns a specific Alert
func (s *server) Login(ctx context.Context, req *api.LoginRequest) (res *api.Token, err error) {
	// Validate input
	if len(req.Username) == 0 {
		log.Errorf("missing Username")
		return nil, status.Error(codes.InvalidArgument, "Missing username")
	}
	if len(req.Password) == 0 {
		log.Errorf("missing Password")
		return nil, status.Error(codes.InvalidArgument, "Missing password")
	}

	// Get references to services
	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	sessionMgr, err := services.GetSessionManager()
	if err != nil {
		log.Errorf("Couldn't get session manager: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	dbUser, err := dbconf.GetUserByEmail(req.Username)
	if err != nil {
		log.Errorf("failed to get User: %s", err)
		return nil, status.Error(codes.Internal, "Error retrieving user")
	}
	if dbUser == nil {
		log.Errorf("username not found: %s", req.Username)
		return nil, status.Error(codes.Unauthenticated, "Username not found")
	}

	// Verify password
	hash, err := util.HashPassword(req.Password, dbUser.PasswordSalt)
	if err != nil {
		log.Errorf("unable to hash password: %s", err)
		return nil, status.Error(codes.Internal, "Error validating credentials")
	}
	if !bytes.Equal(hash[:], dbUser.PasswordHash[:]) {
		log.Errorf("user-provided password does not match")
		return nil, status.Error(codes.Unauthenticated, "Invalid password")
	}

	// Create user session
	session, err := sessionMgr.CreateSession(session.DefaultTimeout, dbUser.UUID, dbUser.OrganizationUUID)
	if err != nil {
		log.Errorf("failed to create user session: %s", err)
		return nil, status.Error(codes.Internal, "Session error")
	}

	// Set session cookie
	err = setSessionCookie(ctx, session)

	// Serialize data for response
	key := base64.RawURLEncoding.EncodeToString(session.Key)
	expires, err := ptypes.TimestampProto(session.Expires)
	if err != nil {
		log.Errorf("couldn't convert expiration timestamp")
	}
	token := &api.Token{
		Token:   key,
		Expires: expires,
	}
	return token, nil
}

func (s *server) GetToken(ctx context.Context, req *empty.Empty) (res *api.Token, err error) {
	sessionMgr, err := services.GetSessionManager()
	if err != nil {
		log.Errorf("Couldn't get session manager: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	// Get session from context
	se := ctx.Value(sessionKey{})
	if se == nil {
		log.Errorf("session not found in context")
		return nil, status.Error(codes.Unauthenticated, "Session not found")
	}
	sess, ok := se.(*session.Session)
	if !ok {
		log.Errorf("invalid session")
		return nil, status.Error(codes.Internal, "Internal error")
	}

	sess, err = sessionMgr.RefreshSession(sess.UUID, session.DefaultTimeout)
	if err != nil {
		log.Errorf("couldn't refresh session: %s", err)
	} else {
		setSessionCookie(ctx, sess)
	}

	// Serialize data for response
	key := base64.RawURLEncoding.EncodeToString(sess.Key)
	expires, err := ptypes.TimestampProto(sess.Expires)
	if err != nil {
		log.Errorf("couldn't convert expiration timestamp")
	}
	token := &api.Token{
		Token:   key,
		Expires: expires,
	}
	return token, nil
}
