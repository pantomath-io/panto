// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"testing"

	"github.com/golang/protobuf/proto"
	"google.golang.org/genproto/protobuf/field_mask"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
)

func TestListAgents(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListAgentsRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
	}
	res, err := s.ListAgents(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list agents: %s", err)
	} else {
		if len(res.Agents) != 10 {
			t.Fatalf("Unexpected agents count: got %d, expected %d", len(res.Agents), 10)
		}
		for i, a := range res.Agents {
			expect := fmt.Sprintf("agent%d.pantomath.io", i)
			if a.DisplayName != expect {
				t.Fatalf("Unexpected agent at index %d: got %v, expected %v", i, a.DisplayName, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestListAgentsPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test pagination
	req := &api.ListAgentsRequest{
		Parent:   "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		PageSize: 4,
	}
	res, err := s.ListAgents(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list agents: %s", err)
	} else {
		if len(res.Agents) != 4 {
			t.Fatalf("Unexpected agents count: got %d, expected %d", len(res.Agents), 4)
		}
		for i, a := range res.Agents {
			expect := fmt.Sprintf("agent%d.pantomath.io", i)
			if a.DisplayName != expect {
				t.Fatalf("Unexpected agent at index %d: got %v, expected %v", i, a.DisplayName, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 2
	page2Token := res.NextPageToken
	req.PageToken = res.NextPageToken
	res, err = s.ListAgents(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list agents: %s", err)
	} else {
		if len(res.Agents) != 4 {
			t.Fatalf("Unexpected agents count: got %d, expected %d", len(res.Agents), 4)
		}
		for i, a := range res.Agents {
			expect := fmt.Sprintf("agent%d.pantomath.io", i+4)
			if a.DisplayName != expect {
				t.Fatalf("Unexpected agent at index %d: got %v, expected %v", i, a.DisplayName, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 3
	req.PageToken = res.NextPageToken
	res, err = s.ListAgents(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list agents: %s", err)
	} else {
		if len(res.Agents) != 2 {
			t.Fatalf("Unexpected agents count: got %d, expected %d", len(res.Agents), 2)
		}
		for i, a := range res.Agents {
			expect := fmt.Sprintf("agent%d.pantomath.io", i+8)
			if a.DisplayName != expect {
				t.Fatalf("Unexpected agent at index %d: got %v, expected %v", i, a.DisplayName, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}

	// Invalid page token
	req.PageToken = page2Token[1:]
	_, err = s.ListAgents(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed agents with invalid token")
	}

	// Invalid request
	req.PageSize = 10
	req.PageToken = page2Token
	_, err = s.ListAgents(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed agents with invalid request")
	}
}

func TestGetAgent(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := api.GetAgentRequest{
		Name: "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
	}
	res, err := s.GetAgent(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to get agent: %s", err)
	} else {
		expect := api.Agent{
			Name:        "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
			DisplayName: "agent0.pantomath.io",
		}
		if !proto.Equal(res, &expect) {
			t.Fatalf("expected %v, got %v", expect, *res)
		}
	}
}

func TestUpdateAgent(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := api.UpdateAgentRequest{
		Agent: &api.Agent{
			Name:        "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
			DisplayName: "Agent Zero",
		},
	}
	res, err := s.UpdateAgent(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to update agent: %s", err)
	} else {
		expect := api.Agent{
			Name:        "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
			DisplayName: "Agent Zero",
		}
		if !proto.Equal(res, &expect) {
			t.Fatalf("expected %v, got %v", expect, *res)
		}
	}

	req = api.UpdateAgentRequest{
		Agent: &api.Agent{
			Name:        "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
			DisplayName: "Agent One",
		},
		UpdateMask: &field_mask.FieldMask{Paths: []string{"display_name"}},
	}
	res, err = s.UpdateAgent(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to update agent: %s", err)
	} else {
		expect := api.Agent{
			Name:        "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
			DisplayName: "Agent One",
		}
		if !proto.Equal(res, &expect) {
			t.Fatalf("expected %v, got %v", expect, *res)
		}
	}

	req = api.UpdateAgentRequest{
		Agent: &api.Agent{
			Name:        "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
			DisplayName: "Agent Zero",
		},
		UpdateMask: &field_mask.FieldMask{Paths: []string{}},
	}
	res, err = s.UpdateAgent(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to update agent: %s", err)
	} else {
		expect := api.Agent{
			Name:        "organizations/KTCCC60hRgWDtGnLxb_I9Q/agents/oKuOJ7EaTuSke81z3lYWDQ",
			DisplayName: "Agent One",
		}
		if !proto.Equal(res, &expect) {
			t.Fatalf("expected %v, got %v", expect, *res)
		}
	}
}

func TestDeleteAgent(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	var err error

	req1 := api.CreateAgentRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		Agent:  &api.Agent{DisplayName: "Temp Agent"},
	}
	a, err := s.CreateAgent(context.Background(), &req1)
	if err != nil {
		t.Fatalf("failed to create test agent: %s", err)
	}

	req2 := api.GetAgentRequest{
		Name: a.Name,
	}
	_, err = s.GetAgent(context.Background(), &req2)
	if err != nil {
		t.Fatalf("failed to retrieve test agent: %s", err)
	}

	req3 := api.DeleteAgentRequest{
		Name: a.Name,
	}
	_, err = s.DeleteAgent(context.Background(), &req3)
	if err != nil {
		t.Fatalf("Failed to delete agent: %s", err)
	}

	req4 := api.GetAgentRequest{
		Name: a.Name,
	}
	res, err := s.GetAgent(context.Background(), &req4)
	if err == nil {
		t.Fatalf("Successfully retrieved deleted agent: %v", *res)
	}
}
