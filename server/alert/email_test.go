// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

import (
	"io/ioutil"
	"strings"
	"testing"

	"github.com/google/uuid"
)

func TestEmailTemplate(t *testing.T) {
	emailInit()
	if emailTemplate == nil {
		t.Fatal("Couldn't initialize email alert template")
	}

	vars := TemplateVars{
		Title:    "CRITICAL alert",
		Target:   uuid.New().String(),
		Agent:    uuid.New().String(),
		Probe:    uuid.New().String(),
		State:    "CRITICAL",
		Reason:   "Test alert",
		Message:  "Test alert is critical",
		Critical: true,
		Warning:  false,
	}
	var message strings.Builder
	err := emailTemplate.Execute(&message, vars)
	if err != nil {
		t.Fatalf("Couldn't execute email alert template: %s", err)
	}
	ioutil.WriteFile("/tmp/panto-email-alert.html", []byte(message.String()), 0666)
}
