// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

import (
	"fmt"

	"github.com/op/go-logging"
)

var log = logging.MustGetLogger("main")

// alertType enum
const (
	AlertEmail     = "email"
	AlertSlack     = "slack"
	AlertHTTP      = "http"
	AlertPagerDuty = "pagerduty"
	AlertTwilio    = "twilio"
)

// Alert is a description of an alert to dispatch. It contains the details about the channel to dispatch the alert on
// and the variables to inject into the message template.
type Alert struct {
	ChannelType   string
	ChannelConfig map[string]interface{}

	Vars TemplateVars
}

// TemplateVars is the set of variables to inject into the message template for a channel type.
type TemplateVars struct {
	Title       string `json:"title"`
	Message     string `json:"message"`
	Target      string `json:"target"`
	Agent       string `json:"agent"`
	Probe       string `json:"probe"`
	State       string `json:"state"`
	Reason      string `json:"reason"`
	OK          bool   `json:"ok"`
	Critical    bool   `json:"critical"`
	Warning     bool   `json:"warning"`
	MissingData bool   `json:"missing_data"`
	Error       bool   `json:"error"`
}

// DispatchAlert actually sends the Alert based on configuration and business rules
func DispatchAlert(a Alert) error {
	log.Debugf("I'm actually sending an alert")
	switch a.ChannelType {
	case AlertEmail:
		return SendEmail(a.ChannelConfig, a.Vars)
	case AlertSlack:
		return SendSlack(a.ChannelConfig, a.Vars)
	case AlertHTTP:
		return SendHTTP(a.ChannelConfig, a.Vars)
	case AlertPagerDuty:
		return SendPagerDuty(a.ChannelConfig, a.Vars)
	case AlertTwilio:
		return SendTwilio(a.ChannelConfig, a.Vars)
	default:
		return fmt.Errorf("unknown alert type (%s)", a.ChannelType)
	}
}
