// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"text/template"
)

// pagerDutyTemplateVars to inject PagerDuty-specific vars into the template
type pagerDutyTemplateVars struct {
	TemplateVars
	Service string
}

var pagerDutyTemplate *template.Template
var pagerDutyOnce sync.Once

func pagerDutyInit() {
	var err error
	pagerDutyTemplate = template.New("pagerDuty")
	pagerDutyTemplate, err = pagerDutyTemplate.Parse(pagerDutyTemplateString)
	if err != nil {
		log.Critical("Unable to parse PagerDuty template, no PagerDuty alert will be sent")
		pagerDutyTemplate = nil
	}
}

// SendPagerDuty post a message on a PagerDuty webhook
func SendPagerDuty(config map[string]interface{}, vars TemplateVars) error {
	pagerDutyOnce.Do(pagerDutyInit)

	if pagerDutyTemplate == nil {
		log.Critical("Unable to parse PagerDuty template, no PagerDuty alert will be sent")
		return fmt.Errorf("no PagerDuty template")
	}

	from, ok := config["from"]
	if !ok {
		return fmt.Errorf("unable to get \"From\" address from PagerDuty config")
	}
	fromStr, ok := from.(string)
	if !ok {
		return fmt.Errorf("from is not a string")
	}

	token, ok := config["token"]
	if !ok {
		return fmt.Errorf("unable to get token from PagerDuty config")
	}
	tokenStr, ok := token.(string)
	if !ok {
		return fmt.Errorf("token is not a string")
	}

	service, ok := config["service"]
	if !ok {
		return fmt.Errorf("unable to get service from PagerDuty config")
	}
	serviceStr, ok := service.(string)
	if !ok {
		return fmt.Errorf("service is not a string")
	}

	pdVars := pagerDutyTemplateVars{vars, serviceStr}

	var message bytes.Buffer
	err := pagerDutyTemplate.Execute(&message, pdVars)
	if err != nil {
		return fmt.Errorf("unable to execute template: %s", err)
	}

	req, err := http.NewRequest("POST", "https://api.pagerduty.com/incidents", &message)
	if err != nil {
		return fmt.Errorf("unable to prepare POST request: %s", err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/vnd.pagerduty+json;version=2")
	req.Header.Set("From", fromStr)
	req.Header.Set("Authorization", "Token token="+tokenStr)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("unable to send message to PagerDuty: %s", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			log.Debugf("PagerDuty alert returned: %s", body)
		}
		return fmt.Errorf("error while POSTing PagerDuty alert: %s", resp.Status)
	}

	return nil
}
