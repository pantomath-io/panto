// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alert

const pagerDutyTemplateString = `{
	"incident": {
		"type": "incident",
		"title": "{{js .Title}}",
		"body": {
			"type": "incident_body",
			"details": "{{js .State}} alert on\nTarget: {{js .Target}}\nProbe: {{js .Probe}}\nAgent: {{js .Agent}}\nState is {{js .Reason}}"
		},
		"service":{
			"type": "service_reference",
			"id":   "{{js .Service}}"
		}
	}
}`
