// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datapipe

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/pantomath-io/panto/server/tsdb"
)

// Threshold is the States definition for a threshold check
type Threshold struct {
	States []struct {
		State      string `json:"state"`
		Match      match  `json:"match"`
		Conditions []struct {
			Field    string      `json:"field"`
			Operator operator    `json:"operator"`
			Value    interface{} `json:"value"`
			Reason   string      `json:"reason"`
		} `json:"conditions"`
		Tags []struct {
			Name     string   `json:"name"`
			Operator operator `json:"operator"`
			Value    string   `json:"value"`
		} `json:"tags"`
	} `json:"states"`
}

// checkResultThreshold test the probeResult alert status (ok, warn, crit)
func checkResultThreshold(ref []byte, result tsdb.Result, _ tsdb.TSDB) (*State, error) {
	var thresholdDefinition Threshold

	// Check input
	if len(ref) == 0 {
		return nil, fmt.Errorf("empty reference")
	}
	if len(result.Fields) == 0 {
		return nil, fmt.Errorf("empty fields")
	}

	dec := json.NewDecoder(bytes.NewBuffer(ref))
	dec.UseNumber()
	err := dec.Decode(&thresholdDefinition)
	if err != nil {
		return nil, fmt.Errorf("unable to parse JSON: %s", err)
	}

	globalResult := make(map[string]*conditionResult)

States:
	for _, state := range thresholdDefinition.States {
		if len(state.Conditions) == 0 {
			log.Debug("state has no conditions, skipping...")
			continue States
		}
		for _, tag := range state.Tags {
			v, ok := result.Tags[tag.Name]
			if !ok {
				continue States
			}
			ok, err = compareString(v, tag.Value, tag.Operator)
			if err != nil {
				log.Errorf("unable to evaluate tag condition: %s", err)
				continue States
			}
			if !ok {
				continue States
			}
		}
		condResult := make([]bool, len(state.Conditions))
		reasons := make([]string, 0, len(state.Conditions))
		for i, condition := range state.Conditions {
			currentField, okField := result.Fields[condition.Field]
			if !okField {
				return nil, fmt.Errorf("unable to find the condition field in the probeResult")
			}
			currentValue := condition.Value
			// HACK: Force value to adopt int/float type from field
			if val, ok := currentValue.(json.Number); ok {
				switch currentField.(type) {
				case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
					if valInt, err := val.Int64(); err == nil {
						currentValue = valInt
					}
				case float32, float64:
					if valFloat, err := val.Float64(); err == nil {
						currentValue = valFloat
					}
				}
			}
			condResult[i], err = compareValues(currentField, currentValue, condition.Operator)
			if err != nil {
				return nil, err
			}
			if condResult[i] {
				reasons = append(reasons, condition.Reason)
			}
		}

		// Add the result array to the global map
		stateResult := &conditionResult{}
		switch state.Match {
		case MatchAny:
			stateResult.Result = any(condResult)
		case MatchAll:
			stateResult.Result = all(condResult)
		default:
			return nil, fmt.Errorf("unknown match type: %s", state.Match)
		}
		stateResult.Reason = strings.Join(reasons, "\n")
		globalResult[state.State] = stateResult
	}

	return combineStates(globalResult)
}
