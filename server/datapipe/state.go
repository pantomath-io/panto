// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datapipe

import (
	"encoding/json"
	"fmt"
	"reflect"
	"regexp"
	"time"

	"gitlab.com/pantomath-io/panto/api"
)

// State represents the state of a tsdb.Result when tested
type State struct {
	Value  StateValue
	Reason string
}

type operator string

// Comparison operators
const (
	OpLowerThan    operator = "lt"
	OpLowerEqual   operator = "le"
	OpEqual        operator = "eq"
	OpNotEqual     operator = "ne"
	OpGreaterEqual operator = "ge"
	OpGreaterThan  operator = "gt"
	OpMatch        operator = "match"
	OpNotMatch     operator = "notmatch"
)

type match string

// Match operator for conditions
const (
	MatchAny match = "any"
	MatchAll match = "all"
)

// StateValue encodes a value evaluated for a state
type StateValue int

const (
	// StateCriticalLabel is the label of the field in the JSON definition of the "critical" state
	StateCriticalLabel string = "critical"
	// StateWarningLabel is the label of the field in the JSON definition of the "warning" state
	StateWarningLabel string = "warning"
)

const (
	// StateValueUnknown encodes a state that was impossible to evaluate
	StateValueUnknown = StateValue(api.State_UNKNOWN)
	// StateValueOK encodes an OK state
	StateValueOK = StateValue(api.State_OK)
	// StateValueWarning encodes a warning state
	StateValueWarning = StateValue(api.State_WARNING)
	// StateValueCritical encodes a critical state
	StateValueCritical = StateValue(api.State_CRITICAL)
	// StateValueMissingData encodes the state of a Probe Configuration
	// that hasn't received data in "a while"
	StateValueMissingData = StateValue(api.State_MISSING_DATA)
	// StateValueError encodes the state of a Probe Configuration
	// that encountered an error while collecting metrics
	StateValueError = StateValue(api.State_ERROR)
)

func (sv StateValue) String() string {
	switch sv {
	case StateValueUnknown:
		return "UNKNOWN"
	case StateValueOK:
		return "OK"
	case StateValueWarning:
		return "WARNING"
	case StateValueCritical:
		return "CRITICAL"
	case StateValueMissingData:
		return "MISSING_DATA"
	case StateValueError:
		return "ERROR"
	default:
		panic(fmt.Sprintf("%d is not a valid StateValue", int(sv)))
	}
}

// StateType
const (
	StateTypeThreshold string = "threshold"
	StateTypeHistory   string = "history"
	StateTypeTrend     string = "trend"
	StateTypeNone      string = "none"
)

// conditionResult represent the result of a single condition evaluation
type conditionResult struct {
	Result bool
	Reason string
}

//////////////////////////////////////////////////////////////////////////////
// Helper functions

// any is the equivalent to the any function from Python
func any(a []bool) bool {
	for _, b := range a {
		if b {
			return true
		}
	}
	return false
}

// all is the equivalent to the all function from Python
func all(a []bool) bool {
	for _, b := range a {
		if !b {
			return false
		}
	}
	return true
}

// combineStates calculates the global state from an array of condition states
func combineStates(listResults map[string]*conditionResult) (*State, error) {
	result := &State{
		Value:  StateValue(api.State_UNKNOWN),
		Reason: "Not evaluated",
	}

	// select the status per state
	for state, status := range listResults {
		if state == StateCriticalLabel && status.Result {
			result.Value = StateValue(api.State_CRITICAL)
			result.Reason = status.Reason
		}
		if state == StateWarningLabel && status.Result && result.Value != StateValue(api.State_CRITICAL) {
			result.Value = StateValue(api.State_WARNING)
			result.Reason = status.Reason
		}
	}

	// if nothing bad had happen, then we are good
	if result.Value == StateValue(api.State_UNKNOWN) {
		result.Value = StateValue(api.State_OK)
		result.Reason = ""
	}

	return result, nil
}

// compareValues compares the two values using the comparison operator and based
// on the value type
func compareValues(reference, value interface{}, op operator) (result bool, err error) {
	log.Debugf("comparing: (%T)%#v %s (%T)%#v", reference, reference, op, value, value)
	if reference == nil {
		return false, fmt.Errorf("nil reference value")
	}
	if value == nil {
		return false, fmt.Errorf("can't compare nil value")
	}
	rType := reflect.TypeOf(reference)
	vType := reflect.TypeOf(value)
	if rType != vType {
		if !rType.ConvertibleTo(vType) {
			return false, fmt.Errorf("type mismatch: value is %T, reference is %T", reference, value)
		}
		reference = reflect.ValueOf(reference).Convert(vType).Interface()
	}

	switch value := value.(type) {
	case int64:
		result, err = compareInt(reference.(int64), value, op)
	case float64:
		result, err = compareFloat(reference.(float64), value, op)
	case string:
		result, err = compareString(reference.(string), value, op)
	case bool:
		result, err = compareBool(reference.(bool), value, op)
	default:
		return false, fmt.Errorf("unable to determine value type for comparison")
	}

	return
}

// compareInt compares two int64 numbers using the comparison operator
func compareInt(lhs, rhs int64, op operator) (bool, error) {
	switch op {
	case OpLowerThan:
		return (lhs < rhs), nil
	case OpLowerEqual:
		return (lhs <= rhs), nil
	case OpEqual:
		return (lhs == rhs), nil
	case OpNotEqual:
		return (lhs != rhs), nil
	case OpGreaterEqual:
		return (lhs >= rhs), nil
	case OpGreaterThan:
		return (lhs > rhs), nil
	default:
		return false, fmt.Errorf("wrong operator %s", op)
	}
}

// compareFloat compares two float64 numbers using the comparison operator
func compareFloat(lhs, rhs float64, op operator) (bool, error) {
	switch op {
	case OpLowerThan:
		return (lhs < rhs), nil
	case OpLowerEqual:
		return (lhs <= rhs), nil
	case OpEqual:
		return (lhs == rhs), nil
	case OpNotEqual:
		return (lhs != rhs), nil
	case OpGreaterEqual:
		return (lhs >= rhs), nil
	case OpGreaterThan:
		return (lhs > rhs), nil
	default:
		return false, fmt.Errorf("wrong operator %s", op)
	}
}

// compareString compares two strings using the comparison operator
func compareString(lhs, rhs string, op operator) (bool, error) {
	switch op {
	case OpEqual:
		return (lhs == rhs), nil
	case OpNotEqual:
		return (lhs != rhs), nil
	case OpMatch:
		return regexp.Match(rhs, []byte(lhs))
	case OpNotMatch:
		result, err := regexp.Match(rhs, []byte(lhs))
		return !result, err
	default:
		return false, fmt.Errorf("wrong operator %s", op)
	}
}

// compareBool compares two bools using the comparison operator
func compareBool(lhs, rhs bool, op operator) (bool, error) {
	switch op {
	case OpEqual:
		return (rhs == lhs), nil
	case OpNotEqual:
		return (rhs != lhs), nil
	default:
		return false, fmt.Errorf("wrong operator %s", op)
	}
}

// ValidateStateConfigurationFormat tries to unmarshal the configuration provided in the right format
func ValidateStateConfigurationFormat(stateType, stateConfiguration string) (err error) {
	switch stateType {
	case StateTypeNone:
		var noneDefinition map[string]interface{}
		err = json.Unmarshal([]byte(stateConfiguration), &noneDefinition)
		if err != nil {
			err = fmt.Errorf("unable to unmarshal state configuration into configuration template")
		}
		if len(noneDefinition) > 0 {
			err = fmt.Errorf("state configuration for type \"none\" must be empty")
		}
	case StateTypeThreshold:
		var thresholdDefinition Threshold
		err = json.Unmarshal([]byte(stateConfiguration), &thresholdDefinition)
	case StateTypeHistory:
		var historyDefinition History
		err = json.Unmarshal([]byte(stateConfiguration), &historyDefinition)
		if err != nil {
			_, err = time.ParseDuration(historyDefinition.Interval)
		}
	case StateTypeTrend:
		var trendDefinition Trend
		err = json.Unmarshal([]byte(stateConfiguration), &trendDefinition)
		if err != nil {
			_, err = time.ParseDuration(trendDefinition.Interval)
		}
	default:
		return fmt.Errorf("unknown state definition (%s)", stateType)
	}

	return err
}
