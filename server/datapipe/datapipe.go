// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package datapipe

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	"github.com/op/go-logging"
	"gitlab.com/pantomath-io/wrapany"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var log = logging.MustGetLogger("main")

const dataCheckSchedule = 2 * time.Minute

// DataPipe is an interface that represents a Results-processing DataPipe as a service.
type DataPipe interface {
	ProcessResults(ctx context.Context, results []*api.Result) error
}

// dataPipe is the main concrete implementation of DataPipe.
type dataPipe struct {
	activityLock sync.RWMutex // ensures that no ProcessResults is run concurrently to a data presence sweep
	ticker       *time.Ticker // the periodic data check ticker
}

// New returns an initialized DataPipe, ready to be used.
func New() DataPipe {
	p := new(dataPipe)
	p.ticker = time.NewTicker(dataCheckSchedule)
	go func() {
		p.doCheckDataPresence()
		for range p.ticker.C {
			p.doCheckDataPresence()
		}
	}()
	return p
}

// ProcessResults takes the results from an Agent's execution of a Probe, and starts processing in the server's context.
// The processing is started concurrently to other results, the function returns if the results were added to the TSDB,
// or if an error was encountered before. The processing continues after ProcessResults returns, allowing for an early
// return from an API call.
func (p *dataPipe) ProcessResults(ctx context.Context, results []*api.Result) error {
	defer func(numResults int, startTime time.Time) {
		log.Infof("entered %d results into TSDB in %s", numResults, time.Since(startTime))
	}(len(results), time.Now())

	done := make(chan error)
	go p.doProcessResults(done, results)

	select {
	case <-ctx.Done():
		return ctx.Err()
	case err := <-done:
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *dataPipe) doProcessResults(response chan<- error, results []*api.Result) {
	// Lock for "reading" - processing a set of results
	p.activityLock.RLock()
	defer p.activityLock.RUnlock()

	ts, err := services.GetTSDB()
	if err != nil {
		response <- err
		close(response)
		return
	}
	db, err := services.GetDBConf()
	if err != nil {
		response <- err
		close(response)
		return
	}

	tsdbResults, err := addToTSDB(results, ts, db)
	if err == nil {
		close(response)
	} else {
		response <- err
		close(response)
	}

	// Iterate over added results, evaluate the resulting states and trigger an alert if needed
	for _, result := range tsdbResults {
		ps, err := getState(result, ts, db)
		if err != nil {
			log.Errorf("error calculating state: %s", err)
			return
		}
		log.Debugf("State is %s (%s)", api.State_Type_name[int32(ps.Value)], ps.Reason)

		sendAlertIfNeeded(db, ts, result, ps)

		// Add state at the end, so that it doesn't interfere in Alert calculation
		err = ts.AddState(tsdb.State{
			Entry:  result.Entry,
			State:  int(ps.Value),
			Reason: ps.Reason,
		})
		if err != nil {
			log.Errorf("error storing the State: %s", err)
			return
		}
	}
}

func (p *dataPipe) doCheckDataPresence() {
	// Lock for "writing" - sweeping ProbeConfigurations for missing data
	p.activityLock.Lock()
	defer p.activityLock.Unlock()

	ts, err := services.GetTSDB()
	if err != nil {
		log.Errorf("couldn't check data presence: %s", err)
		return
	}
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("couldn't check data presence: %s", err)
		return
	}

	now := time.Now()
	defer func(startTime time.Time) {
		log.Infof("Checked data presence for all Probe Configurations in %s", time.Since(startTime))
	}(now)

	// Get all probe configurations
	tpas, err := db.ListTargetProbeAgentsAll(0, 0)
	if err != nil {
		log.Errorf("couldn't check data presence: couldn't list all target_probe_agent: %s", err)
		return
	}

	// Cache checks and agent-centric activity to reduce SQL and TSDB requests
	tps := make(map[uuid.UUID]*dbconf.TargetProbe)
	activity := make(map[uuid.UUID]time.Time)

TargetProbeAgents:
	for _, tpa := range tpas {
		// Get latest agent activity (from map, or from TSDB)
		t, ok := activity[tpa.AgentUUID]
		if !ok {
			a, err := ts.GetActivity(
				tpa.OrganizationUUID,
				tpa.AgentUUID,
				tsdb.Count(1),
				tsdb.OrderBy(tsdb.OrderDescending),
			)
			if err != nil {
				log.Errorf("couldn't get latest activity for agent %s", tpa.AgentUUID)
			}
			if len(a) > 0 {
				t = a[0].Timestamp
			} else {
				// If we have no activity, consider the agent isn't live:
				// last activity was time zero value (aka The Distant Past™)
				t = time.Time{}
			}
			activity[tpa.AgentUUID] = t
		}
		log.Debugf("latest activity for agent %s: %s", tpa.AgentUUID, t)

		var tsConfig time.Time
		if (tpa.TsUpdated == time.Time{}) {
			// configuration was created but never updated, use creation date
			tsConfig = tpa.TsCreated
		} else {
			tsConfig = tpa.TsUpdated
		}
		if t.After(tsConfig) {
			// Agent has had activity since the probe configuration was updated
			log.Debugf("agent activity later than configuration timestamp: %s", tsConfig)

			// Get latest result for ProbeConfiguration (from map, or from TSDB)
			var l time.Time
			latest, err := ts.GetResults(
				tpa.TargetProbeUUID,
				tsdb.QueryFields{},
				tsdb.QueryTags{},
				tsdb.Tag("probe_configuration", util.Base64Encode(tpa.UUID)),
				tsdb.Count(1),
				tsdb.OrderBy(tsdb.OrderDescending),
			)
			if err != nil {
				log.Errorf("couldn't get latest result for target_probe_agent %s", tpa.UUID)
			}
			if len(latest) > 0 {
				l = latest[0].Timestamp
			} else {
				// If we have no results, consider
				// last results was time zero value (aka The Distant Past™)
				l = time.Time{}
			}
			log.Debugf("most recent result for probe configuration %s: %s", tpa.UUID, l)

			if now.After(l.Add(2 * tpa.Schedule)) {
				// If the latest result is more than twice the schedule ago
				log.Debugf("result late by more than twice the schedule, expected at %s", l.Add(2*tpa.Schedule))

				// Get the latest state (from map, or from TSDB)
				var s tsdb.State
				st, err := ts.GetStates(
					[]uuid.UUID{tpa.TargetProbeUUID},
					tsdb.Tag("probe_configuration", util.Base64Encode(tpa.UUID)),
					tsdb.Count(1),
					tsdb.OrderBy(tsdb.OrderDescending),
				)
				if err != nil {
					log.Errorf("couldn't get latest state for target_probe_agent %s", tpa.UUID)
				}
				if len(st) > 0 {
					s = st[0]
				} else {
					// If we have no state, consider
					// last state was UNKNOWN at time zero value (aka The Distant Past™)
					s = tsdb.State{}
				}
				log.Debugf("most recent state for probe configuration %s: {timestamp: %s, state: %s}",
					tpa.UUID, s.Timestamp, StateValue(s.State),
				)

				// Get Check (from map, or from DBConf)
				tp, ok := tps[tpa.TargetProbeUUID]
				if !ok {
					tp, err = db.GetTargetProbe(tpa.OrganizationUUID, tpa.TargetProbeUUID)
					if err != nil {
						log.Errorf("couldn't retrieve target_probe: %s", err)
						continue TargetProbeAgents
					}
					if tp == nil {
						log.Errorf("no target_probe found")
						continue TargetProbeAgents
					}
					tps[tpa.TargetProbeUUID] = tp
				}

				var reason string
				if s.State == int(StateValueMissingData) {
					// Previous state was already missing value, wait for 1 schedule
					// to pass before setting a new "missing value" state
					if now.Before(s.Timestamp.Add(tpa.Schedule)) {
						continue TargetProbeAgents
					}
					// Recycle reason to reflect original issue:
					// the reason is of the form "no data since ...", the timestamp
					// should not be updated
					reason = s.Reason
				} else {
					reason = "No results received"
					if l != (time.Time{}) {
						// If there were some actual results at some time, mention in the reason
						reason += fmt.Sprintf(" since %s", l)
					}
				}

				e := tsdb.Entry{
					Timestamp:          now,
					Organization:       tpa.OrganizationUUID,
					ProbeConfiguration: tpa.UUID,
					Probe:              tp.ProbeUUID,
					Agent:              tpa.AgentUUID,
					Check:              tpa.TargetProbeUUID,
					Target:             tp.TargetUUID,
				}

				sendAlertIfNeeded(
					db,
					ts,
					tsdb.Result{
						Entry: e,
					},
					&State{
						Value:  StateValueMissingData,
						Reason: reason,
					},
				)

				newState := tsdb.State{
					Entry:  e,
					State:  int(StateValueMissingData),
					Reason: reason,
				}
				log.Debugf("Adding %s state at %s for probe configuration %s: \"%s\"",
					StateValue(newState.State), newState.Timestamp, newState.ProbeConfiguration, newState.Reason,
				)
				err = ts.AddState(newState)
				if err != nil {
					log.Errorf("error storing the State: %s", err)
				}
			}
		}
	}
}

func addToTSDB(results []*api.Result, ts tsdb.TSDB, db dbconf.ConfigurationDB) (tsdbResults []tsdb.Result, err error) {

	// List of results in tsdb format
	tsdbResults = make([]tsdb.Result, len(results))

	for i, result := range results {
		// Extract UUIDs
		organizationUUID, agentUUID, err := api.ParseNameAgent(result.GetAgent())
		if err != nil {
			log.Errorf("unable to extract agent UUID (%s)", err)
			return nil, status.Error(codes.InvalidArgument, "invalid agent name")
		}

		checkOrganizationUUID, checkUUID, err := api.ParseNameCheck(result.GetCheck())
		if err != nil {
			log.Errorf("unable to extract check UUID (%s)", err)
			return nil, status.Error(codes.InvalidArgument, "invalid check name")
		}

		if organizationUUID != checkOrganizationUUID {
			log.Errorf("agent's organization (%s) does not match check's organization (%s)", organizationUUID, checkOrganizationUUID)
			return nil, status.Error(codes.InvalidArgument, "agent and check organization do not match")
		}

		pcOrganizationUUID, pcAgentUUID, pcUUID, err := api.ParseNameProbeConfiguration(result.ProbeConfiguration)
		if err != nil {
			log.Errorf("unable to extract probeconfiguration UUID (%s)", err)
			return nil, status.Error(codes.InvalidArgument, "invalid probe_configuration name")
		}

		if organizationUUID != pcOrganizationUUID {
			log.Errorf("probe_configuration's organization (%s) does not match agent's organization (%s)", organizationUUID, pcOrganizationUUID)
			return nil, status.Error(codes.InvalidArgument, "agent and probe_configuration organization do not match")
		}
		if agentUUID != pcAgentUUID {
			log.Errorf("probe_configuration's agent (%s) does not match agent (%s)", pcAgentUUID, agentUUID)
			return nil, status.Error(codes.InvalidArgument, "agent and probe_configuration's agent do not match")
		}

		log.Debugf(
			"Received result for probe configuration %s and check %s from agent %s",
			pcUUID.String(),
			checkUUID.String(),
			agentUUID.String(),
		)

		ts, err := ptypes.Timestamp(result.Timestamp)
		if err != nil {
			log.Errorf("invalid timestamp (%s)", err)
			return nil, status.Error(codes.InvalidArgument, "invalid timestamp")
		}
		// If there is no timestamp, default to now.
		if ts.IsZero() {
			ts = time.Now()
			result.Timestamp, _ = ptypes.TimestampProto(ts)
		}

		check, err := db.GetTargetProbe(organizationUUID, checkUUID)
		if err != nil {
			log.Errorf("unable to get the check %s (%s)", checkUUID, err)
			return nil, status.Error(codes.NotFound, "Check not found.")
		}

		tsdbResults[i].ProbeConfiguration = pcUUID
		tsdbResults[i].Check = checkUUID
		tsdbResults[i].Organization = organizationUUID
		tsdbResults[i].Probe = check.ProbeUUID
		tsdbResults[i].Target = check.TargetUUID
		tsdbResults[i].Agent = agentUUID
		tsdbResults[i].Timestamp = ts

		if !result.Error {
			fields := make(map[string]interface{})
			// Transform type Any to interface{}
			for k, v := range result.Fields {
				fields[k], err = wrapany.Unwrap(v)
				if err != nil {
					log.Errorf("couldn't unwrap field value for %s (%s)", k, err)
					return nil, status.Errorf(codes.InvalidArgument, "Invalid field value for %s", k)
				}
			}
			tsdbResults[i].Fields = fields
			tsdbResults[i].Tags = result.Tags
		} else {
			tsdbResults[i].Error = true
			tsdbResults[i].ErrorMessage = result.ErrorMessage
		}
	}

	err = ts.AddResults(tsdbResults)
	if err != nil {
		log.Errorf("Failed to insert report in TSDB (%s)", err)
		return nil, status.Error(codes.Internal, "Failed to store report.")
	}

	return tsdbResults, nil
}

// getState gets the configuration for a probe and launches the proper
// comparison function.
func getState(r tsdb.Result, ts tsdb.TSDB, db dbconf.ConfigurationDB) (*State, error) {
	if r.Error {
		return &State{
			Value:  StateValueError,
			Reason: r.ErrorMessage,
		}, nil
	}

	state := &State{}

	// Check input
	if r.Fields == nil {
		return nil, fmt.Errorf("missing fields map in result")
	}

	// Get the state configuration
	s, err := db.GetTargetProbe(r.Organization, r.Check)
	if err != nil {
		return nil, err
	}

	config, err := json.Marshal(s.StateConfiguration)
	if err != nil {
		return nil, err
	}

	// Call the right function based on state definition
	switch s.StateType {
	case StateTypeThreshold:
		state, err = checkResultThreshold(config, r, ts)
	case StateTypeHistory:
		state, err = checkResultHistory(config, r, ts)
	case StateTypeTrend:
		state, err = checkResultTrend(config, r, ts)
	case StateTypeNone:
		// case with no state definition
		return state, nil
	default:
		return nil, fmt.Errorf("unknown state type (%s)", s.StateType)
	}
	if err != nil {
		return nil, err
	}

	return state, nil
}

func sendAlertIfNeeded(db dbconf.ConfigurationDB, ts tsdb.TSDB, result tsdb.Result, ps *State) {
	dbAlerts, err := db.ListAlertsByTargetProbe(result.Organization, result.Check, 0, 0)
	if err != nil {
		log.Errorf("error retrieving alerts from DB: %s", err)
		return
	}
	for _, dbAlert := range dbAlerts {

		a, err := GetAlert(result, ps, dbAlert, ts)
		if err != nil {
			log.Errorf("error estimating Alert: %s", err)
			continue
		}
		log.Debugf("AlertState is %t (%s)", a.State, a.Reason)

		if a.State {
			sent, err := a.SendAlert(result, ps, db, ts)
			if err != nil {
				log.Errorf("error sending Alert: %s", err)
				continue
			}
			if sent {
				log.Debugf("Alert sent, storing that")
				err = ts.AddAlertEvent(tsdb.AlertEvent{
					Entry:       result.Entry,
					Event:       tsdb.EventValueNotified,
					Alert:       dbAlert.UUID,
					AlertReason: a.Reason,
					StateReason: ps.Reason,
				})
				if err != nil {
					log.Errorf("error storing the Alert: %s", err)
					continue
				}
			}
		}
	}
}
