// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package datapipe handles the processing of results from the agents' probes.
//
// Threshold
//
// The Threshold states definition compares a field from a probe result against a fixed value to evaluate states.
//
// States definition format (JSON):
//
//	{
//		"states": [{
//			"state": "critical|warning",
//			"match": "all|any",
//			"conditions": [{
//				"field": "field_name",
//				"operator": "lt|le|eq|ne|ge|gt|match|notmatch",
//				"value": X,
//				"reason": "reason of failure"
//			}]
//		}]
//	}
//
// where:
//
//	* state: state of the probe. Default values: Critical, Warning
//	* match: logical operator on the tests performed (all <=> AND / any <=> OR)
//	* conditions: test(s) to perform to reach the state
//	* field: probe' field to apply the test to
//	* operator: type of operation to perform:
//	    - lt / le
//	    - eq / ne
//	    - ge / gt
//	    - match / notmatch
//	* value: reference value for the test.
//	* reason: explanation to provide the user with in case of failure.
//
// History
//
// The History states definition compares a field from a probe result against a past result to evaluate states.
//
// States definition format (JSON):
//
//	{
//	    "interval": I
//	    "states": [{
//	        "state": "critical|warning",
//	        "match": "all|any",
//	        "conditions": [{
//	            "field": "field_name",
//	            "operator": "lt|le|eq|ne|ge|gt|match|notmatch",
//	            "tolerance": X,
//	            "reason": "reason of failure"
//	        }]
//	    }]
//	}
//
// where:
//
//	* interval: is how far back in history the newest result should be compared. Syntax is similar to
//              `time.ParseDuration`
//	* state: state of the probe. Default values: Critical, Warning
//	* match: logical operator on the tests performed (all <=> AND / any <=> OR)
//	* conditions: test(s) to perform to reach the state
//	* field: probe' field to apply the test to
//	* operator: type of operation to perform:
//	    - lt / le
//	    - eq / ne
//	    - ge / gt
//	    - match / notmatch
//	* tolerance: a multiplying factor of the historical value to compare against. Ignored for non-numeric
//	             values. E.g. "operator":lt, "tolerance": 0.1 means that the state will be set if the latest
//	             value is "lower than 90% of the historical value" <=> (r < v - v * 0.1) (see below)
//	* reason: explanation to provide the user with in case of failure.
//
// Tolerance for numeric comparisons can sometimes be confusing, this cheat sheet can be useful.
//
//	      v-v*tol      v      v+v*tol
//	<----------|*******^*******|---------->
//	[__________[                            "lt"
//	[__________________________]            "le"
//	[          [_______________]          ] "eq"
//	[__________[               ]__________] "ne"
//	[          [__________________________] "ge"
//	                           ]__________] "gt"
//
//	* Lower than:            r < v-v*tol
//	* Lower than or equal:   r <= v+v*tol
//	* Equal:                 r >= v-v*tol && r <= v+v*tol
//	* Not equal:             r < v-v*tol || r > v+v*tol
//	* Greater than:          r > v+v*tol
//	* Greater than or equal: r >= v-v*tol
//
// Trend
//
// The Trend states definition compares the rate of change of a field from a probe result against a fixed value to
// evaluate states. The rate of change, or "trend", can be expressed in natural language as: "The value has changed by
// X over the past I".
//
// Example: assuming the CPU usage has been probed 5 minutes ago, returning a value of 5%. The probe runs again now
// and returns a value of 9%. That's "a +4% change over the past 5 minutes". This is a way to express the trend, or rate
// of change. The trend state definition requires an "interval" parameter, that allows expressing the rate of change
// over an arbitrary time duration. In our previous example, if the interval is set to "1m", the trend will be: "a +0.8%
// change over the past minute". The value used in the state evaluation will be 0.8.
//
// States definition format (JSON):
//
//	{
//		"interval": I,
//		"states": [{
//			"state": "critical|warning",
//			"match": "all|any",
//			"conditions": [{
//				"field": "field_name",
//				"operator": "lt|le|eq|ne|ge|gt|match|notmatch",
//				"difference": X,
//				"reason": "reason of failure"
//			}]
//		}]
//	}
//
// where:
//
//	* interval: the interval to compute the trend over. This is the "over 5 minutes" part of the trend.
//	* state: state of the probe. Default values: Critical, Warning
//	* match: logical operator on the tests performed (all <=> AND / any <=> OR)
//	* conditions: test(s) to perform to reach the state
//	* field: probe' field to apply the test to
//	* operator: type of operation to perform:
//	    - lt / le
//	    - eq / ne
//	    - ge / gt
//	    - match / notmatch
//	* difference: difference value for the test. This is the "changed by +4%" part of the trend.
//	* reason: explanation to provide the user with in case of failure.
package datapipe
