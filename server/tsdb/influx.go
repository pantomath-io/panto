// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"sync"

	influx "github.com/influxdata/influxdb/client/v2"
	logging "github.com/op/go-logging"
)

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

// InfluxTSDB is a concrete implementation of `tsdb.TSDB`
// that uses InfluxDB as a time-series database.
type InfluxTSDB struct {
	client       influx.Client
	database     string
	measurements sync.Map
}

// NewInfluxTSDB creates a new connection to an InfluxDB
// TSDB. The `database` argument is the name of the database
// to `USE` inside InfluxDB.
func NewInfluxTSDB(address, database string) (*InfluxTSDB, error) {
	var err error
	var tsdb InfluxTSDB
	tsdb.client, err = influx.NewHTTPClient(influx.HTTPConfig{Addr: address})
	if err != nil {
		return nil, err
	}

	tsdb.database = database

	q := influx.NewQuery(`CREATE DATABASE "`+database+`"`, "", "")
	_, err = tsdb.client.Query(q)
	if err != nil {
		return nil, err
	}

	return &tsdb, nil
}

// Close closes any open connection to InfluxDB. The instance should not be used after calling Close.
func (i *InfluxTSDB) Close() error {
	return i.client.Close()
}
