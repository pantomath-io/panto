// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"fmt"
	"strings"
)

// QueryFunc represents a function applied on a field
type QueryFunc string

// Supported Influx functions:
// - DERIVATIVE (https://docs.influxdata.com/influxdb/v1.7/query_language/functions/#derivative)
// 			will return a rate of change per second between 2 subsequent field values.
const (
	None       QueryFunc = "None"
	Derivative QueryFunc = "Derivative"
)

// QueryField is used to configure queries on the TSDB
type QueryField struct {
	Field    string
	Function QueryFunc
}

func (f QueryField) queryField() string {
	if f.Function == None {
		if f.Field == "*" {
			return fmt.Sprintf("%s", f.Field)
		}
		return fmt.Sprintf("\"%s\"", f.Field)
	}
	if f.Function == Derivative {
		return fmt.Sprintf("DERIVATIVE(\"%s\") AS \"%s\"", f.Field, f.Field)
	}

	return ""
}

// QueryFields is a collection of QueryField
type QueryFields []QueryField

func (fields QueryFields) queryFields() string {
	if len(fields) > 0 {
		flist := make([]string, 0, len(fields))

		for _, field := range fields {
			flist = append(flist, field.queryField())
		}

		return fmt.Sprintf("%s", strings.Join(flist, ", "))
	}

	// default case: all the fields, with no function
	return fmt.Sprintf("*")
}

// splitOnAggregation splits a slice of QueryField in 2 slices: the aggregate QueryFields and the non-aggregate QueryFields
func (fields QueryFields) splitOnAggregation() (QueryFields, QueryFields, error) {
	var aggFields, nonaggFields QueryFields
	for _, field := range fields {
		ok, err := field.isAggregation()
		if err != nil {
			return nil, nil, fmt.Errorf("unknwown QueryFunc: %s", field.Function)
		}
		if ok {
			aggFields = append(aggFields, field)
		} else {
			nonaggFields = append(nonaggFields, field)
		}
	}
	return nonaggFields, aggFields, nil
}

// isAggregation tells if a field has an aggregation function
func (f QueryField) isAggregation() (bool, error) {
	switch f.Function {
	case None:
		return false, nil
	case Derivative:
		return true, nil
	default:
		return false, fmt.Errorf("%s is not a known function", f.Function)
	}
}
