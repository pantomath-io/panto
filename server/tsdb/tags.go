// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"fmt"
	"strings"
)

// QueryTag is used to configure queries on the TSDB
type QueryTag string

func (t QueryTag) queryTag() string {
	return fmt.Sprintf("%s", t)
}

// QueryTags is a collection of QueryTag
type QueryTags []QueryTag

func (tags QueryTags) queryTags() string {
	if len(tags) > 0 {
		tlist := make([]string, 0, len(tags))

		for _, tag := range tags {
			tlist = append(tlist, tag.queryTag())
		}

		return fmt.Sprintf("%s", strings.Join(tlist, ", "))
	}

	// default case: empty list
	return fmt.Sprintf("")
}
