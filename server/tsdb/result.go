// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"encoding/json"
	"fmt"
	"math"
	"strings"
	"time"

	"gitlab.com/pantomath-io/panto/util"

	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
)

// Result contains all the data for the server to enter a
// result from a probe into the time-series database.
type Result struct {
	Entry

	Tags         map[string]string
	Fields       map[string]interface{}
	Error        bool
	ErrorMessage string

	sch *schema // temp reference to schema while parsing row
}

// assignKeyVal fills a field or a tag in the result from a value and the name of the column it is from. It optionally
// takes a measurement schema to infer field types and tags. If the schema is `nil`, the value is guessed from JSON
// and set as a field.
func (r *Result) assignKeyVal(k string, v interface{}, t kvType) error {
	err := r.Entry.assignKeyVal(k, v, t)
	if err == nil {
		// kv-pair was handled by embedded Entry
		return nil
	} else if !isKeyUnknown(err) {
		// real error
		return err
	}

	if k == keyFieldError {
		if errorMessage, ok := v.(string); ok && len(errorMessage) > 0 {
			r.Error = true
			r.ErrorMessage = errorMessage
		}
		return nil
	}

	// Initialize maps if missing
	if r.Fields == nil {
		r.Fields = make(map[string]interface{})
	}
	if r.Tags == nil {
		r.Tags = make(map[string]string)
	}

	if r.sch == nil {
		// No schema, try to guess from kvType and JSON
		if t == kvTypeField {
			switch j := v.(type) {
			case json.Number:
				if n, err := j.Int64(); err == nil {
					// the number is an integer
					r.Fields[k] = n
				} else if n, err := j.Float64(); err == nil {
					// the number is a float
					r.Fields[k] = n
				} else {
					// the number is not a number
					return fmt.Errorf("unable to parse JSON number: %s", j.String())
				}
			default:
				r.Fields[k] = v
			}
		} else if t == kvTypeTag {
			if tag, ok := v.(string); ok {
				r.Tags[k] = tag
			} else {
				return fmt.Errorf("%s tag %v is not a string", k, v)
			}
		}
	} else {
		// Use schema to infer types
		if t, ok := r.sch.field(k); ok {
			if v != nil {
				// It's a field, use the type from the schema
				switch t {
				case fieldTypeFloat:
					if j, ok := v.(json.Number); ok {
						if f, err := j.Float64(); err == nil {
							r.Fields[k] = f
							return nil
						}
					}
					return schemaErrorf("schema mismatch: %s field %v is not a float", k, v)
				case fieldTypeInteger:
					if j, ok := v.(json.Number); ok {
						if f, err := j.Int64(); err == nil {
							r.Fields[k] = f
							return nil
						}
						// HACK: special case when a function is applied, such as DERIVATIVE
						// An integer column in the schema, will end up being in fact a float.
						if f, err := j.Float64(); err == nil {
							r.Fields[k] = f
							return nil
						}
					}
					return schemaErrorf("schema mismatch: %s field %v is not an integer", k, v)
				case fieldTypeString:
					if f, ok := v.(string); ok {
						r.Fields[k] = f
					} else {
						return schemaErrorf("schema mismatch: %s field %v is not a string", k, v)
					}
				case fieldTypeBoolean:
					if f, ok := v.(bool); ok {
						r.Fields[k] = f
					} else {
						return schemaErrorf("schema mismatch: %s field %v is not a boolean", k, v)
					}
				case fieldTypeTimestamp:
					if s, ok := v.(string); ok {
						if f, err := time.Parse(time.RFC3339Nano, s); err == nil {
							r.Fields[k] = f
							return nil
						}
					}
					return schemaErrorf("schema mismatch: %s field %v is not a timestamp", k, v)
				default:
					return schemaErrorf("schema error: %s field is unknown type %s", k, t)
				}
			}
		} else if ok := r.sch.tag(k); ok {
			// It's a tag
			if v != nil {
				if tag, ok := v.(string); ok {
					r.Tags[k] = tag
				} else {
					return schemaErrorf("schema mismatch: %s tag %v is not a string", k, v)
				}
			}
		} else {
			return schemaErrorf("schema mismatch: %s not found in schema", k)
		}
	}

	return nil
}

// AddResults adds a batch of results from a check to the TSDB.
func (tsdb *InfluxTSDB) AddResults(results []Result) error {

	// Create a new point batch
	batch, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database: tsdb.database,
	})
	if err != nil {
		return err
	}

	for _, result := range results {
		tags := make(map[string]string)
		for k, v := range result.Tags {
			if util.Contains(reserved, k) {
				return fmt.Errorf("invalid key found in tags: %s", k)
			}
			tags[k] = v
		}
		tags[keyTagOrganization] = util.Base64Encode(result.Organization)
		tags[keyTagProbeConfiguration] = util.Base64Encode(result.ProbeConfiguration)
		tags[keyTagCheck] = util.Base64Encode(result.Check)
		tags[keyTagProbe] = util.Base64Encode(result.Probe)
		tags[keyTagTarget] = util.Base64Encode(result.Target)
		tags[keyTagAgent] = util.Base64Encode(result.Agent)

		fields := make(map[string]interface{})
		if !result.Error {
			// Check input
			if result.Fields == nil {
				return fmt.Errorf("missing fields map in probe result")
			}
			if len(result.Fields) == 0 {
				return fmt.Errorf("no fields in probe result")
			}

			// ***** HACK *****
			// chrales 2017-11-16
			// Influxdb storage engine does not support uint64 at the time of writing
			// but the client library acts as though it does. If we find one of these types in the
			// fields, we cast it down to a type the engine understands.
			for k, v := range result.Fields {
				switch v2 := v.(type) {
				case uint64:
					if v2 > math.MaxInt64 {
						return fmt.Errorf("value out of range: %d", v)
					}
					fields[k] = int64(v2)
				default:
					fields[k] = v2
				}
			}
		} else {
			fields = map[string]interface{}{keyFieldError: result.ErrorMessage}
		}

		// Create point
		point, err := influx.NewPoint(util.Base64Encode(result.Check), tags, fields, result.Timestamp)
		if err != nil {
			return err
		}
		batch.AddPoint(point)
	}

	// Write the batch
	return tsdb.client.Write(batch)
}

// GetResults retrieves results from the TSDB following the given filters
func (tsdb *InfluxTSDB) GetResults(check uuid.UUID, fields QueryFields, tags QueryTags, opts ...QueryOption) ([]Result, error) {
	// configure options
	o := new(queryOptions)
	for _, f := range opts {
		f(o)
	}

	log.Debugf(
		"Retrieving results from %s (%s)",
		check, o,
	)

	// split the fields between aggregated and non-aggregated
	nonaggFields, aggFields, err := fields.splitOnAggregation()
	if err != nil {
		return nil, err
	}

	// non-aggregate queries (or generic query if no aggregate fields)
	var res []Result
	if len(aggFields) == 0 || len(nonaggFields) > 0 {
		var query string
		if len(tags.queryTags()) > 0 && nonaggFields.queryFields() != "*" {
			query = fmt.Sprintf("SELECT %s, %s FROM \"%s\" %s", nonaggFields.queryFields(), tags.queryTags(), util.Base64Encode(check), o.queryClauses())
		} else {
			query = fmt.Sprintf("SELECT %s FROM \"%s\" %s", nonaggFields.queryFields(), util.Base64Encode(check), o.queryClauses())
		}

		res, err = queryResults(tsdb, check, query)
		if err != nil && isSchemaError(err) {
			// Retry query after purging schema cache
			tsdb.measurements.Delete(util.Base64Encode(check))
			res, err = queryResults(tsdb, check, query)
		}
		if err != nil {
			return nil, err
		}
	}

	// aggregate queries
	if len(aggFields) > 0 {
		o.groupBy = tags.queryTags()
		query := fmt.Sprintf("SELECT %s FROM \"%s\" %s", aggFields.queryFields(), util.Base64Encode(check), o.queryClauses())

		aggResults, err := queryResults(tsdb, check, query)
		if err != nil && isSchemaError(err) {
			// Retry query after purging schema cache
			tsdb.measurements.Delete(util.Base64Encode(check))
			aggResults, err = queryResults(tsdb, check, query)
		}
		if err != nil {
			return nil, err
		}

		// merge result arrays
		if len(res) > 0 {
			for _, aggResult := range aggResults {
				for i, r := range res {
					if aggResult.Timestamp == r.Timestamp {
						for k, v := range aggResult.Fields {
							if _, ok := res[i].Fields[k]; ok {
								log.Debugf(" /!\\ Overwriting field during result merge!")
							}
							res[i].Fields[k] = v
						}
						break
					}
				}
			}
		} else {
			res = aggResults
		}
	}

	return res, nil
}

func queryResults(tsdb *InfluxTSDB, check uuid.UUID, cmd string) ([]Result, error) {
	measurementName := util.Base64Encode(check)
	sch, hasSchema := tsdb.measurements.Load(measurementName)
	if !hasSchema {
		// Fetch measurement schema along with results
		cmd = strings.Join(
			[]string{
				fmt.Sprintf("SHOW FIELD KEYS FROM \"%s\"", measurementName),
				fmt.Sprintf("SHOW TAG KEYS FROM \"%s\"", measurementName),
				cmd,
			},
			"; ",
		)
	}

	// run the query against the TSDB
	q := influx.Query{
		Command:  cmd,
		Database: tsdb.database,
	}
	log.Infof("Fetching results from %s", util.Base64Encode(check))
	log.Debugf("Query: %s", cmd)
	response, err := tsdb.client.Query(q)
	if err != nil {
		return nil, fmt.Errorf("%s (%s)", err, cmd)
	}
	if response.Error() != nil {
		return nil, fmt.Errorf("%s (%s)", response.Error(), cmd)
	}

	results := response.Results

	// parse schema
	if !hasSchema {
		if len(results) != 3 {
			return nil, fmt.Errorf("error fetching measurement schema")
		} else if len(results[0].Series) == 0 || len(results[1].Series) == 0 {
			// the measurement does not exist
			return []Result{}, nil
		}

		sch, err = parseSchema(results[0].Series[0].Values, results[1].Series[0].Values)
		if err != nil {
			return nil, fmt.Errorf("error parsing measurement schema: %s", err)
		}
		tsdb.measurements.Store(measurementName, sch)
		results = results[2:]
	}

	// build array of tsdb.Result from query results
	if len(results) == 0 || len(results[0].Series) == 0 {
		log.Infof("Fetched 0 rows")
		return []Result{}, nil
	}
	serie := results[0].Series[0]
	log.Infof("Fetched %d rows", len(serie.Values))
	res := make([]Result, len(serie.Values))
	s := sch.(schema)
	for i, row := range serie.Values {
		r := &res[i]
		r.sch = &s
		if err = fromRow(r, row, serie.Columns, serie.Tags); err != nil {
			r.sch = nil
			return nil, err
		}
		r.sch = nil
	}
	return res, nil
}
