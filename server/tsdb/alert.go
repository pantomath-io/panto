// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
)

const alertMeasurementPrefix = "alert-"
const keyFieldAlertEvent = "event"
const keyFieldStateReason = "state_reason"
const keyFieldAlertReason = "alert_reason"

// EventValue encodes a value evaluated for an event
type EventValue int

const (
	// EventValueNotified encodes a "notified" event
	EventValueNotified = EventValue(api.Event_NOTIFIED)
	// EventValueAcknowledged encodes an "acknowledged" event
	EventValueAcknowledged = EventValue(api.Event_ACKNOWLEDGED)
	// EventValueSnoozed encodes a "snoozed" event
	EventValueSnoozed = EventValue(api.Event_SNOOZED)
)

func (ev EventValue) String() string {
	switch ev {
	case EventValueNotified:
		return "notified"
	case EventValueAcknowledged:
		return "acknowledged"
	case EventValueSnoozed:
		return "snoozed"
	default:
		panic(fmt.Sprintf("%d is not a valid EventValue", int(ev)))
	}
}

// EventValueFromString convert a string in EventValue
func EventValueFromString(s string) (ev EventValue, err error) {
	switch s {
	case "notified":
		ev = EventValueNotified
	case "acknowledged":
		ev = EventValueAcknowledged
	case "snoozed":
		ev = EventValueSnoozed
	default:
		return ev, fmt.Errorf("%s is not a valid EventValue", s)
	}

	return
}

// AlertEvent contains the data from an alert event in the TSDB.
type AlertEvent struct {
	Entry
	Event       EventValue
	StateReason string    // State.Reason from the TSDB
	Alert       uuid.UUID // Corresponding Alert from the DBConf
	AlertReason string    // Message sent to the user explaining why the Alert is sent
}

func (a *AlertEvent) assignKeyVal(k string, v interface{}, t kvType) error {
	err := a.Entry.assignKeyVal(k, v, t)
	if err == nil {
		// kv-pair was handled by embedded Entry
		return nil
	} else if !isKeyUnknown(err) {
		// real error
		return err
	}

	switch k {
	case keyFieldAlertEvent:
		event, ok := v.(string)
		if ok {
			ev, err := EventValueFromString(event)
			if err != nil {
				return fmt.Errorf("unable to convert event value: %s", err)
			}
			a.Event = ev
		} else {
			return fmt.Errorf("event from TSDB is not a string")
		}
	case keyFieldStateReason:
		reason, ok := v.(string)
		if ok {
			a.StateReason = reason
		} else {
			return fmt.Errorf("state reason from TSDB is not a string")
		}
	case keyFieldAlertReason:
		reason, ok := v.(string)
		if ok {
			a.AlertReason = reason
		} else {
			return fmt.Errorf("alert reason from TSDB is not a string")
		}
	case keyTagProbeConfiguration:
		v, ok := v.(string)
		if ok {
			pcUUID, err := util.Base64Decode(v)
			if err != nil {
				return fmt.Errorf("unable to convert probe_configuration tag: %s", err)
			}
			a.ProbeConfiguration = pcUUID
		} else {
			return fmt.Errorf("probe_configuration tag from TSDB is not a string")
		}
	case keyTagAlert:
		v, ok := v.(string)
		if ok {
			aUUID, err := util.Base64Decode(v)
			if err != nil {
				err = fmt.Errorf("unable to convert alert tag: %s", err)
				return err
			}
			a.Alert = aUUID
		} else {
			return fmt.Errorf("alert tag from TSDB is not a string")
		}
	}
	return nil
}

// AddAlertEvent adds a new alert event to the TSDB.
func (tsdb *InfluxTSDB) AddAlertEvent(alert AlertEvent) error {
	// Create a new point batch
	batch, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database: tsdb.database,
	})
	if err != nil {
		return err
	}

	// Create fields and tags map
	fields := map[string]interface{}{
		keyFieldAlertEvent:  alert.Event,
		keyFieldStateReason: alert.StateReason,
		keyFieldAlertReason: alert.AlertReason,
	}
	tags := map[string]string{
		keyTagOrganization:       util.Base64Encode(alert.Organization),
		keyTagCheck:              util.Base64Encode(alert.Check),
		keyTagProbe:              util.Base64Encode(alert.Probe),
		keyTagTarget:             util.Base64Encode(alert.Target),
		keyTagAgent:              util.Base64Encode(alert.Agent),
		keyTagProbeConfiguration: util.Base64Encode(alert.ProbeConfiguration),
		keyTagAlert:              util.Base64Encode(alert.Alert),
	}

	// Set timestamp to "now" by default
	var ts time.Time
	if alert.Timestamp.IsZero() {
		ts = time.Now()
	} else {
		ts = alert.Timestamp
	}

	// Set measurement name
	measurementName := alertMeasurementPrefix + util.Base64Encode(alert.ProbeConfiguration)

	point, err := influx.NewPoint(measurementName, tags, fields, ts)
	if err != nil {
		return err
	}
	batch.AddPoint(point)

	// Write the batch
	return tsdb.client.Write(batch)
}

// GetAlertEvents retrieves the alert events for a probeConfiguration from the TSDB.
func (tsdb *InfluxTSDB) GetAlertEvents(probeConfigurations []uuid.UUID, opts ...QueryOption) ([]AlertEvent, error) {
	// configure options
	o := new(queryOptions)
	for _, f := range opts {
		f(o)
	}

	alertMeasurements := make([]string, len(probeConfigurations))
	for i, probeConfiguration := range probeConfigurations {
		alertMeasurements[i] = fmt.Sprintf("\"%s%s\"", alertMeasurementPrefix, util.Base64Encode(probeConfiguration))
	}

	log.Debugf(
		"Retrieving alerts for %s (%s)",
		alertMeasurements, o,
	)

	// prepare statement
	query := fmt.Sprintf(
		"SELECT * FROM %s %s",
		strings.Join(alertMeasurements, ","), o.queryClauses(),
	)

	return queryAlertEvents(tsdb, probeConfigurations, query)
}

func queryAlertEvents(tsdb *InfluxTSDB, probeConfigurations []uuid.UUID, cmd string) ([]AlertEvent, error) {
	// run the query against the TSDB
	q := influx.Query{
		Command:  cmd,
		Database: tsdb.database,
	}

	probeConfigurationMeasurements := make([]string, len(probeConfigurations))
	for i, probeConfiguration := range probeConfigurations {
		probeConfigurationMeasurements[i] = util.Base64Encode(probeConfiguration)
	}
	log.Infof("Fetching alert events for %s", strings.Join(probeConfigurationMeasurements, ","))
	log.Debugf("Query: %s", cmd)
	response, err := tsdb.client.Query(q)
	if err != nil {
		return nil, err
	}
	if response.Error() != nil {
		return nil, response.Error()
	}

	// build array of tsdb.AlertEvent from query results
	results := response.Results
	if len(results) == 0 || len(results[0].Series) == 0 {
		log.Infof("Fetched 0 rows")
		return []AlertEvent{}, nil
	}

	res := make([]AlertEvent, 0)

	for _, serie := range results[0].Series {
		log.Infof("Fetched %d rows", len(serie.Values))
		for _, row := range serie.Values {
			a := AlertEvent{}
			err = fromRow(&a, row, serie.Columns, serie.Tags)
			if err != nil {
				return nil, err
			}
			res = append(res, a)
		}
	}
	return res, nil
}
