// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"testing"
)

func TestQueryTag(t *testing.T) {
	cases := []QueryTag{
		"foo",
		"bar",
		"*",
	}
	expect := []string{
		"foo",
		"bar",
		"*",
	}

	for i, c := range cases {
		if c.queryTag() != expect[i] {
			t.Errorf("Unexpected result after applying tags. Got %v, expected %v.", c.queryTag(), expect[i])
		}
	}
}

func TestQueryTags(t *testing.T) {
	cases := []QueryTags{
		{
			"foo",
			"bar",
			"*",
		},
		{},
	}

	expect := []string{
		"foo, bar, *",
		"",
	}

	for i, c := range cases {
		if c.queryTags() != expect[i] {
			t.Errorf("Unexpected result after building tags string. Got %v, expected %v", c.queryTags(), expect[i])
		}
	}
}
