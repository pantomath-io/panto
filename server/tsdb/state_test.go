// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"encoding/json"
	"fmt"
	"reflect"
	"sort"
	"testing"
	"time"

	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/util"
)

func TestStateFromRow(t *testing.T) {
	rows := [][]interface{}{
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", json.Number("123"), "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "This row is valid"}, // Valid row
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", "QSDGFQRSDG", "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "This row is invalid"},     // Gibberish state
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", "noLlUczuSJqkyIEynnCoow", json.Number("123"), "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", 1234},                // Invalid reason
		{"2018-02-05T16:59:47Z", "kDtJz8EqQ0C7KgOF6qwxfg", "i2QYtZBVSnuXjLhQhuAV6w", json.Number("123"), "xUkSnG_BSVCn-AX1khPLYg", "MQF0eIJcRZyEBgG_sdLPWA", "This row is valid"},                           // Missing agent
	}
	cols := [][]string{
		{"time", "organization", "check", "agent", "state", "probe", "target", "reason"},
		{"time", "organization", "check", "agent", "state", "probe", "target", "reason"},
		{"time", "organization", "check", "agent", "state", "probe", "target", "reason"},
		{"time", "organization", "check", "state", "probe", "target", "reason"},
	}
	tags := []map[string]string{
		nil,
		nil,
		nil,
		{"agent": "noLlUczuSJqkyIEynnCoow"},
	}
	expect := []*State{
		{
			Entry: Entry{
				Timestamp:    time.Unix(1517849987, 0).UTC(),
				Organization: organization,
				Check:        check,
				Target:       target,
				Probe:        probe,
				Agent:        agent,
			},
			State:  123,
			Reason: "This row is valid",
		},
		nil,
		nil,
		{
			Entry: Entry{
				Timestamp:    time.Unix(1517849987, 0).UTC(),
				Organization: organization,
				Check:        check,
				Target:       target,
				Probe:        probe,
				Agent:        agent,
			},
			State:  123,
			Reason: "This row is valid",
		},
	}

	// Check sanity of test data
	if len(cols) != len(rows) || len(cols) != len(expect) {
		panic(fmt.Sprintf("Inconsistent test data: %d rows, %d cols, %d expect", len(rows), len(cols), len(expect)))
	}

	for i, row := range rows {
		// check sanity of test data
		if len(row) != len(cols[i]) {
			panic(fmt.Sprintf("Inconsistent test data for row %d: %d values, %d cols", i, len(row), len(cols[i])))
		}
		var s State
		err := fromRow(&s, row, cols[i], tags[i])
		if expect[i] != nil {
			if err != nil {
				t.Errorf("Failed to convert row: %s", err)
				continue
			}
			if !reflect.DeepEqual(s, *expect[i]) {
				t.Error("Row did not convert successfully")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Expected:  %#v", *expect[i])
				t.Logf("Got:       %#v", s)
				continue
			}
		} else {
			if err == nil {
				t.Error("Row converted successfully, but expected error")
				t.Logf("Input row: (%s)%s", cols[i], row)
				t.Logf("Got:       %v", s)
				continue
			}
		}
	}
}

func TestAddStates(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}
	defer db.Close()

	s := State{
		Entry: Entry{
			Timestamp:          time.Unix(1517849987, 0).UTC(),
			Organization:       organization,
			Check:              check,
			Target:             target,
			ProbeConfiguration: probeConfiguration,
			Probe:              probe,
			Agent:              agent,
		},
		State:  123456,
		Reason: "This is the reason.",
	}

	err = db.AddState(s)
	if err != nil {
		t.Fatalf("Failed to add results to TSDB: %s", err)
	}

	measurementName := stateMeasurementPrefix + util.Base64Encode(check)
	res, err := db.client.Query(influx.NewQuery("SELECT * FROM \""+measurementName+"\"", "pantotest", "s"))
	if err != nil {
		t.Fatalf("Failed to query states from TSDB: %s", err)
	}
	if res.Error() != nil {
		t.Fatalf("Failed to query states from TSDB: %s", res.Error())
	}

	// build array of tsdb.State from query results
	results := res.Results
	if len(results) == 0 || len(results[0].Series) == 0 {
		t.Fatalf("Query returned no results")
	}
	serie := results[0].Series[0]
	if len(serie.Values) != 1 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 1, len(serie.Values))
	}

	// Check columns
	columns := []string{"time", "organization", "check", "probe", "probe_configuration", "agent", "target", "state", "reason"}
Expected:
	for _, expect := range columns {
		for _, got := range serie.Columns {
			if got == expect {
				continue Expected // to next column
			}
		}
		t.Fatalf("Expected column \"%s\" not found: %s", expect, serie.Columns)
	}
Got:
	for _, got := range serie.Columns {
		for _, expect := range columns {
			if got == expect {
				continue Got // to next column
			}
		}
		t.Fatalf("Unexpected column \"%s\" was found: %s", got, serie.Columns)
	}

	row := serie.Values[0]
	t.Logf("Row 0: %s%s", serie.Columns, row)
	for i, col := range serie.Columns {
		switch col {
		case "time":
			n, ok := row[i].(json.Number)
			if ok {
				if n2, _ := n.Int64(); n2 == 1517849987 {
					continue
				}
			}
			t.Fatalf("Invalid timestamp in row 0: expected %d, got %v", 1517849987, row[i])
		case keyTagOrganization:
			if row[i].(string) != util.Base64Encode(organization) {
				t.Fatalf("Invalid check in row 0: expected %v, got %v", util.Base64Encode(organization), row[i])
			}
		case keyTagCheck:
			if row[i].(string) != util.Base64Encode(check) {
				t.Fatalf("Invalid check in row 0: expected %v, got %v", util.Base64Encode(check), row[i])
			}
		case keyTagProbe:
			if row[i].(string) != util.Base64Encode(probe) {
				t.Fatalf("Invalid probe in row 0: expected %s, got %v", util.Base64Encode(probe), row[i])
			}
		case keyTagTarget:
			if row[i].(string) != util.Base64Encode(target) {
				t.Fatalf("Invalid target in row 0: expected %s, got %v", util.Base64Encode(target), row[i])
			}
		case keyTagAgent:
			if row[i].(string) != util.Base64Encode(agent) {
				t.Fatalf("Invalid agent in row 0: expected %s, got %v", util.Base64Encode(agent), row[i])
			}
		case keyTagProbeConfiguration:
			if row[i].(string) != util.Base64Encode(probeConfiguration) {
				t.Fatalf("Invalid target in row 0: expected %s, got %v", util.Base64Encode(probeConfiguration), row[i])
			}
		case keyFieldState:
			n, ok := row[i].(json.Number)
			if ok {
				if n2, _ := n.Int64(); n2 == 123456 {
					continue
				}
			}
			t.Fatalf("Invalid state in row 0: expected %d, got %v", 123456, row[i])
		case keyFieldReason:
			s, ok := row[i].(string)
			if ok && s == "This is the reason." {
				continue
			}
			t.Fatalf("Invalid state in row 0: expected %#v, got %v", "This is the reason.", row[i])
		default:
			if row[i] != nil {
				t.Fatalf("Invalid field in row 0: (\"%s\": %v)", col, row[i])
			}
		}
	}
}

func TestGetStates(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}
	defer db.Close()

	res, err := db.GetStates([]uuid.UUID{check}, Count(1), OrderBy(OrderDescending))
	if err != nil {
		t.Fatalf("Could not get last states: %s", err)
	}
	t.Logf("States: %v", res)
	if len(res) != 1 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 1, len(res))
	}

	expect := State{
		Entry: Entry{
			Timestamp:          time.Unix(1517849987, 0).UTC(),
			Organization:       organization,
			ProbeConfiguration: probeConfiguration,
			Check:              check,
			Target:             target,
			Probe:              probe,
			Agent:              agent,
		},
		State:  123456,
		Reason: "This is the reason.",
	}
	if !reflect.DeepEqual(res[0], expect) {
		t.Error("Unexpected entry for last result")
		t.Logf("Expected:  %v", expect)
		t.Logf("Got:       %v", res[0])
	}

	from := time.Unix(1517849980, 0).UTC()
	res, err = db.GetStates([]uuid.UUID{check}, From(from), To(time.Now()))
	if err != nil {
		t.Fatalf("Could not get states in range: %s", err)
	}
	t.Logf("States: %v", res)
	if len(res) != 1 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 1, len(res))
	}

	expect = State{
		Entry: Entry{
			Timestamp:          time.Unix(1517849987, 0).UTC(),
			Organization:       organization,
			ProbeConfiguration: probeConfiguration,
			Check:              check,
			Target:             target,
			Probe:              probe,
			Agent:              agent,
		},
		State:  123456,
		Reason: "This is the reason.",
	}
	if !reflect.DeepEqual(res[0], expect) {
		t.Error("Unexpected entry for last result")
		t.Logf("Expected:  %v", expect)
		t.Logf("Got:       %v", res[0])
	}
}

func TestGetStatesEmpty(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}
	defer db.Close()

	res, err := db.GetStates([]uuid.UUID{}, Count(1), OrderBy(OrderDescending))
	if err != nil {
		t.Fatalf("Could not get last states: %s", err)
	}
	t.Logf("States: %v", res)
	if len(res) > 0 {
		t.Fatalf("Invalid number of results: expected %d, got %d", 0, len(res))
	}
}

func TestGetMultipleStates(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live testing...")
	}

	db, err := NewInfluxTSDB(viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
	if err != nil {
		t.Fatalf("Couldn't create TSDB instance: %s", err)
	}
	defer db.Close()

	expected := []State{
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849987, 0).UTC(),
				Organization:       organization,
				ProbeConfiguration: probeConfiguration,
				Check:              check,
				Target:             target,
				Probe:              probe,
				Agent:              agent,
			},
			State:  1,
			Reason: "This is the reason.",
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849988, 0).UTC(),
				Organization:       organization,
				ProbeConfiguration: probeConfiguration,
				Check:              check2,
				Target:             target,
				Probe:              probe,
				Agent:              agent,
			},
			State:  1,
			Reason: "This is the reason.",
		},
		{
			Entry: Entry{
				Timestamp:          time.Unix(1517849989, 0).UTC(),
				Organization:       organization,
				ProbeConfiguration: probeConfiguration,
				Check:              check2,
				Target:             target,
				Probe:              probe,
				Agent:              agent2,
			},
			State:  2,
			Reason: "This is the reason.",
		},
	}

	for _, s := range expected {
		err = db.AddState(s)
		if err != nil {
			t.Fatalf("Failed to add results to TSDB: %s", err)
		}
	}

	res, err := db.GetStates([]uuid.UUID{check, check2}, Count(1), GroupBy("agent"), OrderBy(OrderDescending))
	if err != nil {
		t.Fatalf("Could not get last states: %s", err)
	}
	t.Logf("States: %v", res)
	if len(res) != len(expected) {
		t.Fatalf("Invalid number of results: expected %d, got %d", len(expected), len(res))
	}

	sort.Slice(res, func(l, r int) bool { return res[l].Entry.Timestamp.Nanosecond() < res[r].Entry.Timestamp.Nanosecond() })

	for i, expect := range expected {
		if (res[i].Agent != expect.Agent) && (res[i].Check != expect.Check) && (res[i].Organization != expect.Organization) && (res[i].State != expect.State) {
			t.Error("Unexpected entry")
			t.Logf("Expected:  %v", expect)
			t.Logf("Got:       %v", res[i])
		}
	}
}
