// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/pantomath-io/panto/util"

	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
)

const stateMeasurementPrefix = "state-"
const keyFieldState = "state"
const keyFieldReason = "reason"

// State contains the data from a state evaluation after receiving a result in the TSDB
type State struct {
	Entry
	State  int
	Reason string
}

func (s *State) assignKeyVal(k string, v interface{}, t kvType) error {
	err := s.Entry.assignKeyVal(k, v, t)
	if err == nil {
		// kv-pair was handled by embedded Entry
		return nil
	} else if !isKeyUnknown(err) {
		// real error
		return err
	}

	switch k {
	case keyFieldState:
		jsonNum, ok := v.(json.Number)
		if ok {
			var num int64
			num, err = jsonNum.Int64()
			if err == nil {
				s.State = int(num)
			}
		} else {
			return fmt.Errorf("state from TSDB is not a number")
		}
	case keyFieldReason:
		reason, ok := v.(string)
		if !ok {
			return fmt.Errorf("reason from TSDB is not a string")
		}
		s.Reason = reason
	}

	return nil
}

// AddState adds a state computed from a probe result into the TSDB
func (tsdb *InfluxTSDB) AddState(state State) error {
	// Create a new point batch
	batch, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database: tsdb.database,
	})
	if err != nil {
		return err
	}

	fields := map[string]interface{}{
		keyFieldState:  state.State,
		keyFieldReason: state.Reason,
	}
	tags := map[string]string{
		keyTagOrganization:       util.Base64Encode(state.Organization),
		keyTagCheck:              util.Base64Encode(state.Check),
		keyTagProbeConfiguration: util.Base64Encode(state.ProbeConfiguration),
		keyTagProbe:              util.Base64Encode(state.Probe),
		keyTagTarget:             util.Base64Encode(state.Target),
		keyTagAgent:              util.Base64Encode(state.Agent),
	}

	// Set timestamp to "now" by default
	var ts time.Time
	if state.Timestamp.IsZero() {
		ts = time.Now()
	} else {
		ts = state.Timestamp
	}

	// Set measurement name
	measurementName := stateMeasurementPrefix + util.Base64Encode(state.Check)

	point, err := influx.NewPoint(measurementName, tags, fields, ts)
	if err != nil {
		return err
	}
	batch.AddPoint(point)

	// Write the batch
	return tsdb.client.Write(batch)
}

// GetStates retrieves the states for a check from the TSDB.
func (tsdb *InfluxTSDB) GetStates(checks []uuid.UUID, opts ...QueryOption) ([]State, error) {
	// configure options
	o := new(queryOptions)
	for _, f := range opts {
		f(o)
	}

	checkMeasurements := make([]string, len(checks))
	for i, check := range checks {
		checkMeasurements[i] = fmt.Sprintf("\"%s%s\"", stateMeasurementPrefix, util.Base64Encode(check))
	}

	log.Debugf(
		"Retrieving states for %s (%s)",
		checkMeasurements, o,
	)

	// prepare statement
	query := fmt.Sprintf(
		"SELECT * FROM %s %s",
		strings.Join(checkMeasurements, ","), o.queryClauses(),
	)

	return queryStates(tsdb, checks, query)
}

func queryStates(tsdb *InfluxTSDB, checks []uuid.UUID, cmd string) (res []State, err error) {
	// run the query against the TSDB
	q := influx.Query{
		Command:  cmd,
		Database: tsdb.database,
	}

	if len(checks) <= 0 {
		return nil, err
	}

	checkMeasurements := make([]string, len(checks))
	for i, check := range checks {
		checkMeasurements[i] = util.Base64Encode(check)
	}
	log.Infof("Fetching states for %s", strings.Join(checkMeasurements, ","))
	log.Debugf("Query: %s", cmd)
	response, err := tsdb.client.Query(q)
	if err != nil {
		return nil, err
	}
	if response.Error() != nil {
		return nil, response.Error()
	}

	// build array of tsdb.State from query results
	results := response.Results
	if len(results) == 0 || len(results[0].Series) == 0 {
		log.Infof("Fetched 0 rows")
		return []State{}, nil
	}

	res = make([]State, 0)

	for _, serie := range results[0].Series {
		log.Infof("Fetched %d rows", len(serie.Values))
		for _, row := range serie.Values {
			s := State{}
			err = fromRow(&s, row, serie.Columns, serie.Tags)
			if err != nil {
				return nil, err
			}
			res = append(res, s)
		}
	}
	return res, nil
}
