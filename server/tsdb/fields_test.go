// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tsdb

import (
	"reflect"
	"testing"
)

func TestQueryField(t *testing.T) {
	cases := []QueryField{
		{Field: "foo", Function: None},
		{Field: "bar", Function: Derivative},
		{Field: "*", Function: None},
	}
	expect := []string{
		"\"foo\"",
		"DERIVATIVE(\"bar\") AS \"bar\"",
		"*",
	}

	for i, c := range cases {
		if c.queryField() != expect[i] {
			t.Errorf("Unexpected result after applying fields. Got %v, expected %v.", c.queryField(), expect[i])
		}
	}
}

func TestQueryFields(t *testing.T) {
	cases := []QueryFields{
		{
			{Field: "foo", Function: None},
			{Field: "bar", Function: Derivative},
			{Field: "*", Function: None},
		},
		{},
	}

	expect := []string{
		"\"foo\", DERIVATIVE(\"bar\") AS \"bar\", *",
		"*",
	}

	for i, c := range cases {
		if c.queryFields() != expect[i] {
			t.Errorf("Unexpected result after building fields string. Got %v, expected %v", c.queryFields(), expect[i])
		}
	}
}

func TestSplitOnAggregation(t *testing.T) {
	cases := []QueryFields{
		{
			{Field: "foo", Function: None},
		},
		{
			{Field: "bar", Function: Derivative},
		},
		{
			{Field: "foo", Function: None},
			{Field: "bar", Function: Derivative},
		},
		{},
	}

	expect := []struct {
		nagg QueryFields
		agg  QueryFields
		err  error
	}{
		{
			QueryFields{
				{Field: "foo", Function: None},
			},
			QueryFields{},
			nil,
		},
		{
			QueryFields{},
			QueryFields{
				{Field: "bar", Function: Derivative},
			},
			nil,
		},
		{
			QueryFields{
				{Field: "foo", Function: None},
			},
			QueryFields{
				{Field: "bar", Function: Derivative},
			},
			nil,
		},
		{
			QueryFields{},
			QueryFields{},
			nil,
		},
	}

	for i, c := range cases {
		n, a, e := c.splitOnAggregation()
		if len(a) > 0 && !reflect.DeepEqual(a, expect[i].agg) {
			t.Errorf("Unexpected aggregation result after split fields on case %d. Got %v, expected %v", i, a, expect[i].agg)
		}
		if len(n) > 0 && !reflect.DeepEqual(n, expect[i].nagg) {
			t.Errorf("Unexpected non-aggregation result after split fields on case %d. Got %v, expected %v", i, n, expect[i].nagg)
		}
		if e != expect[i].err {
			t.Errorf("Unexpected error during split fields on case %d. Got %v, expected %v", i, e, expect[i].err)
		}
	}
}

func TestIsAggregation(t *testing.T) {
	cases := []QueryField{
		{Field: "foo", Function: None},
		{Field: "bar", Function: Derivative},
	}

	except := []struct {
		res bool
		err error
	}{
		{false, nil},
		{true, nil},
	}

	for i, c := range cases {
		b, e := c.isAggregation()
		if except[i].res != b {
			t.Errorf("Unexpected result during test of aggregation on case %d. Got %v, expected %v.", i, e, except[i].err)
		}
		if except[i].err != e {
			t.Errorf("Unexpected error during test of aggregation on case %d. Got %v, expected %v.", i, e, except[i].err)
		}
	}
}
