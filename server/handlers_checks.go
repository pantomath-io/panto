// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/json"
	"path"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/datapipe"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ListChecks returns all the Checks for a given Organization
func (s *server) ListChecks(ctx context.Context, req *api.ListChecksRequest) (res *api.ListChecksResponse, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	// get the optional constraints on target and probe
	targets := make([]uuid.UUID, len(req.Target))
	for i, t := range req.Target {
		oUUID, tUUID, err := api.ParseNameTarget(t)
		if err != nil {
			log.Errorf("couldn't parse Target name: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid Target name")
		}
		if oUUID != organizationUUID {
			log.Errorf("Target is not in Organization")
			return nil, status.Error(codes.InvalidArgument, "Target is not in Organization")
		}
		targets[i] = tUUID
	}
	probes := make([]uuid.UUID, len(req.Probe))
	for i, p := range req.Probe {
		pUUID, err := api.ParseNameProbe(p)
		if err != nil {
			log.Errorf("couldn't parse Probe name: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid Probe name")
		}
		probes[i] = pUUID
	}

	// validate nested children mask
	acceptedChildren := []string{"target", "probe"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid field in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	tps, err := db.ListTargetProbes(organizationUUID, targets, probes, pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list Checks: %s", err)
		return nil, status.Error(codes.NotFound, "No Checks found")
	}

	var nextPage bool
	if int32(len(tps)) > pageSize {
		tps = tps[:pageSize]
		nextPage = true
	}

	checks := make([]*api.Check, 0, len(tps))
	for _, tp := range tps {
		config, err := json.Marshal(tp.StateConfiguration)
		if err != nil {
			log.Errorf("unable to marshal state configuration to JSON: %s", err)
			continue
		}

		check := &api.Check{
			Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(tp.UUID)),
			Target:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTarget, util.Base64Encode(tp.TargetUUID)),
			Probe:         path.Join(api.ResourceNameProbe, util.Base64Encode(tp.ProbeUUID)),
			Type:          tp.StateType,
			Configuration: string(config),
		}

		err = datapipe.ValidateStateConfigurationFormat(check.Type, check.Configuration)
		if err != nil {
			log.Errorf("invalid state configuration: %s", err)
			continue
		}

		if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
			check.Children = &api.Check_Children{}
			if util.Contains(req.ChildrenMask.Paths, "target") {
				check.Children.Target, err = s.GetTarget(ctx, &api.GetTargetRequest{Name: check.Target})
				if err != nil {
					return nil, err
				}
			}
			if util.Contains(req.ChildrenMask.Paths, "probe") {
				check.Children.Probe, err = s.GetProbe(ctx, &api.GetProbeRequest{Name: check.Probe})
				if err != nil {
					return nil, err
				}
			}
		}

		checks = append(checks, check)
	}

	res = &api.ListChecksResponse{
		Checks: checks,
	}

	if nextPage {
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+uint64(len(checks)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetCheck returns a named Check resource
func (s *server) GetCheck(ctx context.Context, req *api.GetCheckRequest) (*api.Check, error) {
	if len(req.GetName()) == 0 {
		log.Errorf("missing Check name")
		return nil, status.Error(codes.InvalidArgument, "Missing Check name")
	}

	// get the organization and check UUIDs from the name field
	organizationUUID, checkUUID, err := api.ParseNameCheck(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Check name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Check name")
	}

	// validate nested children mask
	acceptedChildren := []string{"target", "probe"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid field in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	tp, err := dbconf.GetTargetProbe(organizationUUID, checkUUID)
	if err != nil {
		log.Errorf("failed to get check %s (%s)", checkUUID, err)
		return nil, status.Error(codes.Internal, "Check not found")
	}
	if tp == nil {
		log.Errorf("check not found: %s", req.Name)
		return nil, status.Error(codes.NotFound, "Check not found")
	}

	config, err := json.Marshal(tp.StateConfiguration)
	if err != nil {
		log.Errorf("unable to marshal state configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid States Configuration")
	}
	response := &api.Check{
		Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(tp.UUID)),
		Target:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTarget, util.Base64Encode(tp.TargetUUID)),
		Probe:         path.Join(api.ResourceNameProbe, util.Base64Encode(tp.ProbeUUID)),
		Type:          tp.StateType,
		Configuration: string(config),
	}

	err = datapipe.ValidateStateConfigurationFormat(response.Type, response.Configuration)
	if err != nil {
		log.Errorf("invalid state configuration: %s", err)
		return nil, status.Error(codes.Internal, "Invalid States Configuration")
	}

	if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
		response.Children = &api.Check_Children{}
		if util.Contains(req.ChildrenMask.Paths, "target") {
			response.Children.Target, err = s.GetTarget(ctx, &api.GetTargetRequest{Name: response.Target})
			if err != nil {
				return nil, err
			}
		}
		if util.Contains(req.ChildrenMask.Paths, "probe") {
			response.Children.Probe, err = s.GetProbe(ctx, &api.GetProbeRequest{Name: response.Probe})
			if err != nil {
				return nil, err
			}
		}
	}

	return response, nil
}

// CreateCheck creates a new Check and returns it.
func (s *server) CreateCheck(ctx context.Context, req *api.CreateCheckRequest) (res *api.Check, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	// check input
	if len(req.Check.GetProbe()) == 0 {
		log.Errorf("missing Probe name")
		return nil, status.Error(codes.InvalidArgument, "Missing Probe name")
	}
	if len(req.Check.GetTarget()) == 0 {
		log.Errorf("missing Target name")
		return nil, status.Error(codes.InvalidArgument, "Missing Target name")
	}
	// Type and Configuration need to be set together
	if len(req.Check.GetType()) == 0 && len(req.Check.GetConfiguration()) != 0 {
		log.Errorf("missing type")
		return nil, status.Error(codes.InvalidArgument, "Missing Check type")
	} else if len(req.Check.GetType()) != 0 && len(req.Check.GetConfiguration()) == 0 {
		log.Errorf("missing States Configuration")
		return nil, status.Error(codes.InvalidArgument, "Missing States Configuration")
	}

	probeUUID, err := api.ParseNameProbe(req.Check.Probe)
	if err != nil {
		log.Errorf("unable to extract probe UUID (%s)", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Probe name")
	}
	oUUID, targetUUID, err := api.ParseNameTarget(req.Check.Target)
	if err != nil {
		log.Errorf("unable to extract target UUID (%s)", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Target name")
	}
	if oUUID != organizationUUID {
		log.Errorf("Target is not in Organization")
		return nil, status.Error(codes.InvalidArgument, "Target is not in Organization")
	}

	createTP := &dbconf.TargetProbe{
		OrganizationUUID: organizationUUID,
		TargetUUID:       targetUUID,
		ProbeUUID:        probeUUID,
	}

	if len(req.Check.GetType()) != 0 && len(req.Check.GetConfiguration()) != 0 {
		// makes sure the configuration is the right format
		err = datapipe.ValidateStateConfigurationFormat(req.Check.GetType(), req.Check.GetConfiguration())
		if err != nil {
			log.Errorf("unable to parse the state configuration structure (%s)", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid States Configuration")
		}
		var config map[string]interface{}
		err = json.Unmarshal([]byte(req.Check.Configuration), &config)
		if err != nil {
			log.Errorf("unable to unmarshal the state configuration: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid States Configuration")
		}

		createTP.StateType = req.Check.GetType()
		createTP.StateConfiguration = config
	} else {
		// set placeholders
		createTP.StateType = ""
		createTP.StateConfiguration = map[string]interface{}{}
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	tp, err := db.CreateTargetProbe(createTP)
	if err != nil {
		log.Errorf("failed to create Check: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't create Check")
	}
	if tp == nil {
		log.Errorf("failed to retrieve created Check: %v", createTP)
		return nil, status.Error(codes.Internal, "Couldn't create Check")
	}

	config2, err := json.Marshal(tp.StateConfiguration)
	if err != nil {
		log.Errorf("unable to marshal state configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid States Configuration")
	}
	res = &api.Check{
		Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(tp.UUID)),
		Target:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTarget, util.Base64Encode(tp.TargetUUID)),
		Probe:         path.Join(api.ResourceNameProbe, util.Base64Encode(tp.ProbeUUID)),
		Type:          tp.StateType,
		Configuration: string(config2),
	}

	return
}

// UpdateCheck updates an existing Check and returns it.
func (s *server) UpdateCheck(ctx context.Context, req *api.UpdateCheckRequest) (res *api.Check, err error) {
	// check input
	if len(req.Check.Name) == 0 {
		log.Errorf("missing check name")
		return nil, status.Error(codes.InvalidArgument, "Missing Check name")
	}

	// get the organization and check UUIDs from the name field
	organizationUUID, checkUUID, err := api.ParseNameCheck(req.Check.Name)
	if err != nil {
		log.Errorf("couldn't parse Check name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Check name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	updateTP := &dbconf.TargetProbe{
		UUID:             checkUUID,
		OrganizationUUID: organizationUUID,
	}

	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "name") {
		return nil, status.Error(codes.InvalidArgument, "Check name is read-only")
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "target") {
		if req.Check.GetTarget() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Target")
		}
		oUUID, targetUUID, err := api.ParseNameTarget(req.Check.Target)
		if err != nil {
			log.Errorf("couldn't parse Target name: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid Target name")
		}
		if oUUID != organizationUUID {
			log.Errorf("Target is not in Organization")
			return nil, status.Error(codes.InvalidArgument, "Target is not in Organization")
		}
		updateTP.TargetUUID = targetUUID
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "probe") {
		if req.Check.GetProbe() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Probe")
		}
		probeUUID, err := api.ParseNameProbe(req.Check.Probe)
		if err != nil {
			log.Errorf("couldn't parse Probe name: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid Probe name")
		}
		updateTP.ProbeUUID = probeUUID
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "type") {
		if req.Check.GetType() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Check type")
		}
		updateTP.StateType = req.Check.GetType()
	}

	// If Type is updated, Configuration must also be updated
	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "type") {
		if !util.Contains(req.UpdateMask.Paths, "configuration") {
			log.Errorf("type in update mask without configuration")
			return nil, status.Error(codes.InvalidArgument, "Can't update Check type without States Configuration")
		}
	}

	if req.UpdateMask == nil ||
		(util.Contains(req.UpdateMask.Paths, "configuration") && util.Contains(req.UpdateMask.Paths, "type")) {
		if req.Check.GetType() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Check Type")
		}
		if req.Check.GetConfiguration() == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing States Configuration")
		}
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "configuration") {
		err = json.Unmarshal([]byte(req.Check.Configuration), &updateTP.StateConfiguration)
		if err != nil {
			log.Errorf("Error unmarshaling Configuration: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid States Configuration")
		}
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "type") {
		// makes sure the configuration is the right format
		err = datapipe.ValidateStateConfigurationFormat(req.Check.GetType(), req.Check.GetConfiguration())
		if err != nil {
			log.Errorf("unable to parse the state configuration structure (%s)", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid States Configuration")
		}
	}

	tp, err := db.UpdateTargetProbe(updateTP)
	if err != nil {
		log.Errorf("failed to update Check: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't update Check")
	}

	config, err := json.Marshal(tp.StateConfiguration)
	if err != nil {
		log.Errorf("unable to marshal state configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid States Configuration")
	}
	res = &api.Check{
		Name:          path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameCheck, util.Base64Encode(tp.UUID)),
		Target:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameTarget, util.Base64Encode(tp.TargetUUID)),
		Probe:         path.Join(api.ResourceNameProbe, util.Base64Encode(tp.ProbeUUID)),
		Type:          tp.StateType,
		Configuration: string(config),
	}

	return
}

// DeleteCheck deletes an existing Check, corresponding ProbeConfigurations.
func (s *server) DeleteCheck(ctx context.Context, req *api.DeleteCheckRequest) (res *empty.Empty, err error) {
	if len(req.Name) == 0 {
		log.Errorf("missing check name")
		return nil, status.Error(codes.InvalidArgument, "Missing Check name")
	}

	// get the organization and check UUIDs from the name field
	organizationUUID, checkUUID, err := api.ParseNameCheck(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Check name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Check name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	err = db.DeleteTargetProbe(organizationUUID, checkUUID)
	if err != nil {
		log.Errorf("failed to delete Check: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't delete Check")
	}

	res = &empty.Empty{}
	return
}
