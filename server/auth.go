// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/base64"
	"net/http"
	"path"
	"strings"
	"time"

	"github.com/google/uuid"
	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/session"
	"gitlab.com/pantomath-io/panto/util"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

// A list of methods that don't regquire authorization
var unauthorizedMethods = [...]string{"Login", "GetToken", "GetInfo"}

const sessionIDCookieName = "SessionID"

type sessionKey struct{}
type userUUIDKey struct{}

func setSessionCookie(ctx context.Context, session *session.Session) error {
	expiresHTTP := session.Expires.Format(time.RFC1123)
	return grpc.SendHeader(
		ctx,
		metadata.New(map[string]string{
			"set-cookie": sessionIDCookieName +
				"=" +
				session.UUID.String() +
				"; Expires=" +
				expiresHTTP +
				"; HttpOnly;",
		}),
	)
}

// authInterceptor allow to intercept a RPC call before it triggers
// the service to authenticate the client
func authInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		log.Error("invalid request metadata")
		return nil, status.Error(codes.Internal, "Invalid request metadata")
	}

	// Inject session into context if it can be retrieved from metadata
	var sessErr error
	ctx, sessErr = sessionContextFromMetadata(ctx, md)
	// Check if an internal error occured (other than just a missing session)
	if sessErr != nil {
		if err, ok := status.FromError(sessErr); !ok || err.Code() != codes.Unauthenticated {
			// This is not a gRPC error, or it is not an Unauthenticated status error
			log.Errorf("error while reading session metadata from context")
			return nil, sessErr
		}
	}
	// From here sessErr is nil, or it is a gRPC Unauthenticated status error
	// Get the session from the context (will be nil if Unauthenticated)
	sess := ctx.Value(sessionKey{})

	// Check if the method needs authentication
	_, methodName := path.Split(info.FullMethod)
	authRequired := !util.Contains(unauthorizedMethods[:], methodName)
	if !authRequired {
		return handler(ctx, req)
	}

	// Retrieve auth header from metadata
	auth, ok := md["authorization"]
	if !ok {
		log.Errorf("Missing authorization metadata")
		return nil, status.Error(codes.Unauthenticated, "Missing token")
	}

	// Authenticate panto-agent if found in User-Agent header
	userAgent, ok := util.GetJoinedMetadata(md, "user-agent")
	if ok && strings.HasPrefix(userAgent, "panto-agent/") {
		ctx, err := authenticateAgent(ctx, auth[0][7:])
		if err != nil {
			return nil, err
		}
		return handler(ctx, req)
	}

	// Validate session for a user
	if sess == nil {
		log.Errorf("missing session metadata")
		return nil, sessErr
	}
	if auth == nil || len(auth) == 0 || !strings.HasPrefix(auth[0], "Bearer ") {
		log.Errorf("missing authorization metadata")
		return nil, status.Error(codes.Unauthenticated, "Missing token")
	}

	// Authenticate client with session key
	err := authenticateSessionToken(ctx, auth[0][7:])
	if err != nil {
		return nil, err
	}

	res, err := handler(ctx, req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func authenticateAgent(ctx context.Context, agentName string) (context.Context, error) {
	organizationUUID, agentUUID, err := api.ParseNameAgent(agentName)
	if err != nil {
		log.Errorf("Invalid agent %s (%s)", agentName, err)
		return ctx, status.Errorf(codes.Unauthenticated, "Invalid Agent name")
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return ctx, status.Error(codes.Internal, "Internal error")
	}

	a, err := dbconf.GetAgent(organizationUUID, agentUUID)
	// a is nil if agent is not found
	if a == nil || err != nil {
		log.Errorf("Unknown agent %s (%s)", agentUUID, err)
		return ctx, status.Errorf(codes.Unauthenticated, "Unknown Agent")
	}

	log.Debugf("Authenticated agent [uuid: %v] %s", a.UUID, a.DisplayName)

	ctx = context.WithValue(ctx, agentUUIDKey{}, agentUUID)
	ctx = context.WithValue(ctx, organizationUUIDKey{}, organizationUUID)

	return ctx, nil
}

func sessionContextFromMetadata(ctx context.Context, md metadata.MD) (context.Context, error) {
	sessionMgr, err := services.GetSessionManager()
	if err != nil {
		log.Errorf("couldn't get session manager: %s", err)
		return ctx, status.Error(codes.Internal, "Internal error")
	}
	cookies := cookiesFromMetadata(md)
	sessionID, ok := cookies[sessionIDCookieName]
	if !ok {
		log.Errorf("missing session UUID cookie")
		return ctx, status.Error(codes.Unauthenticated, "Missing session ID")
	}
	sessionUUID, err := uuid.Parse(sessionID)
	if err != nil {
		log.Errorf("unable to parse session UUID: %s", err)
		return ctx, status.Error(codes.Unauthenticated, "Invalid session ID")
	}

	s, err := sessionMgr.GetSession(sessionUUID)
	if err != nil {
		log.Errorf("error retrieving session: %s", err)
		return ctx, status.Error(codes.Internal, "Internal error")
	}
	if s == nil {
		log.Errorf("session not found: %s", sessionUUID)
		return ctx, status.Error(codes.Unauthenticated, "Session not found")
	}

	ctx = context.WithValue(ctx, sessionKey{}, s)
	ctx = context.WithValue(ctx, userUUIDKey{}, s.UserUUID)
	ctx = context.WithValue(ctx, organizationUUIDKey{}, s.OrganizationUUID)

	return ctx, nil
}

func authenticateSessionToken(ctx context.Context, token string) error {
	s, ok := ctx.Value(sessionKey{}).(*session.Session)
	if !ok {
		log.Errorf("invalid session data")
		return status.Error(codes.Unauthenticated, "Session not found")
	}
	if s == nil {
		log.Errorf("session not found")
		return status.Error(codes.Unauthenticated, "Session not found")
	}
	if s.Expires.Before(time.Now()) {
		log.Errorf("session expired on %s", s.Expires.Format(time.RFC3339))
		return status.Error(codes.Unauthenticated, "Session expired")
	}
	key := base64.RawURLEncoding.EncodeToString(s.Key)
	if token != key {
		log.Errorf("session key does not match")
		return status.Error(codes.Unauthenticated, "Invalid token")
	}

	// Refresh session after authentication
	sessionMgr, err := services.GetSessionManager()
	if err != nil {
		log.Errorf("couldn't get session manager: %s", err)
		return status.Error(codes.Internal, "Internal error")
	}
	s, err = sessionMgr.RefreshSession(s.UUID, session.DefaultTimeout)
	if err != nil {
		log.Errorf("couldn't refresh session: %s", err)
	} else {
		setSessionCookie(ctx, s)
	}

	return nil
}

func cookiesFromMetadata(md metadata.MD) map[string]string {
	// HACK: There is no stand-alone cookie parsing function in the stdlib,
	// see https://github.com/golang/go/issues/25194
	// Use this hack: https://stackoverflow.com/a/33926065/769262

	header := http.Header{}
	for _, cookieString := range md.Get("cookie") {
		header.Add("Cookie", cookieString)
	}
	request := http.Request{Header: header}

	m := make(map[string]string)
	for _, c := range request.Cookies() {
		m[c.Name] = c.Value
	}

	return m
}
