// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/json"
	"strings"
	"testing"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
	"google.golang.org/genproto/protobuf/field_mask"
)

func TestListChecks(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListChecksRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
	}
	res, err := s.ListChecks(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list checks: %s", err)
	} else {
		if len(res.Checks) != 10 {
			t.Fatalf("Unexpected checks count: got %d, expected %d", len(res.Checks), 10)
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestListChecksWithNesting(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	{
		req := &api.ListChecksRequest{
			Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		}
		res, err := s.ListChecks(context.Background(), req)
		if err != nil {
			t.Fatalf("Failed to list checks: %s", err)
		} else {
			if len(res.Checks) != 10 {
				t.Fatalf("Unexpected checks count: got %d, expected %d", len(res.Checks), 10)
			} else if res.Checks[0].Children != nil {
				t.Fatalf("Unexpected children, got %#v", res.Checks[0].Children)
			}
		}
	}
	{
		req := &api.ListChecksRequest{
			Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
			ChildrenMask: &field_mask.FieldMask{
				Paths: []string{"target", "probe"},
			},
		}
		res, err := s.ListChecks(context.Background(), req)
		if err != nil {
			t.Fatalf("Failed to list checks: %s", err)
		} else {
			if len(res.Checks) != 10 {
				t.Fatalf("Unexpected checks count: got %d, expected %d", len(res.Checks), 10)
			} else {
				if res.Checks[0].Children == nil {
					t.Fatalf("Expected children, got nothing ¯\\_(ツ)_/¯")
				}
				if res.Checks[0].Children.Target == nil {
					t.Fatalf("Expected children Target, got nothing ¯\\_(ツ)_/¯")
				}
				if res.Checks[0].Children.Probe == nil {
					t.Fatalf("Expected children Probe, got nothing ¯\\_(ツ)_/¯")
				}
			}
		}
	}
	{
		req := &api.ListChecksRequest{
			Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
			ChildrenMask: &field_mask.FieldMask{
				Paths: []string{"target"},
			},
		}
		res, err := s.ListChecks(context.Background(), req)
		if err != nil {
			t.Fatalf("Failed to list checks: %s", err)
		} else {
			if len(res.Checks) != 10 {
				t.Fatalf("Unexpected checks count: got %d, expected %d", len(res.Checks), 10)
			} else {
				if res.Checks[0].Children == nil {
					t.Fatalf("Expected children, got nothing ¯\\_(ツ)_/¯")
				}
				if res.Checks[0].Children.Target == nil {
					t.Fatalf("Expected children Target, got nothing ¯\\_(ツ)_/¯")
				}
				if res.Checks[0].Children.Probe != nil {
					t.Fatalf("Unexpected children Probe, got %#v", res.Checks[0].Children.Probe)
				}
			}
		}
	}
}

func TestListChecksPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test pagination
	req := &api.ListChecksRequest{
		Parent:   "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		PageSize: 4,
	}
	res, err := s.ListChecks(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list checks: %s", err)
	} else {
		if len(res.Checks) != 4 {
			t.Fatalf("Unexpected checks count: got %d, expected %d", len(res.Checks), 4)
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 2
	page2Token := res.NextPageToken
	req.PageToken = res.NextPageToken
	res, err = s.ListChecks(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list checks: %s", err)
	} else {
		if len(res.Checks) != 4 {
			t.Fatalf("Unexpected checks count: got %d, expected %d", len(res.Checks), 4)
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 3
	req.PageToken = res.NextPageToken
	res, err = s.ListChecks(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list checks: %s", err)
	} else {
		if len(res.Checks) != 2 {
			t.Fatalf("Unexpected checks count: got %d, expected %d", len(res.Checks), 2)
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}

	// Invalid page token
	req.PageToken = page2Token[1:]
	_, err = s.ListChecks(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed checks with invalid token")
	}

	// Invalid request
	req.PageSize = 10
	req.PageToken = page2Token
	_, err = s.ListChecks(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed checks with invalid request")
	}
}

func TestCreateCheck_WithEmptyValues(t *testing.T) {
	// BUGFIX: #259
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.CreateCheckRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		Check: &api.Check{
			Target: "organizations/KTCCC60hRgWDtGnLxb_I9Q/targets/wCSdWuxMRU6E_GhtyIrSpg",
			Probe:  "probes/xUkSnG_BSVCn-AX1khPLYg",
		},
	}
	res, err := s.CreateCheck(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to create check: %s", err)
	}
	if !strings.HasPrefix(res.Name, "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks") ||
		res.Probe != req.Check.Probe ||
		res.Target != req.Check.Target ||
		res.Type != "" ||
		res.Configuration != "{}" {
		b, _ := json.Marshal(res)
		t.Fatalf("unexpected result: %s", string(b))
	}
}

func TestUpdateCheck_WithInvalidConfiguration(t *testing.T) {
	// https://gitlab.com/pantomath-io/panto/issues/288
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.UpdateCheckRequest{
		Check: &api.Check{
			Name: "organizations/KTCCC60hRgWDtGnLxb_I9Q/checks/FKbFtQWgTsKtNqPEP-XUcw",
			// Configuration should be a JSON object with a "states" property containing the array, not the array directly
			Configuration: `[{"state":"critical","match":"any","conditions":[{"field":"avg","operator":"lt","value":"10","reason":"foobar"}]},{"state":"warning","match":"all","conditions":[{"field":null,"operator":null,"value":0,"reason":null}]}]`,
			Type:          "threshold",
		},
		UpdateMask: &field_mask.FieldMask{
			Paths: []string{"configuration", "type"},
		},
	}
	res, err := s.UpdateCheck(context.Background(), req)
	if err == nil {
		b, _ := json.Marshal(res)
		t.Fatalf("expected error, got %s", string(b))
	}
}
