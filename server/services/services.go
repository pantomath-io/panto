// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package services manages "service discovery". It is currently implemented as a set of default singletons.
// Calling one of the `Get` functions returns a lazily-created instance of a service. All getters are concurrency-safe
// and can be used from several goroutines.
package services

import (
	"fmt"
	"sync"

	"github.com/op/go-logging"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/session"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"
)

var log = logging.MustGetLogger("main")

// This file consists in a number of functions to get singletons of the main services. They are lazily
// instantiated on call, and concurrency-safe and can therefore be called from anywhere in the code.

var tsdbMutex sync.Mutex
var tsdbInstance tsdb.TSDB

// GetTSDB returns the main instance to the TSDB service. It is instantiated lazily. Safe for concurrent use.
func GetTSDB() (tsdb.TSDB, error) {
	var err error
	tsdbMutex.Lock()
	defer tsdbMutex.Unlock()
	if tsdbInstance == nil {
		tsdbInstance, err = tsdb.NewInfluxTSDB(
			viper.GetString("influxdb.address"),
			viper.GetString("influxdb.database"),
		)
		if err == nil {
			log.Debugf("Connected to TSDB on %s, using database \"%s\"", viper.GetString("influxdb.address"), viper.GetString("influxdb.database"))
		} else {
			tsdbInstance = nil
		}
	}
	return tsdbInstance, err
}

var dbconfMutex sync.Mutex
var dbconfInstance dbconf.ConfigurationDB

// GetDBConf returns the main instance to the TSDB service. It is instantiated lazily. Safe for concurrent use.
func GetDBConf() (dbconf.ConfigurationDB, error) {
	var err error
	dbconfMutex.Lock()
	defer dbconfMutex.Unlock()
	if dbconfInstance == nil {
		dbPath := viper.GetString("db.path")
		dbPath, err = util.ExpandAbs(dbPath)
		if err != nil {
			return nil, fmt.Errorf("unable to resolve database path: %s", err)
		}
		dbconfInstance, err = dbconf.NewSQLiteDB(dbPath)
		if err == nil {
			log.Debugf("Connected to DBConf %s", dbPath)
		} else {
			dbconfInstance = nil
		}
	}
	return dbconfInstance, err
}

var sessionMgrMutex sync.Mutex
var sessionMgrInstance session.Manager

// GetSessionManager returns the main session manager. It is instantiated lazily. Safe for concurrent use.
func GetSessionManager() (session.Manager, error) {
	var err error
	sessionMgrMutex.Lock()
	defer sessionMgrMutex.Unlock()
	if sessionMgrInstance == nil {
		sessionMgrInstance, err = session.NewInMemory(session.InMemoryConfig{})
		if err != nil {
			return nil, fmt.Errorf("unable to create session manager: %s", err)
		}
	}
	return sessionMgrInstance, err
}
