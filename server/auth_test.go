// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/base64"
	"fmt"
	"math/rand"
	"path"
	"reflect"
	"testing"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/session"
	"gitlab.com/pantomath-io/panto/util"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func TestAuthInterceptor(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	handlerCalled := false
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		handlerCalled = true
		return nil, nil
	}

	getTokenHandler := func(ctx context.Context, req interface{}) (interface{}, error) {
		s := server{}
		handlerCalled = true
		return s.GetToken(ctx, &empty.Empty{})
	}

	sessionMgr, _ := services.GetSessionManager()
	sess, _ := sessionMgr.GetSession(session1.UUID)
	sessMD := metadata.Pairs("cookie", sessionIDCookieName+"="+sess.UUID.String())
	authMD := metadata.Pairs("authorization", "Bearer "+base64.RawURLEncoding.EncodeToString(sess.Key))
	badSessMD := metadata.Pairs("cookie", sessionIDCookieName+"="+uuid.New().String())
	badAuthMD := metadata.Pairs("authorization", "Bearer "+base64.RawURLEncoding.EncodeToString(sess.UUID[:]))

	agentName := path.Join(
		api.ResourceNameOrganization, util.Base64Encode(organization),
		api.ResourceNameAgent, util.Base64Encode(agent1),
	)
	badAgentName := path.Join(
		api.ResourceNameOrganization, util.Base64Encode(organization),
		api.ResourceNameAgent, util.Base64Encode(sess.UUID),
	)
	agentMD := metadata.Pairs("authorization", "Bearer "+agentName, "user-agent", "panto-agent/1.0.0")
	badAgentMD := metadata.Pairs("authorization", "Bearer "+badAgentName, "user-agent", "panto-agent/1.0.0")

	type testCase struct {
		method                       string
		md                           metadata.MD
		handler                      func(ctx context.Context, req interface{}) (interface{}, error)
		expectSuccess, expectHandler bool
	}
	cases := []testCase{
		// Unauthorized method, no metadata
		{"Login", metadata.MD{}, handler, true, true},
		// Unauthorized method, partially valid metadata
		{"Login", sessMD, handler, true, true},
		// Unauthorized method, partially valid metadata
		{"Login", authMD, handler, true, true},
		// Unauthorized method, partially valid/invalid metadata
		{"Login", metadata.Join(sessMD, badAuthMD), handler, true, true},
		// Unauthorized method, partially valid/invalid metadata
		{"Login", metadata.Join(badSessMD, authMD), handler, true, true},
		// Unauthorized method, partially invalid metadata
		{"Login", metadata.Join(badSessMD, badAuthMD), handler, true, true},
		// Unauthorized method, invalid agent metadata
		{"Login", badAgentMD, handler, true, true},
		// Unauthorized method, valid agent metadata
		{"Login", agentMD, handler, true, true},
		// Unauthorized method, valid metadata
		{"Login", metadata.Join(sessMD, authMD), handler, true, true},

		// Authorized method, no metadata
		{"ListOrganizations", metadata.MD{}, handler, false, false},
		// Authorized method, partially valid metadata
		{"ListOrganizations", sessMD, handler, false, false},
		// Authorized method, partially valid metadata
		{"ListOrganizations", authMD, handler, false, false},
		// Authorized method, partially valid/invalid metadata
		{"ListOrganizations", metadata.Join(sessMD, badAuthMD), handler, false, false},
		// Authorized method, partially valid/invalid metadata
		{"ListOrganizations", metadata.Join(badSessMD, authMD), handler, false, false},
		// Authorized method, partially invalid metadata
		{"ListOrganizations", metadata.Join(badSessMD, badAuthMD), handler, false, false},
		// Authorized method, invalid agent metadata
		{"ListOrganizations", badAgentMD, handler, false, false},
		// Authorized method, valid agent metadata
		{"ListOrganizations", agentMD, handler, true, true},
		// Authorized method, valid metadata
		{"ListOrganizations", metadata.Join(sessMD, authMD), handler, true, true},

		// GetToken method, no metadata
		{"GetToken", metadata.MD{}, getTokenHandler, false, true},
		// GetToken method, partially valid metadata
		{"GetToken", sessMD, getTokenHandler, true, true},
		// GetToken method, partially valid metadata
		{"GetToken", authMD, getTokenHandler, false, true},
		// GetToken method, partially valid/invalid metadata
		{"GetToken", metadata.Join(sessMD, badAuthMD), getTokenHandler, true, true},
		// GetToken method, partially valid/invalid metadata
		{"GetToken", metadata.Join(badSessMD, authMD), getTokenHandler, false, true},
		// GetToken method, partially invalid metadata
		{"GetToken", metadata.Join(badSessMD, badAuthMD), getTokenHandler, false, true},
		// GetToken method, invalid agent metadata
		{"GetToken", badAgentMD, getTokenHandler, false, true},
		// GetToken method, valid agent metadata
		{"GetToken", agentMD, getTokenHandler, false, true},
		// GetToken method, valid metadata
		{"GetToken", metadata.Join(sessMD, authMD), getTokenHandler, true, true},
	}

	for _, c := range cases {
		handlerCalled = false
		ctx := metadata.NewIncomingContext(context.Background(), c.md)
		_, err := authInterceptor(
			ctx,
			nil,
			&grpc.UnaryServerInfo{FullMethod: c.method},
			c.handler,
		)
		if c.expectSuccess {
			if err != nil {
				t.Fatalf("Unexpected error: %s", err)
			}
		} else {
			if err == nil {
				t.Fatalf("Expected error, got success")
			}
		}
		if handlerCalled {
			if !c.expectHandler {
				t.Fatalf("Expected handler not to be called")
			}
		} else {
			if c.expectHandler {
				t.Fatalf("Expected handler to be called")
			}
		}
	}
}

func TestAuthInterceptorLive(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	md := metadata.Pairs(
		"cookie",
		sessionIDCookieName+"="+session1.UUID.String(),
	)
	ctx := metadata.NewIncomingContext(context.Background(), md)

	s := server{}

	_, err := authInterceptor(
		ctx,
		&empty.Empty{},
		&grpc.UnaryServerInfo{FullMethod: "GetToken"},
		func(ctx context.Context, req interface{}) (interface{}, error) {
			return s.GetToken(ctx, req.(*empty.Empty))
		},
	)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err)
	}
}

func TestAuthenticateAgent(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	ctx := metadata.NewIncomingContext(context.Background(), metadata.MD{})
	agentName := path.Join(
		api.ResourceNameOrganization, util.Base64Encode(organization),
		api.ResourceNameAgent, util.Base64Encode(agent1),
	)
	ctx, err := authenticateAgent(ctx, agentName)
	if err != nil {
		log.Fatalf("unexpected error: %s", err)
	}
	organizationUUID := ctx.Value(organizationUUIDKey{})
	if organizationUUID != organization {
		log.Errorf("unexpected organization UUID: %s", organizationUUID)
	}
	agentUUID := ctx.Value(agentUUIDKey{})
	if agentUUID != agent1 {
		log.Errorf("unexpected agent UUID: %s", agentUUID)
	}
}

func TestSessionContextFromMetadata(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	md := metadata.MD{}
	ctx := metadata.NewIncomingContext(
		context.Background(),
		md,
	)
	_, err := sessionContextFromMetadata(ctx, md)
	if err == nil {
		t.Fatalf("Expected error for missing session cookie")
	}

	md.Set("cookie", "toto")
	_, err = sessionContextFromMetadata(ctx, md)
	if err == nil {
		t.Fatalf("Expected error for invalid session cookie")
	}

	md.Set("cookie", sessionIDCookieName+"=toto")
	_, err = sessionContextFromMetadata(ctx, md)
	if err == nil {
		t.Fatalf("Expected error for invalid UUID in session cookie")
	}

	md.Set("cookie", sessionIDCookieName+"="+uuid.New().String())
	_, err = sessionContextFromMetadata(ctx, md)
	if err == nil {
		t.Fatalf("Expected error for no session found")
	}

	md.Set("cookie", sessionIDCookieName+"="+uuid.New().String())
	_, err = sessionContextFromMetadata(ctx, md)
	if err == nil {
		t.Fatalf("Expected error for no session found")
	}

	md.Set("cookie", sessionIDCookieName+"="+session1.UUID.String())
	ctx, err = sessionContextFromMetadata(ctx, md)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err)
	}
	s := ctx.Value(sessionKey{}).(*session.Session)
	if s == nil {
		t.Fatalf("Expected session, got: %#v", ctx.Value(sessionKey{}))
	}
	s.Expires = session1.Expires
	if !reflect.DeepEqual(s, session1) {
		t.Fatalf("expected %#v, got %#v", session1, s)
	}
	userUUID := ctx.Value(userUUIDKey{}).(uuid.UUID)
	if userUUID != s.UserUUID {
		t.Fatalf("Expected %s, got: %s", s.UserUUID, ctx.Value(userUUIDKey{}))
	}
}

func TestAuthenticateSessionToken(t *testing.T) {
	md := metadata.MD{}
	ctx := metadata.NewIncomingContext(
		context.Background(),
		md,
	)

	err := authenticateSessionToken(ctx, "")
	if err == nil {
		t.Fatalf("Expected error for missing session")
	}

	md.Set("cookie", sessionIDCookieName+"="+uuid.New().String())
	ctx, err = sessionContextFromMetadata(ctx, md)
	err = authenticateSessionToken(ctx, "")
	if err == nil {
		t.Fatalf("Expected error for session not found")
	}

	md.Set("cookie", sessionIDCookieName+"="+session1.UUID.String())
	ctx, err = sessionContextFromMetadata(ctx, md)
	err = authenticateSessionToken(ctx, "")
	if err == nil {
		t.Fatalf("Expected error for mismatching token")
	}

	var token [32]byte
	n, err := rand.Read(token[:])
	if err != nil {
		panic("unexpected error generating random token" + err.Error())
	}
	if n != 32 {
		panic(fmt.Sprintf("unexpected number of bytes generating random token: %d", n))
	}
	err = authenticateSessionToken(ctx, base64.RawURLEncoding.EncodeToString(token[:]))
	if err == nil {
		t.Fatalf("Expected error for mismatched token")
	}

	err = authenticateSessionToken(ctx, base64.RawURLEncoding.EncodeToString(session1.Key))
	if err != nil {
		t.Fatalf("Unexpected error: %s", err)
	}
	s := ctx.Value(sessionKey{})
	s.(*session.Session).Expires = session1.Expires
	if !reflect.DeepEqual(s.(*session.Session), session1) {
		t.Fatalf("expected %#v, got %#v", session1, s)
	}
}

func TestCookiesFromMetadata(t *testing.T) {
	md := metadata.Pairs("cookie", "hello=world")

	expect := map[string]string{"hello": "world"}
	got := cookiesFromMetadata(md)
	if !reflect.DeepEqual(expect, got) {
		t.Errorf("expected %#v, got %#v", expect, got)
	}

	md.Append("cookie", "barbes=rochechouart")
	expect["barbes"] = "rochechouart"
	got = cookiesFromMetadata(md)
	if !reflect.DeepEqual(expect, got) {
		t.Errorf("expected %#v, got %#v", expect, got)
	}

	md.Append("cookie", "toto=3000; hello=panto")
	expect["toto"] = "3000"
	expect["hello"] = "panto"
	got = cookiesFromMetadata(md)
	if !reflect.DeepEqual(expect, got) {
		t.Errorf("expected %#v, got %#v", expect, got)
	}
}
