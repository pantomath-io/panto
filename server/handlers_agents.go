// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"path"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/empty"
	tspb "github.com/golang/protobuf/ptypes/timestamp"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"
)

// ListAgents returns all the agents from an organization
func (s *server) ListAgents(ctx context.Context, req *api.ListAgentsRequest) (res *api.ListAgentsResponse, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	ts, err := services.GetTSDB()
	if err != nil {
		log.Errorf("Couldn't find TSDB service: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbAgents, err := db.ListAgents(organizationUUID, pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list Agents: %s", err)
		return nil, status.Error(codes.NotFound, "No Agents found")
	}

	var nextPage bool
	if int32(len(dbAgents)) > pageSize {
		dbAgents = dbAgents[:pageSize]
		nextPage = true
	}

	agents := make([]*api.Agent, len(dbAgents))
	for i, agent := range dbAgents {
		t, v, err := getAgentActivity(organizationUUID, agent.UUID, ts)
		if err != nil {
			log.Errorf("%s", err)
		}
		agents[i] = &api.Agent{
			Name: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
				api.ResourceNameAgent, util.Base64Encode(agent.UUID),
			),
			DisplayName:  agent.DisplayName,
			LastActivity: t,
			LastVersion:  v,
		}
	}

	res = &api.ListAgentsResponse{
		Agents: agents,
	}

	if nextPage {
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+uint64(len(agents)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetAgent returns a specific Agent.
func (s *server) GetAgent(ctx context.Context, req *api.GetAgentRequest) (*api.Agent, error) {
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	ts, err := services.GetTSDB()
	if err != nil {
		log.Errorf("Couldn't find TSDB service: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if len(req.Name) == 0 {
		log.Errorf("missing agent name")
		return nil, status.Error(codes.InvalidArgument, "Missing Agent name")
	}

	// get the organization and agent UUIDs from the name field
	organizationUUID, agentUUID, err := api.ParseNameAgent(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Agent name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Agent name")
	}

	dbAgent, err := db.GetAgent(organizationUUID, agentUUID)
	if err != nil {
		log.Errorf("couldn't retrieve agent: %s", err)
		return nil, status.Error(codes.Internal, "Agent not found")
	}
	if dbAgent == nil {
		log.Errorf("agent not found: %s", req.Name)
		return nil, status.Error(codes.NotFound, "Agent not found")
	}

	t, v, err := getAgentActivity(organizationUUID, agentUUID, ts)
	if err != nil {
		log.Errorf("%s", err)
	}

	res := api.Agent{
		Name: path.Join(
			api.ResourceNameOrganization,
			util.Base64Encode(organizationUUID),
			api.ResourceNameAgent,
			util.Base64Encode(dbAgent.UUID),
		),
		DisplayName:  dbAgent.DisplayName,
		LastActivity: t,
		LastVersion:  v,
	}

	return &res, nil
}

// CreateAgent inserts a new Agent in an Organization
func (s *server) CreateAgent(ctx context.Context, req *api.CreateAgentRequest) (res *api.Agent, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if req.GetAgent() == nil {
		log.Errorf("missing agent object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing Agent")
	}

	if len(req.Agent.GetDisplayName()) == 0 {
		log.Errorf("missing display_name in agent object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing display name")
	}

	dbAgent, err := db.CreateAgent(&dbconf.Agent{
		OrganizationUUID: organizationUUID,
		DisplayName:      req.Agent.DisplayName,
	})
	if err != nil {
		log.Errorf("failed to create Agent: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't create Agent")
	}

	res = &api.Agent{
		Name:        path.Join(api.ResourceNameOrganization, util.Base64Encode(organizationUUID), api.ResourceNameAgent, util.Base64Encode(dbAgent.UUID)),
		DisplayName: dbAgent.DisplayName,
	}

	return
}

// UpdateAgent updates an existing Agent and returns it.
func (s *server) UpdateAgent(ctx context.Context, req *api.UpdateAgentRequest) (*api.Agent, error) {
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	ts, err := services.GetTSDB()
	if err != nil {
		log.Errorf("Couldn't find TSDB service: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if len(req.Agent.Name) == 0 {
		log.Error("missing agent name")
		return nil, status.Error(codes.InvalidArgument, "Missing Agent name")
	}

	agent := req.Agent
	if agent == nil {
		log.Error("missing agent")
		return nil, status.Error(codes.InvalidArgument, "Missing Agent")
	}

	// get the organization and agent UUIDs from the name field
	organizationUUID, agentUUID, err := api.ParseNameAgent(req.Agent.Name)
	if err != nil {
		log.Errorf("couldn't parse Agent name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Agent name")
	}

	updateAgent := &dbconf.Agent{
		UUID:             agentUUID,
		OrganizationUUID: organizationUUID,
	}

	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "name") {
		return nil, status.Error(codes.InvalidArgument, "Agent name is read-only")
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "display_name") {
		if agent.DisplayName == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing agent display name")
		}
		updateAgent.DisplayName = agent.DisplayName
	}

	dbAgent, err := db.UpdateAgent(updateAgent)
	if err != nil {
		log.Errorf("failed to update agent: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't update Agent")
	}

	t, v, err := getAgentActivity(organizationUUID, agentUUID, ts)
	if err != nil {
		log.Errorf("%s", err)
	}

	res := api.Agent{
		Name: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
			api.ResourceNameAgent, util.Base64Encode(agentUUID),
		),
		DisplayName:  dbAgent.DisplayName,
		LastActivity: t,
		LastVersion:  v,
	}

	return &res, nil
}

// DeleteAgent deletes an existing Agent and corresponding ProbeConfigurations.
func (s *server) DeleteAgent(ctx context.Context, req *api.DeleteAgentRequest) (*empty.Empty, error) {
	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if len(req.Name) == 0 {
		log.Error("missing agent name")
		return nil, status.Error(codes.InvalidArgument, "Missing Agent name")
	}

	// get the organization and agent UUIDs from the name field
	organizationUUID, agentUUID, err := api.ParseNameAgent(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Agent name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Agent name")
	}

	err = db.DeleteAgent(organizationUUID, agentUUID)
	if err != nil {
		log.Errorf("failed to delete agent: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't delete Agent")
	}

	return new(empty.Empty), nil
}

// getAgentActivity returns the agent's most recent activity
func getAgentActivity(organizationUUID, agentUUID uuid.UUID, ts tsdb.TSDB) (*tspb.Timestamp, string, error) {
	// get last activity entry for this agent
	var lastActivity time.Time
	activities, err := ts.GetActivity(
		organizationUUID,
		agentUUID,
		tsdb.Count(1),
		tsdb.OrderBy(tsdb.OrderDescending))
	if err != nil {
		return nil, "", fmt.Errorf("unable to get activity for agent %s: %s", agentUUID.String(), err)
	} else if len(activities) == 0 {
		return nil, "", fmt.Errorf("no activity for agent %s", agentUUID)
	}
	t, err := ptypes.TimestampProto(activities[0].Timestamp)
	if err != nil {
		return nil, "", fmt.Errorf("Could not convert last activity timestamp (%s): %s", lastActivity, err)
	}

	return t, activities[0].Version, nil
}
