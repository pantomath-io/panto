// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"reflect"
	"testing"

	"gitlab.com/pantomath-io/panto/api"
)

func TestDeprecated389(t *testing.T) {
	{
		got := &api.Check{
			Type:          "none",
			Configuration: "{}",
		}
		expect := &api.Check{
			Type:          "none",
			Configuration: "{}",
		}
		m := make(map[string]struct{})
		expectMap := map[string]struct{}{}
		deprecated389(m, got)
		if !reflect.DeepEqual(expect, got) {
			t.Fatalf("expected %s, got %s", expect, got)
		}
		if !reflect.DeepEqual(expectMap, m) {
			t.Fatalf("expected deprecation list %s, got %s", expectMap, m)
		}
	}

	{
		got := &api.Check{
			StateType:     "none",
			Configuration: "{}",
		}
		expect := &api.Check{
			Type:          "none",
			Configuration: "{}",
		}
		m := make(map[string]struct{})
		expectMap := map[string]struct{}{"Check.state_type": {}}
		deprecated389(m, got)
		if !reflect.DeepEqual(expect, got) {
			t.Fatalf("expected %s, got %s", expect, got)
		}
		if !reflect.DeepEqual(expectMap, m) {
			t.Fatalf("expected deprecation list %s, got %s", expectMap, m)
		}
	}

	{
		got := &api.Check{
			StateType:          "none",
			StateConfiguration: "{}",
		}
		expect := &api.Check{
			Type:          "none",
			Configuration: "{}",
		}
		m := make(map[string]struct{})
		expectMap := map[string]struct{}{"Check.state_type": {}, "Check.state_configuration": {}}
		deprecated389(m, got)
		if !reflect.DeepEqual(expect, got) {
			t.Fatalf("expected %s, got %s", expect, got)
		}
		if !reflect.DeepEqual(expectMap, m) {
			t.Fatalf("expected deprecation list %s, got %s", expectMap, m)
		}
	}

	{
		got := &api.CreateCheckRequest{
			Check: &api.Check{
				StateType:          "none",
				StateConfiguration: "{}",
			},
		}
		expect := &api.CreateCheckRequest{
			Check: &api.Check{
				Type:          "none",
				Configuration: "{}",
			},
		}
		m := make(map[string]struct{})
		expectMap := map[string]struct{}{"Check.state_type": {}, "Check.state_configuration": {}}
		deprecated389(m, got)
		if !reflect.DeepEqual(expect, got) {
			t.Fatalf("expected %s, got %s", expect, got)
		}
		if !reflect.DeepEqual(expectMap, m) {
			t.Fatalf("expected deprecation list %s, got %s", expectMap, m)
		}
	}
}
