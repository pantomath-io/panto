// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"testing"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/util"
)

func TestListTargets(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	req := &api.ListTargetsRequest{
		Parent: "organizations/KTCCC60hRgWDtGnLxb_I9Q",
	}
	res, err := s.ListTargets(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list targets: %s", err)
	} else {
		if len(res.Targets) != 10 {
			t.Fatalf("Unexpected targets count: got %d, expected %d", len(res.Targets), 10)
		}
		for i, target := range res.Targets {
			expect := fmt.Sprintf("target%d.pantomath.io", i)
			if target.Address != expect {
				t.Fatalf("Unexpected target at index %d: got %v, expected %v", i, target.Address, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}
}

func TestListTargetsPagination(t *testing.T) {
	if !util.LiveTest(t) {
		t.Skip("Skipping live test...")
	}

	s := server{}

	// Test pagination
	req := &api.ListTargetsRequest{
		Parent:   "organizations/KTCCC60hRgWDtGnLxb_I9Q",
		PageSize: 4,
	}
	res, err := s.ListTargets(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list targets: %s", err)
	} else {
		if len(res.Targets) != 4 {
			t.Fatalf("Unexpected targets count: got %d, expected %d", len(res.Targets), 4)
		}
		for i, target := range res.Targets {
			expect := fmt.Sprintf("target%d.pantomath.io", i)
			if target.Address != expect {
				t.Fatalf("Unexpected target at index %d: got %v, expected %v", i, target.Address, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 2
	page2Token := res.NextPageToken
	req.PageToken = res.NextPageToken
	res, err = s.ListTargets(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list targets: %s", err)
	} else {
		if len(res.Targets) != 4 {
			t.Fatalf("Unexpected targets count: got %d, expected %d", len(res.Targets), 4)
		}
		for i, target := range res.Targets {
			expect := fmt.Sprintf("target%d.pantomath.io", i+4)
			if target.Address != expect {
				t.Fatalf("Unexpected target at index %d: got %v, expected %v", i, target.Address, expect)
			}
		}
		if res.NextPageToken == "" {
			t.Fatalf("Missing next page token: %s", res.NextPageToken)
		}
	}

	// Page 3
	req.PageToken = res.NextPageToken
	res, err = s.ListTargets(context.Background(), req)
	if err != nil {
		t.Fatalf("Failed to list targets: %s", err)
	} else {
		if len(res.Targets) != 2 {
			t.Fatalf("Unexpected targets count: got %d, expected %d", len(res.Targets), 2)
		}
		for i, target := range res.Targets {
			expect := fmt.Sprintf("target%d.pantomath.io", i+8)
			if target.Address != expect {
				t.Fatalf("Unexpected target at index %d: got %v, expected %v", i, target.Address, expect)
			}
		}
		if res.NextPageToken != "" {
			t.Fatalf("Unexpected next page token: %s", res.NextPageToken)
		}
	}

	// Invalid page token
	req.PageToken = page2Token[1:]
	_, err = s.ListTargets(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed targets with invalid token")
	}

	// Invalid request
	req.PageSize = 10
	req.PageToken = page2Token
	_, err = s.ListTargets(context.Background(), req)
	if err == nil {
		t.Fatalf("Successfully listed targets with invalid request")
	}
}
