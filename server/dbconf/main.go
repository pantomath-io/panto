// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"database/sql/driver"
	"time"

	"github.com/google/uuid"
	logging "github.com/op/go-logging"
)

// ConfigurationDB represents an abstract SQL database.
type ConfigurationDB interface {
	ListOrganizations(count int32, offset uint64) ([]*Organization, error)
	GetOrganization(organizationUUID uuid.UUID) (*Organization, error)
	UpdateOrganization(organization *Organization) (*Organization, error)
	CreateOrganization(organization *Organization) (*Organization, error)

	ListAgents(organizationUUID uuid.UUID, count int32, offset uint64) ([]*Agent, error)
	GetAgent(organizationUUID, agentUUID uuid.UUID) (*Agent, error)
	CreateAgent(agent *Agent) (*Agent, error)
	UpdateAgent(agent *Agent) (*Agent, error)
	DeleteAgent(organizationUUID, agentUUID uuid.UUID) error

	ListTargets(organizationUUID uuid.UUID, count int32, offset uint64) ([]*Target, error)
	GetTarget(organizationUUID, targetUUID uuid.UUID) (*Target, error)
	CreateTarget(target *Target) (*Target, error)
	UpdateTarget(target *Target) (*Target, error)
	DeleteTarget(organizationUUID, targetUUID uuid.UUID) error

	ListTargetProbes(organizationUUID uuid.UUID, targetUUIDs, probeUUIDs []uuid.UUID, count int32, offset uint64) ([]*TargetProbe, error)
	GetTargetProbe(organizationUUID, targetProbeUUID uuid.UUID) (*TargetProbe, error)
	CreateTargetProbe(targetProbe *TargetProbe) (*TargetProbe, error)
	UpdateTargetProbe(targetProbe *TargetProbe) (*TargetProbe, error)
	DeleteTargetProbe(organizationUUID, targetProbeUUID uuid.UUID) error

	ListTargetProbesByAlert(organizationUUID, alertUUID uuid.UUID, count int32, offset uint64) (targetProbes []*TargetProbe, err error)
	ListAlertsByTargetProbe(organizationUUID, targetProbeUUID uuid.UUID, count int32, offset uint64) ([]*Alert, error)
	LinkTargetProbesToAlert(force bool, organizationUUID, alertUUID uuid.UUID, targetProbes ...uuid.UUID) error

	ListTargetProbeAgents(organizationUUID, agentUUID uuid.UUID, count int32, offset uint64) ([]*TargetProbeAgent, error)
	ListTargetProbeAgentsForOrganization(organizationUUID uuid.UUID, checkUUIDs []uuid.UUID, pageSize int32, pageOffset uint64) ([]*TargetProbeAgent, error)
	ListTargetProbeAgentsAll(count int32, offset uint64) ([]*TargetProbeAgent, error)
	GetTargetProbeAgent(organizationUUID, agentUUID, targetProbeAgentUUID uuid.UUID) (*TargetProbeAgent, error)
	CreateTargetProbeAgent(targetProbeAgent *TargetProbeAgent) (*TargetProbeAgent, error)
	UpdateTargetProbeAgent(targetProbeAgent *TargetProbeAgent) (*TargetProbeAgent, error)
	DeleteTargetProbeAgent(organizationUUID, agentUUID, targetProbeAgentUUID uuid.UUID) error

	ListChannels(organizationUUID uuid.UUID, count int32, offset uint64) ([]*Channel, error)
	GetChannel(organizationUUID, channelUUID uuid.UUID) (*Channel, error)
	CreateChannel(channel *Channel) (*Channel, error)
	UpdateChannel(channel *Channel) (*Channel, error)
	DeleteChannel(organizationUUID, channelUUID uuid.UUID) error

	ListAlerts(organizationUUID uuid.UUID, count int32, offset uint64) ([]*Alert, error)
	GetAlert(organizationUUID, alertUUID uuid.UUID) (*Alert, error)
	CreateAlert(alert *Alert) (*Alert, error)
	UpdateAlert(alert *Alert) (*Alert, error)
	DeleteAlert(organizationUUID, alertUUID uuid.UUID) error

	GetUserByEmail(email string) (*User, error)
	CreateUser(user *User) (*User, error)
	UpdateUser(user *User) (*User, error)
	DeleteUser(organizationUUID, userUUID uuid.UUID) error

	ListAlertsByChannel(organizationUUID, channelUUID uuid.UUID, count int32, offset uint64) ([]*Alert, error)

	ListTags(organizationUUID uuid.UUID, count int32, offset uint64) ([]*Tag, error)
	GetTag(organizationUUID uuid.UUID, slug string) (*Tag, error)
	CreateTag(tag *Tag) (*Tag, error)
	UpdateTag(tag *Tag) (*Tag, error)
	DeleteTag(organizationUUID uuid.UUID, slug string) error

	ListTagsByTarget(organizationUUID, targetUUID uuid.UUID, pageSize int32, pageOffset uint64) ([]*Tag, error)
	ListTargetsByTag(organizationUUID uuid.UUID, tagName string, pageSize int32, pageOffset uint64) ([]*Target, error)
	LinkTagsToTarget(force bool, organizationUUID, targetUUID uuid.UUID, tagSlugs ...string) error
	UnlinkTagsFromTarget(organizationUUID, targetUUID uuid.UUID, tagSlugs ...string) error

	ListChannelTypes(count int32, offset uint64) ([]*ChannelType, error)
	GetChannelType(label string) (*ChannelType, error)

	ListProbes(count int32, offset uint64) ([]*Probe, error)
	GetProbe(probeUUID uuid.UUID) (*Probe, error)

	// Close DB connection
	Close() error

	// Private methods
	getClient() *sql.DB
	getDialect() string
}

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

// NullTime is useful to get value from DB that might be null
type NullTime struct {
	Time  time.Time
	Valid bool // Valid is true if Time is not NULL
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}

// NullBytes is useful to get value from DB that might be null
type NullBytes struct {
	Bytes []byte
	Valid bool
}

// Scan implements the Scanner interface.
func (nb *NullBytes) Scan(value interface{}) error {
	nb.Bytes, nb.Valid = value.([]byte)
	if nb.Valid {
		return nil
	}
	var s string
	s, nb.Valid = value.(string)
	if nb.Valid {
		nb.Bytes = []byte(s)
		return nil
	}
	return nil
}

// Value implements the driver Valuer interface.
func (nb NullBytes) Value() (driver.Value, error) {
	if !nb.Valid {
		return nil, nil
	}
	return nb.Bytes, nil
}

// row is a helper interface to encapsulate sql.Row and sql.Rows
type row interface {
	Scan(dest ...interface{}) error
}
