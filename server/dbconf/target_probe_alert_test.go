// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"reflect"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
)

func TestListTargetProbesByAlert(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	tp0 := TargetProbe{
		ID:                 1,
		UUID:               testTargetProbeUUID,
		TargetUUID:         testTargetUUID,
		ProbeUUID:          testProbeUUID,
		OrganizationUUID:   testOrganizationUUID,
		StateType:          "threshold",
		StateConfiguration: map[string]interface{}{"states": ""},
		TsCreated:          now,
	}
	tp1 := TargetProbe{
		ID:                 2,
		UUID:               uuid.New(),
		TargetUUID:         testTargetUUID,
		ProbeUUID:          testProbeUUID,
		OrganizationUUID:   testOrganizationUUID,
		StateType:          "history",
		StateConfiguration: map[string]interface{}{"states": ""},
		TsCreated:          now,
	}

	rows := sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).
		AddRow(tp0.ID, tp0.UUID.String(), tp0.TargetUUID.String(), tp0.ProbeUUID.String(), tp0.OrganizationUUID.String(), tp0.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate)).
		AddRow(tp1.ID, tp1.UUID.String(), tp1.TargetUUID.String(), tp1.ProbeUUID.String(), tp1.OrganizationUUID.String(), tp1.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp, target AS t, probe AS p, organization AS o, state_type AS ref, target_probe_alert as tpa, alert as a`).
		WithArgs(testOrganizationUUID, testAlertUUID).
		WillReturnRows(rows)
	tpa, err := mockDb.ListTargetProbesByAlert(testOrganizationUUID, testAlertUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tpa) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(tpa))
	}
	if !reflect.DeepEqual(tpa[0], &tp0) {
		t.Fatalf("expected %v, got %v", tp0, *tpa[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestListAlertsByTargetProbe(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	a0 := Alert{
		ID:               1,
		UUID:             testAlertUUID,
		OrganizationUUID: testOrganizationUUID,
		ChannelUUID:      testChannelUUID,
		Type:             "recurrence",
		Configuration:    map[string]interface{}{"recurrence": 3.0},
		TsCreated:        now,
	}
	a1 := Alert{
		ID:               2,
		UUID:             uuid.New(),
		OrganizationUUID: testOrganizationUUID,
		ChannelUUID:      testChannelUUID,
		Type:             "change",
		Configuration:    map[string]interface{}{},
		TsCreated:        now,
	}

	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "o.uuid", "c.uuid", "a.type", "a.configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted", "ref.configuration_template",
	}).
		AddRow(a0.ID, a0.UUID.String(), a0.OrganizationUUID.String(), a0.ChannelUUID.String(), a0.Type, []byte(`{"recurrence":3}`), now, nil, nil, []byte(recurrenceTemplate)).
		AddRow(a1.ID, a1.UUID.String(), a1.OrganizationUUID.String(), a1.ChannelUUID.String(), a1.Type, []byte(`{}`), now, nil, nil, []byte(`[]`))
	mock.ExpectQuery(`
		SELECT a\.id, a\.uuid, o\.uuid, c\.uuid, a\.type, a\.configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted, ref\.configuration_template
		FROM alert AS a, organization AS o, channel AS c
		`).
		WithArgs(testOrganizationUUID, testTargetProbeUUID).
		WillReturnRows(rows)
	alerts, err := mockDb.ListAlertsByTargetProbe(testOrganizationUUID, testTargetProbeUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(alerts) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(alerts))
	}

	got := alerts[0]
	expect := &a0
	if got.ID != expect.ID ||
		got.UUID != expect.UUID ||
		got.OrganizationUUID != expect.OrganizationUUID ||
		got.ChannelUUID != expect.ChannelUUID ||
		got.Type != expect.Type ||
		got.TsCreated != expect.TsCreated {
		t.Fatalf("expected %#v, got %#v", expect, got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestLinkTargetProbesToAlert(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectBegin()
	mock.ExpectExec(`DELETE FROM target_probe_alert WHERE`).
		WithArgs(testAlertUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 8))
	mock.ExpectExec(`INSERT INTO target_probe_alert \(target_probe, alert\) VALUES`).
		WithArgs(testTargetProbeUUID, testOrganizationUUID, testAlertUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	err = mockDb.LinkTargetProbesToAlert(true, testOrganizationUUID, testAlertUUID, testTargetProbeUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
