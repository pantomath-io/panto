// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/util"
)

// Channel is the representation of a channel in the DB
type Channel struct {
	ID               int
	UUID             uuid.UUID
	OrganizationUUID uuid.UUID
	Type             string
	DisplayName      string
	Configuration    map[string]interface{}
	TsCreated        time.Time
	TsUpdated        time.Time
	TsDeleted        time.Time
}

// ListChannels returns all Channels from the DBConf
func (db *SQLiteDB) ListChannels(organizationUUID uuid.UUID, pageSize int32, pageOffset uint64) (channels []*Channel, err error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT c.id, c.uuid, o.uuid, c.display_name, c.type, c.ts_created, c.ts_updated, c.ts_deleted, c.configuration, ct.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c.organization = o.id AND c.type = ct.label AND c.ts_deleted IS NULL AND o.ts_deleted IS NULL
			AND o.uuid = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	channels = make([]*Channel, 0)
	for rows.Next() {
		c, err := scanChannel(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		channels = append(channels, c)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Channel{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// GetChannel returns a Channel in the DB based on its UUID and its parent organization's UUID
func (db *SQLiteDB) GetChannel(organizationUUID, channelUUID uuid.UUID) (*Channel, error) {
	o, err := scanChannel(db.client.QueryRow(`
		SELECT c.id, c.uuid, o.uuid, c.display_name, c.type, c.ts_created, c.ts_updated, c.ts_deleted, c.configuration, ct.configuration_template
		FROM channel AS c, organization AS o, channel_type AS ct
		WHERE c.organization = o.id AND c.type = ct.label AND c.ts_deleted IS NULL
			AND o.uuid = ? AND c.uuid = ?
	`, organizationUUID, channelUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve Channel: %s", err)
	}

	return o, nil
}

// CreateChannel creates a new Channel in the DB
func (db *SQLiteDB) CreateChannel(channel *Channel) (*Channel, error) {
	if len(channel.Type) == 0 {
		return nil, fmt.Errorf("missing channel type")
	}

	configuration, err := db.marshalChannelConfiguration(channel)
	if err != nil {
		return nil, err
	}

	u := uuid.New()
	_, err = db.client.Exec(
		`INSERT INTO channel (uuid, display_name, type, configuration, organization)
		VALUES (?,?,?,?,
			(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = ?)
		)`,
		u, channel.DisplayName, channel.Type, configuration, channel.OrganizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	return db.GetChannel(channel.OrganizationUUID, u)
}

// UpdateChannel updates an existing Channel in the DB
func (db *SQLiteDB) UpdateChannel(channel *Channel) (*Channel, error) {
	// Specify both channel and config OR none.
	if channel.Type == "" && channel.Configuration != nil {
		return nil, fmt.Errorf("missing Channel type")
	}
	if channel.Type != "" && channel.Configuration == nil {
		return nil, fmt.Errorf("missing Channel config")
	}

	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, "UPDATE channel SET ")

	// User-supplied fields to change
	if channel.Type != "" && channel.Configuration != nil {
		configuration, err := db.marshalChannelConfiguration(channel)
		if err != nil {
			return nil, err
		}
		fmt.Fprint(req, "type=?, configuration=?, ")
		args = append(args, channel.Type, configuration)
	}
	fmt.Fprint(req, "display_name=?, ")
	args = append(args, channel.DisplayName)

	// Change update timestamp
	fmt.Fprintf(req, `ts_updated=(strftime('%%s', 'now')) `)

	// Channel & Organization constraint
	fmt.Fprint(req, `WHERE uuid = ? AND organization = (SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid=?) `)
	args = append(args, channel.UUID, channel.OrganizationUUID)

	// Soft delete constraint
	fmt.Fprint(req, `AND ts_deleted IS NULL`)

	res, err := db.client.Exec(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if rowsAffected, err := res.RowsAffected(); err == nil && rowsAffected < 1 {
		return nil, fmt.Errorf("failed to update channel")
	}

	return db.GetChannel(channel.OrganizationUUID, channel.UUID)
}

// DeleteChannel deletes a Channel and all linked entities (TargetProbeChannel) from the DB
func (db *SQLiteDB) DeleteChannel(organizationUUID, channelUUID uuid.UUID) error {
	_, err := db.client.Exec(`UPDATE channel 
		SET ts_deleted=(strftime('%s', 'now'))
		WHERE id = 
			(SELECT c.id FROM channel AS c, organization AS o WHERE c.organization=o.id AND c.uuid=? AND o.uuid=?)
		AND ts_deleted IS NULL`,
		channelUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete channel: %s", err)
	}

	return nil
}

func scanChannel(r row) (*Channel, error) {
	var c Channel
	var tsCreated, tsUpdated, tsDeleted NullTime
	var configuration, template NullBytes
	err := r.Scan(
		&c.ID,
		&c.UUID,
		&c.OrganizationUUID,
		&c.DisplayName,
		&c.Type,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
		&configuration,
		&template,
	)
	if err != nil {
		return nil, err
	}
	var def []util.Parameter
	if template.Valid {
		if err = util.UnmarshalJSON(template.Bytes, &def); err != nil {
			return nil, fmt.Errorf("couldn't unmarshal ChannelType configuration: %s", err)
		}
	} else {
		return nil, fmt.Errorf("ChannelType configuration template is invalid")
	}
	if configuration.Valid {
		if c.Configuration, err = util.UnmarshalAndValidateParameters(configuration.Bytes, def); err != nil {
			return nil, fmt.Errorf("invalid configuration: %s", err)
		}
	} else {
		return nil, fmt.Errorf("Channel config is invalid")
	}
	if tsCreated.Valid {
		c.TsCreated = tsCreated.Time
	} else {
		c.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		c.TsUpdated = tsUpdated.Time
	} else {
		c.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		c.TsDeleted = tsDeleted.Time
	} else {
		c.TsDeleted = time.Time{}
	}
	return &c, nil
}

func (db *SQLiteDB) marshalChannelConfiguration(channel *Channel) ([]byte, error) {
	ct, err := db.GetChannelType(channel.Type)
	if err != nil {
		return nil, err
	}
	if ct == nil {
		return nil, fmt.Errorf("channel_type not found: %s", channel.Type)
	}
	detail, err := util.MarshalJSON(channel.Configuration)
	if err != nil {
		return nil, fmt.Errorf("couldn't marshal channel config to JSON: %s", err)
	}
	_, err = util.UnmarshalAndValidateParameters(detail, ct.ConfigurationTemplate)
	if err != nil {
		return nil, fmt.Errorf("invalid configuration: %s", err)
	}

	return detail, nil
}
