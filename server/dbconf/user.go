// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
	"gitlab.com/pantomath-io/panto/util"
)

// User is the representation of a user in the DB
type User struct {
	ID               int
	UUID             uuid.UUID
	OrganizationUUID uuid.UUID
	Email            string
	PasswordSalt     util.Salt
	PasswordHash     util.Hash
	Details          map[string]interface{}
	TsCreated        time.Time
	TsUpdated        time.Time
	TsDeleted        time.Time
}

// GetUserByEmail returns a User from the DB based on its email
func (db *SQLiteDB) GetUserByEmail(email string) (*User, error) {
	user, err := scanUser(db.client.QueryRow(`
		SELECT u.id, u.uuid, o.uuid, u.email, u.passwd_salt, u.passwd_hash, u.details, u.ts_created, u.ts_updated, u.ts_deleted
		FROM user AS u 
		INNER JOIN organization AS o ON u.organization = o.id
		WHERE u.ts_deleted IS NULL
		AND u.email = ?
	`, email))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve User: %s", err)
	}

	return user, nil
}

// CreateUser creates a new User in the DB
func (db *SQLiteDB) CreateUser(user *User) (*User, error) {
	if len(user.Email) == 0 {
		return nil, fmt.Errorf("missing email address")
	}

	u := uuid.New()

	details, err := util.MarshalJSON(user.Details)
	if err != nil {
		return nil, err
	}

	salt := hex.EncodeToString(user.PasswordSalt[:])
	hash := hex.EncodeToString(user.PasswordHash[:])

	_, err = db.client.Exec(
		`INSERT INTO user (uuid, email, passwd_hash, passwd_salt, details, organization)
		VALUES (?,?,?,?,?,
			(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = ?)
		)`,
		u, user.Email, hash, salt, details, user.OrganizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	return db.GetUserByEmail(user.Email)
}

// UpdateUser updates an existing User in the DB
func (db *SQLiteDB) UpdateUser(user *User) (*User, error) {
	if len(user.Email) == 0 {
		return nil, fmt.Errorf("missing email address")
	}
	if len(user.PasswordHash) == 0 {
		return nil, fmt.Errorf("missing password hash")
	}
	if len(user.PasswordSalt) == 0 {
		return nil, fmt.Errorf("missing password salt")
	}

	details, err := util.MarshalJSON(user.Details)
	if err != nil {
		return nil, err
	}

	salt := hex.EncodeToString(user.PasswordSalt[:])
	hash := hex.EncodeToString(user.PasswordHash[:])

	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, "UPDATE user SET ")

	// User-supplied fields to change
	// * Can't change email as it is used to identify user
	fmt.Fprint(req, "passwd_hash=?, ")
	args = append(args, hash)

	fmt.Fprint(req, "passwd_salt=?, ")
	args = append(args, salt)

	fmt.Fprint(req, "details=?, ")
	args = append(args, details)

	// Change update timestamp
	fmt.Fprintf(req, `ts_updated=(strftime('%%s', 'now')) `)

	// User & Organization constraint
	fmt.Fprint(req, `WHERE uuid=? AND organization = (SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid=?) `)
	args = append(args, user.UUID, user.OrganizationUUID)

	// Soft delete constraint
	fmt.Fprint(req, `AND ts_deleted IS NULL`)

	res, err := db.client.Exec(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if rowsAffected, err := res.RowsAffected(); err == nil && rowsAffected < 1 {
		return nil, fmt.Errorf("failed to update user")
	}

	return db.GetUserByEmail(user.Email)
}

// DeleteUser deletes a User from the DB
func (db *SQLiteDB) DeleteUser(organizationUUID, userUUID uuid.UUID) error {
	// Delete user
	_, err := db.client.Exec(`UPDATE user 
		SET ts_deleted=(strftime('%s', 'now'))
		WHERE id = (SELECT u.id FROM user AS u, organization AS o
			WHERE u.organization=o.id AND u.uuid=? AND o.uuid=?)
		AND ts_deleted IS NULL`,
		userUUID, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete User: %s", err)
	}

	return nil
}

func scanUser(r row) (*User, error) {
	var u User
	var tsCreated, tsUpdated, tsDeleted NullTime
	var salt, hash, details sql.NullString
	err := r.Scan(
		&u.ID,
		&u.UUID,
		&u.OrganizationUUID,
		&u.Email,
		&salt,
		&hash,
		&details,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, err
	}
	if tsCreated.Valid {
		u.TsCreated = tsCreated.Time
	} else {
		u.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		u.TsUpdated = tsUpdated.Time
	} else {
		u.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		u.TsDeleted = tsDeleted.Time
	} else {
		u.TsDeleted = time.Time{}
	}
	if salt.Valid {
		n, err := hex.Decode(u.PasswordSalt[:], []byte(salt.String))
		if err != nil {
			return nil, fmt.Errorf("couldn't read password salt: %s", err)
		}
		if n != 32 {
			return nil, fmt.Errorf("unexpected password salt length: %d", n)
		}
	}
	if hash.Valid {
		n, err := hex.Decode(u.PasswordHash[:], []byte(hash.String))
		if err != nil {
			return nil, fmt.Errorf("couldn't read password hash: %s", err)
		}
		if n != 32 {
			return nil, fmt.Errorf("unexpected password hash length: %d", n)
		}
	}
	if details.Valid {
		err = json.Unmarshal([]byte(details.String), &u.Details)
		if err != nil {
			log.Errorf("couldn't unmarshal user details: %s", err)
		}
	} else {
		u.Details = make(map[string]interface{})
	}
	return &u, nil
}
