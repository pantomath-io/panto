// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
)

// An Organization is the representation of an organization in the DB
type Organization struct {
	ID        int
	UUID      uuid.UUID
	Name      string
	TsCreated time.Time
	TsUpdated time.Time
	TsDeleted time.Time
}

// ListOrganizations returns all Organizations from the DBConf
func (db *SQLiteDB) ListOrganizations(pageSize int32, pageOffset uint64) (orgs []*Organization, err error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
		FROM organization
		WHERE ts_deleted IS NULL
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String())
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	orgs = make([]*Organization, 0)
	for rows.Next() {
		o, err := scanOrganization(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		orgs = append(orgs, o)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Organization{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// GetOrganization returns an Organization in the DB based on its UUID
func (db *SQLiteDB) GetOrganization(organizationUUID uuid.UUID) (*Organization, error) {
	o, err := scanOrganization(db.client.QueryRow(`
		SELECT id, uuid, name, ts_created, ts_updated, ts_deleted
		FROM organization
		WHERE ts_deleted IS NULL AND uuid = ?
	`, organizationUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve Organization: %s", err)
	}

	return o, nil
}

// CreateOrganization creates a new Organization in the DB
func (db *SQLiteDB) CreateOrganization(organization *Organization) (*Organization, error) {
	if len(organization.Name) == 0 {
		return nil, fmt.Errorf("missing organization name")
	}

	u := uuid.New()
	_, err := db.client.Exec("INSERT INTO organization (uuid, name) VALUES (?,?)", u, organization.Name)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	return db.GetOrganization(u)
}

// UpdateOrganization updates an existing Organization in the DB
func (db *SQLiteDB) UpdateOrganization(organization *Organization) (*Organization, error) {
	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, "UPDATE organization SET")

	if organization.Name != "" {
		fmt.Fprint(req, " name=?,")
		args = append(args, organization.Name)
	}

	fmt.Fprintf(req, ` ts_updated=(strftime('%%s', 'now')) WHERE ts_deleted IS NULL AND uuid=?`)
	args = append(args, organization.UUID)

	res, err := db.client.Exec(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if rowsAffected, err := res.RowsAffected(); err == nil && rowsAffected < 1 {
		return nil, fmt.Errorf("failed to update organization")
	}

	return db.GetOrganization(organization.UUID)
}

func scanOrganization(r row) (*Organization, error) {
	var o Organization
	var tsCreated, tsUpdated, tsDeleted NullTime
	err := r.Scan(
		&o.ID,
		&o.UUID,
		&o.Name,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, err
	}
	if tsCreated.Valid {
		o.TsCreated = tsCreated.Time
	} else {
		o.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		o.TsUpdated = tsUpdated.Time
	} else {
		o.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		o.TsDeleted = tsDeleted.Time
	} else {
		o.TsDeleted = time.Time{}
	}
	return &o, nil
}
