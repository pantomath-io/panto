// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"reflect"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
)

func TestListTags(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	t0 := Tag{
		ID:               1,
		OrganizationUUID: testOrganizationUUID,
		Slug:             "tag",
		DisplayName:      "tag",
		Note:             "This is a note.",
		Color:            "#2F4C99",
		TsCreated:        now,
	}
	t1 := Tag{
		ID:               2,
		OrganizationUUID: testOrganizationUUID,
		Slug:             "tag-2",
		DisplayName:      "tag",
		Note:             "This is another note.",
		Color:            "#00A994",
		TsCreated:        now.Add(time.Hour),
	}

	rows := sqlmock.NewRows([]string{
		"t.id", "o.uuid", "t.slug", "t.display_name", "t.note", "t.color", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).
		AddRow(t0.ID, t0.OrganizationUUID.String(), t0.Slug, t0.DisplayName, t0.Note, t0.Color, t0.TsCreated, nil, nil).
		AddRow(t1.ID, t1.OrganizationUUID.String(), t0.Slug, t0.DisplayName, t1.Note, t1.Color, t1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, o\.uuid, t\.slug, t\.display_name, t\.note, t\.color, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM tag AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \?`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	tags, err := mockDb.ListTags(testOrganizationUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tags) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(tags))
	}
	if !reflect.DeepEqual(tags[0], &t0) {
		t.Fatalf("expected %v, got %v", t0, *tags[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"t.id", "o.uuid", "t.slug", "t.display_name", "t.note", "t.color", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(t1.ID, t1.OrganizationUUID.String(), t1.Slug, t1.DisplayName, t1.Note, t1.Color, t1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, o\.uuid, t\.slug, t\.display_name, t\.note, t\.color, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM tag AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT -1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	tags, err = mockDb.ListTags(testOrganizationUUID, 0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tags) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(tags))
	}
	if !reflect.DeepEqual(tags[0], &t1) {
		t.Fatalf("expected %v, got %v", t1, *tags[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"t.id", "o.uuid", "t.slug", "t.display_name", "t.note", "t.color", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(t0.ID, t0.OrganizationUUID.String(), t0.Slug, t0.DisplayName, t0.Note, t0.Color, t0.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, o\.uuid, t\.slug, t\.display_name, t\.note, t\.color, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM tag AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT 1 OFFSET 0`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	tags, err = mockDb.ListTags(testOrganizationUUID, 1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tags) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(tags))
	}
	if !reflect.DeepEqual(tags[0], &t0) {
		t.Fatalf("expected %v, got %v", t0, *tags[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"t.id", "o.uuid", "t.slug", "t.display_name", "t.note", "t.color", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(t0.ID, t0.OrganizationUUID.String(), t0.Slug, t0.DisplayName, t0.Note, t0.Color, t0.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, o\.uuid, t\.slug, t\.display_name, t\.note, t\.color, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM tag AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \? LIMIT 1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	tags, err = mockDb.ListTags(testOrganizationUUID, 1, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tags) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(tags))
	}
	if !reflect.DeepEqual(tags[0], &t0) {
		t.Fatalf("expected %v, got %v", t0, *tags[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetTag(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	expect := Tag{
		ID:               1,
		OrganizationUUID: testOrganizationUUID,
		Slug:             "tag",
		DisplayName:      "tag",
		Note:             "This is a note.",
		Color:            "#2F4C99",
		TsCreated:        now,
	}
	rows := sqlmock.NewRows([]string{
		"t.id", "o.uuid", "t.slug", "t.display_name", "t.note", "t.color", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(expect.ID, expect.OrganizationUUID.String(), expect.Slug, expect.DisplayName, expect.Note, expect.Color, expect.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, o\.uuid, t\.slug, t\.display_name, t\.note, t\.color, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM tag AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.uuid = \? AND t\.slug = \?`).
		WithArgs(testOrganizationUUID, expect.Slug).
		WillReturnRows(rows)

	got, err := mockDb.GetTag(testOrganizationUUID, expect.Slug)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(&expect, got) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"t.id", "o.uuid", "t.slug", "t.display_name", "t.note", "t.color", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	})

	u0 := uuid.New()
	slug := "barbes"
	mock.ExpectQuery(`SELECT t\.id, o\.uuid, t\.slug, t\.display_name, t\.note, t\.color, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM tag AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.uuid = \? AND t\.slug = \?`).
		WithArgs(u0, slug).
		WillReturnRows(rows)

	got, err = mockDb.GetTag(u0, slug)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got != nil {
		t.Fatalf("expected nil, got %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestCreateTag(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	tag := Tag{
		OrganizationUUID: testOrganizationUUID,
		Slug:             "tag",
		DisplayName:      "Tag",
		Note:             "This is a note.",
		Color:            "#2F4C99",
	}

	mock.ExpectQuery(`SELECT t\.slug FROM tag AS t, organization AS o WHERE t\.organization = o\.id AND t\.slug REGEXP`).
		WithArgs(sqlmock.AnyArg(), tag.OrganizationUUID).
		WillReturnRows(sqlmock.NewRows([]string{"slug"}))

	mock.ExpectExec(`INSERT INTO tag \(slug, display_name, note, color, organization\)
	VALUES \(\?,\?,\?,\?,
		\(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = \?\)
	\)`).
		WithArgs(tag.Slug, tag.DisplayName, tag.Note, tag.Color, tag.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"t.id", "o.uuid", "t.slug", "t.display_name", "t.note", "t.color", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(1, tag.OrganizationUUID.String(), tag.Slug, tag.DisplayName, tag.Note, tag.Color, now, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, o\.uuid, t\.slug, t\.display_name, t\.note, t\.color, t\.ts_created, t\.ts_updated, t\.ts_deleted
		FROM tag AS t, organization AS o
		WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.uuid = \? AND t\.slug = \?`).
		WithArgs(testOrganizationUUID, tag.Slug).
		WillReturnRows(rows)

	got, err := mockDb.CreateTag(&tag)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != 1 ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.Slug != tag.Slug ||
		got.DisplayName != tag.DisplayName ||
		got.Note != tag.Note ||
		got.Color != tag.Color ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdateTag(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	tag := Tag{
		Slug:             "tag",
		DisplayName:      "Tag",
		OrganizationUUID: testOrganizationUUID,
		Note:             "This is a note.",
		Color:            "#2F4C99",
	}

	mock.ExpectExec(`UPDATE tag SET .+ ts_updated=\(strftime\('%s', 'now'\)\)
		WHERE slug=\? AND organization = \(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid=\?\)`).
		WithArgs(tag.Note, tag.DisplayName, tag.Color, tag.Slug, tag.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"t.id", "o.uuid", "t.slug", "t.display_name", "t.note", "t.color", "t.ts_created", "t.ts_updated", "t.ts_deleted",
	}).AddRow(1, tag.OrganizationUUID.String(), tag.Slug, tag.DisplayName, tag.Note, tag.Color, now, nil, nil)
	mock.ExpectQuery(`SELECT t\.id, o\.uuid, t\.slug, t\.display_name, t\.note, t\.color, t\.ts_created, t\.ts_updated, t\.ts_deleted
			FROM tag AS t, organization AS o
			WHERE t\.organization = o\.id AND t\.ts_deleted IS NULL AND o\.uuid = \? AND t\.slug = \?`).
		WithArgs(testOrganizationUUID, tag.Slug).
		WillReturnRows(rows)

	got, err := mockDb.UpdateTag(&tag)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != 1 ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.Slug != tag.Slug ||
		got.DisplayName != tag.DisplayName ||
		got.Note != tag.Note ||
		got.Color != tag.Color ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteTag(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectExec(`UPDATE tag 
		SET ts_deleted=\(strftime\('%s', 'now'\)\)
		WHERE id = \(SELECT t\.id FROM tag AS t, organization AS o WHERE t\.organization=o\.id AND t\.slug=\? AND o\.uuid=\?\)`).
		WithArgs("tag", testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = mockDb.DeleteTag(testOrganizationUUID, "tag")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestSlugifyName(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	testCases := []struct {
		name   string
		rows   *sqlmock.Rows
		expect string
	}{
		{
			"tag",
			sqlmock.NewRows([]string{"slug"}),
			"tag",
		},
		{
			"tag",
			sqlmock.NewRows([]string{"slug"}).AddRow("tag-1"),
			"tag-2",
		},
		{
			"tag",
			sqlmock.NewRows([]string{"slug"}).AddRow("tag"),
			"tag-1",
		},
		{
			"Tag Display Name",
			sqlmock.NewRows([]string{"slug"}),
			"tag-display-name",
		},
	}

	for _, c := range testCases {
		mockDb := &SQLiteDB{client: db}
		mock.ExpectQuery(`SELECT t\.slug FROM tag AS t, organization AS o WHERE t\.organization = o\.id AND t\.slug REGEXP`).
			WithArgs(sqlmock.AnyArg(), testOrganizationUUID).
			WillReturnRows(c.rows)

		slug, err := mockDb.slugifyName(&Tag{DisplayName: c.name, OrganizationUUID: testOrganizationUUID})

		if err != nil {
			t.Errorf("unexpected error: %s", err)
		}
		if slug != c.expect {
			t.Errorf("expected %s, got %s", c.expect, slug)
		}
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	}
}
