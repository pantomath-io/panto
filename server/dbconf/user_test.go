// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"encoding/hex"
	"encoding/json"
	"reflect"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/pantomath-io/panto/util"
)

func TestGetUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	testEmail := "hello@world.com"
	expect := User{
		ID:               1,
		UUID:             testUserUUID,
		OrganizationUUID: testOrganizationUUID,
		Email:            testEmail,
		Details: map[string]interface{}{
			"fullname": "Hello World",
			"type":     "admin",
		},
		TsCreated: now,
	}
	jsonDetails, err := json.Marshal(expect.Details)
	if err != nil {
		t.Fatalf("couldn't marshal details to JSON: %s", err)
	}
	expect.PasswordSalt, err = util.GenerateSalt()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when generating a random salt", err)
	}
	salt := hex.EncodeToString(expect.PasswordSalt[:])

	expect.PasswordHash, err = util.HashPassword("helloworld", expect.PasswordSalt)
	if err != nil {
		t.Fatalf("an error '%s' was not expected when generating a hash", err)
	}
	hash := hex.EncodeToString(expect.PasswordHash[:])

	rows := sqlmock.NewRows([]string{
		"u.id", "u.uuid", "o.uuid", "u.email", "u.passwd_salt", "u.passwd_hash", "u.details", "u.ts_created", "u.ts_updated", "u.ts_deleted",
	}).AddRow(expect.ID, testUserUUID.String(), expect.OrganizationUUID.String(), testEmail, salt, hash, jsonDetails, expect.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT u\.id, u\.uuid, o\.uuid, u\.email, u\.passwd_salt, u\.passwd_hash, u\.details, u\.ts_created, u\.ts_updated, u\.ts_deleted
		FROM user AS u`).
		WithArgs(testEmail).
		WillReturnRows(rows)

	got, err := mockDb.GetUserByEmail(testEmail)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(&expect, got) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestCreateUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}

	uuidArg := StoreArg()
	user := User{
		OrganizationUUID: testOrganizationUUID,
		Email:            "hello@world.com",
	}

	user.PasswordSalt, err = util.GenerateSalt()
	if err != nil {
		t.Fatalf("Unable to generate Salt: %s", err)
	}
	user.PasswordHash, err = util.HashPassword("helloworld", user.PasswordSalt)
	if err != nil {
		t.Fatalf("Unable to hash password: %s", err)
	}

	salt := hex.EncodeToString(user.PasswordSalt[:])
	hash := hex.EncodeToString(user.PasswordHash[:])
	details, err := util.MarshalJSON(user.Details)
	if err != nil {
		t.Fatalf("Unable to marshal details: %s", err)
	}

	mock.ExpectExec(`INSERT INTO user \(uuid, email, passwd_hash, passwd_salt, details, organization\)
	VALUES \(\?,\?,\?,\?,\?,
		\(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = \?\)
	\)`).
		WithArgs(uuidArg, user.Email, hash, salt, details, user.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"u.id", "u.uuid", "o.uuid", "u.email", "u.passwd_salt", "u.passwd_hash", "u.details", "u.ts_created", "u.ts_updated", "u.ts_deleted",
	}).AddRow(1, testUserUUID.String(), user.OrganizationUUID.String(), user.Email, salt, hash, details, now, nil, nil)
	mock.ExpectQuery(`SELECT u\.id, u\.uuid, o\.uuid, u\.email, u\.passwd_salt, u\.passwd_hash, u\.details, u\.ts_created, u\.ts_updated, u\.ts_deleted
		FROM user AS u INNER JOIN organization AS o ON u\.organization = o\.id
		WHERE u\.ts_deleted IS NULL AND u\.email = \?`).
		WithArgs(user.Email).
		WillReturnRows(rows)

	got, err := mockDb.CreateUser(&user)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != 1 ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.Email != user.Email ||
		got.PasswordHash != user.PasswordHash ||
		got.PasswordSalt != user.PasswordSalt ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdateUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}

	user := User{
		ID:               42,
		UUID:             testUserUUID,
		OrganizationUUID: testOrganizationUUID,
		Email:            "hello@world.com",
	}

	salt := "3d2438f53d68a77ceb66050b01aa9628c958845989f97a56003272fe06c2c29d"
	n, err := hex.Decode(user.PasswordSalt[:], []byte(salt))
	if err != nil {
		t.Fatalf("unable to read salt: %s", err)
	}
	if n != 32 {
		t.Fatalf("wrong salt decoding")
	}

	hash := "8074fbaf8af8a0200c7dcb0c89a2740c0cbff9d309d64cb5a75e0fe2cd9e0b4e"
	n, err = hex.Decode(user.PasswordHash[:], []byte(hash))
	if err != nil {
		t.Fatalf("unable to read hash: %s", err)
	}
	if n != 32 {
		t.Fatalf("wrong hash decoding")
	}
	details, err := util.MarshalJSON(user.Details)
	if err != nil {
		t.Fatalf("Unable to marshal details: %s", err)
	}
	tsCreated := time.Now().AddDate(-1, 0, 0)
	tsUpdated := time.Now()

	rows := sqlmock.NewRows([]string{
		"u.id", "u.uuid", "o.uuid", "u.email", "u.passwd_salt", "u.passwd_hash", "u.details", "u.ts_created", "u.ts_updated", "u.ts_deleted",
	}).AddRow(user.ID, user.UUID.String(), user.OrganizationUUID.String(), user.Email, salt, hash, details, tsCreated, tsUpdated, nil)

	mock.ExpectExec(`UPDATE user SET passwd_hash=\?, passwd_salt=\?, details=\?, ts_updated=\(strftime\('%s', 'now'\)\)
	WHERE uuid=\? AND organization = \(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid=\?\) AND ts_deleted IS NULL`).
		WithArgs(hash, salt, details, user.UUID, user.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectQuery(`SELECT u\.id, u\.uuid, o\.uuid, u\.email, u\.passwd_salt, u\.passwd_hash, u\.details, u\.ts_created, u\.ts_updated, u\.ts_deleted
		FROM user AS u INNER JOIN organization AS o ON u\.organization = o\.id
		WHERE u\.ts_deleted IS NULL AND u\.email = \?`).
		WithArgs(user.Email).
		WillReturnRows(rows)

	got, err := mockDb.UpdateUser(&user)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != user.ID ||
		got.UUID != user.UUID ||
		got.OrganizationUUID != user.OrganizationUUID ||
		got.Email != user.Email ||
		got.PasswordHash != user.PasswordHash ||
		got.PasswordSalt != user.PasswordSalt ||
		got.TsCreated != tsCreated {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectExec(`UPDATE user
		SET ts_deleted=\(strftime\('%s', 'now'\)\) 
		WHERE id = \(SELECT u\.id FROM user AS u, organization AS o
			WHERE u\.organization=o\.id AND u\.uuid=\? AND o\.uuid=\?\)
		AND ts_deleted IS NULL`).
		WithArgs(testUserUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = mockDb.DeleteUser(testOrganizationUUID, testUserUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
