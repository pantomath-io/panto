// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"

	"github.com/google/uuid"
)

const stateTemplate = `{"states": ""}`

func TestListTargetProbes(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	tp0 := TargetProbe{
		ID:                 1,
		UUID:               testTargetProbeUUID,
		TargetUUID:         testTargetUUID,
		ProbeUUID:          testProbeUUID,
		OrganizationUUID:   testOrganizationUUID,
		StateType:          "threshold",
		StateConfiguration: map[string]interface{}{"states": ""},
		TsCreated:          now,
	}
	tp1 := TargetProbe{
		ID:                 2,
		UUID:               uuid.New(),
		TargetUUID:         testTargetUUID,
		ProbeUUID:          testProbeUUID,
		OrganizationUUID:   testOrganizationUUID,
		StateType:          "history",
		StateConfiguration: map[string]interface{}{"states": ""},
		TsCreated:          now,
	}

	rows := sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).
		AddRow(tp0.ID, tp0.UUID.String(), tp0.TargetUUID.String(), tp0.ProbeUUID.String(), tp0.OrganizationUUID.String(), tp0.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate)).
		AddRow(tp1.ID, tp1.UUID.String(), tp1.TargetUUID.String(), tp1.ProbeUUID.String(), tp1.OrganizationUUID.String(), tp1.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label, 
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND
		`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	tps, err := mockDb.ListTargetProbes(testOrganizationUUID, make([]uuid.UUID, 0), make([]uuid.UUID, 0), 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tps) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(tps))
	}
	if !reflect.DeepEqual(tps[0], &tp0) {
		t.Fatalf("expected %v, got %v", tp0, *tps[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).
		AddRow(tp0.ID, tp0.UUID.String(), tp0.TargetUUID.String(), tp0.ProbeUUID.String(), tp0.OrganizationUUID.String(), tp0.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate)).
		AddRow(tp1.ID, tp1.UUID.String(), tp1.TargetUUID.String(), tp1.ProbeUUID.String(), tp1.OrganizationUUID.String(), tp1.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label, 
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND
		`).
		WithArgs(testOrganizationUUID, testTargetUUID).
		WillReturnRows(rows)
	tps, err = mockDb.ListTargetProbes(testOrganizationUUID, []uuid.UUID{testTargetUUID}, make([]uuid.UUID, 0), 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tps) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(tps))
	}
	if !reflect.DeepEqual(tps[0], &tp0) {
		t.Fatalf("expected %v, got %v", tp0, *tps[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).
		AddRow(tp0.ID, tp0.UUID.String(), tp0.TargetUUID.String(), tp0.ProbeUUID.String(), tp0.OrganizationUUID.String(), tp0.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate)).
		AddRow(tp1.ID, tp1.UUID.String(), tp1.TargetUUID.String(), tp1.ProbeUUID.String(), tp1.OrganizationUUID.String(), tp1.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label, 
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND
		`).
		WithArgs(testOrganizationUUID, testProbeUUID).
		WillReturnRows(rows)
	tps, err = mockDb.ListTargetProbes(testOrganizationUUID, make([]uuid.UUID, 0), []uuid.UUID{testProbeUUID}, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tps) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(tps))
	}
	if !reflect.DeepEqual(tps[0], &tp0) {
		t.Fatalf("expected %v, got %v", tp0, *tps[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).
		AddRow(tp0.ID, tp0.UUID.String(), tp0.TargetUUID.String(), tp0.ProbeUUID.String(), tp0.OrganizationUUID.String(), tp0.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate)).
		AddRow(tp1.ID, tp1.UUID.String(), tp1.TargetUUID.String(), tp1.ProbeUUID.String(), tp1.OrganizationUUID.String(), tp1.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label, 
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND
		`).
		WithArgs(testOrganizationUUID, testTargetUUID, testProbeUUID).
		WillReturnRows(rows)
	tps, err = mockDb.ListTargetProbes(testOrganizationUUID, []uuid.UUID{testTargetUUID}, []uuid.UUID{testProbeUUID}, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tps) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(tps))
	}
	if !reflect.DeepEqual(tps[0], &tp0) {
		t.Fatalf("expected %v, got %v", tp0, *tps[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).
		AddRow(tp1.ID, tp1.UUID.String(), tp1.TargetUUID.String(), tp1.ProbeUUID.String(), tp1.OrganizationUUID.String(), tp1.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label,
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND
		.+ LIMIT -1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)

	tps, err = mockDb.ListTargetProbes(testOrganizationUUID, make([]uuid.UUID, 0), make([]uuid.UUID, 0), 0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tps) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(tps))
	}
	if !reflect.DeepEqual(tps[0], &tp1) {
		t.Fatalf("expected %v, got %v", tp1, *tps[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).
		AddRow(tp0.ID, tp0.UUID.String(), tp0.TargetUUID.String(), tp0.ProbeUUID.String(), tp0.OrganizationUUID.String(), tp0.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label,
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND
		.+ LIMIT 1 OFFSET 0`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)

	tps, err = mockDb.ListTargetProbes(testOrganizationUUID, make([]uuid.UUID, 0), make([]uuid.UUID, 0), 1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tps) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(tps))
	}
	if !reflect.DeepEqual(tps[0], &tp0) {
		t.Fatalf("expected %v, got %v", tp0, *tps[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetTargetProbe(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	expect := TargetProbe{
		ID:                 1,
		UUID:               testTargetProbeUUID,
		TargetUUID:         testTargetUUID,
		ProbeUUID:          testProbeUUID,
		OrganizationUUID:   testOrganizationUUID,
		StateType:          "threshold",
		StateConfiguration: map[string]interface{}{"states": ""},
		TsCreated:          now,
	}
	rows := sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).AddRow(expect.ID, expect.UUID.String(), expect.TargetUUID.String(), expect.ProbeUUID.String(), expect.OrganizationUUID.String(), expect.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label,
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND 
		`).
		WithArgs(testOrganizationUUID, testTargetProbeUUID).
		WillReturnRows(rows)

	got, err := mockDb.GetTargetProbe(testOrganizationUUID, testTargetProbeUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(&expect, got) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	})

	u0 := uuid.New()
	u1 := uuid.New()
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label,
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND 
		`).
		WithArgs(u0, u1).
		WillReturnRows(rows)

	got, err = mockDb.GetTargetProbe(u0, u1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got != nil {
		t.Fatalf("expected nil, got %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	// Test target_probe without state
	expect = TargetProbe{
		ID:                 1,
		UUID:               testTargetProbeUUID,
		TargetUUID:         testTargetUUID,
		ProbeUUID:          testProbeUUID,
		OrganizationUUID:   testOrganizationUUID,
		StateType:          "",
		StateConfiguration: nil,
		TsCreated:          now,
	}
	rows = sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).AddRow(expect.ID, expect.UUID.String(), expect.TargetUUID.String(), expect.ProbeUUID.String(), expect.OrganizationUUID.String(), nil, nil, now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label,
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND 
		`).
		WithArgs(testOrganizationUUID, testTargetProbeUUID).
		WillReturnRows(rows)

	got, err = mockDb.GetTargetProbe(testOrganizationUUID, testTargetProbeUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(&expect, got) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestCreateTargetProbe(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	tp := TargetProbe{
		TargetUUID:         testTargetUUID,
		ProbeUUID:          testProbeUUID,
		OrganizationUUID:   testOrganizationUUID,
		StateType:          "threshold",
		StateConfiguration: map[string]interface{}{"states": ""},
	}

	mock.ExpectQuery(`SELECT configuration_template FROM state_type WHERE label = `).
		WithArgs(tp.StateType).
		WillReturnRows(sqlmock.NewRows([]string{"configuration_template"}).AddRow([]byte(stateTemplate)))

	values, _ := json.Marshal(tp.StateConfiguration)
	uuidArg := StoreArg()
	mock.ExpectExec(`INSERT INTO target_probe`).
		WithArgs(uuidArg, tp.TargetUUID, tp.OrganizationUUID, tp.ProbeUUID, tp.StateType, values).
		WillReturnResult(sqlmock.NewResult(1, 1))

	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).AddRow(1, tp.UUID.String(), tp.TargetUUID.String(), tp.ProbeUUID.String(), tp.OrganizationUUID.String(), tp.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label,
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND 
		`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.CreateTargetProbe(&tp)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != 1 ||
		got.UUID != [16]byte{} ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.TargetUUID != testTargetUUID ||
		got.ProbeUUID != testProbeUUID ||
		got.StateType != "threshold" ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdateTargetProbe(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	tp := TargetProbe{
		UUID:               testTargetProbeUUID,
		ProbeUUID:          testProbeUUID,
		OrganizationUUID:   testOrganizationUUID,
		StateType:          "threshold",
		StateConfiguration: map[string]interface{}{"states": ""},
	}

	mock.ExpectQuery(`SELECT configuration_template FROM state_type`).
		WithArgs("threshold").
		WillReturnRows(sqlmock.NewRows([]string{"configuration_template"}).AddRow([]byte(stateTemplate)))
	mock.ExpectExec(`UPDATE target_probe SET .+ ts_updated = \(strftime\('%s', 'now'\)\) .+`).
		WithArgs(tp.ProbeUUID, tp.StateType, []byte(`{"states":""}`), tp.UUID, tp.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	uuidArg := StoreArg()
	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).AddRow(1, tp.UUID.String(), tp.TargetUUID.String(), tp.ProbeUUID.String(), tp.OrganizationUUID.String(), tp.StateType, []byte(`{"states": ""}`), now, nil, nil, []byte(stateTemplate))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp LEFT JOIN state_type AS ref ON tp.state_type = ref.label,
			target AS t, probe AS p, organization AS o
		WHERE tp\.target = t\.id AND tp\.probe = p\.id AND t\.organization = o\.id AND 
		`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.UpdateTargetProbe(&tp)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	u, ok := uuidArg.Value().(string)
	if !ok ||
		u != testTargetProbeUUID.String() ||
		got.ID != 1 ||
		got.UUID != testTargetProbeUUID ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.ProbeUUID != testProbeUUID ||
		got.StateType != "threshold" ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteTargetProbe(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectExec(`UPDATE target_probe_agent 
		SET ts_deleted=\(strftime\('%s', 'now'\)\)`).
		WithArgs(testTargetProbeUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectExec(`UPDATE target_probe 
		SET ts_deleted=\(strftime\('%s', 'now'\)\)`).
		WithArgs(testTargetProbeUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = mockDb.DeleteTargetProbe(testOrganizationUUID, testTargetProbeUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
