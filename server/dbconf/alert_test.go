// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
)

const recurrenceTemplate = `[{"name": "recurrence", "type":"int", "description": "Number of iterations it a wrong state", "mandatory": true}]`

func TestListAlerts(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	a0 := Alert{
		ID:               1,
		UUID:             testAlertUUID,
		OrganizationUUID: testOrganizationUUID,
		ChannelUUID:      testChannelUUID,
		Type:             "recurrence",
		Configuration:    map[string]interface{}{"recurrence": 3.0},
		TsCreated:        now,
	}
	a1 := Alert{
		ID:               2,
		UUID:             uuid.New(),
		OrganizationUUID: testOrganizationUUID,
		ChannelUUID:      testChannelUUID,
		Type:             "change",
		Configuration:    map[string]interface{}{},
		TsCreated:        now,
	}

	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "o.uuid", "c.uuid", "a.type", "a.configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted", "ref.configuration_template",
	}).
		AddRow(a0.ID, a0.UUID.String(), a0.OrganizationUUID.String(), a0.ChannelUUID.String(), a0.Type, []byte(`{"recurrence":3}`), now, nil, nil, []byte(recurrenceTemplate)).
		AddRow(a1.ID, a1.UUID.String(), a1.OrganizationUUID.String(), a1.ChannelUUID.String(), a1.Type, []byte(`{}`), now, nil, nil, []byte(`[]`))
	mock.ExpectQuery(`
		SELECT a\.id, a\.uuid, o\.uuid, c\.uuid, a\.type, a\.configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted, ref\.configuration_template
		FROM alert AS a, organization AS o, channel AS c
		`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	alerts, err := mockDb.ListAlerts(testOrganizationUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(alerts) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(alerts))
	}

	got := alerts[0]
	expect := &a0

	if got.ID != expect.ID ||
		got.UUID != expect.UUID ||
		got.OrganizationUUID != expect.OrganizationUUID ||
		got.ChannelUUID != expect.ChannelUUID ||
		got.Type != expect.Type ||
		got.TsCreated != expect.TsCreated {
		t.Fatalf("expected %#v, got %#v", expect, got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"a.id", "a.uuid", "o.uuid", "c.uuid", "a.type", "a.configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted", "ref.configuration_template",
	}).AddRow(a1.ID, a1.UUID.String(), a1.OrganizationUUID.String(), a1.ChannelUUID.String(), a1.Type, []byte(`{}`), now, nil, nil, []byte(`[]`))
	mock.ExpectQuery(`
		SELECT a\.id, a\.uuid, o\.uuid, c\.uuid, a\.type, a\.configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted, ref\.configuration_template
		FROM alert AS a, organization AS o, channel AS c, alert_type AS ref
		WHERE a\.channel = c\.id AND c\.organization = o\.id AND a\.type = ref\.label AND a\.ts_deleted IS NULL AND c\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \?
		LIMIT -1 OFFSET 1
		`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	alerts, err = mockDb.ListAlerts(testOrganizationUUID, 0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(alerts) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(alerts))
	}
	got = alerts[0]
	expect = &a1

	if got.ID != expect.ID ||
		got.UUID != expect.UUID ||
		got.OrganizationUUID != expect.OrganizationUUID ||
		got.ChannelUUID != expect.ChannelUUID ||
		got.Type != expect.Type ||
		got.TsCreated != expect.TsCreated {
		t.Fatalf("expected %#v, got %#v", expect, got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"a.id", "a.uuid", "o.uuid", "c.uuid", "a.type", "a.configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted", "ref.configuration_template",
	}).AddRow(a0.ID, a0.UUID.String(), a0.OrganizationUUID.String(), a0.ChannelUUID.String(), a0.Type, []byte(`{"recurrence":3}`), now, nil, nil, []byte(recurrenceTemplate))
	mock.ExpectQuery(`
		SELECT a\.id, a\.uuid, o\.uuid, c\.uuid, a\.type, a\.configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted, ref\.configuration_template
		FROM alert AS a, organization AS o, channel AS c, alert_type AS ref
		WHERE a\.channel = c\.id AND c\.organization = o\.id AND a\.type = ref\.label AND a\.ts_deleted IS NULL AND c\.ts_deleted IS NULL AND o\.ts_deleted IS NULL AND o\.uuid = \?
		LIMIT 1 OFFSET 0
		`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	alerts, err = mockDb.ListAlerts(testOrganizationUUID, 1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(alerts) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(alerts))
	}
	got = alerts[0]
	expect = &a0

	if got.ID != expect.ID ||
		got.UUID != expect.UUID ||
		got.OrganizationUUID != expect.OrganizationUUID ||
		got.ChannelUUID != expect.ChannelUUID ||
		got.Type != expect.Type ||
		got.TsCreated != expect.TsCreated {
		t.Fatalf("expected %#v, got %#v", expect, got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestListAlertsByChannel(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	a0 := Alert{
		ID:               1,
		UUID:             testAlertUUID,
		OrganizationUUID: testOrganizationUUID,
		ChannelUUID:      testChannelUUID,
		Type:             "recurrence",
		Configuration:    map[string]interface{}{"recurrence": 3.0},
		TsCreated:        now,
	}
	a1 := Alert{
		ID:               2,
		UUID:             uuid.New(),
		OrganizationUUID: testOrganizationUUID,
		ChannelUUID:      testChannelUUID,
		Type:             "change",
		Configuration:    map[string]interface{}{},
		TsCreated:        now,
	}

	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "o.uuid", "c.uuid", "a.type", "a.configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted", "ref.configuration_template",
	}).
		AddRow(a0.ID, a0.UUID.String(), a0.OrganizationUUID.String(), a0.ChannelUUID.String(), a0.Type, []byte(`{"recurrence":3}`), now, nil, nil, []byte(recurrenceTemplate)).
		AddRow(a1.ID, a1.UUID.String(), a1.OrganizationUUID.String(), a1.ChannelUUID.String(), a1.Type, []byte(`{}`), now, nil, nil, []byte(`[]`))
	mock.ExpectQuery(`
		SELECT a\.id, a\.uuid, o\.uuid, c\.uuid, a\.type, a\.configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted, ref\.configuration_template
		FROM alert AS a
			INNER JOIN channel AS c ON a\.channel = c\.id
			INNER JOIN organization AS o ON c\.organization = o\.id
			INNER JOIN alert_type AS ref ON a\.type = ref\.label
		`).
		WithArgs(testOrganizationUUID, testChannelUUID).
		WillReturnRows(rows)
	alerts, err := mockDb.ListAlertsByChannel(testOrganizationUUID, testChannelUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(alerts) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(alerts))
	}

	got := alerts[0]
	expect := &a0

	if got.ID != expect.ID ||
		got.UUID != expect.UUID ||
		got.OrganizationUUID != expect.OrganizationUUID ||
		got.ChannelUUID != expect.ChannelUUID ||
		got.Type != expect.Type ||
		got.TsCreated != expect.TsCreated {
		t.Fatalf("expected %#v, got %#v", expect, got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetAlert(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	expect := Alert{
		ID:               1,
		UUID:             testAlertUUID,
		OrganizationUUID: testOrganizationUUID,
		ChannelUUID:      testChannelUUID,
		Type:             "recurrence",
		Configuration:    map[string]interface{}{"recurrence": 3.0},
		TsCreated:        now,
	}
	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "o.uuid", "c.uuid", "a.type", "a.configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted", "ref.configuration_template",
	}).AddRow(expect.ID, expect.UUID.String(), expect.OrganizationUUID.String(), expect.ChannelUUID.String(), expect.Type, []byte(`{"recurrence":3}`), now, nil, nil, []byte(recurrenceTemplate))
	mock.ExpectQuery(`
		SELECT a\.id, a\.uuid, o\.uuid, c\.uuid, a\.type, a\.configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted, ref\.configuration_template
		FROM alert AS a, organization AS o, channel AS c
		`).
		WithArgs(testOrganizationUUID, testAlertUUID).
		WillReturnRows(rows)

	got, err := mockDb.GetAlert(testOrganizationUUID, testAlertUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != expect.ID ||
		got.UUID != expect.UUID ||
		got.OrganizationUUID != expect.OrganizationUUID ||
		got.ChannelUUID != expect.ChannelUUID ||
		got.Type != expect.Type ||
		got.TsCreated != expect.TsCreated {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"a.id", "a.uuid", "a.display_name", "o.uuid", "a.ts_configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted",
	})

	u0 := uuid.New()
	u1 := uuid.New()
	mock.ExpectQuery(`
		SELECT a\.id, a\.uuid, o\.uuid, c\.uuid, a\.type, a\.configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted, ref\.configuration_template
		FROM alert AS a, organization AS o, channel AS c
		`).
		WithArgs(u0, u1).
		WillReturnRows(rows)

	got, err = mockDb.GetAlert(u0, u1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got != nil {
		t.Fatalf("expected nil, got %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestCreateAlert(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	alert := Alert{
		OrganizationUUID: testOrganizationUUID,
		ChannelUUID:      testChannelUUID,
		Type:             "recurrence",
		Configuration:    map[string]interface{}{"recurrence": int64(3)},
	}

	mock.ExpectQuery(`SELECT configuration_template FROM alert_type`).
		WithArgs(alert.Type).
		WillReturnRows(sqlmock.NewRows([]string{"configuration_template"}).AddRow([]byte(recurrenceTemplate)))

	configuration, _ := json.Marshal(alert.Configuration)
	uuidArg := StoreArg()
	mock.ExpectExec(`INSERT INTO alert`).
		WithArgs(uuidArg, alert.Type, configuration, alert.ChannelUUID, alert.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "o.uuid", "c.uuid", "a.type", "a.configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted", "ref.configuration_template",
	}).AddRow(1, testAlertUUID.String(), alert.OrganizationUUID.String(), alert.ChannelUUID.String(), alert.Type, []byte(`{"recurrence":3}`), now, nil, nil, []byte(recurrenceTemplate))
	mock.ExpectQuery(`
		SELECT a\.id, a\.uuid, o\.uuid, c\.uuid, a\.type, a\.configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted, ref\.configuration_template
		FROM alert AS a, organization AS o, channel AS c
		`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.CreateAlert(&alert)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if got.ID != 1 ||
		got.UUID != testAlertUUID ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.ChannelUUID != testChannelUUID ||
		got.Type != "recurrence" ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdateAlert(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	alert := Alert{
		UUID:             testAlertUUID,
		OrganizationUUID: testOrganizationUUID,
		ChannelUUID:      testChannelUUID,
		Type:             "recurrence",
		Configuration:    map[string]interface{}{"recurrence": int64(3)},
	}

	mock.ExpectQuery(`SELECT configuration_template FROM alert_type`).
		WithArgs("recurrence").
		WillReturnRows(sqlmock.NewRows([]string{"configuration_template"}).AddRow([]byte(recurrenceTemplate)))
	mock.ExpectExec(`UPDATE alert SET .+ ts_updated=\(strftime\('%s', 'now'\)\) .+`).
		WithArgs(alert.ChannelUUID, alert.OrganizationUUID, alert.Type, []byte(`{"recurrence":3}`), alert.UUID, alert.OrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	uuidArg := StoreArg()
	now := time.Now()
	rows := sqlmock.NewRows([]string{
		"a.id", "a.uuid", "o.uuid", "c.uuid", "a.type", "a.configuration", "a.ts_created", "a.ts_updated", "a.ts_deleted", "ref.configuration_template",
	}).AddRow(1, testAlertUUID.String(), alert.OrganizationUUID.String(), alert.ChannelUUID.String(), alert.Type, []byte(`{"recurrence":3}`), now, nil, nil, []byte(recurrenceTemplate))
	mock.ExpectQuery(`
		SELECT a\.id, a\.uuid, o\.uuid, c\.uuid, a\.type, a\.configuration, a\.ts_created, a\.ts_updated, a\.ts_deleted, ref\.configuration_template
		FROM alert AS a, organization AS o, channel AS c
		`).
		WithArgs(testOrganizationUUID, uuidArg).
		WillReturnRows(rows)

	got, err := mockDb.UpdateAlert(&alert)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	u, ok := uuidArg.Value().(string)
	if !ok ||
		u != testAlertUUID.String() ||
		got.ID != 1 ||
		got.UUID != testAlertUUID ||
		got.OrganizationUUID != testOrganizationUUID ||
		got.ChannelUUID != testChannelUUID ||
		got.Type != "recurrence" ||
		got.TsCreated != now {
		t.Fatalf("unexpected result %v", *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteAlert(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectExec(`DELETE FROM target_probe_alert WHERE`).
		WithArgs(testAlertUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 8))

	mock.ExpectExec(`UPDATE alert 
		SET ts_deleted=\(strftime\('%s', 'now'\)\)`).
		WithArgs(testAlertUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = mockDb.DeleteAlert(testOrganizationUUID, testAlertUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
