// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/mozillazg/go-slugify"
)

// Tag is the representation of a tag in the DB
type Tag struct {
	ID               int
	OrganizationUUID uuid.UUID
	Slug             string
	DisplayName      string
	Note             string
	Color            string
	TsCreated        time.Time
	TsUpdated        time.Time
	TsDeleted        time.Time
}

// ListTags returns all Tags from the DBConf
func (db *SQLiteDB) ListTags(organizationUUID uuid.UUID, pageSize int32, pageOffset uint64) (tags []*Tag, err error) {
	req := new(strings.Builder)
	fmt.Fprint(req, `
		SELECT t.id, o.uuid, t.slug, t.display_name, t.note, t.color, t.ts_created, t.ts_updated, t.ts_deleted
		FROM tag AS t, organization AS o
		WHERE t.organization = o.id AND t.ts_deleted IS NULL AND o.ts_deleted IS NULL
			AND o.uuid = ?
	`)

	if pageSize > 0 || pageOffset > 0 {
		// if pageSize is 0, returns all the results (starting from pageOffset)
		if pageSize == 0 {
			pageSize = -1
		}
		fmt.Fprintf(req, " LIMIT %d OFFSET %d", pageSize, pageOffset)
	}

	rows, err := db.client.Query(req.String(), organizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	defer rows.Close()

	tags = make([]*Tag, 0)
	for rows.Next() {
		t, err := scanTag(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		tags = append(tags, t)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Tag{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return
}

// GetTag returns a Tag in the DB based on its name and its parent organization's UUID
func (db *SQLiteDB) GetTag(organizationUUID uuid.UUID, slug string) (*Tag, error) {
	t, err := scanTag(db.client.QueryRow(`
		SELECT t.id, o.uuid, t.slug, t.display_name, t.note, t.color, t.ts_created, t.ts_updated, t.ts_deleted
		FROM tag AS t, organization AS o
		WHERE t.organization = o.id AND t.ts_deleted IS NULL
			AND o.uuid = ? AND t.slug = ?
	`, organizationUUID, slug))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve Tag: %s", err)
	}

	return t, nil
}

// Matches all the final digits in a string
var slugRegexp = regexp.MustCompile(`\d+$`)

// CreateTag creates a new Tag in the DB
func (db *SQLiteDB) CreateTag(tag *Tag) (*Tag, error) {
	if !validateTagColor(tag.Color) {
		return nil, fmt.Errorf("invalid color string")
	}
	if tag.DisplayName == "" {
		return nil, fmt.Errorf("missing display name")
	}

	slug, err := db.slugifyName(tag)
	if err != nil {
		return nil, fmt.Errorf("failed to generate slug name: %s", err)
	}

	_, err = db.client.Exec(
		`INSERT INTO tag (slug, display_name, note, color, organization)
		VALUES (?,?,?,?,
			(SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid = ?)
		)`,
		slug, tag.DisplayName, tag.Note, tag.Color, tag.OrganizationUUID)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}

	return db.GetTag(tag.OrganizationUUID, slug)
}

// UpdateTag updates an existing Tag in the DB
func (db *SQLiteDB) UpdateTag(tag *Tag) (*Tag, error) {
	if tag.Slug == "" {
		return nil, fmt.Errorf("missing tag slug")
	}

	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, "UPDATE tag SET ")

	// User-supplied fields to change
	// * Can't change slug as it is used to identify tag
	fmt.Fprint(req, "note=?, ")
	args = append(args, tag.Note)

	if tag.DisplayName != "" {
		fmt.Fprint(req, "display_name=?, ")
		args = append(args, tag.DisplayName)
	}

	if validateTagColor(tag.Color) {
		fmt.Fprint(req, "color=?, ")
		args = append(args, tag.Color)
	}

	// Change update timestamp
	fmt.Fprintf(req, `ts_updated=(strftime('%%s', 'now')) `)

	// Tag & Organization constraint
	fmt.Fprint(req, `WHERE slug=? AND organization = (SELECT id FROM organization WHERE ts_deleted IS NULL AND uuid=?) `)
	args = append(args, tag.Slug, tag.OrganizationUUID)

	// Soft delete constraint
	fmt.Fprint(req, `AND ts_deleted IS NULL`)

	res, err := db.client.Exec(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("error running the query: %s", err)
	}
	if rowsAffected, err := res.RowsAffected(); err == nil && rowsAffected < 1 {
		return nil, fmt.Errorf("failed to update tag")
	}

	return db.GetTag(tag.OrganizationUUID, tag.Slug)
}

// DeleteTag deletes a Tag from the DB
func (db *SQLiteDB) DeleteTag(organizationUUID uuid.UUID, slug string) error {
	// Delete tag
	_, err := db.client.Exec(`UPDATE tag 
		SET ts_deleted=(strftime('%s', 'now'))
		WHERE id = (SELECT t.id FROM tag AS t, organization AS o
			WHERE t.organization=o.id AND t.slug=? AND o.uuid=?)
		AND ts_deleted IS NULL`,
		slug, organizationUUID)
	if err != nil {
		return fmt.Errorf("failed to delete Tag: %s", err)
	}

	return nil
}

func scanTag(r row) (*Tag, error) {
	var t Tag
	var tsCreated, tsUpdated, tsDeleted NullTime
	err := r.Scan(
		&t.ID,
		&t.OrganizationUUID,
		&t.Slug,
		&t.DisplayName,
		&t.Note,
		&t.Color,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, err
	}
	if !validateTagColor(t.Color) {
		return nil, fmt.Errorf("invalid tag color")
	}
	if tsCreated.Valid {
		t.TsCreated = tsCreated.Time
	} else {
		t.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		t.TsUpdated = tsUpdated.Time
	} else {
		t.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		t.TsDeleted = tsDeleted.Time
	} else {
		t.TsDeleted = time.Time{}
	}
	return &t, nil
}

var tagColorRegexp *regexp.Regexp
var tagColorRegexpOnce sync.Once

func validateTagColor(color string) bool {
	tagColorRegexpOnce.Do(func() {
		tagColorRegexp = regexp.MustCompile(`^#[0-9a-fA-F]{6}$`)
	})
	return tagColorRegexp.MatchString(color)
}

func (db *SQLiteDB) slugifyName(tag *Tag) (string, error) {
	slug := slugify.Slugify(tag.DisplayName)
	slugQuery := `^` + slug + `(-\d+)?$`
	res := db.client.QueryRow(
		`SELECT t.slug 
		FROM tag AS t, organization AS o
		WHERE t.organization = o.id
		AND t.slug REGEXP ? AND o.uuid = ?
		ORDER BY t.slug DESC
		LIMIT 1`,
		slugQuery,
		tag.OrganizationUUID,
	)
	var s string
	err := res.Scan(&s)
	if err == sql.ErrNoRows {
		return slug, nil
	}
	if err != nil {
		return "", err
	}

	idxStr := slugRegexp.FindString(s)
	if idxStr == "" {
		return slug + "-1", nil
	}
	idx, err := strconv.Atoi(idxStr)
	if err != nil {
		return slug, nil
	}

	return slug + "-" + strconv.Itoa(idx+1), nil
}
