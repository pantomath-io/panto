// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"reflect"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"

	"github.com/google/uuid"
)

const addressTemplate = `[{"name":"address","type":"string"}]`

func TestListTargetProbeAgent(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	tpa0 := TargetProbeAgent{
		UUID:               testTargetProbeAgentUUID,
		OrganizationUUID:   testOrganizationUUID,
		AgentUUID:          testAgentUUID,
		TargetProbeUUID:    testTargetProbeUUID,
		ProbeLabel:         "foo",
		ProbeConfiguration: map[string]interface{}{"address": "target.pantomath.io"},
		Schedule:           5 * time.Minute,
		TsCreated:          now,
	}
	tpa1 := TargetProbeAgent{
		UUID:               uuid.New(),
		OrganizationUUID:   testOrganizationUUID,
		AgentUUID:          testAgentUUID,
		TargetProbeUUID:    uuid.New(),
		ProbeLabel:         "bar",
		ProbeConfiguration: map[string]interface{}{},
		Schedule:           time.Hour,
		TsCreated:          now.Add(time.Hour),
	}

	rows := sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).
		AddRow(tpa0.UUID.String(), tpa0.OrganizationUUID.String(), tpa0.AgentUUID.String(), tpa0.TargetProbeUUID.String(), tpa0.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(tpa0.Schedule), []byte(addressTemplate), now, nil, nil).
		AddRow(tpa1.UUID.String(), tpa1.OrganizationUUID.String(), tpa1.AgentUUID.String(), tpa1.TargetProbeUUID.String(), tpa1.ProbeLabel, []byte(`{}`), int64(tpa1.Schedule), []byte(addressTemplate), tpa1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p`).
		WithArgs(testOrganizationUUID, testAgentUUID).
		WillReturnRows(rows)
	agents, err := mockDb.ListTargetProbeAgents(testOrganizationUUID, testAgentUUID, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(agents) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(agents))
	}
	if !reflect.DeepEqual(agents[0], &tpa0) {
		t.Fatalf("expected %v, got %v", tpa0, *agents[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).AddRow(tpa1.UUID.String(), tpa1.OrganizationUUID.String(), tpa1.AgentUUID.String(), tpa1.TargetProbeUUID.String(), tpa1.ProbeLabel, []byte(`{}`), int64(tpa1.Schedule), []byte(addressTemplate), tpa1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		.+ LIMIT -1 OFFSET 1`).
		WithArgs(testOrganizationUUID, testAgentUUID).
		WillReturnRows(rows)
	agents, err = mockDb.ListTargetProbeAgents(testOrganizationUUID, testAgentUUID, 0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(agents) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(agents))
	}
	if !reflect.DeepEqual(agents[0], &tpa1) {
		t.Fatalf("expected %v, got %v", tpa1, *agents[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).AddRow(tpa0.UUID.String(), tpa0.OrganizationUUID.String(), tpa0.AgentUUID.String(), tpa0.TargetProbeUUID.String(), tpa0.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(tpa0.Schedule), []byte(addressTemplate), now, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		.+ LIMIT 1 OFFSET 0`).
		WithArgs(testOrganizationUUID, testAgentUUID).
		WillReturnRows(rows)
	agents, err = mockDb.ListTargetProbeAgents(testOrganizationUUID, testAgentUUID, 1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(agents) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(agents))
	}
	if !reflect.DeepEqual(agents[0], &tpa0) {
		t.Fatalf("expected %v, got %v", tpa0, *agents[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).AddRow(tpa0.UUID.String(), tpa0.OrganizationUUID.String(), tpa0.AgentUUID.String(), tpa0.TargetProbeUUID.String(), tpa0.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(tpa0.Schedule), []byte(addressTemplate), now, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		.+ LIMIT 1 OFFSET 1`).
		WithArgs(testOrganizationUUID, testAgentUUID).
		WillReturnRows(rows)
	agents, err = mockDb.ListTargetProbeAgents(testOrganizationUUID, testAgentUUID, 1, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(agents) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(agents))
	}
	if !reflect.DeepEqual(agents[0], &tpa0) {
		t.Fatalf("expected %v, got %v", tpa0, *agents[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestListTargetProbeAgentForOrganization(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	tpa0 := TargetProbeAgent{
		UUID:               testTargetProbeAgentUUID,
		OrganizationUUID:   testOrganizationUUID,
		AgentUUID:          testAgentUUID,
		TargetProbeUUID:    testTargetProbeUUID,
		ProbeLabel:         "foo",
		ProbeConfiguration: map[string]interface{}{"address": "target.pantomath.io"},
		Schedule:           5 * time.Minute,
		TsCreated:          now,
	}
	tpa1 := TargetProbeAgent{
		UUID:               uuid.New(),
		OrganizationUUID:   testOrganizationUUID,
		AgentUUID:          testAgentUUID,
		TargetProbeUUID:    uuid.New(),
		ProbeLabel:         "bar",
		ProbeConfiguration: map[string]interface{}{},
		Schedule:           time.Hour,
		TsCreated:          now.Add(time.Hour),
	}

	rows := sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).
		AddRow(tpa0.UUID.String(), tpa0.OrganizationUUID.String(), tpa0.AgentUUID.String(), tpa0.TargetProbeUUID.String(), tpa0.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(tpa0.Schedule), []byte(addressTemplate), now, nil, nil).
		AddRow(tpa1.UUID.String(), tpa1.OrganizationUUID.String(), tpa1.AgentUUID.String(), tpa1.TargetProbeUUID.String(), tpa1.ProbeLabel, []byte(`{}`), int64(tpa1.Schedule), []byte(addressTemplate), tpa1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	tpas, err := mockDb.ListTargetProbeAgentsForOrganization(testOrganizationUUID, []uuid.UUID{}, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tpas) != 2 {
		t.Fatalf("expected %d results, got %d", 2, len(tpas))
	}
	if !reflect.DeepEqual(tpas[0], &tpa0) {
		t.Fatalf("expected %v, got %v", tpa0, *tpas[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).AddRow(tpa1.UUID.String(), tpa1.OrganizationUUID.String(), tpa1.AgentUUID.String(), tpa1.TargetProbeUUID.String(), tpa1.ProbeLabel, []byte(`{}`), int64(tpa1.Schedule), []byte(addressTemplate), tpa1.TsCreated, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		.+ LIMIT -1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	tpas, err = mockDb.ListTargetProbeAgentsForOrganization(testOrganizationUUID, []uuid.UUID{}, 0, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tpas) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(tpas))
	}
	if !reflect.DeepEqual(tpas[0], &tpa1) {
		t.Fatalf("expected %v, got %v", tpa1, *tpas[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).AddRow(tpa0.UUID.String(), tpa0.OrganizationUUID.String(), tpa0.AgentUUID.String(), tpa0.TargetProbeUUID.String(), tpa0.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(tpa0.Schedule), []byte(addressTemplate), now, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		.+ LIMIT 1 OFFSET 0`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	tpas, err = mockDb.ListTargetProbeAgentsForOrganization(testOrganizationUUID, []uuid.UUID{}, 1, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tpas) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(tpas))
	}
	if !reflect.DeepEqual(tpas[0], &tpa0) {
		t.Fatalf("expected %v, got %v", tpa0, *tpas[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).AddRow(tpa0.UUID.String(), tpa0.OrganizationUUID.String(), tpa0.AgentUUID.String(), tpa0.TargetProbeUUID.String(), tpa0.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(tpa0.Schedule), []byte(addressTemplate), now, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p
		.+ LIMIT 1 OFFSET 1`).
		WithArgs(testOrganizationUUID).
		WillReturnRows(rows)
	tpas, err = mockDb.ListTargetProbeAgentsForOrganization(testOrganizationUUID, []uuid.UUID{}, 1, 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tpas) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(tpas))
	}
	if !reflect.DeepEqual(tpas[0], &tpa0) {
		t.Fatalf("expected %v, got %v", tpa0, *tpas[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}

	rows = sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).
		AddRow(tpa0.UUID.String(), tpa0.OrganizationUUID.String(), tpa0.AgentUUID.String(), tpa0.TargetProbeUUID.String(), tpa0.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(tpa0.Schedule), []byte(addressTemplate), now, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p`).
		WithArgs(testOrganizationUUID, tpa0.TargetProbeUUID).
		WillReturnRows(rows)
	tpas, err = mockDb.ListTargetProbeAgentsForOrganization(testOrganizationUUID, []uuid.UUID{tpa0.TargetProbeUUID}, 0, 0)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(tpas) != 1 {
		t.Fatalf("expected %d results, got %d", 1, len(tpas))
	}
	if !reflect.DeepEqual(tpas[0], &tpa0) {
		t.Fatalf("expected %v, got %v", tpa0, *tpas[0])
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetTargetProbeAgent(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	expect := TargetProbeAgent{
		UUID:               testTargetProbeAgentUUID,
		OrganizationUUID:   testOrganizationUUID,
		AgentUUID:          testAgentUUID,
		TargetProbeUUID:    testTargetProbeUUID,
		ProbeLabel:         "foo",
		ProbeConfiguration: map[string]interface{}{"address": "target.pantomath.io"},
		Schedule:           5 * time.Minute,
		TsCreated:          now,
	}

	rows := sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).
		AddRow(expect.UUID.String(), expect.OrganizationUUID.String(), expect.AgentUUID.String(), expect.TargetProbeUUID.String(), expect.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(expect.Schedule), []byte(addressTemplate), now, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p`).
		WithArgs(testTargetProbeAgentUUID, testOrganizationUUID, testAgentUUID).
		WillReturnRows(rows)
	got, err := mockDb.GetTargetProbeAgent(testOrganizationUUID, testAgentUUID, testTargetProbeAgentUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(got, &expect) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestCreateTargetProbeAgent(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	expect := TargetProbeAgent{
		UUID:               testTargetProbeAgentUUID,
		OrganizationUUID:   testOrganizationUUID,
		AgentUUID:          testAgentUUID,
		TargetProbeUUID:    testTargetProbeUUID,
		ProbeLabel:         "foo",
		ProbeConfiguration: map[string]interface{}{"address": "target.pantomath.io"},
		Schedule:           5 * time.Minute,
		TsCreated:          now,
	}
	metrics := `{"metrics":[{"name":"avg","type":"int","description":"Average time"}],"tags":[{"name":"host","description":"The host that was pinged"}]}`
	graphs := `{"graphs":[{"title":"RTT","type":"lines","metrics":[{"name":"min","color":"#000000"},{"name":"max","color":"#000000"},{"name":"avg","color":"#000000"}],"tags":[]}]}`

	// Expect GetTargetProbe
	tpRows := sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).AddRow(1, testTargetProbeUUID.String(), testTargetUUID.String(), testProbeUUID.String(), testOrganizationUUID.String(), 0, []byte("{}"), nil, nil, nil, []byte("{}"))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp`).
		WithArgs(testOrganizationUUID, testTargetProbeUUID).
		WillReturnRows(tpRows)
	// Expect GetProbe
	pRows := sqlmock.NewRows([]string{"id", "uuid", "label", "display_name", "description", "configuration_template", "metrics", "graphs", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(1, testProbeUUID.String(), "", "", "", []byte(addressTemplate), []byte(metrics), []byte(graphs), nil, nil, nil)
	mock.ExpectQuery(`SELECT id, uuid, label, display_name, description, configuration_template, metrics, graphs, ts_created, ts_updated, ts_deleted
	FROM probe`).
		WithArgs(testProbeUUID).
		WillReturnRows(pRows)

	uuidArg := StoreArg()
	mock.ExpectExec(`INSERT INTO target_probe_agent \(uuid, probe_configuration, schedule, agent, target_probe\)`).
		WithArgs(uuidArg, []byte(`{"address":"target.pantomath.io"}`), expect.Schedule,
			testAgentUUID, testOrganizationUUID,
			testTargetProbeUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec(`UPDATE agent SET ts_configuration=`).
		WithArgs(testAgentUUID, testOrganizationUUID).WillReturnResult(sqlmock.NewResult(1, 1))

	rows := sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).
		AddRow(testTargetProbeAgentUUID.String(), expect.OrganizationUUID.String(), expect.AgentUUID.String(), expect.TargetProbeUUID.String(), expect.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(expect.Schedule), []byte(addressTemplate), now, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p`).
		WithArgs(uuidArg, testOrganizationUUID, testAgentUUID).
		WillReturnRows(rows)
	got, err := mockDb.CreateTargetProbeAgent(&expect)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if _, ok := uuidArg.Value().(string); !ok {
		t.Fatalf("UUID arg is not string: %#v", uuidArg.Value())
	}
	if _, err = uuid.Parse(uuidArg.Value().(string)); err != nil {
		t.Fatalf("UUID arg is not UUID: %s", err)
	}
	if !reflect.DeepEqual(got, &expect) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdateTargetProbeAgent(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	now := time.Now()
	expect := TargetProbeAgent{
		UUID:               testTargetProbeAgentUUID,
		OrganizationUUID:   testOrganizationUUID,
		AgentUUID:          testAgentUUID,
		TargetProbeUUID:    testTargetProbeUUID,
		ProbeLabel:         "foo",
		ProbeConfiguration: map[string]interface{}{"address": "target.pantomath.io"},
		Schedule:           5 * time.Minute,
		TsCreated:          now,
	}
	metrics := `{"metrics":[{"name":"avg","type":"int","description":"Average time"}],"tags":[{"name":"host","description":"The host that was pinged"}]}`
	graphs := `{"graphs":[{"title":"RTT","type":"lines","metrics":[{"name":"min","color":"#000000"},{"name":"max","color":"#000000"},{"name":"avg","color":"#000000"}],"tags":[]}]}`
	// Expect GetTargetProbe
	tpRows := sqlmock.NewRows([]string{
		"tp.id", "tp.uuid", "t.uuid", "p.uuid", "o.uuid", "tp.state_type", "tp.state_configuration", "tp.ts_created", "tp.ts_updated", "tp.ts_deleted", "ref.configuration_template",
	}).AddRow(1, testTargetProbeUUID.String(), testTargetUUID.String(), testProbeUUID.String(), testOrganizationUUID.String(), 0, []byte("{}"), nil, nil, nil, []byte("{}"))
	mock.ExpectQuery(`
		SELECT tp\.id, tp\.uuid, t\.uuid, p\.uuid, o\.uuid, tp\.state_type, tp\.state_configuration, tp\.ts_created, tp\.ts_updated, tp\.ts_deleted, ref\.configuration_template
		FROM target_probe AS tp`).
		WithArgs(testOrganizationUUID, testTargetProbeUUID).
		WillReturnRows(tpRows)
	// Expect GetProbe
	pRows := sqlmock.NewRows([]string{"id", "uuid", "label", "display_name", "description", "configuration_template", "metrics", "graphs", "ts_created", "ts_updated", "ts_deleted"}).
		AddRow(1, testProbeUUID.String(), "", "", "", []byte(addressTemplate), []byte(metrics), []byte(graphs), nil, nil, nil)
	mock.ExpectQuery(`SELECT id, uuid, label, display_name, description, configuration_template, metrics, graphs, ts_created, ts_updated, ts_deleted
	FROM probe`).
		WithArgs(testProbeUUID).
		WillReturnRows(pRows)

	mock.ExpectExec(`UPDATE target_probe_agent SET schedule = \?, probe_configuration = \?`).
		WithArgs(expect.Schedule, []byte(`{"address":"target.pantomath.io"}`),
			testTargetProbeAgentUUID,
			testTargetProbeAgentUUID, testAgentUUID, testOrganizationUUID,
		).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec(`UPDATE agent SET ts_configuration=`).
		WithArgs(testAgentUUID, testOrganizationUUID).WillReturnResult(sqlmock.NewResult(1, 1))

	rows := sqlmock.NewRows([]string{
		"tpa.uuid", "o.uuid", "a.uuid", "tp.uuid", "p.label", "tpa.probe_configuration", "tpa.schedule", "p.configuration_template", "tpa.ts_created", "tpa.ts_updated", "tpa.ts_deleted",
	}).
		AddRow(testTargetProbeAgentUUID.String(), expect.OrganizationUUID.String(), expect.AgentUUID.String(), expect.TargetProbeUUID.String(), expect.ProbeLabel, []byte(`{"address":"target.pantomath.io"}`), int64(expect.Schedule), []byte(addressTemplate), now, nil, nil)
	mock.ExpectQuery(`SELECT tpa\.uuid, o\.uuid, a\.uuid, tp\.uuid, p\.label, tpa\.probe_configuration, tpa\.schedule, p\.configuration_template, tpa\.ts_created, tpa\.ts_updated, tpa\.ts_deleted
		FROM target_probe_agent AS tpa, target_probe AS tp, agent AS a, organization AS o, probe AS p`).
		WithArgs(testTargetProbeAgentUUID, testOrganizationUUID, testAgentUUID).
		WillReturnRows(rows)
	got, err := mockDb.UpdateTargetProbeAgent(&expect)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !reflect.DeepEqual(got, &expect) {
		t.Fatalf("expected %v, got %v", expect, *got)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteTargetProbeAgent(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	mockDb := &SQLiteDB{client: db}
	mock.ExpectExec(`UPDATE target_probe_agent 
		SET ts_deleted=\(strftime\('%s', 'now'\)\)`).
		WithArgs(testTargetProbeAgentUUID, testTargetProbeAgentUUID, testAgentUUID, testOrganizationUUID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec(`UPDATE agent SET ts_configuration=`).
		WithArgs(testAgentUUID, testOrganizationUUID).WillReturnResult(sqlmock.NewResult(1, 1))

	err = mockDb.DeleteTargetProbeAgent(testOrganizationUUID, testAgentUUID, testTargetProbeAgentUUID)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("there were unfulfilled expectations: %s", err)
	}
}
