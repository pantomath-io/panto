// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dbconf

import (
	"database/sql/driver"
	"os"
	"path/filepath"
	"reflect"
	"testing"

	"github.com/google/uuid"

	"gitlab.com/pantomath-io/panto/util"
)

// Helper type to store sqlmock arguments during request for further checks
type ArgStore struct {
	s *store // Trick to copy pointer to store in non-pointer `Match`
}

func StoreArg() ArgStore { return ArgStore{&store{}} }

type store struct{ v driver.Value }

func (a ArgStore) Value() driver.Value { return a.s.v }

// Match matches any argument if this is the first argument it meets. Otherwise,
// it matches the value in parameter with the stored value.
func (a ArgStore) Match(v driver.Value) bool {
	if a.s.v == nil {
		a.s.v = v
		return true
	}
	return reflect.DeepEqual(a.s.v, v)
}

var testClientUUID,
	testOrganizationUUID,
	testAgentUUID,
	testAltAgentUUID,
	testChannelUUID,
	testProbeUUID,
	testTargetUUID,
	testAltTargetUUID,
	testTargetProbeUUID,
	testAlertUUID,
	testTargetProbeAgentUUID,
	testUserUUID uuid.UUID

var testDB ConfigurationDB

func TestMain(m *testing.M) {
	setUp()        // Setup for tests
	res := m.Run() // Run the actual tests
	tearDown()     // Teardown after running the tests
	os.Exit(res)
}

func setUp() {
	if util.LiveTest(nil) {
		err := os.MkdirAll(filepath.Dir(dbPath), 0755)
		if err != nil {
			panic(err)
		}
		f, err := os.OpenFile(dbPath, os.O_CREATE|os.O_TRUNC, 0644)
		if err != nil {
			panic(err)
		}
		f.Close()
		testDB, err = NewSQLiteDB(dbPath)
		if err != nil {
			panic(err)
		}
	}

	testClientUUID, _ = uuid.Parse("f6e48ddb-e94c-4916-baf3-bf5dbe3a2fb3")
	testOrganizationUUID, _ = uuid.Parse("903b49cf-c12a-4340-bb2a-0385eaac317e")
	testAgentUUID, _ = uuid.Parse("9e82e551-ccee-489a-a4c8-81329e70a8a3")
	testAltAgentUUID, _ = uuid.Parse("3e6e46da-358d-484d-b7fb-7800dfa202b5")
	testChannelUUID, _ = uuid.Parse("e559e715-db7d-4023-a7be-965923fac36c")
	testProbeUUID, _ = uuid.Parse("c549129c-6fc1-4950-a7f8-05f59213cb62")
	testTargetUUID, _ = uuid.Parse("31017478-825c-459c-8406-01bfb1d2cf58")
	testAltTargetUUID, _ = uuid.Parse("3e690b01-c1bc-40e5-8ee4-065ae0e6ed0d")
	testTargetProbeUUID, _ = uuid.Parse("8b6418b5-9055-4a7b-978c-b85086e015eb")
	testAlertUUID, _ = uuid.Parse("6ba644a7-fa80-4d2d-b3fc-ed96086d60be")
	testTargetProbeAgentUUID, _ = uuid.Parse("8cb4dc3e-cce5-49bb-adcb-7094406cfbfe")
	testUserUUID, _ = uuid.Parse("f67121fc-1d20-11ea-844e-63e40047dbe5")
}

func tearDown() {}
