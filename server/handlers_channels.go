// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"encoding/json"
	"path"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ListChannelTypes returns all the ChannelTypes
func (s *server) ListChannelTypes(ctx context.Context, req *api.ListChannelTypesRequest) (res *api.ListChannelTypesResponse, err error) {
	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbChannelTypes, err := dbconf.ListChannelTypes(pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list ChannelTypes: %s", err)
		return nil, status.Error(codes.NotFound, "No Channel Type found")
	}

	var nextPage bool
	if int32(len(dbChannelTypes)) > pageSize {
		dbChannelTypes = dbChannelTypes[:pageSize]
		nextPage = true
	}

	cts := make([]*api.ChannelType, len(dbChannelTypes))
	for i, ct := range dbChannelTypes {
		// Parameters is a repeated field of type ProbeParameter
		var curParameters []*api.ChannelType_Parameter
		for _, parameter := range ct.ConfigurationTemplate {
			curParameters = append(curParameters, &api.ChannelType_Parameter{
				Name:        parameter.Name,
				Type:        parameter.Type,
				Description: parameter.Description,
				Mandatory:   parameter.Mandatory,
			})
		}

		cts[i] = &api.ChannelType{
			Name:          path.Join(api.ResourceNameChannelType, ct.Label),
			Label:         ct.Label,
			Configuration: curParameters,
		}
	}

	res = &api.ListChannelTypesResponse{
		Channeltypes: cts,
	}

	if nextPage {
		nextPageToken, err := util.NextPageToken(req, pageOffset+uint64(len(cts)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			nextPageToken = ""
		}
		res.NextPageToken = nextPageToken
	}

	return
}

// ListChannels returns all the Channels associated to the parent Organization
func (s *server) ListChannels(ctx context.Context, req *api.ListChannelsRequest) (res *api.ListChannelsResponse, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization name")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := util.ValidatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	// validate nested children mask
	acceptedChildren := []string{"channel_type"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid field in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbChannels, err := dbconf.ListChannels(organizationUUID, pageSize+1, pageOffset)
	if err != nil {
		log.Errorf("failed to list Channels: %s", err)
		return nil, status.Error(codes.NotFound, "No Channels found")
	}

	var nextPage bool
	if int32(len(dbChannels)) > pageSize {
		dbChannels = dbChannels[:pageSize]
		nextPage = true
	}

	var channels []*api.Channel
Channels:
	for _, channel := range dbChannels {
		channelConfig, err := json.Marshal(channel.Configuration)
		if err != nil {
			log.Errorf("unable to marshal configuration to JSON: %s", err)
			continue Channels
		}

		chanType, err := dbconf.GetChannelType(channel.Type)
		if err != nil {
			log.Errorf("couldn't get the channel type %s: %s", channel.Type, err)
			return nil, status.Error(codes.Internal, "Channel Type not found")
		}

		apiChan := &api.Channel{
			Name: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
				api.ResourceNameChannel, util.Base64Encode(channel.UUID),
			),
			ChannelType:   path.Join(api.ResourceNameChannelType, chanType.Label),
			DisplayName:   channel.DisplayName,
			Configuration: string(channelConfig),
		}

		if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
			apiChan.Children = &api.Channel_Children{}
			if util.Contains(req.ChildrenMask.Paths, "channel_type") {
				// Parameters is a repeated field of type ProbeParameter
				var curParameters []*api.ChannelType_Parameter
				for _, parameter := range chanType.ConfigurationTemplate {
					curParameters = append(curParameters, &api.ChannelType_Parameter{
						Name:        parameter.Name,
						Type:        parameter.Type,
						Description: parameter.Description,
						Mandatory:   parameter.Mandatory,
					})
				}

				apiChan.Children.ChannelType = &api.ChannelType{
					Name:          path.Join(api.ResourceNameChannelType, chanType.Label),
					Label:         chanType.Label,
					Configuration: curParameters,
				}
			}
		}

		channels = append(channels, apiChan)
	}

	res = &api.ListChannelsResponse{
		Channels: channels,
	}

	if nextPage {
		res.NextPageToken, err = util.NextPageToken(req, pageOffset+uint64(len(channels)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetChannel returns a specific Channel
func (s *server) GetChannel(ctx context.Context, req *api.GetChannelRequest) (*api.Channel, error) {
	if len(req.Name) == 0 {
		log.Errorf("missing Channel name")
		return nil, status.Error(codes.InvalidArgument, "Missing Channel name")
	}

	// get the organization and channel UUIDs from the name field
	organizationUUID, channelUUID, err := api.ParseNameChannel(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Channel name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Channel name")
	}

	// validate nested children mask
	acceptedChildren := []string{"channel_type"}
	if req.ChildrenMask != nil {
		for _, child := range req.ChildrenMask.Paths {
			if !util.Contains(acceptedChildren, child) {
				log.Errorf("invalid field in children_mask: %s", child)
				return nil, status.Errorf(codes.InvalidArgument, "Invalid field in children_mask: %s", child)
			}
		}
	}

	dbconf, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}
	dbChannel, err := dbconf.GetChannel(organizationUUID, channelUUID)
	if err != nil {
		log.Errorf("failed to get Channel: %s", err)
		return nil, status.Error(codes.Internal, "Channel not found")
	}

	channelConfig, err := json.Marshal(dbChannel.Configuration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid Channel configuration")
	}

	chanType, err := dbconf.GetChannelType(dbChannel.Type)
	if err != nil {
		log.Errorf("couldn't get the channel type %s: %s", dbChannel.Type, err)
		return nil, status.Error(codes.Internal, "Channel Type not found")
	}

	res := api.Channel{
		Name: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
			api.ResourceNameChannel, util.Base64Encode(dbChannel.UUID),
		),
		ChannelType:   path.Join(api.ResourceNameChannelType, chanType.Label),
		DisplayName:   dbChannel.DisplayName,
		Configuration: string(channelConfig),
	}

	if req.ChildrenMask != nil && len(req.ChildrenMask.Paths) > 0 {
		res.Children = &api.Channel_Children{}
		if util.Contains(req.ChildrenMask.Paths, "channel_type") {
			// Parameters is a repeated field of type ProbeParameter
			var curParameters []*api.ChannelType_Parameter
			for _, parameter := range chanType.ConfigurationTemplate {
				curParameters = append(curParameters, &api.ChannelType_Parameter{
					Name:        parameter.Name,
					Type:        parameter.Type,
					Description: parameter.Description,
					Mandatory:   parameter.Mandatory,
				})
			}

			res.Children.ChannelType = &api.ChannelType{
				Name:          path.Join(api.ResourceNameChannelType, chanType.Label),
				Label:         chanType.Label,
				Configuration: curParameters,
			}
		}
	}

	return &res, nil
}

// CreateChannel inserts a new Channel in an Organization
func (s *server) CreateChannel(ctx context.Context, req *api.CreateChannelRequest) (res *api.Channel, err error) {
	if len(req.Parent) == 0 {
		log.Errorf("missing parent Organization")
		return nil, status.Error(codes.InvalidArgument, "Missing parent Organization name")
	}

	// get the organization UUID from the parent field
	organizationUUID, err := api.ParseNameOrganization(req.Parent)
	if err != nil {
		log.Errorf("couldn't parse parent Organization name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid parent Organization name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	if req.GetChannel() == nil {
		log.Errorf("missing channel object in request")
		return nil, status.Error(codes.InvalidArgument, "Missing Channel")
	}

	var channelConfig map[string]interface{}
	if err = json.Unmarshal([]byte(req.Channel.GetConfiguration()), &channelConfig); err != nil {
		log.Errorf("couldn't unmarshal JSON configuration: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Channel configuration")
	}

	channelType, err := api.ParseNameChannelType(req.Channel.ChannelType)
	if err != nil {
		log.Errorf("invalid channel type in request")
		return nil, status.Error(codes.InvalidArgument, "Invalid ChannelType")
	}

	dbChannel := &dbconf.Channel{
		OrganizationUUID: organizationUUID,
		Type:             channelType,
		DisplayName:      req.Channel.DisplayName,
		Configuration:    channelConfig,
	}
	dbChannel, err = db.CreateChannel(dbChannel)
	if err != nil {
		log.Errorf("failed to create Channel: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't create Channel")
	}

	dbchannelConfig, err := json.Marshal(dbChannel.Configuration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid Channel configuration")
	}

	chanType, err := db.GetChannelType(channelType)
	if err != nil {
		log.Errorf("couldn't get the channel type %s: %s", channelType, err)
		return nil, status.Error(codes.Internal, "Channel Type not found")
	}
	if chanType == nil {
		log.Errorf("channel type not found: %s", channelType)
		return nil, status.Error(codes.NotFound, "Channel Type not found")
	}

	res = &api.Channel{
		Name: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
			api.ResourceNameChannel, util.Base64Encode(dbChannel.UUID),
		),
		ChannelType:   path.Join(api.ResourceNameChannelType, chanType.Label),
		DisplayName:   req.Channel.DisplayName,
		Configuration: string(dbchannelConfig),
	}

	return
}

// UpdateChannel updates an existing Channel and returns it.
func (s *server) UpdateChannel(ctx context.Context, req *api.UpdateChannelRequest) (res *api.Channel, err error) {
	// Channel input
	if len(req.Channel.Name) == 0 {
		log.Errorf("missing Channel name")
		return nil, status.Error(codes.InvalidArgument, "Missing Channel name")
	}

	// get the organization and Channel UUIDs from the name field
	organizationUUID, channelUUID, err := api.ParseNameChannel(req.Channel.Name)
	if err != nil {
		log.Errorf("couldn't parse Channel name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Channel name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	updateChannel := &dbconf.Channel{
		UUID:             channelUUID,
		OrganizationUUID: organizationUUID,
	}

	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "name") {
		return nil, status.Error(codes.InvalidArgument, "Channel name is read-only")
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "display_name") {
		if req.Channel.DisplayName == "" {
			return nil, status.Error(codes.InvalidArgument, "Channel display name cannot be empty")
		}
		updateChannel.DisplayName = req.Channel.DisplayName
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "channel_type") {
		if req.Channel.ChannelType == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Channel type")
		}
		updateChannel.Type, err = api.ParseNameChannelType(req.Channel.ChannelType)
		if err != nil {
			log.Errorf("invalid channel type")
			return nil, status.Error(codes.InvalidArgument, "Invalid channel_type")
		}
	}

	// If Channel type is updated, Configuration must also be updated
	if req.UpdateMask != nil && util.Contains(req.UpdateMask.Paths, "channel_type") {
		if !util.Contains(req.UpdateMask.Paths, "configuration") {
			log.Errorf("channel_type in update mask without configuration")
			return nil, status.Error(codes.InvalidArgument, "Can't update Channel type without Configuration")
		}
	}

	if req.UpdateMask == nil ||
		(util.Contains(req.UpdateMask.Paths, "configuration") && util.Contains(req.UpdateMask.Paths, "channel_type")) {
		if req.Channel.ChannelType == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Channel Type")
		}
		if req.Channel.Configuration == "" {
			return nil, status.Error(codes.InvalidArgument, "Missing Channel Configuration")
		}
	}

	if req.UpdateMask == nil || util.Contains(req.UpdateMask.Paths, "configuration") {
		if err = json.Unmarshal([]byte(req.Channel.Configuration), &updateChannel.Configuration); err != nil {
			log.Errorf("couldn't unmarshal JSON configuration: %s", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid Channel configuration")
		}
	}

	updateChannel, err = db.UpdateChannel(updateChannel)
	if err != nil {
		log.Errorf("failed to update Channel: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't update Channel")
	}

	updateChannelConfig, err := json.Marshal(updateChannel.Configuration)
	if err != nil {
		log.Errorf("unable to marshal configuration to JSON: %s", err)
		return nil, status.Error(codes.Internal, "Invalid Channel configuration")
	}

	chanType, err := db.GetChannelType(updateChannel.Type)
	if err != nil {
		log.Errorf("couldn't get the channel type %s: %s", updateChannel.Type, err)
		return nil, status.Error(codes.Internal, "Channel Type not found")
	}
	if chanType == nil {
		log.Errorf("channel type not found: %s", updateChannel.Type)
		return nil, status.Error(codes.NotFound, "Channel Type not found")
	}

	res = &api.Channel{
		Name: path.Join(
			api.ResourceNameOrganization, util.Base64Encode(organizationUUID),
			api.ResourceNameChannel, util.Base64Encode(updateChannel.UUID),
		),
		ChannelType:   path.Join(api.ResourceNameChannelType, chanType.Label),
		DisplayName:   req.Channel.DisplayName,
		Configuration: string(updateChannelConfig),
	}

	return
}

// DeleteChannel delets an existing Channel
func (s *server) DeleteChannel(ctx context.Context, req *api.DeleteChannelRequest) (res *empty.Empty, err error) {
	if len(req.Name) == 0 {
		log.Errorf("missing Channel name")
		return nil, status.Error(codes.InvalidArgument, "Missing Channel name")
	}

	// get the organization and Channel UUIDs from the name field
	organizationUUID, ChannelUUID, err := api.ParseNameChannel(req.Name)
	if err != nil {
		log.Errorf("couldn't parse Channel name: %s", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid Channel name")
	}

	db, err := services.GetDBConf()
	if err != nil {
		log.Errorf("Couldn't connect to configuration database: %s", err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	alerts, err := db.ListAlertsByChannel(organizationUUID, ChannelUUID, 1, 0)
	if err != nil {
		log.Errorf("Couldn't list alerts linked to Channel: %s", err)
		return nil, status.Error(codes.Internal, "Internal Error")
	}
	if len(alerts) > 0 {
		return nil, status.Error(codes.FailedPrecondition, "Channel is in use by one or more Alerts")
	}

	err = db.DeleteChannel(organizationUUID, ChannelUUID)
	if err != nil {
		log.Errorf("failed to delete Channel: %s", err)
		return nil, status.Error(codes.Internal, "Couldn't delete Channel")
	}

	res = &empty.Empty{}
	return
}
