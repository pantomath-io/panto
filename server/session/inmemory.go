// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package session

import (
	"crypto/rand"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/patrickmn/go-cache"
)

const defaultTimeout = 24 * time.Hour
const defaultCleanupInterval = 20 * time.Minute

// InMemorySessionManager is a session manager using an in-memory store
type InMemorySessionManager struct {
	cache          *cache.Cache
	defaultTimeout time.Duration
}

// InMemoryConfig contains the configuration options for the in-memory cache manager
type InMemoryConfig struct {
	// the timeout duration for a session, default: 24 hours
	Timeout time.Duration
}

// NewInMemory returns a new Ristretto session manager
func NewInMemory(config InMemoryConfig) (*InMemorySessionManager, error) {
	timeout := config.Timeout
	if timeout == 0 {
		timeout = defaultTimeout
	}
	sm := InMemorySessionManager{
		cache:          cache.New(timeout, defaultCleanupInterval),
		defaultTimeout: timeout,
	}
	return &sm, nil
}

// CreateSession creates a new in-memory session manager
func (mgr *InMemorySessionManager) CreateSession(timeout time.Duration, userUUID, organizationUUID uuid.UUID) (*Session, error) {
	var key [32]byte
	n, err := rand.Read(key[:])
	if err != nil {
		return nil, fmt.Errorf("couldn't create session key: %s", err)
	}
	if n != 32 {
		return nil, fmt.Errorf("unexpected session key length: %d", n)
	}
	s := Session{
		UUID:             uuid.New(),
		Key:              key[:],
		UserUUID:         userUUID,
		OrganizationUUID: organizationUUID,
	}
	uuidString := s.UUID.String()
	if timeout == DefaultTimeout {
		mgr.cache.SetDefault(uuidString, s)
	} else {
		mgr.cache.Set(uuidString, s, timeout)
	}
	_, expires, ok := mgr.cache.GetWithExpiration(uuidString)
	if !ok {
		return nil, fmt.Errorf("session was not created")
	}
	s.Expires = expires.UTC()
	return &s, nil
}

// GetSession returns a session for a given ID, if it exists
func (mgr *InMemorySessionManager) GetSession(sessionUUID uuid.UUID) (*Session, error) {
	v, expires, ok := mgr.cache.GetWithExpiration(sessionUUID.String())
	if !ok {
		return nil, nil
	}
	s, ok := v.(Session)
	if !ok {
		return nil, fmt.Errorf("invalid session")
	}
	s.Expires = expires.UTC()
	return &s, nil
}

// RefreshSession returns a session for a given ID, if it exists
func (mgr *InMemorySessionManager) RefreshSession(sessionUUID uuid.UUID, timeout time.Duration) (*Session, error) {
	v, _, ok := mgr.cache.GetWithExpiration(sessionUUID.String())
	if !ok {
		return nil, fmt.Errorf("session not found")
	}
	s, ok := v.(Session)
	if !ok {
		return nil, fmt.Errorf("invalid session")
	}
	uuidString := s.UUID.String()
	if timeout == DefaultTimeout {
		mgr.cache.SetDefault(uuidString, s)
	} else {
		mgr.cache.Set(uuidString, s, timeout)
	}
	return mgr.GetSession(sessionUUID)
}
