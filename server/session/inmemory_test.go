// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package session

import (
	"reflect"
	"testing"
	"time"

	"github.com/google/uuid"
)

var userUUID = uuid.New()
var organizationUUID = uuid.New()

func TestNewInMemory(t *testing.T) {
	_, err := NewInMemory(InMemoryConfig{})
	if err != nil {
		t.Fatalf("unexpected error creating session manager: %s", err)
	}
	_, err = NewInMemory(InMemoryConfig{Timeout: 5 * time.Minute})
	if err != nil {
		t.Fatalf("unexpected error creating session manager: %s", err)
	}
}

func TestCreateSession(t *testing.T) {
	mgr, err := NewInMemory(InMemoryConfig{})
	if err != nil {
		t.Fatalf("unexpected error creating session manager: %s", err)
	}

	s, err := mgr.CreateSession(DefaultTimeout, userUUID, organizationUUID)
	if err != nil {
		t.Fatalf("error creating session: %s", err)
	}
	var zeroUUID uuid.UUID
	if s.UUID == zeroUUID {
		t.Fatal("session UUID should not be zero")
	}
	if len(s.Key) != 32 {
		t.Fatalf("expected a 32-byte key, got %d bytes", len(s.Key))
	}
	if s.Expires.IsZero() {
		t.Fatalf("unexpected zero expiration time")
	}
	if s.Expires.Location() != time.UTC {
		t.Fatalf("expiration timestamp is not in UTC timezone")
	}
	if s.UserUUID != userUUID {
		t.Fatalf("user UUID: expected %s, got, %s", userUUID, s.UserUUID)
	}
	if s.OrganizationUUID != organizationUUID {
		t.Fatalf("user UUID: expected %s, got, %s", organizationUUID, s.OrganizationUUID)
	}
}

func TestGetSession(t *testing.T) {
	mgr, err := NewInMemory(InMemoryConfig{})
	if err != nil {
		t.Fatalf("unexpected error creating session manager: %s", err)
	}

	expect, err := mgr.CreateSession(DefaultTimeout, userUUID, organizationUUID)
	if err != nil {
		t.Fatalf("error creating session: %s", err)
	}
	got, err := mgr.GetSession(expect.UUID)
	if err != nil {
		t.Fatalf("error retrieving session: %s", err)
	}
	if !reflect.DeepEqual(expect, got) {
		t.Fatalf("expected %#v, got %#v", expect, got)
	}
}

func TestSessionExpired(t *testing.T) {
	mgr, err := NewInMemory(InMemoryConfig{Timeout: 500 * time.Millisecond})
	if err != nil {
		t.Fatalf("unexpected error creating session manager: %s", err)
	}

	expect, err := mgr.CreateSession(DefaultTimeout, userUUID, organizationUUID)
	if err != nil {
		t.Fatalf("error creating session: %s", err)
	}
	_, err = mgr.GetSession(expect.UUID)
	if err != nil {
		t.Fatalf("error retrieving session: %s", err)
	}
	time.Sleep(500 * time.Millisecond)
	got, err := mgr.GetSession(expect.UUID)
	if got == nil && err != nil {
		t.Fatalf("error retrieving session: %s", err)
	}
	now := time.Now().UTC()
	if got != nil && now.Before(got.Expires) {
		t.Fatalf("session should have expired")
	}

	mgr, err = NewInMemory(InMemoryConfig{Timeout: 24 * time.Hour})
	if err != nil {
		t.Fatalf("unexpected error creating session manager: %s", err)
	}

	expect, err = mgr.CreateSession(500*time.Millisecond, userUUID, organizationUUID)
	if err != nil {
		t.Fatalf("error creating session: %s", err)
	}
	time.Sleep(501 * time.Millisecond)
	got, err = mgr.GetSession(expect.UUID)
	if got == nil && err != nil {
		t.Fatalf("error retrieving session: %s", err)
	}
	now = time.Now().UTC()
	if got != nil && now.Before(got.Expires) {
		t.Fatalf("session should have expired")
	}
}

func TestRefreshSession(t *testing.T) {
	mgr, err := NewInMemory(InMemoryConfig{Timeout: 500 * time.Millisecond})
	if err != nil {
		t.Fatalf("unexpected error creating session manager: %s", err)
	}
	expect, err := mgr.CreateSession(500*time.Millisecond, userUUID, organizationUUID)
	if err != nil {
		t.Fatalf("error creating session: %s", err)
	}
	time.Sleep(200 * time.Millisecond)
	got, err := mgr.GetSession(expect.UUID)
	if got == nil && err != nil {
		t.Fatalf("error retrieving session: %s", err)
	}
	now := time.Now().UTC()
	if got != nil && now.After(got.Expires) {
		t.Fatalf("session should not have expired")
	}

	mgr.RefreshSession(expect.UUID, DefaultTimeout)
	time.Sleep(501 * time.Millisecond)
	got, err = mgr.GetSession(expect.UUID)
	if got == nil && err != nil {
		t.Fatalf("error retrieving session: %s", err)
	}
	now = time.Now().UTC()
	if got != nil && now.Before(got.Expires) {
		t.Fatalf("session should have expired")
	}
}
