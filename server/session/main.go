// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package session

import (
	"time"

	"github.com/google/uuid"
)

// Session contains all the data related to a user session
type Session struct {
	UUID uuid.UUID
	// A temporary API key attached to the session
	Key []byte
	// Session will expire after this time.
	Expires time.Time
	//  The UUID of the user of this session
	UserUUID uuid.UUID
	//  The UUID of the organization the user belongs to
	OrganizationUUID uuid.UUID
}

// DefaultTimeout is used
const DefaultTimeout time.Duration = 0

// Manager is an interface describing a session management system
type Manager interface {
	// Creates a new session
	CreateSession(timeout time.Duration, userUUID, organizationUUID uuid.UUID) (*Session, error)
	// Returns a session for a given ID, or nil if no such session exists
	GetSession(sessionUUID uuid.UUID) (*Session, error)
	// Refresh session extends the timeout for a session by timeout
	RefreshSession(sessionUUID uuid.UUID, timeout time.Duration) (*Session, error)
}
