// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"sort"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"github.com/google/uuid"
	influx "github.com/influxdata/influxdb/client/v2"
	_ "github.com/mattn/go-sqlite3"
	"github.com/spf13/viper"
	"gitlab.com/pantomath-io/wrapany"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/server/services"
	"gitlab.com/pantomath-io/panto/server/session"
	"gitlab.com/pantomath-io/panto/server/tsdb"
	"gitlab.com/pantomath-io/panto/util"
)

const numPoints = 10000

var agent1, agent2, check1, check2, target, probe, organization uuid.UUID
var probeConfiguration1, probeConfiguration2, probeConfiguration3, probeConfiguration4 uuid.UUID
var user uuid.UUID
var results map[uuid.UUID][]*api.Result
var states []*api.State
var notifications []*api.Notification
var startTime time.Time
var session1 *session.Session

func TestMain(m *testing.M) {
	setUp()        // Setup for tests
	res := m.Run() // Run the actual tests
	tearDown()     // Teardown after running the tests
	os.Exit(res)
}

func setUp() {
	// Check if this is live testing
	if util.LiveTest(nil) {
		rand.Seed(time.Now().UnixNano())

		// Set configuration variables
		viper.BindEnv("influxdb.address", "INFLUXDB_ADDRESS")
		viper.BindEnv("influxdb.database", "INFLUXDB_DATABASE")
		viper.BindEnv("db.path", "DB_PATH")
		viper.BindEnv("db.fixturespath", "DB_FIXTURESPATH")

		viper.SetDefault("influxdb.address", "http://influxdb:8086")
		viper.SetDefault("influxdb.database", "pantotest")
		viper.SetDefault("db.path", "/tmp/pantotest.sqlite")
		viper.SetDefault("db.fixturespath", "../data/fixtures")

		// Create SQLite database file
		err := os.MkdirAll(filepath.Dir(viper.GetString("db.path")), 0755)
		if err != nil {
			panic(err)
		}
		f, err := os.OpenFile(viper.GetString("db.path"), os.O_CREATE|os.O_TRUNC, 0644)
		if err != nil {
			panic(err)
		}
		f.Close()

		// Create test TS database
		influxClient, err := influx.NewHTTPClient(influx.HTTPConfig{Addr: viper.GetString("influxdb.address")})
		if err != nil {
			panic("Couldn't connect to influx database on localhost:8086 : " + err.Error())
		}
		defer influxClient.Close()

		_, err = influxClient.Query(influx.NewQuery("CREATE DATABASE "+viper.GetString("influxdb.database"), "", ""))
		if err != nil {
			panic("Couldn't create TS database \"" + viper.GetString("influxdb.database") + "\": " + err.Error())
		}

		// Create test DBConf database
		db, err := dbconf.NewSQLiteDB(viper.GetString("db.path"))
		if err != nil {
			panic("couldn't get configuration db: " + err.Error())
		}
		err = dbconf.Migrate(db)
		if err != nil {
			fmt.Printf("error while performing migration: %v\n", err)
			panic("Couldn't create DBConf database \"" + viper.GetString("db.path") + "\"")
		}
		db.Close()

		// Sleep 100 milliseconds to avoid weird timing problem in the CI.
		time.Sleep(100 * time.Millisecond)
		sqliteClient, err := sql.Open("sqlite3", viper.GetString("db.path"))
		if err != nil {
			panic("Couldn't open sqlite database at " + viper.GetString("db.path"))
		}
		defer sqliteClient.Close()

		results = make(map[uuid.UUID][]*api.Result)
		states = make([]*api.State, numPoints*4)
		notifications = make([]*api.Notification, numPoints*4)
		loc, _ := time.LoadLocation("Europe/Paris")
		startTime = time.Date(2018, 2, 13, 20, 0, 0, 0, loc)

		// Sleep 1 second to avoid weird timing problem in the CI.
		time.Sleep(time.Second)
		setUpUUIDs()
		setUpResults(influxClient)
		setUpStates(influxClient)
		setUpNotifications(influxClient)
		setUpPages(sqliteClient)
		setUpSessions()
	}
}

func setUpUUIDs() {
	organization, _ = uuid.Parse("2930820b-ad21-4605-83b4-69cbc5bfc8f5")
	agent1, _ = uuid.Parse("a0ab8e27-b11a-4ee4-a47b-cd73de56160d")
	agent2, _ = uuid.Parse("66a52d20-ae35-49d6-8028-9cd7f34f4313")
	target, _ = uuid.Parse("c0249d5a-ec4c-454e-84fc-686dc88ad2a6")
	check1, _ = uuid.Parse("14a6c5b5-05a0-4ec2-ad36-a3c43fe5d473")
	check2, _ = uuid.Parse("1b41ee6a-42e9-460e-a618-5f34a9d9113c")
	probeConfiguration1, _ = uuid.Parse("76047891-c417-42b1-b3af-af6d1bb974b8") // check 1, agent 1
	probeConfiguration2, _ = uuid.Parse("222e4d41-c8e2-44ee-ad7d-d07f1d9edc82") // check 1, agent 2
	probeConfiguration3, _ = uuid.Parse("1535460e-86b5-4557-2866-83e31535ec4c") // check 2, agent 1
	probeConfiguration4, _ = uuid.Parse("0f459ccb-0e82-4e57-9acf-9fa72cb82fe9") // check 1, agent 1
	user, _ = uuid.Parse("8c470774-1dca-11ea-b5c9-d70fe87a28e2")
}

func setUpResults(client influx.Client) {
	// Insert fake probe results
	points, _ := influx.NewBatchPoints(influx.BatchPointsConfig{Database: viper.GetString("influxdb.database")})

	measurementName1 := util.Base64Encode(check1)
	measurementName2 := util.Base64Encode(check2)

	t := startTime
	results[probeConfiguration1] = make([]*api.Result, numPoints)
	interval := 30 * time.Minute
	tags := map[string]string{
		"organization":        util.Base64Encode(organization),
		"agent":               util.Base64Encode(agent1),
		"target":              util.Base64Encode(target),
		"check":               util.Base64Encode(check1),
		"probe_configuration": util.Base64Encode(probeConfiguration1),
		"probe":               util.Base64Encode(probe),
	}
	for i := 0; i < numPoints; i++ {
		val := rand.NormFloat64()*10*1e6 + 50*1e6
		p, _ := influx.NewPoint(
			measurementName1,
			tags,
			map[string]interface{}{
				"sent":     val,
				"received": val,
				"min":      val,
				"max":      val,
				"avg":      val,
				"stddev":   val,
			},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		valany, _ := wrapany.Wrap(val)
		results[probeConfiguration1][i] = &api.Result{
			Timestamp: tpb,
			Check: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameCheck, util.Base64Encode(check1),
			),
			Agent: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent1),
			),
			ProbeConfiguration: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent1),
				api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
			),
			Fields: map[string]*any.Any{
				"sent":     valany,
				"received": valany,
				"min":      valany,
				"max":      valany,
				"avg":      valany,
				"stddev":   valany,
			},
		}

		t = t.Add(interval)
	}

	results[probeConfiguration2] = make([]*api.Result, numPoints)
	t = time.Date(2018, 2, 13, 20, 15, 0, 0, startTime.Location())
	tags = map[string]string{
		"organization":        util.Base64Encode(organization),
		"agent":               util.Base64Encode(agent2),
		"target":              util.Base64Encode(target),
		"check":               util.Base64Encode(check1),
		"probe_configuration": util.Base64Encode(probeConfiguration2),
		"probe":               util.Base64Encode(probe),
	}
	for i := 0; i < numPoints; i++ {
		val := rand.NormFloat64()*10*1e6 + 50*1e6
		p, _ := influx.NewPoint(
			measurementName2,
			tags,
			map[string]interface{}{
				"sent":     val,
				"received": val,
				"min":      val,
				"max":      val,
				"avg":      val,
				"stddev":   val,
			},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		valany, _ := wrapany.Wrap(val)
		results[probeConfiguration2][i] = &api.Result{
			Timestamp: tpb,
			Check: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameCheck, util.Base64Encode(check1),
			),
			Agent: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent2),
			),
			ProbeConfiguration: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent2),
				api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration2),
			),
			Fields: map[string]*any.Any{
				"sent":     valany,
				"received": valany,
				"min":      valany,
				"max":      valany,
				"avg":      valany,
				"stddev":   valany,
			},
		}

		t = t.Add(interval)
	}

	err := client.Write(points)
	if err != nil {
		panic(fmt.Sprintf("couldn't set up live test data: %s", err))
	}

	sort.Slice(results[probeConfiguration1], func(i, j int) bool {
		tsi, _ := ptypes.Timestamp(results[probeConfiguration1][i].Timestamp)
		tsj, _ := ptypes.Timestamp(results[probeConfiguration1][j].Timestamp)
		return tsi.Before(tsj)
	})
	sort.Slice(results[probeConfiguration2], func(i, j int) bool {
		tsi, _ := ptypes.Timestamp(results[probeConfiguration2][i].Timestamp)
		tsj, _ := ptypes.Timestamp(results[probeConfiguration2][j].Timestamp)
		return tsi.Before(tsj)
	})
}

func setUpStates(client influx.Client) {
	// Insert fake states
	points, _ := influx.NewBatchPoints(influx.BatchPointsConfig{Database: viper.GetString("influxdb.database")})

	t := startTime
	interval := 30 * time.Minute
	tags := map[string]string{
		"organization":        util.Base64Encode(organization),
		"agent":               util.Base64Encode(agent1),
		"target":              util.Base64Encode(target),
		"check":               util.Base64Encode(check1),
		"probe_configuration": util.Base64Encode(probeConfiguration1),
		"probe":               util.Base64Encode(probe),
		"reason":              "A perfectly good reason.",
	}
	for i := 0; i < numPoints; i++ {
		p, _ := influx.NewPoint(
			"state-"+util.Base64Encode(check1),
			tags,
			map[string]interface{}{"state": int(api.State_OK)},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		states[i] = &api.State{
			Timestamp: tpb,
			Check: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameCheck, util.Base64Encode(check1),
			),
			Agent: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent1),
			),
			ProbeConfiguration: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent1),
				api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
			),
			Type:   api.State_OK,
			Reason: "A perfectly good reason.",
		}

		t = t.Add(interval)
	}

	t = time.Date(2018, 2, 13, 20, 15, 0, 0, startTime.Location())
	tags = map[string]string{
		"organization":        util.Base64Encode(organization),
		"agent":               util.Base64Encode(agent1),
		"target":              util.Base64Encode(target),
		"check":               util.Base64Encode(check1),
		"probe_configuration": util.Base64Encode(probeConfiguration1),
		"probe":               util.Base64Encode(probe),
		"reason":              "A perfectly good reason.",
	}
	for i := 0; i < numPoints; i++ {
		p, _ := influx.NewPoint(
			"state-"+util.Base64Encode(check1),
			tags,
			map[string]interface{}{"state": int(api.State_CRITICAL)},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		states[i+numPoints] = &api.State{
			Timestamp: tpb,
			Check: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameCheck, util.Base64Encode(check1),
			),
			Agent: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent1),
			),
			ProbeConfiguration: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent1),
				api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration1),
			),
			Type:   api.State_CRITICAL,
			Reason: "A perfectly good reason.",
		}

		t = t.Add(interval)
	}

	tags = map[string]string{
		"organization":        util.Base64Encode(organization),
		"agent":               util.Base64Encode(agent2),
		"target":              util.Base64Encode(target),
		"check":               util.Base64Encode(check1),
		"probe_configuration": util.Base64Encode(probeConfiguration2),
		"probe":               util.Base64Encode(probe),
		"reason":              "A perfectly good reason.",
	}
	for i := 0; i < numPoints; i++ {
		p, _ := influx.NewPoint(
			"state-"+util.Base64Encode(check1),
			tags,
			map[string]interface{}{"state": int(api.State_OK)},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		states[i+2*numPoints] = &api.State{
			Timestamp: tpb,
			Check: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameCheck, util.Base64Encode(check1),
			),
			Agent: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent2),
			),
			ProbeConfiguration: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent2),
				api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration2),
			),
			Type:   api.State_OK,
			Reason: "A perfectly good reason.",
		}

		t = t.Add(interval)
	}

	tags = map[string]string{
		"organization":        util.Base64Encode(organization),
		"agent":               util.Base64Encode(agent1),
		"target":              util.Base64Encode(target),
		"check":               util.Base64Encode(check2),
		"probe_configuration": util.Base64Encode(probeConfiguration3),
		"probe":               util.Base64Encode(probe),
		"reason":              "A perfectly good reason.",
	}
	for i := 0; i < numPoints; i++ {
		p, _ := influx.NewPoint(
			"state-"+util.Base64Encode(check2),
			tags,
			map[string]interface{}{"state": int(api.State_CRITICAL)},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		states[i+3*numPoints] = &api.State{
			Timestamp: tpb,
			Check: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameCheck, util.Base64Encode(check2),
			),
			Agent: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent1),
			),
			ProbeConfiguration: path.Join(
				api.ResourceNameOrganization, util.Base64Encode(organization),
				api.ResourceNameAgent, util.Base64Encode(agent1),
				api.ResourceNameProbeConfiguration, util.Base64Encode(probeConfiguration3),
			),
			Type:   api.State_CRITICAL,
			Reason: "A perfectly good reason.",
		}

		t = t.Add(interval)
	}

	err := client.Write(points)
	if err != nil {
		panic(fmt.Sprintf("couldn't set up live test data: %s", err))
	}

	sort.Slice(states, func(i, j int) bool {
		tsi, _ := ptypes.Timestamp(states[i].Timestamp)
		tsj, _ := ptypes.Timestamp(states[j].Timestamp)
		return tsi.Before(tsj)
	})
}

func setUpNotifications(client influx.Client) {
	// Insert fake states
	points, _ := influx.NewBatchPoints(influx.BatchPointsConfig{Database: viper.GetString("influxdb.database")})

	t := startTime
	interval := 30 * time.Minute
	tags := map[string]string{
		"agent":  util.Base64Encode(agent1),
		"target": util.Base64Encode(target),
		"check":  util.Base64Encode(check1),
		"probe":  util.Base64Encode(probe),
	}
	for i := 0; i < numPoints; i++ {
		p, _ := influx.NewPoint(
			"alert-"+util.Base64Encode(probeConfiguration1),
			tags,
			map[string]interface{}{"event": tsdb.EventValueNotified},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		notifications[i] = &api.Notification{
			Timestamp: tpb,
			Check:     path.Join(api.ResourceNameCheck, util.Base64Encode(check1)),
			Agent:     path.Join(api.ResourceNameAgent, util.Base64Encode(agent1)),
			Event:     api.Event_NOTIFIED,
		}

		t = t.Add(interval)
	}

	t = time.Date(2018, 2, 13, 20, 15, 0, 0, startTime.Location())
	tags = map[string]string{
		"agent":  util.Base64Encode(agent1),
		"target": util.Base64Encode(target),
		"check":  util.Base64Encode(check1),
		"probe":  util.Base64Encode(probe),
	}
	for i := 0; i < numPoints; i++ {
		p, _ := influx.NewPoint(
			"alert-"+util.Base64Encode(probeConfiguration1),
			tags,
			map[string]interface{}{"event": tsdb.EventValueNotified},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		notifications[i+numPoints] = &api.Notification{
			Timestamp: tpb,
			Check:     path.Join(api.ResourceNameCheck, util.Base64Encode(check1)),
			Agent:     path.Join(api.ResourceNameAgent, util.Base64Encode(agent1)),
			Event:     api.Event_NOTIFIED,
		}

		t = t.Add(interval)
	}

	tags = map[string]string{
		"agent":  util.Base64Encode(agent2),
		"target": util.Base64Encode(target),
		"check":  util.Base64Encode(check1),
		"probe":  util.Base64Encode(probe),
	}
	for i := 0; i < numPoints; i++ {
		p, _ := influx.NewPoint(
			"alert-"+util.Base64Encode(probeConfiguration1),
			tags,
			map[string]interface{}{"event": tsdb.EventValueNotified},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		notifications[i+2*numPoints] = &api.Notification{
			Timestamp: tpb,
			Check:     path.Join(api.ResourceNameCheck, util.Base64Encode(check1)),
			Agent:     path.Join(api.ResourceNameAgent, util.Base64Encode(agent2)),
			Event:     api.Event_NOTIFIED,
		}

		t = t.Add(interval)
	}

	tags = map[string]string{
		"agent":  util.Base64Encode(agent1),
		"target": util.Base64Encode(target),
		"check":  util.Base64Encode(check2),
		"probe":  util.Base64Encode(probe),
	}
	for i := 0; i < numPoints; i++ {
		p, _ := influx.NewPoint(
			"alert-"+util.Base64Encode(probeConfiguration4),
			tags,
			map[string]interface{}{"event": tsdb.EventValueNotified},
			t,
		)
		points.AddPoint(p)

		tpb, _ := ptypes.TimestampProto(t)
		notifications[i+3*numPoints] = &api.Notification{
			Timestamp: tpb,
			Check:     path.Join(api.ResourceNameCheck, util.Base64Encode(check2)),
			Agent:     path.Join(api.ResourceNameAgent, util.Base64Encode(agent1)),
			Event:     api.Event_NOTIFIED,
		}

		t = t.Add(interval)
	}

	err := client.Write(points)
	if err != nil {
		panic(fmt.Sprintf("couldn't set up live test data: %s", err))
	}

	sort.Slice(notifications, func(i, j int) bool {
		tsi, _ := ptypes.Timestamp(notifications[i].Timestamp)
		tsj, _ := ptypes.Timestamp(notifications[j].Timestamp)
		return tsi.Before(tsj)
	})
}

func setUpPages(client *sql.DB) {
	stmt, err := ioutil.ReadFile("../data/fixtures/002_PaginationTestFixtures.sql")
	if err != nil {
		panic("error reading SQL script ../data/fixtures/002_PaginationTestFixtures.sql: " + err.Error())
	}
	res, err := client.Exec(string(stmt))
	if err != nil {
		panic("error setting up pages in sqlite: " + err.Error())
	}

	affected, _ := res.RowsAffected()
	fmt.Printf("setUpPages: %d rows affected.\n", affected)
}

func setUpSessions() {
	sessionMgr, err := services.GetSessionManager()
	if err != nil {
		panic("error setting up session manager: " + err.Error())
	}
	session1, err = sessionMgr.CreateSession(session.DefaultTimeout, user, organization)
	if err != nil {
		panic("error setting up session: " + err.Error())
	}
}

func tearDown() {
	// Check if this is live testing
	if util.LiveTest(nil) {
		// Cleanup test TS database
		client, _ := influx.NewHTTPClient(influx.HTTPConfig{Addr: "http://localhost:8086"})
		defer client.Close()

		client.Query(influx.NewQuery("DROP DATABASE "+viper.GetString("influxdb.database"), "", ""))

		// Remove the DBConf database
		os.Remove(viper.GetString("db.path"))
		os.Remove(viper.GetString("db.path") + "-shm")
		os.Remove(viper.GetString("db.path") + "-wal")
	}
}
