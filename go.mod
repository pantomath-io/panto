// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

module gitlab.com/pantomath-io/panto

require (
	github.com/DATA-DOG/go-sqlmock v1.3.0
	github.com/Microsoft/go-winio v0.4.11 // indirect
	github.com/Pallinder/go-randomdata v1.1.0
	github.com/StackExchange/wmi v0.0.0-20180725035823-b12b22c5341f // indirect
	github.com/alexkohler/prealloc v0.0.0-20181002111628-4fce4887dad9 // indirect
	github.com/antihax/optional v1.0.0 // indirect
	github.com/beevik/ntp v0.2.0
	github.com/buger/jsonparser v0.0.0-20180910192245-6acdf747ae99 // indirect
	github.com/cncf/udpa/go v0.0.0-20200313221541-5f7e5dd04533 // indirect
	github.com/docker/distribution v2.6.2+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.3.3 // indirect
	github.com/go-ole/go-ole v1.2.1 // indirect
	github.com/go-redis/redis v6.15.1+incompatible
	github.com/go-sql-driver/mysql v1.4.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/mock v1.4.3 // indirect
	github.com/golang/protobuf v1.3.5
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/google/uuid v1.0.0
	github.com/googleapis/googleapis v0.0.0-20200417033322-bcc476396e79 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.1-0.20190118093823-f849b5445de4
	github.com/grpc-ecosystem/grpc-gateway v1.14.3
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/influxdata/influxdb v1.6.3
	github.com/kr/text v0.2.0 // indirect
	github.com/lib/pq v1.0.0
	github.com/loderunner/go-ping v0.0.0-20180323132356-0ea0cfb81ce5
	github.com/loderunner/goose v2.4.1-0.20200902163152-9979ef942602+incompatible // indirect
	github.com/mattn/go-sqlite3 v2.0.1+incompatible
	github.com/mitchellh/go-homedir v1.0.0
	github.com/mongodb/mongo-go-driver v0.0.14
	github.com/mozillazg/go-slugify v0.2.0
	github.com/mozillazg/go-unidecode v0.1.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/onsi/gomega v1.4.2 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/peterh/liner v1.1.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/cors v1.5.0
	github.com/shirou/gopsutil v2.17.12+incompatible
	github.com/shirou/w32 v0.0.0-20160930032740-bb4de0191aa4 // indirect
	github.com/soniah/gosnmp v0.0.0-20190109014816-922fbec967ca
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.2
	github.com/spf13/viper v1.2.0
	github.com/stevvooe/resumable v0.0.0-20180830230917-22b14a53ba50 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/thda/tds v0.1.6
	github.com/tidwall/pretty v0.0.0-20180105212114-65a9db5fad51 // indirect
	github.com/tomasen/fcgi_client v0.0.0-20180423082037-2bb3d819fd19
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/pantomath-io/wrapany v0.0.0-20180416192657-466d0321bbf9
	golang.org/x/crypto v0.0.0-20200317142112-1b76d66859c6
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200317113312-5766fd39f98d // indirect
	google.golang.org/appengine v1.6.5 // indirect
	google.golang.org/genproto v0.0.0-20200318110522-7735f76e9fa5
	google.golang.org/grpc v1.28.0
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

go 1.13
