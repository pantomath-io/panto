// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/cmd/panto-ctl/ask"
	"gitlab.com/pantomath-io/panto/cmd/panto-ctl/conf"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/util"
)

// userCmd represents the user command
var userCmd = &cobra.Command{
	Use:          "user",
	Short:        "manage the users for panto-web",
	Long:         `Create, update, delete panto-web users from command line`,
	SilenceUsage: true,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
		for _, o := range conf.UserOptions {
			if err := util.BindOption(o, cmd.Flags()); err != nil {
				panic(fmt.Sprint("failed to bind option:", err))
			}
		}
		return nil
	},
}

var userCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "create a user",
	Long:  "Creates a new user for panto-web.",
	RunE:  userCreateCmdRun,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		for _, o := range conf.UserOptions {
			if err := util.BindOption(o, cmd.Flags()); err != nil {
				return err
			}
		}
		return nil
	},
}

func userCreateCmdRun(cmd *cobra.Command, args []string) error {
	var err error
	verbose := viper.GetBool("verbose")

	if !viper.IsSet("db.path") || viper.GetString("db.path") == "" {
		return fmt.Errorf("no configuration database file: use --db-path or specify a configuration file")
	}
	dbPath := viper.GetString("db.path")
	dbPath, err = util.ExpandAbs(dbPath)
	if err != nil {
		return err
	}
	db, err = dbconf.NewSQLiteDB(dbPath)
	if err != nil {
		return err
	}
	if verbose {
		fmt.Printf("Using configuration database file: %s\n", dbPath)
	}

	fmt.Println("Creating a new Panto user")
	fmt.Println()

	userOrganization, err := ask.String("Enter your Organization name (e.g. organizations/pUNH8aZDRki_KQPigL7Eww):").
		Ask()
	if err != nil {
		return err
	}
	organizationUUID, err := api.ParseNameOrganization(userOrganization)
	if err != nil {
		return err
	}
	organization, err := db.GetOrganization(organizationUUID)
	if err != nil {
		return err
	}
	if organization == nil {
		return fmt.Errorf("Organization %s does not exist", userOrganization)
	}

	userEmail, err := ask.String("Enter the new User's email address (e.g. user@example.com):").
		Ask()
	if err != nil {
		return err
	}
	existingUser, err := db.GetUserByEmail(userEmail)
	if err != nil {
		return err
	}
	if existingUser != nil {
		return fmt.Errorf("User %s already exists", userEmail)
	}

	userPassword, err := ask.String("Enter password (the password will not be echoed):").
		Password().
		Ask()
	if err != nil {
		return err
	}
	salt, err := util.GenerateSalt()
	if err != nil {
		return err
	}
	hash, err := util.HashPassword(userPassword, salt)
	if err != nil {
		return err
	}

	details := make(map[string]interface{})

	_, err = db.CreateUser(&dbconf.User{
		OrganizationUUID: organization.UUID,
		Email:            userEmail,
		PasswordSalt:     salt,
		PasswordHash:     hash,
		Details:          details,
	})
	if err != nil {
		return err
	}
	fmt.Println()
	fmt.Printf("User %s has been created in %s\n", userEmail, organization.Name)

	return err
}

var userUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "update a user",
	Long:  "Updates an existing user for panto-web.",
	RunE:  userUpdateCmdRun,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		for _, o := range conf.UserOptions {
			if err := util.BindOption(o, cmd.Flags()); err != nil {
				return err
			}
		}
		return nil
	},
}

func userUpdateCmdRun(cmd *cobra.Command, args []string) error {
	var err error
	verbose := viper.GetBool("verbose")

	if !viper.IsSet("db.path") || viper.GetString("db.path") == "" {
		return fmt.Errorf("no configuration database file: use --db-path or specify a configuration file")
	}
	dbPath := viper.GetString("db.path")
	dbPath, err = util.ExpandAbs(dbPath)
	if err != nil {
		return err
	}
	db, err = dbconf.NewSQLiteDB(dbPath)
	if err != nil {
		return err
	}
	if verbose {
		fmt.Printf("Using configuration database file: %s\n", dbPath)
	}

	fmt.Println("Updating an existing Panto user")
	fmt.Println()

	userOrganization, err := ask.String("Enter your Organization name (e.g. organizations/pUNH8aZDRki_KQPigL7Eww):").
		Ask()
	if err != nil {
		return err
	}
	organizationUUID, err := api.ParseNameOrganization(userOrganization)
	if err != nil {
		return err
	}
	organization, err := db.GetOrganization(organizationUUID)
	if err != nil {
		return err
	}
	if organization == nil {
		return fmt.Errorf("Organization %s does not exist", userOrganization)
	}

	userEmail, err := ask.String("Enter the User's email address (e.g. user@example.com):").
		Ask()
	if err != nil {
		return err
	}
	user, err := db.GetUserByEmail(userEmail)
	if err != nil {
		return err
	}
	if user == nil {
		return fmt.Errorf("User %s does not exist", userEmail)
	}

	userPassword, err := ask.String("Enter password (the password will not be echoed):").
		Password().
		Ask()
	if err != nil {
		return err
	}

	user.PasswordSalt, err = util.GenerateSalt()
	if err != nil {
		return err
	}
	user.PasswordHash, err = util.HashPassword(userPassword, user.PasswordSalt)
	if err != nil {
		return err
	}

	_, err = db.UpdateUser(user)
	if err != nil {
		return err
	}
	fmt.Println()
	fmt.Printf("User %s has been updated in %s\n", user.UUID, organization.Name)

	return err
}

var userDeleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "delete a user",
	Long:  "Deletes an existing user for panto-web.",
	RunE:  userDeleteCmdRun,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		for _, o := range conf.UserOptions {
			if err := util.BindOption(o, cmd.Flags()); err != nil {
				return err
			}
		}
		return nil
	},
}

func userDeleteCmdRun(cmd *cobra.Command, args []string) error {
	var err error
	verbose := viper.GetBool("verbose")

	if !viper.IsSet("db.path") || viper.GetString("db.path") == "" {
		return fmt.Errorf("no configuration database file: use --db-path or specify a configuration file")
	}
	dbPath := viper.GetString("db.path")
	dbPath, err = util.ExpandAbs(dbPath)
	if err != nil {
		return err
	}
	db, err = dbconf.NewSQLiteDB(dbPath)
	if err != nil {
		return err
	}
	if verbose {
		fmt.Printf("Using configuration database file: %s\n", dbPath)
	}

	fmt.Println("Deleting a Panto user")
	fmt.Println()

	userOrganization, err := ask.String("Enter your Organization name (e.g. organizations/pUNH8aZDRki_KQPigL7Eww):").
		Ask()
	if err != nil {
		return err
	}
	organizationUUID, err := api.ParseNameOrganization(userOrganization)
	if err != nil {
		return err
	}
	organization, err := db.GetOrganization(organizationUUID)
	if err != nil {
		return err
	}
	if organization == nil {
		return fmt.Errorf("Organization %s does not exist", userOrganization)
	}

	userEmail, err := ask.String("Enter the User's email address (e.g. user@example.com):").
		Ask()
	if err != nil {
		return err
	}
	user, err := db.GetUserByEmail(userEmail)
	if err != nil {
		return err
	}
	if user == nil {
		return fmt.Errorf("User %s does not exist", userEmail)
	}

	b, err := ask.Bool("Are you sure?").Default(true).Ask()
	if err != nil {
		return err
	}
	if !b {
		fmt.Printf("Leaving User %s untouched\n", user.UUID)
		return err
	}

	err = db.DeleteUser(organization.UUID, user.UUID)
	if err != nil {
		return err
	}
	fmt.Println()
	fmt.Printf("User %s has been deleted from %s\n", user.UUID, organization.Name)

	return err
}

func init() {
	rootCmd.AddCommand(userCmd)
	userCmd.AddCommand(
		userCreateCmd,
		userUpdateCmd,
		userDeleteCmd,
	)

	for _, o := range conf.UserOptions {
		if err := util.AddOption(o, userCmd.PersistentFlags()); err != nil {
			panic(fmt.Sprint("failed to add option:", err))
		}
	}
}
