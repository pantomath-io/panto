// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"os"
	"path/filepath"
	"text/template"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/cmd/panto-ctl/ask"
	"gitlab.com/pantomath-io/panto/cmd/panto-ctl/conf"
	"gitlab.com/pantomath-io/panto/server/dbconf"
	"gitlab.com/pantomath-io/panto/util"
)

var initCmd = cobra.Command{
	Use:   "init",
	Short: "initialize the Panto environment",
	Long:  `Initialize the Panto environment with an interactive dialog. Should be invoked before running Panto for the first time.`,
	RunE:  initCmdRun,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		for _, o := range conf.InitOptionsPersistent {
			if err := util.BindOption(o, cmd.Flags()); err != nil {
				return err
			}
		}
		return nil
	},
	PreRunE: func(cmd *cobra.Command, args []string) error {
		for _, o := range conf.InitOptions {
			if err := util.BindOption(o, cmd.Flags()); err != nil {
				return err
			}
		}
		return nil
	},
	SilenceUsage: true,
}

// initRunCmd prompts the user for their config variables then passes them on to the init subcommands
func initCmdRun(cmd *cobra.Command, args []string) error {
	var err error
	installPrefix := viper.GetString("install-prefix")
	installPrefix, err = util.ExpandAbs(installPrefix)
	if err != nil {
		return err
	}

	fmt.Println("Welcome to Panto!")
	fmt.Println("panto-ctl will now ask you a few questions to set up your initial Panto environment.")
	fmt.Println()

	// Data path
	{
		defaultDataPath := filepath.Join(installPrefix, "var", "lib", "panto")
		dataPath, err := ask.Path("Enter Panto data path:").
			Default(defaultDataPath).
			Ask()
		if err != nil {
			return err
		}
		dataPath, err = util.ExpandAbs(dataPath)
		if err != nil {
			return err
		}
		viper.Set(keyDataPath, dataPath)

		dbPath := filepath.Join(dataPath, "panto.sqlite")
		// #HACK: Set both keys to work for conf file or CLI flag
		viper.Set("db.path", dbPath)
		viper.Set(keyDBPath, dbPath)

		_, err = os.Stat(dbPath)
		if err == nil {
			if ok, _ := ask.Boolf("File \"%s\" already exists. Do you want to overwrite it?", dbPath).
				Default(true).
				Ask(); !ok {
				return nil
			}
		}
	}

	// Configuration path
	{
		defaultConfPath := filepath.Join(installPrefix, "etc", "panto", "panto.yaml")
		confPath, err := ask.Path("Enter panto configuration file location:").
			Default(defaultConfPath).
			Ask()
		if err != nil {
			return err
		}
		confPath, err = util.ExpandAbs(confPath)
		if err != nil {
			return err
		}
		viper.Set(keyConfPath, confPath)
	}

	// Log file path
	{
		defaultLogPath := filepath.Join(installPrefix, "var", "log", "panto", "panto.log")
		logPath, err := ask.Path("Enter Panto log file path (\"-\" for no log file):").
			Default(defaultLogPath).
			Ask()
		if err != nil {
			return err
		}
		if logPath != "-" {
			logPath, err = util.ExpandAbs(logPath)
			if err != nil {
				return err
			}
			viper.Set(keyLogPath, logPath)
		}
	}

	// Server settings
	fmt.Println()
	fmt.Println("Server settings:")
	gRPCAddress, err := ask.String("Enter gRPC API server bind address:").
		Default("0.0.0.0:7575").
		Ask()
	if err != nil {
		return err
	}
	viper.Set(keyGRPCAddress, gRPCAddress)
	restAddress, err := ask.String("Enter REST API server bind address:").
		Default("0.0.0.0:7576").
		Ask()
	if err != nil {
		return err
	}
	viper.Set(keyRESTAddress, restAddress)

	// TLS/SSL settings
	fmt.Println()
	fmt.Println("TLS/SSL settings:")
	enableTLSSSL, err := ask.Boolf("Do you want to use TLS/SSL between server and agents:").
		Default(true).
		Ask()
	if err != nil {
		return err
	}
	if enableTLSSSL {
		defaultCertFilePath := filepath.Join(installPrefix, "etc", "panto", "fullchain.pem")
		certFilePath, err := ask.Path("Enter certificate file path:").
			Default(defaultCertFilePath).
			Ask()
		if err != nil {
			return err
		}
		certFilePath, err = util.ExpandAbs(certFilePath)
		if err != nil {
			return err
		}
		viper.Set(keyCertFile, certFilePath)

		defaultCertKeyPath := filepath.Join(installPrefix, "etc", "panto", "privkey.pem")
		certKeyPath, err := ask.Path("Enter certificate key file path:").
			Default(defaultCertKeyPath).
			Ask()
		if err != nil {
			return err
		}
		certKeyPath, err = util.ExpandAbs(certKeyPath)
		if err != nil {
			return err
		}
		viper.Set(keyCertKey, certKeyPath)
	}

	fmt.Println()

	err = initDataCmdRun(cmd, args)
	if err != nil {
		return err
	}

	err = initConfigCmdRun(cmd, args)
	if err != nil {
		return err
	}

	execPath := filepath.Join(filepath.Dir(os.Args[0]), "panto")
	fmt.Println()
	fmt.Println("Panto successfully configured!")
	fmt.Println()
	fmt.Println("To start Panto server run")
	fmt.Println()
	fmt.Printf("    %s --conf=%s\n", execPath, viper.GetString(keyConfPath))

	return nil
}

var initConfigCmd = cobra.Command{
	Use:   "config",
	Short: "initialize the Panto configuration",
	Long:  `Create the Panto files and directories, and save the Panto configuration file.`,
	RunE:  initConfigCmdRun,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		for _, o := range conf.InitConfigOptions {
			if err := util.BindOption(o, cmd.Flags()); err != nil {
				return err
			}
		}
		return nil
	},
}

func initConfigCmdRun(cmd *cobra.Command, args []string) error {
	var err error

	dryRun := viper.GetBool("dry-run")
	verbose := dryRun || viper.GetBool("verbose")
	gRPCAddress := viper.GetString(keyGRPCAddress)
	restAddress := viper.GetString(keyRESTAddress)

	dbPath := viper.GetString("db.path")
	dbPath, err = util.ExpandAbs(dbPath)
	if err != nil {
		return err
	}

	logPath := viper.GetString(keyLogPath)
	logPath, err = util.ExpandAbs(logPath)
	if err != nil {
		return err
	}

	confPath := viper.GetString(keyConfPath)
	confPath, err = util.ExpandAbs(confPath)
	if err != nil {
		return err
	}

	certFile := viper.GetString(keyCertFile)
	certFile, err = util.ExpandAbs(certFile)
	if err != nil {
		return err
	}

	certKey := viper.GetString(keyCertKey)
	certKey, err = util.ExpandAbs(certKey)
	if err != nil {
		return err
	}

	conf := configTemplate{
		LogPath:     logPath,
		GRPCAddress: gRPCAddress,
		RESTAddress: restAddress,
		AllowOrigin: "*",
		CertFile:    certFile,
		CertKey:     certKey,
		DBPath:      dbPath,
	}

	// Save config file
	if verbose {
		fmt.Printf("Output configuration to %s\n", confPath)
	}
	if !dryRun {
		err := os.MkdirAll(filepath.Dir(confPath), 0755)
		if err != nil {
			return err
		}
		f, err := os.OpenFile(confPath, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		defer f.Close()
		tmpl := template.New("config")
		tmpl, err = tmpl.Parse(configTemplateString)
		if err != nil {
			return err
		}
		err = tmpl.Execute(f, conf)
		if err != nil {
			return err
		}
	}

	return nil
}

var initDataCmd = cobra.Command{
	Use:   "data",
	Short: "initialize the database",
	Long:  "Inserts initial values in the database before the first run.",
	RunE:  initDataCmdRun,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		for _, o := range conf.InitDataOptions {
			if err := util.BindOption(o, cmd.Flags()); err != nil {
				return err
			}
		}
		return nil
	},
}

func initDataCmdRun(cmd *cobra.Command, args []string) error {
	var err error

	dryRun := viper.GetBool("dry-run")
	verbose := dryRun || viper.GetBool("verbose")

	dbPath := viper.GetString(keyDBPath)
	dbPath, err = util.ExpandAbs(dbPath)
	if err != nil {
		return err
	}

	fmt.Println()
	fmt.Println("Panto will now set up the database with your initial environment.")

	// Create DB file
	if verbose {
		fmt.Printf("Create database file %s\n", dbPath)
	}
	if !dryRun {
		err = os.MkdirAll(filepath.Dir(dbPath), 0755)
		if err != nil {
			return err
		}
		// Remove WAL if necessary
		os.Remove(dbPath + "-shm")
		os.Remove(dbPath + "-wal")
		// Create database file
		f, err := os.OpenFile(dbPath, os.O_CREATE|os.O_TRUNC, 0644)
		if err != nil {
			return err
		}
		f.Close()
	}

	var db dbconf.ConfigurationDB
	if verbose {
		fmt.Printf("Open database file %s\n", dbPath)
	}
	if !dryRun {
		db, err = dbconf.NewSQLiteDB(dbPath)
		if err != nil {
			return nil
		}
	}

	if verbose {
		fmt.Printf("Apply all migrations to %s\n", dbPath)
	}
	if !dryRun {
		fmt.Println()
		fmt.Println("Initializing database schema and applying migrations...")
		err = dbconf.Migrate(db)
		if err != nil {
			return err
		}
	}

	// Create first organization
	fmt.Println()
	orgName, err := ask.String("What is the name of your Organization?").
		Default("Organization").
		Ask()
	if err != nil {
		return err
	}

	var org *dbconf.Organization
	if verbose {
		fmt.Printf("Create Organization %s in database\n", orgName)
	}
	if !dryRun {
		org, err = db.CreateOrganization(&dbconf.Organization{Name: orgName})
		if err != nil {
			return err
		}
		fmt.Println()
		fmt.Println("Your organization:")
		fmt.Println("    Name:", org.Name)
		fmt.Println("    API Name:", filepath.Join(api.ResourceNameOrganization, util.Base64Encode(org.UUID)))
	}

	fmt.Println()
	fmt.Println("Creating a new Panto user")
	userEmail, err := ask.String("Enter the new User's email address (e.g. user@example.com):").
		Ask()
	if err != nil {
		return err
	}

	userPassword, err := ask.String("Enter password (the password will not be echoed):").
		Password().
		Ask()
	if err != nil {
		return err
	}
	salt, err := util.GenerateSalt()
	if err != nil {
		return err
	}
	hash, err := util.HashPassword(userPassword, salt)
	if err != nil {
		return err
	}

	details := make(map[string]interface{})
	if !dryRun {
		_, err = db.CreateUser(&dbconf.User{
			OrganizationUUID: org.UUID,
			Email:            userEmail,
			PasswordSalt:     salt,
			PasswordHash:     hash,
			Details:          details,
		})
		if err != nil {
			return err
		}
		fmt.Println()
		fmt.Printf("User %s has been created in %s\n", userEmail, org.Name)
	}

	return nil
}

func init() {
	rootCmd.AddCommand(&initCmd)
	initCmd.AddCommand(&initConfigCmd, &initDataCmd)

	// Init flags for init command
	for _, o := range conf.InitOptionsPersistent {
		if err := util.AddOption(o, initCmd.PersistentFlags()); err != nil {
			panic(err)
		}
	}
	for _, o := range conf.InitOptions {
		if err := util.AddOption(o, initCmd.Flags()); err != nil {
			panic(err)
		}
	}

	// Init flags for config command
	for _, o := range conf.InitConfigOptions {
		if err := util.AddOption(o, initConfigCmd.Flags()); err != nil {
			panic(err)
		}
		if !o.Optional {
			if err := initConfigCmd.MarkFlagRequired(o.Flag); err != nil {
				panic(err)
			}
		}
	}

	// Init flags for data command
	for _, o := range conf.InitDataOptions {
		if err := util.AddOption(o, initDataCmd.Flags()); err != nil {
			panic(err)
		}
		if !o.Optional {
			if err := initDataCmd.MarkFlagRequired(o.Flag); err != nil {
				panic(err)
			}
		}
	}
}
