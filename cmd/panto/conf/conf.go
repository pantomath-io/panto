// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package conf

import (
	"gitlab.com/pantomath-io/panto/util"
)

// Options is a list of the panto executable
var Options = []util.Option{
	// Command-only options
	{
		Default: false,
		Usage:   "suppress all output",
		Flag:    "quiet",
		Short:   "q",
	},
	{
		Default: false,
		Usage:   "display version and exit",
		Flag:    "version",
		Short:   "V",
	},
	{
		Default: "",
		Usage:   "path to configuration file",
		Flag:    "conf",
	},
	// General options
	{
		Name:    "verbose",
		Default: 1,
		Usage:   "set verbosity level (0: silent, 1: warnings and errors, 2: verbose, 3: debug)",
		Flag:    "verbose",
		Short:   "v",
		Env:     "PANTO_VERBOSE",
	},
	// logging options
	{
		Name:    "log.file",
		Default: "",
		Usage:   "path of a file to log Panto output",
		Flag:    "log-file",
		Env:     "PANTO_LOG_FILE",
	},
	{
		Name:    "log.syslog",
		Default: false,
		Usage:   "log Panto output to syslog",
		Env:     "PANTO_LOG_SYSLOG",
	},
	// server options
	{
		Name:    "server.grpc-address",
		Default: ":7575",
		Usage:   "address to bind gRPC API to",
		Flag:    "grpc-address",
	},
	{
		Name:    "server.rest-address",
		Default: ":7576",
		Usage:   "address to bind REST API to",
		Flag:    "rest-address",
	},
	{
		Name:    "server.allow-origin",
		Default: "",
		Usage:   "comma-separated list of origin addresses to allow in the via CORS",
		Flag:    "allow-origin",
	},
	{
		Name:    "server.certfile",
		Default: "",
		Usage:   "path to a TLS certificate file",
		Flag:    "certfile",
		Env:     "PANTO_CERTFILE",
	},
	{
		Name:    "server.certkey",
		Default: "",
		Usage:   "path to a TLS private key file",
		Flag:    "certkey",
		Env:     "PANTO_CERTKEY",
	},
	{
		Name:    "server.no-tls",
		Default: false,
		Usage:   "disable SSL/TLS",
		Flag:    "no-tls",
	},
	// Server info
	{
		Name:    "server-info.public-grpc-address",
		Default: "",
		Usage:   "the public address where a client can reach the gRPC API",
		Env:     "PANTO_PUBLIC_GRPC_ADDRESS",
	},
	{
		Name:    "server-info.public-rest-address",
		Default: "",
		Usage:   "the public address where a client can reach the REST API",
		Env:     "PANTO_PUBLIC_REST_ADDRESS",
	},
	// InfluxDB options
	{
		Name:    "influxdb.address",
		Default: "http://localhost:8086",
		Usage:   "address of an InfluxDB server",
		Flag:    "influxdb-address",
	},
	{
		Name:    "influxdb.database",
		Default: "panto",
		Usage:   "name of the InfluxDB database",
		Flag:    "influxdb-database",
	},
	// SQLite options
	{
		Name:    "db.path",
		Default: "/var/lib/panto/panto.sqlite",
		Usage:   "path to a SQLite configuration database file",
		Flag:    "db-path",
	},
	// SMTP options
	{
		Name:    "smtp.server",
		Default: "",
		Usage:   "address of a SMTP server",
		Env:     "PANTO_SMTP_SERVER",
	},
	{
		Name:    "smtp.port",
		Default: 587,
		Usage:   "port to use for SMTP server",
		Env:     "PANTO_SMTP_PORT",
	},
	{
		Name:    "smtp.username",
		Default: "",
		Usage:   "username for SMTP server",
		Env:     "PANTO_SMTP_USERNAME",
	},
	{
		Name:    "smtp.password",
		Default: "",
		Usage:   "password to use for SMTP server",
		Env:     "PANTO_SMTP_PASSWORD",
	},
	{
		Name:    "smtp.from",
		Default: "hello@panto.app",
		Usage:   "email address to send the mails from",
		Env:     "PANTO_SMTP_FROM",
	},
}
