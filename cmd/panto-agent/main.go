// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"
	"time"

	"gitlab.com/pantomath-io/panto/agent"
	"gitlab.com/pantomath-io/panto/agent/probe"
	"gitlab.com/pantomath-io/panto/api"
	"gitlab.com/pantomath-io/panto/cmd/panto-agent/conf"
	"gitlab.com/pantomath-io/panto/util"

	"github.com/op/go-logging"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

// program ID
var (
	family  = "panto"
	program = "panto-agent"
	version string
	githash string
)

// configureLogger chooses the backend(s) to set
func configureLogger() {

	// Configure log level, select most verbose first
	verbose := viper.GetInt("verbose")
	if verbose > 3 {
		verbose = 3
	}
	var logLevel logging.Level
	switch verbose {
	case 0:
		// silence output, don't configure any loggers
		logging.SetBackend()
		return
	case 1:
		logLevel = logging.WARNING
	case 2:
		logLevel = logging.INFO
	case 3:
		logLevel = logging.DEBUG
	default:
		logLevel = logging.WARNING
	}

	var backendList = make([]logging.Backend, 0, 3)

	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} [%{level:.4s}] %{shortfunc}%{color:reset}: %{message} (%{shortfile})`,
	)

	// Configure console logger
	consoleBackend := logging.NewLogBackend(os.Stderr, "", 0)
	consoleBackendFormatter := logging.NewBackendFormatter(consoleBackend, format)
	consoleBackendLeveled := logging.AddModuleLevel(consoleBackendFormatter)
	consoleBackendLeveled.SetLevel(logLevel, "")
	backendList = append(backendList, consoleBackendLeveled)

	// Configure file logger
	if viper.IsSet("log.file") {
		var fileformat = logging.MustStringFormatter(
			`%{time:2006-01-02T15:04:05.000} [%{level:.4s}] %{shortfunc}: %{message}`,
		)

		f, err := os.OpenFile(viper.GetString("log.file"), os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
		if err != nil {
			log.Errorf("Error opening log file: %v", err)
		} else {
			filebackend := logging.NewLogBackend(f, "", 0)
			filebackendFormatter := logging.NewBackendFormatter(filebackend, fileformat)
			filebackendLeveled := logging.AddModuleLevel(filebackendFormatter)
			filebackendLeveled.SetLevel(logLevel, "")

			backendList = append(backendList, filebackendLeveled)
		}
	}

	// Configure syslog logger
	if viper.IsSet("log.syslog") && viper.GetBool("log.syslog") {
		syslogBackend, err := logging.NewSyslogBackend("")
		if err != nil {
			log.Errorf("Error with syslog: %v", err)
		}

		backendList = append(backendList, syslogBackend)
	}

	logging.SetBackend(backendList...)
}

// configureFlags configures all the command-line flags
func configureFlags(flags *pflag.FlagSet) {
	// if --quiet, silence all output
	quietFlag := flags.Lookup("quiet")
	if quietFlag != nil && quietFlag.Value.String() == "true" {
		viper.Set("verbose", 0)
	}

	argc := flags.NArg()

	if argc > 0 {
		viper.Set("server.address", flags.Arg(0))
	}
}

// loadConfiguration reads config file and set default values
func loadConfiguration(flags *pflag.FlagSet) error {
	viper.SetConfigName(program) // config file named panto-agent.yaml

	confPathFlag := flags.Lookup("conf")
	if confPathFlag != nil && confPathFlag.Changed {
		confPath, err := util.ExpandAbs(confPathFlag.Value.String())
		if err != nil {
			return fmt.Errorf("could not resolve configuration file path: %s", err)
		}
		viper.SetConfigFile(confPath)
	}

	viper.AddConfigPath(filepath.Join("/etc", family)) // config file in /etc/panto
	viper.AddConfigPath(filepath.Join("etc", family))  // config file in $PWD/etc/panto

	err := viper.ReadInConfig()
	if err != nil && confPathFlag != nil && confPathFlag.Changed {
		return fmt.Errorf("could not load config file: %s", err)
	}

	return nil
}

// sigHandler manages OS signals:
// os.Interrupt and SIGTERM exits the program
func sigHandler() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	for sig := range c {
		log.Debug("Received signal ", sig)

		switch sig {
		case syscall.SIGTERM, os.Interrupt:
			log.Info("Bye bye")
			os.Exit(0)
		}
	}
}

// fetchAndConfigureScheduler retrieves a valid configuration from the server and load it into the scheduler
func fetchAndConfigureScheduler(cl *agent.Client, scheduler *agent.Scheduler) error {
	var err error
	var schedulerConfig *api.ListProbeConfigurationsResponse
	var configVersion string

	// retrieve the configuration from the server
	schedulerConfig, configVersion, err = cl.RequestConfiguration()
	if err != nil {
		return err
	}
	// flush everything in the Scheduler
	if err := scheduler.RemoveAllProbes(); err != nil {
		return err
	}

	// Load the agent configuration in the scheduler
	err = scheduler.ConfigureScheduler(schedulerConfig, configVersion)
	if err != nil {
		return err
	}

	return nil
}

func run(cmd *cobra.Command, args []string) {
	// Command line management
	configureFlags(cmd.Flags())

	versionString := fmt.Sprintf("%s %s-%s", program, version, githash)
	versionFlag := cmd.Flags().Lookup("version")
	if versionFlag != nil && versionFlag.Value.String() == "true" {
		fmt.Println(versionString)
		os.Exit(0)
	}
	viper.Set("agent-version", version)

	if err := loadConfiguration(cmd.Flags()); err != nil {
		log.Critical(err.Error())
		os.Exit(1)
	}

	configureLogger()

	log.Infof(versionString)

	jsonSettings, _ := json.Marshal(viper.AllSettings())
	log.Debugf("Configuration: %s", jsonSettings)

	// Set number of parallel threads to number of CPUs.
	runtime.GOMAXPROCS(runtime.NumCPU())

	// manage signals
	go sigHandler()

	if !util.AddressHasPort(viper.GetString("server.address")) {
		log.Criticalf("the address of Panto server is not in the 'host:port' format")
		os.Exit(1)
	}
	certFilePath, err := util.ExpandAbs(viper.GetString("server.certfile"))
	if err != nil {
		log.Criticalf("unable to resolve TLS certificate file path: %s", err)
		os.Exit(1)
	}
	confDumpPath, err := util.ExpandAbs(viper.GetString("agent.configuration-dump"))
	if err != nil {
		log.Criticalf("unable to resolve configuration dump file path: %s", err)
		os.Exit(1)
	}

	// Create client
	cl, err := agent.NewClient(agent.ClientConfiguration{
		Address:           viper.GetString("server.address"),
		CertFilePath:      certFilePath,
		Timeout:           viper.GetDuration("agent.timeout"),
		AgentName:         viper.GetString("agent.name"),
		NoTLS:             viper.GetBool("server.no-tls"),
		ConfigurationDump: confDumpPath,
	})
	if err != nil {
		log.Fatalf("couldn't open connection to server: %s", err)
	}
	defer cl.Close()

	// Get the default Scheduler and configure it
	scheduler := agent.DefaultScheduler

	// Create the spooler
	spooler, err := agent.NewSpooler(viper.GetInt("agent.max-spooled-results"), viper.GetString("agent.spooler-dump-path"))
	if err != nil {
		log.Criticalf("couldn't create a result spooler: %s", err)
		log.Critical("the agent could not create a result spooler, all results that are not sent to the server will be lost")
	}

	configVersion := scheduler.GetVersion()
	for {
		// check the config version and reload the scheduler if needed
		if scheduler.GetVersion() == "" || configVersion != scheduler.GetVersion() {
			var probeConfigurationCount int
			for probeConfigurationCount == 0 {
				err = fetchAndConfigureScheduler(cl, scheduler)

				probeConfigurationCount = len(scheduler.GetProbes())
				if err != nil {
					log.Errorf("couldn't fetch agent configuration: %s", err)
				} else if probeConfigurationCount == 0 {
					log.Info("empty agent configuration")
				}

				if probeConfigurationCount == 0 {
					log.Debug("sleeping a while before asking again")
					time.Sleep(30 * time.Second)
				}
			}
		}
		res := <-scheduler.Results()

		configVersion, err = cl.ReportResults(res)
		if err != nil {
			log.Errorf("could not send results: %v", err)
			if err := status.Convert(err); err.Code() == codes.Unavailable {
				// the server is unavailable:
				// spool the results to send later, when connectivity is reestablished
				if spooler != nil {
					log.Infof("Panto server could not be reached, spooling %d results", len(res))
					spooler.AddResults(res)
				}
			}
			continue
		}

		if spooler.Count() > 0 {
			_, err = cl.ReportResults(spooler.Results())
			if err != nil {
				log.Errorf("could not send results: %v", err)
				if err := status.Convert(err); err.Code() != codes.Unavailable {
					// Sending spooled results failed, but not because of connectivity
					// clear the spooler
					spooler.Clear()
				}
			} else {
				log.Infof("sent %d spooled results", spooler.Count())
				spooler.Clear()
			}
		}
	}
}

func runProbe(cmd *cobra.Command, args []string) error {
	if viper.GetBool("probe-list") {
		for _, p := range probe.Probes {
			fmt.Println(p.Name())
		}
		return nil
	}

	if len(args) < 1 {
		return fmt.Errorf("missing argument: [probe name]")
	}

	name := args[0]
	p, err := probe.NewProbe(name)
	if err != nil {
		return err
	}

	var config map[string]interface{}
	if len(args) > 1 {
		err = json.Unmarshal([]byte(args[1]), &config)
		if err != nil {
			return fmt.Errorf("unable to read JSON probe configuration: %s", err)
		}
	}

	err = p.Configure(config)
	if err != nil {
		return fmt.Errorf("unable to configure probe \"%s\": %s", name, err)
	}

	res, err := p.Execute()
	if err != nil {
		return fmt.Errorf("probe \"%s\" failed to execute: %s", name, err)
	}
	now := time.Now()
	for _, r := range res {
		r.Timestamp = now
	}

	var j []byte
	if viper.GetBool("probe-pretty") {
		j, err = json.MarshalIndent(res, "", "  ")
	} else {
		j, err = json.Marshal(res)
	}
	if err != nil {
		return fmt.Errorf("failed to marshal results to JSON: %s", err)
	}

	fmt.Println(string(j))
	return nil
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "panto-agent [flags] [server address]",
	Short: "panto-agent gathers metrics about targets and reports to a Panto server",
	Long:  `panto-agent gathers metrics about targets and reports to a Panto server`,
	Run:   run,
	Args:  cobra.MaximumNArgs(1),
}

var probeCmd = &cobra.Command{
	Use:   "probe [flags] [probe name [JSON probe configuration]]",
	Short: "runs a probe and outputs the results",
	Long:  "runs any registered probe once and writes the results to standard output",
	RunE:  runProbe,
	Args:  cobra.MaximumNArgs(2),
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %s", err)
		os.Exit(1)
	}
}

func init() {
	for _, o := range conf.Options {
		if err := util.AddOption(o, rootCmd.Flags()); err != nil {
			panic(err)
		}
		if err := util.BindOption(o, rootCmd.Flags()); err != nil {
			panic(err)
		}
	}
	rootCmd.Flags().SortFlags = false

	rootCmd.SetUsageTemplate(rootCmd.UsageTemplate() + `
Configuration file:
More options can be configured using a configuration file.
panto-agent will look for a configuration file in the following locations:
  * at the path specified on the command line, if present
  * /etc/panto/panto-agent.yaml
  * <PWD>/etc/panto/panto-agent.yaml
Options set on the command line will override options from the configuration file
`)

	probeCmd.SetUsageTemplate(probeCmd.UsageTemplate())

	for _, o := range conf.ProbeOptions {
		if err := util.AddOption(o, probeCmd.Flags()); err != nil {
			panic(err)
		}
		if err := util.BindOption(o, probeCmd.Flags()); err != nil {
			panic(err)
		}
	}
	probeCmd.Flags().SortFlags = false

	rootCmd.AddCommand(probeCmd)
}
