// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package conf

import (
	"time"

	"gitlab.com/pantomath-io/panto/util"
)

// Options is the list of options for the panto-agent executable
var Options = []util.Option{
	// Command-only options
	{
		Default: false,
		Usage:   "suppress all output",
		Flag:    "quiet",
		Short:   "q",
	},
	{
		Default: false,
		Usage:   "display version and exit",
		Flag:    "version",
		Short:   "V",
	},
	{
		Default: "",
		Usage:   "path to configuration file",
		Flag:    "conf",
	},
	// General options
	{
		Name:    "verbose",
		Default: 1,
		Usage:   "set verbosity level (0: silent, 1: warnings and errors, 2: verbose, 3: debug)",
		Flag:    "verbose",
		Short:   "v",
		Env:     "PANTO_AGENT_VERBOSE",
	},
	// logging options
	{
		Name:    "log.file",
		Default: "",
		Usage:   "path of a file to log output",
		Flag:    "log-file",
		Env:     "PANTO_AGENT_LOG_FILE",
	},
	{
		Name:    "log.syslog",
		Default: false,
		Usage:   "log output to syslog",
		Env:     "PANTO_AGENT_LOG_SYSLOG",
	},
	// agent options
	{
		Name:    "agent.name",
		Default: "",
		Usage:   "name of the agent in the Panto API",
		Flag:    "name",
		Short:   "n",
		Env:     "PANTO_AGENT_NAME",
	},
	{
		Name:    "agent.timeout",
		Default: 45 * time.Second,
		Usage:   "timeout on requests and connection",
		Flag:    "timeout",
		Env:     "PANTO_AGENT_TIMEOUT",
	},
	{
		Name:    "agent.configuration-dump",
		Default: "",
		Usage:   "Path for the configuration dump file",
		Flag:    "configuration-dump",
		Env:     "PANTO_AGENT_CONFIGURATION_DUMP",
	},
	{
		Name:    "agent.max-spooled-results",
		Default: 100,
		Usage:   "maximum number of results spooled while server can't be reached",
		Flag:    "max-spooled-results",
		Env:     "PANTO_MAX_SPOOLED_RESULTS",
	},
	{
		Name:    "agent.spooler-dump-path",
		Default: "",
		Usage:   "path of the dump file for the spooled results",
		Flag:    "spooler-dump-path",
		Env:     "PANTO_SPOOLER_DUMP_PATH",
	},
	// server options
	{
		Name:    "server.address",
		Default: "",
		Usage:   "address of Panto server (host:port)",
	},
	{
		Name:    "server.certfile",
		Default: "",
		Usage:   "path to a TLS certificate file",
		Flag:    "certfile",
	},
	{
		Name:    "server.no-tls",
		Default: false,
		Usage:   "disable SSL/TLS",
		Flag:    "no-tls",
	},
}

// ProbeOptions is the list of options for the panto-agent probe command
var ProbeOptions = []util.Option{
	{
		Name:    "probe-list",
		Default: false,
		Usage:   "list all available probes",
		Flag:    "list",
		Short:   "l",
	},
	{
		Name:    "probe-pretty",
		Default: false,
		Usage:   "pretty-print JSON",
		Flag:    "pretty",
	},
}
