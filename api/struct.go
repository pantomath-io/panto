// Copyright 2020 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"bytes"
	"encoding/json"

	"github.com/golang/protobuf/jsonpb"
	structpb "github.com/golang/protobuf/ptypes/struct"
)

// NewStruct creates a new *structpb.Value (a glorified JSON wrapper) out of a Go type
func NewStruct(v interface{}) (*structpb.Value, error) {
	var s structpb.Value
	b, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}
	err = jsonpb.Unmarshal(bytes.NewReader(b), &s)
	if err != nil {
		return nil, err
	}
	return &s, nil
}
