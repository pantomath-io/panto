// Copyright 2017 Pantomath SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"fmt"
	"net/url"
	"regexp"

	"github.com/google/uuid"
	"gitlab.com/pantomath-io/panto/util"
)

// Standard names of the resources
const (
	ResourceNameCheck              string = "checks"
	ResourceNameOrganization       string = "organizations"
	ResourceNameAgent              string = "agents"
	ResourceNameTarget             string = "targets"
	ResourceNameProbeConfiguration string = "probeconfigurations"
	ResourceNameProbe              string = "probes"
	ResourceNameAlert              string = "alerts"
	ResourceNameChannel            string = "channels"
	ResourceNameChannelType        string = "channeltypes"
	ResourceNameTag                string = "tags"
)

// The resources are organized hierarchically as follows:
// /
// ├── organizations
// │   ├── agents
// │   │   └── probeconfigurations
// │   ├── targets
// │   ├── checks
// │   ├── alerts
// │   ├── channels
// │   └── tags
// │   └── states
// ├── probes
// └── channeltypes

const uuidPattern = "([-_a-zA-Z0-9]{22})"

// a "name" can be any sequence of non-whitespace characters not in the URI "reserved" character list (RFC3986)
// https://tools.ietf.org/html/rfc3986#section-2.2
const uriComponentPattern = `([^\s\:/?#\[\]@!$&'()*+,;=]+)`

var organizationRegexp = regexp.MustCompile(
	"^" +
		ResourceNameOrganization + "/" + uuidPattern +
		"$",
)

// ParseNameOrganization checks the validity of an Organization name and extracts the component UUIDs
func ParseNameOrganization(name string) (organizationUUID uuid.UUID, err error) {
	matches := organizationRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Organization name")
		return
	}
	organizationUUID, err = util.Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Organization UUID: %s", err)
		return
	}
	return
}

var probeRegexp = regexp.MustCompile(
	"^" +
		ResourceNameProbe + "/" + uuidPattern +
		"$",
)

// ParseNameProbe checks the validity of a Probe name and extracts the component UUIDs
func ParseNameProbe(name string) (probeUUID uuid.UUID, err error) {
	matches := probeRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Probe name")
		return
	}
	probeUUID, err = util.Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Probe UUID: %s", err)
		return
	}
	return
}

var channelTypeRegexp = regexp.MustCompile(
	"^" +
		ResourceNameChannelType + "/" + uriComponentPattern +
		"$",
)

// ParseNameChannelType checks the validity of a ChannelType name and extracts the component label
func ParseNameChannelType(name string) (channelTypeLabel string, err error) {
	matches := channelTypeRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid ChannelType name")
		return
	}
	return matches[1], nil
}

var agentRegexp = regexp.MustCompile(
	"^" +
		ResourceNameOrganization + "/" + uuidPattern +
		"/" + ResourceNameAgent + "/" + uuidPattern +
		"$",
)

// ParseNameAgent checks the validity of a Agent name and extracts the component UUIDs
func ParseNameAgent(name string) (organizationUUID, agentUUID uuid.UUID, err error) {
	matches := agentRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Agent name")
		return
	}
	organizationUUID, err = util.Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Organization UUID: %s", err)
		return
	}
	agentUUID, err = util.Base64Decode(matches[2])
	if err != nil {
		err = fmt.Errorf("invalid Agent UUID: %s", err)
		return
	}
	return
}

var targetRegexp = regexp.MustCompile(
	"^" +
		ResourceNameOrganization + "/" + uuidPattern +
		"/" + ResourceNameTarget + "/" + uuidPattern +
		"$",
)

// ParseNameTarget checks the validity of a Target name and extracts the component UUIDs
func ParseNameTarget(name string) (organizationUUID, targetUUID uuid.UUID, err error) {
	matches := targetRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Target name")
		return
	}
	organizationUUID, err = util.Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Organization UUID: %s", err)
		return
	}
	targetUUID, err = util.Base64Decode(matches[2])
	if err != nil {
		err = fmt.Errorf("invalid Target UUID: %s", err)
		return
	}
	return
}

var checkRegexp = regexp.MustCompile(
	"^" +
		ResourceNameOrganization + "/" + uuidPattern +
		"/" + ResourceNameCheck + "/" + uuidPattern +
		"$",
)

// ParseNameCheck checks the validity of a Check name and extracts the component UUIDs
func ParseNameCheck(name string) (organizationUUID, checkUUID uuid.UUID, err error) {
	matches := checkRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Check name")
		return
	}
	organizationUUID, err = util.Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Organization UUID: %s", err)
		return
	}
	checkUUID, err = util.Base64Decode(matches[2])
	if err != nil {
		err = fmt.Errorf("invalid Check UUID: %s", err)
		return
	}
	return
}

var alertRegexp = regexp.MustCompile(
	"^" +
		ResourceNameOrganization + "/" + uuidPattern +
		"/" + ResourceNameAlert + "/" + uuidPattern +
		"$",
)

// ParseNameAlert checks the validity of a Alert name and extracts the component UUIDs
func ParseNameAlert(name string) (organizationUUID, alertUUID uuid.UUID, err error) {
	matches := alertRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Alert name")
		return
	}
	organizationUUID, err = util.Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Organization UUID: %s", err)
		return
	}
	alertUUID, err = util.Base64Decode(matches[2])
	if err != nil {
		err = fmt.Errorf("invalid Alert UUID: %s", err)
		return
	}
	return
}

var channelRegexp = regexp.MustCompile(
	"^" +
		ResourceNameOrganization + "/" + uuidPattern +
		"/" + ResourceNameChannel + "/" + uuidPattern +
		"$",
)

// ParseNameChannel checks the validity of a Channel name and extracts the component UUIDs
func ParseNameChannel(name string) (organizationUUID, channelUUID uuid.UUID, err error) {
	matches := channelRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Channel name")
		return
	}
	organizationUUID, err = util.Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Organization UUID: %s", err)
		return
	}
	channelUUID, err = util.Base64Decode(matches[2])
	if err != nil {
		err = fmt.Errorf("invalid Channel UUID: %s", err)
		return
	}
	return
}

var tagRegexp = regexp.MustCompile(
	"^" +
		ResourceNameOrganization + "/" + uuidPattern +
		"/" + ResourceNameTag + "/" + uriComponentPattern +
		"$",
)

// ParseNameTag checks the validity of a Tag name and extracts the component UUIDs
func ParseNameTag(name string) (organizationUUID uuid.UUID, tagName string, err error) {
	matches := tagRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Tag name")
		return
	}
	organizationUUID, err = util.Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Organization UUID: %s", err)
		return
	}
	tagName, err = url.PathUnescape(matches[2])
	if err != nil {
		err = fmt.Errorf("invalid Tag name: %s", err)
		return
	}

	return
}

var probeConfigurationRegexp = regexp.MustCompile(
	"^" +
		ResourceNameOrganization + "/" + uuidPattern +
		"/" + ResourceNameAgent + "/" + uuidPattern +
		"/" + ResourceNameProbeConfiguration + "/" + uuidPattern +
		"$",
)

// ParseNameProbeConfiguration checks the validity of a ProbeConfiguration name and extracts the component UUIDs
func ParseNameProbeConfiguration(name string) (organizationUUID, agentUUID, probeConfigurationUUID uuid.UUID, err error) {
	matches := probeConfigurationRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid ProbeConfiguration name")
		return
	}
	organizationUUID, err = util.Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Organization UUID: %s", err)
		return
	}
	agentUUID, err = util.Base64Decode(matches[2])
	if err != nil {
		err = fmt.Errorf("invalid Agent UUID: %s", err)
		return
	}
	probeConfigurationUUID, err = util.Base64Decode(matches[3])
	if err != nil {
		err = fmt.Errorf("invalid ProbeConfiguration UUID: %s", err)
		return
	}
	return
}
