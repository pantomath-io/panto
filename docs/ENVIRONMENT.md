# Development Environment

## Go environment

Panto is a project written in Golang, so you'll obviously need to have a working Go 1.13 environenment to play with it. Please refer to the [official documentation](https://golang.org/doc/install) from golang.org.


## InfluxDB

Panto server uses InfluxDB v1.7.10 to store the results of the probes. Please refer to the [official documentation](https://influxdata.com/downloads/#influxdb) from influxdata.com.

## Database migration

Panto use a SQLite database to store the configurations (for agents, probes, alerts, ...). In order to manage schema migration, we use [goose](https://github.com/loderunner/goose) (note: this is a fork of original [goose](https://bitbucket.org/liamstask/goose)). Please refer to [the official documentation](https://github.com/loderunner/goose#usage) to know more about how to use goose. In our case, to generate a new migration script we use the following command:

```sh
$ goose -dir data/migrations sqlite3 data/panto.sqlite create ExplicitMigrationName
```

You should adapt the path of the database file (`data/panto.sqlite`), and the name of the migration (`ExplicitMigrationName`). Note that this generates a [go migration](https://github.com/loderunner/goose#go-migrations). The generated file needs to be tweaked:

* change package name:
Replace `package migration` by `package migrations`

# Vagrant / Ansible

Some tests require to have the whole production setup working (i.e. a functionnal golang environment with a working InfluxDB). For the convenience of the developer (and for the Continous Integration processes), an [Ansible playbook](http://docs.ansible.com/ansible/playbooks.html) is at your disposal to deploy everything you need. In order to use that playbook, you need to have Ansible installed. Please refer to the [official documenation](http://docs.ansible.com/ansible/intro_installation.html) from ansible.com.

The [Gitlab CI script](/.gitlab-ci.yml) uses this playbook to configure the CI environment, prior to run the tests.

This setup can be done on a virtual machine, using [Vagrant](https://www.vagrantup.com) with [Virtualbox](https://www.virtualbox.org/) as provider. To work with Vagrant, please refer to the [official documentation](https://www.vagrantup.com/intro/getting-started/install.html) from vagrantup.com.

## Create environment

To run and deploy the Vagrant machine, simply go to the [vagrant](/vagrant) directory, and fire up the machine:

```sh
$ vagrant up
```

The machine shall be created and the provisioning automatically done.
