# Panto Documentation

* The [Concepts](CONCEPTS.md) document is a presentation of the different components of Panto, and the definition of the terms used in the project.
* The [API](../api/README.md) holds its own documentation.
* The [Security](SECURITY.md) document gathers everything about the security aspect of the project, starting with the Certficate used in the gRPC server/agent.
* The [Build](BUILD.md) document explains how to build the Panto project, and how to run it in several configurations.
* The [Testing](TESTING.md) document shows how to setup a proper testing environment.
* The [Process](PROCESS.md) document explains the development process of the Panto contributor team.
