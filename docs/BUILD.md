# Build

This document collects all the prerequisites, dependencies and steps required to build Panto. If you're in a hurry, you can skip the first part and easily prepare a developer's VM using [Vagrant](#vagrant).

- [Build](#build)
  - [Prerequisites](#prerequisites)
    - [Go](#go)
    - [Node.js & npm](#nodejs--npm)
    - [Clang & clang-format](#clang--clang-format)
      - [Install Clang for Debian/Ubuntu:](#install-clang-for-debianubuntu)
      - [Install Clang for macOS](#install-clang-for-macos)
      - [Other platforms](#other-platforms)
    - [Protocol Buffers](#protocol-buffers)
    - [Various](#various)
    - [Go dependencies](#go-dependencies)
  - [Build](#build-1)
    - [Building the project](#building-the-project)
  - [Vagrant](#vagrant)
    - [Prerequisites](#prerequisites-1)
    - [Build Vagrant VM](#build-vagrant-vm)
    - [Updating Vagrant VM](#updating-vagrant-vm)

## Prerequisites

### Go

Panto is a [Go](https://golang.org) project, written for Go >= 1.13. Download and install Go for your platform by following the [official instructions](https://golang.org/doc/install) from the Go website, or use your preferred package manager (unsupported).

### Node.js & npm

The web client app uses the Node.js and `npm` packages to build. Download and install Node 12.x for your platform by following the [official instructions](https://nodejs.org/en/download) from the Node.js website, or [use your preferred package manager](https://nodejs.org/en/download/package-manager).

### Clang & clang-format

Panto uses the `clang` C compiler to build, and `clang-format` to automatically format certain file types. 

#### Install Clang for Debian/Ubuntu:

Installing `clang` and `clang-format` for Debian/Ubuntu is easier done by using the [LLVM APT repository](http://apt.llvm.org/).

From the above page, add the sources for your specific distribution to your [`sources.list`](https://wiki.debian.org/SourcesList) file and type the following commands:

```shell
# Retrieve the archive signature
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -
# Update the APT cache
sudo apt-get update
# Install clang and clang-format
sudo apt-get install clang clang-format
```

Be sur to configure `clang` as your default C compiler, by typing this command in a shell or editing your `~/.profile` file (or similar):

```shell
export CC=clang
```

#### Install Clang for macOS

`clang` is the default compiler for macOS. To make sure the developer tools are installed on your computer, either install [Xcode](https://developer.apple.com/xcode/) (not recommended), or just the command-line tools by typing:

```shell
xcode-select --install
```

and follow the instructions on screen.

To install `clang-format`, the easiest way is to go through the [Homebrew package manager](https://brew.sh). Make sure `brew` is installed and type:

```shell
brew install clang-format
```

#### Other platforms

To install `clang` and `clang-format` on another platform, download and install binaries from the [releases page](http://releases.llvm.org/download.html) or build it from source.

### Protocol Buffers

Panto uses [Protocol Buffers](https://developers.google.com/protocol-buffers/) 3.5.x to define the API for clients of the server. Download and install the `protoc` binaries for your platform from the [releases page](https://github.com/google/protobuf/releases), or use your preferred package manager.

### Various

Various basic tools should be present on your computer, such as:

* `git`
* `curl`
* GNU `make`

Install them with your preferred package manager.

### Go dependencies

The following dependencies are installed using the `go get` tool:

* [Go `protoc` plugin](https://github.com/golang/protobuf)
* [golint](https://github.com/golang/lint)
* [grpc-gateway `protoc` plugins](https://github.com/grpc-ecosystem/grpc-gateway)

From the command line, type the following:

```shell
go get -u github.com/golang/lint
go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
go get -u github.com/golang/protobuf/protoc-gen-go
```

Also download all indirect modules using:

```
go mod download
```

## Build

Once your environment is setup – either by following the above steps or by using the [Vagrant](#vagrant) VM, it's time to download and build Panto.

### Building the project

To build the project navigate to the Panto directory in your Go path, and type:

```
make
```

The project should download all dependencies and build all binaries and the web client. For more details about the `make` targets, see the project [Makefile](https://gitlab.com/pantomath-io/panto/blob/develop/Makefile).

After this, you should find the built binaries in the `bin/` directory, and the built web client in `web/dist/`.

## Vagrant

The easiest way to setup a complete developer environment with all prerequisites is to use the Vagrant VM included in the repository.

### Prerequisites

Required:

* [Vagrant](https://www.vagrantup.com/)
* [Ansible](https://www.ansible.com/)
* [Go](https://golang.org)

Recommended on macOS:

* [VirtualBox](https://www.virtualbox.org/)
* [vagrant-vbguest](https://github.com/dotless-de/vagrant-vbguest)

### Build Vagrant VM

To setup and launch the VM using vagrant, [get the Panto sources](#downloading-the-repository), navigate to the Panto directory in your Go path, and type the following commands:

```shell
cd vagrant
vagrant up
```

This will setup and boot a virtual machine with all dependencies to build and run Panto.

```shell
vagrant ssh
```

If all went well, you should now be connected to the VM and see the following prompt:

```
vagrant@buster:~$
```

The repository on your host machine and the one on the VM should be shared. You can use your host system to work on the files inside the repo and build using the VM, [following the above instructions](#building-the-project).

### Updating Vagrant VM

If the `Vagrantfile` has been updated in the repository, you will probably need to reload the VM. From the command-line, navigate to the Panto repository and type the following comands:

```shell
cd vagrant
vagrant reload
```

This will shutdown and restart the Vagrant VM, using the new `Vagrantfile`.

If the `playbook-setup.yml` or `requirement.yml` files have changed, you will need to provision the VM again. From the command-line, navigate to the Panto repository and type the following comands:

```shell
cd vagrant
vagrant provision
```

In any case, if your VM seems broken for any reason, and you need to start from a fresh one, you can nuke it by typing:

```shell
vagrant destroy
vagrant up
```