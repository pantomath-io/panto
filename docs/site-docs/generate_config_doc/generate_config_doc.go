// This is a helper to turn configurations into markdown for the documentation
package main

import (
	"fmt"

	panto_agent "gitlab.com/pantomath-io/panto/cmd/panto-agent/conf"
	panto_ctl "gitlab.com/pantomath-io/panto/cmd/panto-ctl/conf"
	panto "gitlab.com/pantomath-io/panto/cmd/panto/conf"
	"gitlab.com/pantomath-io/panto/util"
)

func printConfig(opts []util.Option) {
	fmt.Println("|Configuration file|Command-line|Environment variable|Usage|Default|")
	fmt.Println("|---|---|---|---|---|")
	for _, o := range opts {
		// Config file name
		fmt.Print("| ")
		if o.Name != "" {
			fmt.Printf("`%s`", o.Name)
		}
		// Command-line flag
		fmt.Print(" | ")
		if o.Flag != "" {
			fmt.Printf("`--%s`", o.Flag)
		}
		if o.Short != "" {
			fmt.Printf(", `-%s`", o.Short)
		}
		// Env var
		fmt.Print(" | ")
		if o.Env != "" {
			fmt.Printf("`%s`", o.Env)
		}
		// Usage
		fmt.Print(" | ")
		if o.Usage != "" {
			fmt.Printf("%s", o.Usage)
		}
		// Default
		fmt.Print(" | ")
		if o.Default != "" {
			fmt.Printf("`%v`", o.Default)
		}
		fmt.Println(" |")
	}
}

func main() {
	fmt.Println("------ panto options ------")
	printConfig(panto.Options)
	fmt.Println()

	fmt.Println("------ panto-agent options ------")
	printConfig(panto_agent.Options)
	fmt.Println()

	fmt.Println("------ panto-ctl init persistent options ------")
	printConfig(panto_ctl.InitOptionsPersistent)
	fmt.Println()

	fmt.Println("------ panto-ctl init options")
	printConfig(panto_ctl.InitOptions)
	fmt.Println()

	fmt.Println("------ panto-ctl init config options ------")
	printConfig(panto_ctl.InitConfigOptions)
	fmt.Println()

	fmt.Println("------ panto-ctl init data options ------")
	printConfig(panto_ctl.InitDataOptions)
	fmt.Println()

	fmt.Println("------ panto-ctl dbconf options ------")
	printConfig(panto_ctl.DBConfOptions)
	fmt.Println()

	fmt.Println("------ panto-ctl user options ------")
	printConfig(panto_ctl.UserOptions)
	fmt.Println()
}
