% Getting Started

Panto is a platform for monitoring your infrastructure, servers and applications in production. Agents collect metrics about your systems in real time, and report their health and performance to the server. The server stores metrics, evaluates the state according to user-defined rules, and, in case the state is considered dysfunctional, dispatches alerts to your teams.

⚠️ Note: all commands prepended with a `#` prompt might need to be run as `root` or using `sudo`. The commands started with a `$` prompt could be run as any unprivileged user, except stated otherwise.

## Installing Panto

There are plenty of way to install Panto in your environment, check out the [detailed documentation](002-Detailed-Documentation.md#installing-panto) for your prefered methods.

### Panto server

The recommended setup to run a `panto-server` instance is to use the [Docker Compose](https://docs.docker.com/compose/) version. After making sure `docker-compose` is installed, download the [`docker-compose.yml`](https://gitlab.com/pantomath-io/panto/raw/master/docker/docker-compose.yml) file to your computer, you can edit it to adapt the configuration to your environment.

This will download all the necessary images from the official repositories, and run all containers and connect the pieces for you.

By default, this will start all the components (the `panto-server`, the TSDB, one `panto-agent` and the `panto-web`), and will expose the following ports:

* `TCP/7575`: `panto-server`, gRPC interface
* `TCP/7576`: `panto-server`, REST interface
* `TCP/8080`: `panto-web`, the web application

### Panto agent

The recommended setup to run a `panto-agent` instance is to use a package version.

Debian packages are available in a dedicated apt repository:

```shell
$ wget -qO- https://packages.panto.app/pantomath.key | sudo apt-key add -
$ echo "deb https://packages.panto.app/debian stable main" | sudo tee /etc/apt/sources.list.d/panto.list
$ sudo apt update
```

You can now install the package:

```shell
$ sudo apt install panto-agent
```

## Running Panto

Once you've [downloaded and installed](#installing-panto) the Panto files, it's time to get Panto running. Panto comes in three parts: the [server](#server), the [agents](#agent) and the [web client](#web-client).

### Server

The server is the "brain" of Panto. It stores the monitoring data for your environment: the metrics from your targets, and the setup of targets, probes and agents in your infrastructure. This is where the agents will report metrics from targets; this is where the web client will fetch and send data to display and update your environment; this is where alerts will be spawned when an agent reports critical behavior.

Now the `docker-compose.yml` is confiugred, type:

```shell
$ docker-compose up
```

from the same directory as the `docker-compose.yml` file.

When running for the first time, you need to initialize the persistent database. Use the following command, once the docker compose is up:

```shell
$ docker exec -it panto-server /usr/bin/panto-ctl init data
```

You will be asked for your `Organization`, and to create your first `User`.

⚠️ Note: The `panto-agent` container of this `docker-compose` might not be working until you [configured](002-Detailed-Documentation.md#agent-initialization) it.

To learn more about configuration options, use `panto --help` or see the [documentation](002-Detailed-Documentation.md#panto).

### Web client

The web client is a static web application that runs in the browser. The client is the main frontend to display metrics, create targets, configure alerts, etc. Panto uses [Caddy](https://caddyserver.com/) to serve the files. This part is included in the `docker-compose` setup, so you don't have to run it manually, but you will need to configure a [proxy](#proxy).

To learn more about setup and configuration options, see the [documentation](002-Detailed-Documentation.md#web-client).

### Agent

The agents are the workers of your Panto environment. Agents collect performance and application metrics and report back to the Panto server.

Before you run the agent, make sure it is declared on the server, and you have the configuration file (e.g. `/etc/panto/panto-agent.yaml`) properly updated. The `panto-agent` is registered as a service, so you can type:

```shell
$ sudo service panto-agent start
```

To learn more about configuration options, use `panto-agent --help`or check the [documentation](002-Detailed-Documentation.md#panto-agent).

### Proxy

You can connect directly to your `panto` or `panto-web` applications, or you can have them behind a proxy, such as [nginx](https://nginx.org/). Installing nginx is out of the scope of this documentation, but here are examples of configuration. Obviously, you'll need to adapt these, to include a proper TLS certificate, or to setup logs. The configurations shown focus on the proxy part.

For `panto-web`, it's pretty simple, as the application is serving HTTP content:

```
upstream panto-web {
    server localhost:8080 fail_timeout=0;
}

server {
    listen 443 ssl;
    server_name panto.app;

    location / {
        proxy_pass http://panto-web;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $remote_addr;
        if ($uri != '/') {
                expires 30d;
            }
    }
}
```

For `panto`, there are 2 sides: the gRPC server, and the REST gateway. The latter is as straightforward as `panto-web`:

```
upstream panto-api {
    server localhost:7576 fail_timeout=0;
}

server {
    listen 443 ssl;
    server_name api.panto.app;

    location / {
        proxy_pass http://panto-api;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $remote_addr;
    }
}
```

The gRPC server can also be proxied, using the [gRPC module](http://nginx.org/en/docs/http/ngx_http_grpc_module.html) (introduced in nginx-1.13.10: read [announcement](https://www.nginx.com/blog/nginx-1-13-10-grpc/)).

```
server {
    listen 443 ssl http2;
    server_name rpc.panto.app;

    location / {
        grpc_pass grpc://127.0.0.1:7575;
    }
}
```

Note that this configuration assumes that gRPC is started with `--no-tls` activated, and the TLS part is handled by the nginx (so the agent **must not** use `--no-tls`).
Please refer to [the official documentation](http://nginx.org/en/docs/http/ngx_http_grpc_module.html) for other options.

## Your first environment

Once you've successfully configured a server and a web client, it's time to start using Panto. Follow these step-by-step instructions to get your first environment up and running.

### Run your first Agent

To begin, you will deploy an Agent on your server.

1. Navigate to the Environment page
2. Click the "+ Agent" button

![Click the "+ Agent" button](images/001-Getting-Started/01-add-agent.png)

3. Choose a recognizable name for your Agent. We strongly recommend using a fully-qualified domain name (FQDN) where applicable. Make sure the "Create Target and setup default Probe configuration for new Agent" option is checked. Click Next.

![Choose a name for your Agent](images/001-Getting-Started/02-choose-agent-name.png)

Your Agent has now been created, but you still need to run it on the server to start collecting metrics. The following dialog guides you along the last steps to get an Agent running. Until the Agent has connected to the server for the first time, you should see a spinning animation in the lower part of the dialog.

![Waiting for the Agent](images/001-Getting-Started/03-waiting-for-agent.png)

4. Start by downloading the agent binaries from [downloads page](//packages.panto.app). Make sure you choose an Agent binary with the same version as your Panto server version (hover over the Panto logo on the top left to find the server version).
5. Run the agent by copying the executable line from the dialog and pasting it to a shell. See the Agent configuration [documentation](002-Detailed-Documentation.md#panto-agent) to learn more about Agent run options.
> In production, it is recommended to run `panto-agent` as a service. See the [documentation](#installing-panto) to find out how to setup `panto-agent` as a service with `systemd` or `init`.
6. Once your Agent is running, the spinning animation should be replaced by a message saying the "Agent is live!". You can click "Save & Close" now.

![Agent is live](images/001-Getting-Started/04-agent-is-live.png)

Congratulations! You've successfully deployed your first Panto Agent. To see the status of the Agent, navigate to the Environment page, find your Agent in the left sidebar on the left, and click the cog icon to open the Agent settings dialog.

![Agent status dialog](images/001-Getting-Started/05-agent-status.png)

You can see the time the Agent was last seen by the server, as well as the version of the Agent at that time.

You now have an Agent running, speaking with the server. But in order to start collecting monitoring data, you need to configure a Probe on this Agent.

### Configure your first Probe

The job of the Agent is to collect monitoring metrics on a Target, and report them to the server. By configuring Probes on the Agent, you can decide what type of data to collect and how. In the Environment page, select your Agent and you should see the Probe catalog.

Probes act as plugins; if you can’t find the plugin you need, you can drop a feature request on our [GitLab page](//gitlab.com/pantomath-io/panto). Or develop it yourself and submit a Merge Request, as Panto is [open-source software](//gitlab.com/pantomath-io/panto/blob/master/LICENSE).


If you checked the "Create Target and setup default Probe configuration for new Agent" option when creating the Agent, a number of Probes are already configured on your Agent. Follow these steps to create a new Probe Configuration.

1. Find the Probe you wish to install in the catalog, and click the "Install" button to the right.

> Throughout this example, we will choose the Ping probe. Ping sends ICMP packets to a target and collects data on the exchange. It is often used as a simple "liveness" test for distant servers.

![Probe catalog](images/001-Getting-Started/06-probe-catalog.png)

2. To configure a Probe, you need to enter:
    * Frequency: the time between two measurements by the Agent, here set to 5 minutes
    * Probe-specific parameters: here, we will ping the `target.pantomath.io` address, and leave the other options to default
    * Target: the name of the piece of infrastructure, such as a server or hardware, to collect data about (see below)

![Probe configuration](images/001-Getting-Started/07-probe-configuration.png)

### Your first Target

Before you continue configuring your Probe, you need to define a Target to monitor. A Target is any piece of your equipment you want to track and collect monitoring metrics about: a database server, an application server, a switch or router, etc. Here's how to create and assign a Target to your Probe configuration.

1. Click on the "Create a target" button to open the target creation dialog.

![Create a Target](images/001-Getting-Started/08-create-target-dialog.png)

2. Choose a name for your Target. Here, we will target the `target.pantomath.io` server. As with the Agent, where applicable, we recommend choosing an FQDN as the Target name. Click the "Create" button.
3. By clicking the "+" button, you can create and assign Tags to a Target. Tags are an easy way to group Targets sharing certain characteristics (e.g. production vs. staging, database, application server...)

![Configure Tags](images/001-Getting-Started/09-configure-tags.png)

Now that your Target is set, the Probe is completely configured, and you can finish configuring the Probe on this Agent with the "Next" button.

If the Agent is still running on your server, it should update its configuration automatically and start gathering Ping data about the Target and reporting it to the server. To make good use of this data, you need to define what constitutes a functional or dysfunctional state in your system.

### State

Without States, a Probe only reports measurements to the server. You can display the metrics in a graph in the Dashboard, or query them through the [API](007-API-Reference.md). However, Panto cannot use these metrics to recognize dysfunctional states, and take action (such as alerting your team) in case such a state occurs. States can be configured as soon as you have configured a Probe to run on an Agent (monitoring a Target). 

You can define 2 States of increasing importance:

* Warning
* Critical

Here's how to configure Panto to set a Critical state when the average Ping return-trip time takes over 300ms.

1. Click on the "Add a state condition" button.

![Add a State condition](images/001-Getting-Started/10-add-state-condition.png)

> There are two types of combinations of conditions: All or Any. A State of type "All" needs all conditions to be met for the State to be set (logical AND); a State in "Any" type will be set if any of the conditions is met (logical OR).

2. Choose `avg` in the Metrics drop-down list.
3. Choose `>` in the Operator drop-down list.
4. Enter `300000000` in the Value field (300,000,000ns = 300ms)
5. Enter a description of the State in the Output message box. This message will be repeated in Alerts and other places the State is mentioned.

![Configure the Critical state](images/001-Getting-Started/11-configure-warning-state.png)

6. Click the "Save" button.

Your Critical state is now configured. If the Agent reports an average return-trip time of over 300ms for the execution of a Ping probe on this Target, a Critical state will be set on this configuration until an execution of the Probe does not verify these conditions anymore.

### Alert

Now that your Agent is running, reporting Ping metrics to the server, and the server can tell when the report requires attention, you can setup an Alert to notify you when an issue occurs. Here's how:

1. Navigate to the Alerts page.
2. Click the "Create an Alert" button.

![Create an Alert](images/001-Getting-Started/12-create-an-alert.png)

In the following dialog,

3. Choose the "State Change" alert type. This will dispatch an Alert every time the state changes from OK to Warning or to Critical (or the other way around).
4. Choose the Check you want to receive Alerts about. A Check embodies the States (Warning & Critical) and the associated conditions set to be evaluated against a Probe's results. Here we choose the Check on Ping that we configured earlier.

![Configure Alert](images/001-Getting-Started/13-alert-configuration.png)

An Alert is sent on a Channel. A Channel represents a way to contact you or your team, in case Panto detects an unusual state. You can configure e-mail, Slack, SMS (via Twilio)... Here, we'll configure our Alert to notify via e-mail.

5. Click the "+ New" to create a new Alert Channel.
6. Choose a name for your Channel. The name should be unique and descriptive. Here, we choose "E-mail: alert@pantomath.io"
7. Choose the Channel type. Here, we choose "email".
8. Each Channel type has its own set of configuration options. For e-mail, you can choose the main e-mail address to send the alert to, as well as a list of addresses to Cc: to. Here, we just set the main address to "alert@pantomath.io"
9. Review your settings and click the "+ Add" button to finish creating your Channel.

![Create new Alert Channel](images/001-Getting-Started/14-create-alert-channel.png)

10. When done, click the "Create" button to finish creating your Alert.

![Alert page](images/001-Getting-Started/15-alert-page.png)

Congratulations! You now have Panto setup to alert you by e-mail any time your Agent detects a slow ping on your Target.

You can configure more Agents, Targets, Probes and Alerts using the same steps. You can also discover the other sections by navigating to the Pulse or Dashboard pages. Find out more about Panto by reading the [documentation](002-Detailed-Documentation.md).
